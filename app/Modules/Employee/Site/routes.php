<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\Employee\Site\Controllers;
use App\Modules\Employee\Site\Models\Admin;

$manifestJson=file_get_contents(__DIR__.'\..\Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,"\'"), 1);



Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers','middleware'=>['web','guest:Employee'] ], function ()
{

	Route::get('/',function(){
		return 'hoi';
	});

	Route::get('test2',function(){
		return 'hi';
	});


});

Route::group(['prefix' => $moduleName.'/'.$moduleSide],function(){

Route::get('logout2',function(){
	Auth::guard('test')->logout();
})->middleware('web');


Route::get('check2',function(){
		if(Auth::guard('test')->check()){
			return 'true';
		}else{
			return 'false';
		}
	
	})->middleware('web');
Route::get('login2',function(){

		Auth::guard('test')->attempt(['username' => 'mohamad', 'password' => '1533512']);
		
})->middleware('web');


});


// Auth::routes();

