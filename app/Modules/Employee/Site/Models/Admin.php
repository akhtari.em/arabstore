<?php

namespace App\Modules\Employee\Site\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{	
	protected $table='admin';
    public $timestamps=true;
    protected $primaryKey = 'id';
}