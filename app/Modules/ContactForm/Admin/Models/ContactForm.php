<?php

namespace App\Modules\ContactForm\Admin\Models;

use Illuminate\Database\Eloquent\Model;
class ContactForm extends Model
{
	protected $table='contact_form';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected  $fillable = [
		'name', 'email', 'message'
	];
	
	public static function Search($title,$offset,$limit)
	{
		return ContactForm::where('contact_form.name','like','%'.$title.'%')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['contact_form.id','contact_form.name','contact_form.email']);
	}
	
	public static function Count($searchFiled)
	{
		return ContactForm::where('name','like','%'.$searchFiled.'%')->count();
	}
}