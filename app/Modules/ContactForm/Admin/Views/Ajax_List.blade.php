@if($data['pageInfo']['pagesCount']>1)
	<ul class="pagination ">
		@for($i=1;$i<=$data['pageInfo']['pagesCount'];$i++)
			<li class=@if($data['pageInfo']['curPage']==$i)
				{{'active'}}
					@elseif(empty($data['pageInfo']['curPage']) && $i==1)
				{{'active'}}
					@endif
			>
				
				<a class="pagingitem" onclick="paginating({{$i}});">{{$i}}</a>
			
			</li>
		@endfor
	
	</ul>
@endif


<table id="stream_table" class="table table-striped table-bordered">
	<thead>
	<tr>
		<th><span>شماره</span></th>
		<th><span>نام</span></th>
		<th><span>ایمیل</span></th>
		<th><span>نگارش</span></th>
	</tr>
	</thead>
	@foreach($data['contacts'] as $contact)
		<tbody>
		<tr>
			<td align="center" data-th="شماره کاربری">{{$contact['id']}}</td>
			
			<td align="center" data-th="نام ">{{$contact['name']}}</td>
			
			<td align="center" data-th="ایمیل ">{{$contact['email']}}</td>
				<th scope="row" class="table-check-cell" data-th="نگارش">
					<div class="btn-group btn-group-sm">
						
						<a type="button" class="btn btn-sm green-bg" href="{{route('ShowContactView',$contact['id'])}}">
							<i class="fa fa-share-square-o"></i>
						</a>
						
					</div>
				</th>
		</tr>
		
		
		</tbody>
	@endforeach
</table>
<script>
	$('[data-toggle=confirmation]').confirmation({
		rootSelector: '[data-toggle=confirmation]',
		container: 'body'
	});
	var currency = '';
	$('#custom-confirmation').confirmation({
		rootSelector: '#custom-confirmation',
		container: 'body',
		title: null,
		onConfirm: function (currency) {
			alert('You choosed ' + currency);
		},
		buttons: [
			{
				class: 'btn btn-danger',
				icon: '',
			}
		]
	});
</script>

@if($data['pageInfo']['pagesCount']>1)
	<ul class="pagination ">
		@for($i=1;$i<=$data['pageInfo']['pagesCount'];$i++)
			<li class=@if($data['pageInfo']['curPage']==$i)
				{{'active'}}
					@elseif(empty($data['pageInfo']['curPage']) && $i==1)
				{{'active'}}
					@endif
			>
				
				<a class="pagingitem" onclick="paginating({{$i}});">{{$i}}</a>
			
			</li>
		@endfor
	
	</ul>
@endif
