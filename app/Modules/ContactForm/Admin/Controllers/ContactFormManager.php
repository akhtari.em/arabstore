<?php


namespace App\Modules\ContactForm\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\ContactForm\Admin\Models\ContactForm;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\App\Admin\Models\App;
use App\Modules\Developer\Admin\Models\Developer;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
class ContactFormManager extends Controller
{
	private $appService;
	public function __construct(AppServices $appService)
	{
		$this->appService = $appService;
	}
	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}
	



	public function ListView()
	{
		return view($this->GetPreView().'List');
	}

	public function ShowView($id)
	{

		$contactForm = ContactForm::findOrFail($id);
		return view($this->GetPreView().'Show')->with(['contactForm'=>$contactForm]);

	}





}
