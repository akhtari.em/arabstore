<?php

namespace App\Modules\WishList\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Users\Admin\Models\Users;
use App\Services\UserService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{	
	protected  $fillable =[
		'client_id', 'app_id',
	];
	protected $table='wish_list';
	public $timestamps=true;
	protected $hidden = [
		 'updated_at', 'created_at'
	];
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	public function app()
	{
		return $this->belongsTo(App::class, 'app_id');
	}
}
