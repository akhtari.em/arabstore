<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\AppCategory\Admin\Controllers\AppCategoryManager;
use App\Http\Controllers\CoreCommon;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	
	//Views(Get) Routes
	Route::get('Add','AppCategoryManager@AddView')->name('AddAppCategoryView');

	Route::get('List','AppCategoryManager@ListView')->name('AppCategoryListView');
	Route::get('Ajax_GetList','AjaxHandler@ListView');
	Route::get('Edit/{id}', 'AppCategoryManager@EditView')->name('EditAppCategoryView');
	Route::post('Add','AppCategoryManager@Add')->name('AddAppCategory');
	Route::post('/deleteIcon','AppCategoryManager@deleteIcon')->name('delete.deleteIcon');
	Route::post('/deleteIcon2','AppCategoryManager@deleteIcon2')->name('delete.deleteIcon2');

	//Update(Put) Routes
	Route::post('Edit/{id}','AppCategoryManager@Edit')->name('EditAppCategory');
		
	//Delete(Delete) Routes
	Route::get('Delete/{id}','AppCategoryManager@Delete')->name('DeleteAppCategory');

});








//These Routes Use When User Not Logged In

