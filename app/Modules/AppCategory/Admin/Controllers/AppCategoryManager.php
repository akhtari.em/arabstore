<?php


namespace App\Modules\AppCategory\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppCategoryRequest;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\Users\Admin\Models\Users;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use App\Services\VitrineItemService;
class AppCategoryManager extends Controller
{
	
	private $appCategoryService;
	public function __construct(AppServices $appCategoryService)
	{
		$this->appCategoryService = $appCategoryService;
	}
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}


	//Just Return Views
	public function AddView()
	{
		$categories=AppCategory::GetParents();
		return view($this->GetPreView().'Add', compact('categories'));
	}

	public function ListView()
	{
		return view($this->GetPreView().'List');
	}

	public function EditView($id)
	{	
		$id=AppCategory::findOrFail($id);
        $LocalhostPath = CoreCommon::getLocalhostPath("");
		$allAppCategory=AppCategory::where('parent',0)->where('remove',0)->oldest('title')->pluck('title', 'id');
		return view($this->GetPreView().'Edit', compact('allAppCategory','LocalhostPath'))->with(['id' => $id]);
	}

	//Controllers That Do Somethings
	public function Add(AppCategoryRequest $request)
	{	
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		if ($request->hasFile('imageicon') && $request->file('imageicon')->isValid())
		{
			$filenameIcon = $this->appCategoryService->uploadAvatar($request->file('imageicon'));
			$request->request->add(['icon2' => $filenameIcon]);
		}
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appCategoryService->uploadAvatar($request->file('avatar'));
			$request->request->add(['icon' => $filename]);
		}
		AppCategory::create($request->all());
		return redirect(route('AppCategoryListView'));
	}

	public function deleteIcon(Request $request)
	{
        $key = $request->get('key');
        $t = AppCategory::findOrFail($key);
		$t->icon = "0.png";
		$t->save();
        return '{}';
	}
	
	public function deleteIcon2(Request $request)
	{
		$key = $request->get('key');
		$t = AppCategory::findOrFail($key);
		$t->icon2 = "0.png";
		$t->save();
		return '{}';
	}

	public function Delete($id)
	{
		$appCategory = AppCategory::findOrFail($id);
		$appCategory->delete();
		return redirect(route('AppCategoryListView'));
	}

	public function Edit(AppCategoryRequest $request, $id)
	{	
		$appCategory = AppCategory::findOrFail($id);
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		if ($request->hasFile('imageicon') && $request->file('imageicon')->isValid())
		{
			$filenameIcon = $this->appCategoryService->uploadAvatar($request->file('imageicon'));
			$request->request->add(['icon2' => $filenameIcon]);
		}
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appCategoryService->uploadAvatar($request->file('avatar'));
			$request->request->add(['icon' => $filename]);
		}
		$appCategory->update($request->all());
		return redirect(route('AppCategoryListView'));
	}



}
