<?php

namespace App\Modules\AppCategory\Admin\Models;

use App\Modules\Vitrine\Admin\Models\Vitrine;
use Illuminate\Database\Eloquent\Model;
use App\Services\AppServices;
class AppCategory extends Model
{
	protected $table='app_category';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected $hidden = [
		'status', 'created_at','updated_at',
		'remove', 'parent','row3',
		'row1', 'row2','user', 'status'
	
	];
	protected $fillable = [
		'id','title', 'icon','icon2','order',
		'title', 'status',
		'parent','remove', 'user'
	];
	
	
	protected $appends = [
		'icon_path',
	];
	//get all parents
	public static function GetParents()
	{
		return AppCategory::where('parent','<>',0)->where('remove',0)->oldest('title')->pluck('title', 'id');
	}
	// get childs of specefic parent
	public static function GetChilds($id)
	{
		if(AppCategory::where('parent',$id)->exists())
		{
			return AppCategory::where('parent',$id)->get(['id','title']);
		}
	}

	//get parent of specific child
	public static function GetParent($id)
	{
		if(AppCategory::where('id',$id)->where('remove',0)->exists())
		{
			$parentId=AppCategory::where('id',$id)->where('remove',0)->get()[0]['parent'];
			if(AppCategory::where('id',$parentId)->where('remove',0)->exists())
			{
				return AppCategory::where('id',$parentId)->where('remove',0)->get()[0];
			}
			
		}
	}

	public static function GetWithId($id){
		if(AppCategory::where('id',$id)->where('remove',0)->exists())
		{
			return AppCategory::where('id',$id)->where('remove',0)->get()[0];
		}
		else
		{
			return false;
		}	
	}

	public static function GetAll()
	{
		return AppCategory::where('status',1)->where('remove','=',0)->get(['id','title','parent']);
	}

	public static function Search($title,$offset,$limit)
	{
		$appCategories=AppCategory::where('title','like','%'.$title.'%')->where('remove',0)->offset($offset)->take($limit)->orderBy('id', 'desc')->get();

		foreach($appCategories as $appCategory)
		{	
			if($appCategory['parent'] != 0)
			{
				$title=AppCategory::where('id',$appCategory['parent'])->get(['title'])[0]['title'];
				$appCategory['parentTitle']=$title;
			}
	
			
		}

		return $appCategories;
		
	}

	public static function Remove($id)
	{
		if(AppCategory::where('id',$id)->exists())
		{
			$appCategory=AppCategory::where('id','=',$id)->get()[0];
			$appCategory->remove=time();
			$appCategory->save();
		}		
	}

	public static function CheckExists($id)
	{
		if(AppCategory::where('id',$id)->where('remove',0)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	public static function Count($searchFiled)
	{
		return AppCategory::where('title','like','%'.$searchFiled.'%')->where('remove',0)->count();
	}
	
	public function vitrine()
	{
		return $this->hasOne(Vitrine::class);
	}
	
	public function getGravatar($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->icon)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->icon");
		}
		
	}
	
	public function getGravatar2()
	{
		if ($this->icon2)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->icon2");
		}

	}
	
	public function app()
	{
		return $this->belongsTo(AppCategory::class);
	}
	
	public function getIconPathAttribute()
	{
		return AppServices::getAvatarDisplayPath("/uploads/App/" . $this->icon);
	}
}