@if($data['pageInfo']['pagesCount']>1)
	<ul class="pagination ">
		@for($i=1;$i<=$data['pageInfo']['pagesCount'];$i++)
			<li class=@if($data['pageInfo']['curPage']==$i)
				{{'active'}}
					@elseif(empty($data['pageInfo']['curPage']) && $i==1)
				{{'active'}}
					@endif
			>
				
				<a class="pagingitem" onclick="paginating({{$i}});">{{$i}}</a>
			
			</li>
		@endfor
	
	</ul>
@endif


<table id="stream_table" class="table table-striped table-bordered">
	<thead>
	<tr>
		<th><span>شماره</span></th>
		<th><span>عنوان</span></th>
		<th><span>والد</span></th>
		<th><span>تاریخ ثبت</span></th>
		<th><span>وضعیت</span></th>
		<th><span>نگارش</span></th>
	</tr>
	</thead>
	{{--{{dd($data['appCategories'])}}--}}
	@foreach($data['appCategories'] as $appCategory)

		<tbody>
		<tr>
			<td align="center" data-th="شماره کاربری">{{$appCategory['id']}}</td>
			<td align="center" data-th="نام نمایشی">{{$appCategory['title']}}</td>
			<th><span>
                            @if($appCategory['parent']==0)
						{{'ریشه'}}
					@else
						{{$appCategory['parentTitle']}}
					@endif
                            </span></th>
			<td align="center">{{jDate::forge($appCategory->created_at)->format('date')}}</td>
			<td data-th="تاریخ ثبت نام">
				@if($appCategory['status']==1)
					{{'فعال'}}
				@else
					{{'غیر فعال'}}
				@endif</td>
			
			<th scope="row" class="table-check-cell" data-th="نگارش">
				<div class="btn-group btn-group-sm">
					<a class="btn btn-sm btn-danger alrt-btn" data-toggle="confirmation" data-btn-ok-label="ﺑﻠﻪ"
					   data-btn-ok-class="btn-success btn-xs btn-alrt" data-btn-cancel-label="ﺧﯿﺮ"
					   data-btn-cancel-class="btn-danger btn-xs btn-alrt" data-title=""
					   data-content="ﺁﯾﺎ اﺯ ﺣﺬﻑ اﯾﻦ کاربر اﻃﻤﯿﻨﺎﻥ ﺩاﺭﯾﺪ؟" href="{{route('DeleteAppCategory',$appCategory['id'])}}"
					   {{--data-content="ﺁﯾﺎ اﺯ ﺣﺬﻑ اﯾﻦ کاربر اﻃﻤﯿﻨﺎﻥ ﺩاﺭﯾﺪ؟" href=""--}}
					   data-original-title="" title="">
						<i class="fa fa-trash"></i>
					</a>
					<a type="button" class="btn btn-sm green-bg"
					   {{--href="">--}}
					   href="{{route('EditAppCategoryView',$appCategory['id'])}}">
						<i class="fa fa-pencil"></i>
					</a>
				</div>
			</th>
		</tr>
		
		
		</tbody>
	@endforeach
</table>
<script>
	$('[data-toggle=confirmation]').confirmation({
		rootSelector: '[data-toggle=confirmation]',
		container: 'body'
	});
	var currency = '';
	$('#custom-confirmation').confirmation({
		rootSelector: '#custom-confirmation',
		container: 'body',
		title: null,
		onConfirm: function (currency) {
			alert('You choosed ' + currency);
		},
		buttons: [
			{
				class: 'btn btn-danger',
				icon: '',
			}
		]
	});
</script>

@if($data['pageInfo']['pagesCount']>1)
	<ul class="pagination ">
		@for($i=1;$i<=$data['pageInfo']['pagesCount'];$i++)
			<li class=@if($data['pageInfo']['curPage']==$i)
				{{'active'}}
					@elseif(empty($data['pageInfo']['curPage']) && $i==1)
				{{'active'}}
					@endif
			>
				
				<a class="pagingitem" onclick="paginating({{$i}});">{{$i}}</a>
			
			</li>
		@endfor
	
	</ul>
@endif
