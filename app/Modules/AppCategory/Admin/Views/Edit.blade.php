@extends('Master_Admin::MasterPage')

@section('title')
    افزودن دسته بندی اپلیکشن
@stop

@section('css')
    <link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop


@section('content')
	
	{{--<script src="{{asset('backend/assets/slider/js/fileinput2.js')}}" type="text/javascript"></script>--}}
    <div class="main-title-sec">
        <div class="row">
            <div class="col-md-3 column">
                <div class="heading-profile">
                    <h2>{{trans('modules.Edit')}}</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-area container-fluid">
        @include('errors')
        <div class="row">
            <div class="col-md-12">
                <div class="widget with-padding">
                    <div class="wizard-form-h">
                        <form class="form container-fluid" method="post" name="form"
                              onsubmit="return EditAppCategoryValidate()"
                              action="{{route('EditAppCategory', ['id' => $id->id])}}" id="form"
                              enctype="multipart/form-data">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">دسته بندی</label>
                                        <select class="form-control selectpicker wide _category" name="parent"
                                                id="parent" data-live-search="true">
                                            @foreach($allAppCategory as $index=>$value)
                                                <option value="{{$index}}" {{($index == $id->parent)? 'selected' : ''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="inline-form">
                                        <label class="c-label">نام دستبندی <span>*</span> </label>
                                        <input class="input-style" value="{{$id->title}}" id="title" type="text"
                                               name="title">
                                    </div>
                                </div>
                            </div>
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div style="width: 100%">
                                            <div style="width: 90%;">
                                                <input id="avatar" name="avatar" type="file" multiple
                                                       class="file-loading">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row frst-row-form">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="inline-form">
                                            <label> عکس داخل فایل (فایل مجاز jpg)</label>
                                            <div class="col-lg-8" style="width: 100%">
                                                {{--<img src="{{ $id->getGravatar2()}}" alt="{{ $id->name }}"--}}
                                                {{--style="width:150px;height:150px;">--}}
                                                <input id="imageicon" name="imageicon" type="file" multiple
                                                       class="file-loading"
                                                       accept="image/*">
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row frst-row-form">
                                    <div class="col-sm-12 col-md-2 col-lg-2">
                                        <div class="inline-form">
                                            <label class="c-label">اولویت</label>
                                            <input class="input-style" type="number" id="order" name="order"
                                                   value="{{$id->order}}" dir="ltr">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-form">
                                    <div class="col-sm-12 col-md-8 col-lg-6">
                                        <div class="toggle-setting">
                                            <label class="pull-right">وضعیت</label>
                                            <select class="form-control" name="status" id="status">
                                                <option value="1" {{($id->status == '1')?'selected':''}}>فعال
                                                </option>
                                                <option value="0" {{($id->status == '0')?'selected':''}}>غیرفعال
                                                </option>
                                            </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-form">
                                    <div class="col-sm-12 col-md-8 col-lg-6">
                                        <button class="btn green-bg pull-right" type="submit">ویرایش</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('Assets/AppCategory/Admin/js/checkAppCategoryValidate.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#avatar").fileinput({
                theme: 'fas',
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                overwriteInitial: false,
                initialPreviewAsData: true,
                initialPreview: [
                    '{{$id->getGravatar()}}',
                ],
                initialPreviewConfig: [
                    {caption: "{{$id->icon}}", url: "{{route('delete.deleteIcon')}}", key: '{{$id->id}}'  }
                ]
            });


        });

    </script>
    <script>
        $(document).ready(function () {
	        $("#imageicon").fileinput({
		        theme: 'fas',
		        showUpload: false,
		        showCaption: false,
		        browseClass: "btn btn-primary btn-lg",
		        fileType: "any",
		        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
		        overwriteInitial: false,
		        initialPreviewAsData: true,
		        initialPreview: [
			        '{{$id->getGravatar2()}}',
		        ],
		        initialPreviewConfig: [
			        {caption: "{{$id->icon2}}", url: "{{route('delete.deleteIcon2')}}", key: '{{$id->id}}'  }
		        ]
	        });
        });
    </script>



@stop