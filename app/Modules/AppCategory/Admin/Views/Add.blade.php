<?php use  App\Http\Controllers\CoreCommon; ?>
@extends('Master_Admin::MasterPage')

@section('title')
افزودن دسته بندی اپلیکشن
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop


@section('content')
	@include('errors')
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Add')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post" onsubmit="return AddAppCategoryValidate()" name="form" action="{{route('AddAppCategory')}}" enctype="multipart/form-data" id="form">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" >

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">دسته بندی</label>
                                    <select class="form-control selectpicker wide _category" name="parent" id="parent" data-live-search="true">
                                        <option value="0">والد</option>
                                        @foreach($categories as $index=> $value)
                                            <option value="{{$index}}" {{$index == old('category')? 'selected': ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">نام دستبندی <span>*</span> </label>
                                    <input class="input-style" type="text" id="title" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">

	                        <div class="col-md-12 float-right">

		                        <div class="inline-form" >
			                        <label>تصویر فایل (فایل مجاز jpg)</label>
			                        <div class="col-lg-8" style="width: 100%">
				                        <input id="avatar" name="avatar" type="file" multiple class="file-loading" accept="image/*" >
			                        </div>
		                        </div>
	                        </div>
                        </div>
                        <div class="row frst-row-form">
	                        <div class="col-md-12 float-right">
		                        <div class="inline-form" >
			                        <label> عکس داخل فایل (فایل مجاز jpg)</label>
			                        <div class="col-lg-8" style="width: 100%">
				                        <input id="imageicon" name="imageicon" type="file" multiple class="file-loading" accept="image/*" >
			                        </div>
		                        </div>
	                        </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number" id="order" value="0" name="order" dir="ltr">
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
	                        <div class="col-sm-12 col-md-8 col-lg-6">
		                        <label class="pull-right">وضعیت</label>
		                        <select class="form-control" name="status" id="status">
			                        <option value="1" {{(old('status') == '1')?'selected':''}}>فعال
			                        </option>
			                        <option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال
			                        </option>
		                        </select>
		                        </label>
	                        </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        </div>


</div>

@stop
@section('js')
	<script src="{{asset('Assets/AppCategory/Admin/js/checkAppCategoryValidate.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddAppCategory') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#imageicon").fileinput({
			uploadUrl: "{{ route('AddAppCategory') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop