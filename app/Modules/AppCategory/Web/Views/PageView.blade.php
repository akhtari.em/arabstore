@extends('Master_Web::IndexPage')
@section('body')
    <section class="wrapper firstBlur">
        <section class="container">
            @if (($resultAppCategory->isEmpty() && $resultApp->isEmpty()))
                <br/>
                <br/>
                <br/>
                <div align="center"><img src="{{asset('Assets/Master/Web/images/error.png')}}"
                                         style="width:100px; height:100px;margin-bottom:10px;"/></div>
                <div align="center" dir="rtl"><strong>آیتمی جهت نمایش وجود ندارد</strong></div>
                <br/>
                <br/>
            @else
                @if(!$resultAppCategory->isEmpty())
                    <div class="product-wrapp more-product cat-product">
                        @foreach($resultAppCategory as $item)
                            <article class="col-xs-6 col-sm-4 col-md-2" id="{{$item->id}}">
                                <div class="product">
                                    <figure>
                                        <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">
                                            <span class="product__overlay"></span>
                                            <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon}}"
                                                 class="img-responsive"
                                                 title="{{$item->title}}"
                                                 alt="{{$item->title}}"></a></figure>
                                    <div class="product__detail">
                                        <h3 class="text-center">
                                            <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">
                                                {{$item->title}}
                                            </a>
                                        </h3>
                                    </div><!--.product__detail-->
                                </div><!--.product-->
                            </article>
                        @endforeach
                        <div class="clearfix"></div>
                    </div><!--.product-wrapp-->
                    <div class="product-wrapp more-product" style="display: none">
                        @foreach($resultAppCategory_2 as $item)
                            <article class="col-xs-6 col-sm-4 col-md-2" id="{{$item->id}}">
                                <div class="product">
                                    <figure>
                                        <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">
                                            <span class="product__overlay"></span>
                                            <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon}}"
                                                 class="img-responsive"
                                                 title="{{$item->title}}"
                                                 alt="{{$item->title}}"></a></figure>
                                    <div class="product__detail">
                                        <h3 class="text-center">
                                            <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">
                                                {{$item->title}}
                                            </a>
                                        </h3>
                                    </div><!--.product__detail-->
                                </div><!--.product-->
                            </article>
                        @endforeach
                        <div class="clearfix"></div>
                    </div><!--.product-wrapp-->
                @else
                    <div class="product-wrapp more-product cat-product">
                        <?php $RateView = 0 ?>
                        @foreach($resultApp as $item)
                            <article class="col-xs-6 col-sm-4 col-md-2">
                                <div class="product">
                                    <figure>
                                        <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">
                                            <span class="product__overlay"></span>
                                            <img src="{{$LocalhostPath.'/uploads/App/'.$item->image}}"
                                                 class="img-responsive"
                                                 title="{{$item->title}}"
                                                 alt="{{$item->title}}"></a></figure>
                                    <div class="product__detail">
                                        <h3 class="text-center">
                                            <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">
                                                {{$item->title}}
                                            </a>
                                        </h3>
                                        <ul class="text-center product__rate">
                                            @if ($btw_RateCnt[$RateView] == '0')
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '0.5')
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '1')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '1.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '2')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '2.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '3')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '3.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '4')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '4.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                            @endif
                                            @if ($btw_RateCnt[$RateView] == '5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                            @endif
                                        </ul>
                                    </div><!--.product__detail-->
                                </div><!--.product-->
                            </article>
                            <?php $RateView++; ?>
                        @endforeach
                        <div class="clearfix"></div>
                    </div><!--.product-wrapp-->
                    <div class="product-wrapp more-product" style="display: none">
                        <?php $RateView_2 = 0 ?>
                        @foreach($resultApp_2 as $item)
                            <article class="col-xs-6 col-sm-4 col-md-2">
                                <div class="product">
                                    <figure>
                                        <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">
                                            <span class="product__overlay"></span>
                                            <img src="{{$LocalhostPath.'/uploads/App/'.$item->image}}"
                                                 class="img-responsive"
                                                 title="{{$item->title}}"
                                                 alt="{{$item->title}}"></a></figure>
                                    <div class="product__detail">
                                        <h3 class="text-center">
                                            <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">
                                                {{$item->title}}
                                            </a>
                                        </h3>
                                        <ul class="text-center product__rate">
                                            @if ($btw_RateCnt_2[$RateView_2] == '0')
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '0.5')
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '1')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '1.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '2')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '2.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '3')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '3.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '4')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '4.5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star-half-o"></li>
                                            @endif
                                            @if ($btw_RateCnt_2[$RateView_2] == '5')
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                            @endif
                                        </ul>
                                    </div><!--.product__detail-->
                                </div><!--.product-->
                            </article>
                            <?php $RateView_2++ ?>
                        @endforeach
                        <div class="clearfix"></div>
                    </div><!--.product-wrapp-->
                @endif

                @if(!$resultAppCategory_2->isEmpty() || !$resultApp_2->isEmpty())
                    <div class="loadMore">
                        <a href="#">أكثر
                        </a>
                    </div>
                @endif
            @endif
        </section>
    </section>

@stop