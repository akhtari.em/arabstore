<?php

namespace App\Modules\AppCategory\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Services\AppServices;

class AppCategoryManager extends Controller
{
    private $appCategoryService;

    public function __construct(AppServices $appCategoryService)
    {
        $this->appCategoryService = $appCategoryService;
    }
    public function GetPreView()
    {
        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        }

        $preView = $moduleName . '_' . $side . '::';


        return $preView;
    }

    public function index($id)
    {

//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");


        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];


        $resultAppCategory = AppCategory::where('parent', $id)
            ->latest('id')
            ->skip(0)
            ->take(30)
            ->get();

        $resultApp = App::where('category', $id)
            ->latest('id')
            ->skip(0)
            ->take(30)
            ->get();

        $rowCat = 0;
        foreach ($resultApp as $item)
        {
            $btw_RateCnt[$rowCat] = AppRate::where('app_id' , $item->id)
                ->count();
            $rowCat++;
        }


        $resultAppCategory_2 = AppCategory::where('parent', $id)
            ->latest('id')
            ->skip(30)
            ->take(100000000)
            ->get();

        $resultApp_2 = App::where('category', $id)
            ->latest('id')
            ->skip(30)
            ->take(100000000)
            ->get();


        $rowCat_2 = 0;
        foreach ($resultApp_2 as $item_2)
        {
            $btw_RateCnt_2[$rowCat_2] = AppRate::where('app_id' , $item_2->id)
                ->count();
            $rowCat_2++;
        }



        $allAppCategory = AppCategory::where('parent', 0)->where('remove', 0)->oldest('title')->pluck('title', 'id');
        return view($this->GetPreView() . 'PageView', compact('moduleName','LocalhostPath','allAppCategory','data','resultAppCategory','resultApp','resultAppCategory_2','resultApp_2','btw_RateCnt','btw_RateCnt_2'))->with(['id' => $id]);
    }
}