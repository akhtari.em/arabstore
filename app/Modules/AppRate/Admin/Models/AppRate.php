<?php

namespace App\Modules\AppRate\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Users\Admin\Models\Users;
use App\Services\UserService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class AppRate extends Model
{	
	protected  $fillable =[
		'client_id', 'app_id', 'rate',
	];
	protected $table='app_rate';
	public $timestamps=true;
	protected $hidden = [
		 'updated_at', 'created_at'
	];
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	public function app()
	{
		return $this->belongsTo(App::class, 'item_id');
	}


}
