<?php

namespace App\Modules\Confirmation;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Client\Admin\Models\Client;
class Confirmation extends Model
{
	protected  $fillable = [
		'client_id','code','expire_code'
	];
	
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	protected  $table = "confirmation";
}
