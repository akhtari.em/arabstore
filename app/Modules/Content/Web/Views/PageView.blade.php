@extends('Master_Web::IndexPage')
@section('body')
    <section class="wrapper firstBlur">
        <div class="main-title">
            <h1 class="container">{{$resultContent->title}}</h1>
        </div><!--.main-title-->
        <section class="container">
            <div class="wrapper__content">
                {!! $resultContent->details !!}
            </div><!--.wrapper__content-->
        </section>
    </section>
@stop