<?php

namespace App\Modules\Content\Admin\Models;

use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
class Content extends Model
{
	protected $table='content';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected  $fillable = [
		'image', 'title', 'details'
	];
	
	
	
	public static function Count($searchFiled)
	{
		return Content::where('title','like','%'.$searchFiled.'%')->count();
	}

	public static function Search($title,$offset,$limit)
	{
		return Content::where('content.title','like','%'.$title.'%')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['content.id','content.title','content.image']);
	}

	public function getImage()
	{
		if ($this->image)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->image");
		}
	}
	
}