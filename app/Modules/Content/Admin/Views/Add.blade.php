@extends('Master_Admin::MasterPage')

@section('title')
افزودن محتوا
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop


@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Add')}}</h2>
             </div>
        </div>
   </div>
</div>
<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post"  name="form" action="{{route('AddContent')}}" enctype="multipart/form-data" id="form">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">عنوان <span>الزامی است*</span> </label>
                                    <input class="input-style" value="{{old('title')}}" type="text" id="title" name="title">
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form" >
                                    <label><span>الزامی است</span>ایکون (فایل مجاز jpg)</label>
                                    <div class="col-lg-8" style="width: 100%">
                                        <input id="avatar" name="avatar" type="file" multiple class="file-loading" accept="image/*" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
                                    <label>جزئیات :<span>الزامی است*</span></label>
                                    <div class="form-editor">
                                      <textarea name="details" id="details" class="ckeditor"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
</div>
@stop
@section('js')
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddContent') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>

@stop