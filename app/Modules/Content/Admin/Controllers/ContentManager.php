<?php


namespace App\Modules\Content\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\ContentRequest;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\Content\Admin\Models\Content;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\App\Admin\Models\App;
use App\Modules\Developer\Admin\Models\Developer;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
class ContentManager extends Controller
{
	private $appService;
	public function __construct(AppServices $appService)
	{
		$this->appService = $appService;
	}
	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}
	

	//Just Return Views
	public function AddView()
	{	
		return view($this->GetPreView().'Add');
	}

	public function ListView()
	{
		return view($this->GetPreView().'List');
	}

	public function EditView($id)
	{	
		$content=Content::findOrFail($id);
		return view($this->GetPreView().'Edit', compact('content'));
	}

	//Controllers That Do Somethings
	public function Add(ContentRequest $request)
	{
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appService->uploadAvatar($request->file('avatar'));
			$request->request->add(['image' => $filename]);
		}
	
		Content::create($request->all());
		return redirect(route('ContentListView'));
	}

	public function Delete($id)
	{
		$content = Content::findOrFail($id);
		$content->delete();
		// delete app in vitrineItem
		return redirect(route('ContentListView'));
	}

	public function Edit(ContentRequest $request, $id)
	{	
		$content = Content::findOrFail($id);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appService->uploadAvatar($request->file('avatar'));
			$request->request->add(['image' => $filename]);
		}
		$content->update($request->all());
		return redirect(route('ContentListView'));
	}

}
