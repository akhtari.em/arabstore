<?php

namespace App\Modules\App\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Services\AppServices;

class AppManager extends Controller
{
    private $appService;
    public function __construct(AppServices $appService)
    {
        $this->appService = $appService;
    }
    public function GetPreView()
    {
        $dd = CoreCommon::osD();

        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        }
        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }

    public function index($id)
    {
//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");


        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

        $resultApp = App::where('id', $id)
            ->first();

        $appMedia = AppMedia::where('app_id', $id)
            ->latest('id')
            ->get();


        $appCategory = App::where('category', $resultApp->category)
            ->take(6)
            ->get();


        $appCategoryName = AppCategory::where('id', $resultApp->category)
            ->first();


        $comment = Comment::where('item_id', $resultApp->id)
            ->take(5)
            ->get();

        $comment_2 = Comment::where('item_id', $resultApp->id)
            ->offset(5)
            ->limit(10000)
            ->get();

        $clientInfo=[];
        $commentRow = 0;
        foreach ($comment as $item)
        {
            $clientInfo[$commentRow] = Client::where('id', $item->client_id)
                ->first();
            $appRate[$commentRow] = AppRate::where('client_id', $item->client_id )
                ->where('app_id',  $resultApp->id)
                ->count();
            $commentRow++;
        }

        $clientInfo_2=[];
        $commen2tRow = 0;
        foreach ($comment_2 as $item_2)
        {
            $clientInfo_2[$commen2tRow] = Client::where('id', $item_2->client_id)
                ->first();
            $appRate_2[$commen2tRow] = AppRate::where('client_id', $item_2->client_id )
                ->where('app_id',  $resultApp->id)
                ->count();
            $commen2tRow++;
        }

        $totalRate = AppRate::where('app_id' , $resultApp->id)
            ->get();

        $totalRateCnt = AppRate::where('app_id' , $resultApp->id)
            ->count();

        $btw4_5RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',4)
            ->where('rate','<=',5)
            ->count();

        $btw3_4RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',3)
            ->where('rate','<=',4)
            ->count();

        $btw2_3RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',2)
            ->where('rate','<=',3)
            ->count();

        $btw1_2RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',1)
            ->where('rate','<=',2)
            ->count();

        $btw0_1RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>=',0)
            ->where('rate','<=',1)
            ->count();

        $rowCat = 0;
        foreach ($appCategory as $item)
        {
            $btw_RateCntCategory[$rowCat] = AppRate::where('app_id' , $item->id)
                ->count();
            $rowCat++;
        }

        $sumRate =0;
        foreach ($totalRate as $item)
        {
            $sumRate += $item->rate;
        }
        if ($totalRateCnt==0) {
            $averageRate = $sumRate / 1;
        } else {
            $averageRate = $sumRate / $totalRateCnt;
        }

        return view($this->GetPreView() . 'PageView', compact('moduleName','LocalhostPath','resultApp','appMedia','averageRate','totalRateCnt','appCategory','appCategoryName','btw_RateCntCategory','btw4_5RateCnt','btw3_4RateCnt','btw2_3RateCnt','btw1_2RateCnt','btw0_1RateCnt','comment','comment_2','clientInfo','clientInfo_2','appRate','appRate_2'))->with(['id' => $id]);
    }
}