@extends('Master_Web::IndexPage')
@section('body')
    <div class="modal fade" id="barcodeModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div><!--.modal-header-->
                <div class="modal-body">
                    <p>
                        لتنزيل هذا البرنامج ، قم بمسح الكود التالي :
                    </p>
                    <?php
                    Route::get('qr-code', function () {
                        return QR_Code\QR_Code::text('QR Code')->setSize(4)->png();
                    });
                    ?>
                </div>
            </div><!--.modal-content-->
        </div><!--.modal-dialog-->
    </div><!--#barcodeModal-->


    <section class="wrapper firstBlur">
        <section class="container">
            <div class="featured__menu">
                <ul>
                    <li>
                        <a class="page-scroll" href="#pr__similar"></a><span>مشابه</span>
                    </li>
                    <li class="featured__menu__like">
                        <a class="page-scroll" ref="#pr__rating">{{$averageRate}}</a>
                        <span>{{$totalRateCnt}}</span>
                    </li>
                    <li class="featured__menu__cat">
                        <a href="#"></a>
                        <span>سجّل</span>
                    </li>
                    <li class="featured__menu__install">
                        <a>{{$resultApp->downloadCount}} <span>ملابین</span></a>
                        <span>التنزیلات</span>
                    </li>
                </ul>
            </div><!--.featured__menu-->
            <div class="pr-wrapp row">
                <div class="col-xs-12 col-md-8 col-md-push-2">
                    <div class="row">
                        <div class="pr-wrapp__title col-xs-12 col-sm-9">
                            <h2>
                                {{$resultApp->title}}
                            </h2>
                            <div class="pr-wrapp__more pull-right">
                                <button data-toggle="modal" data-target="#barcodeModal">تثبيت</button>
                            </div><!--.pr-wrapp__more-->
                        </div><!--.pr-wrapp__title-->
                        <figure class="pr-wrapp__img col-xs-12 col-sm-3">
                            <img src="{{$LocalhostPath.'/uploads/App/'.$resultApp->image}}"
                                 class="img-responsive pull-left" title="" alt="">
                        </figure>
                    </div><!--.row-->
                </div>
                <div class="pr__slider col-xs-12 col-md-8 col-md-push-2">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($appMedia as $item)
                                <div class="swiper-slide">
                                    @if ($item->type == 'video')
                                        <a data-fancybox data-width="640" data-height="360"
                                           href="{{$LocalhostPath.'/uploads/App/'.$item->file}}">
                                            <img class="img-responsive"
                                                 src="{{$LocalhostPath.'/uploads/App/'.$item->videoImage}}"
                                                 title="{{$resultApp->title}}" alt="{{$resultApp->title}}">
                                        </a>
                                    @else
                                        <a href="{{$LocalhostPath.'/uploads/App/'.$item->videoImage}}"
                                           data-fancybox="images">
                                            <img src="{{$LocalhostPath.'/uploads/App/'.$item->videoImage}}"
                                                 title="{{$resultApp->title}}" alt="{{$resultApp->title}}">
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        </div><!--.swiper-wrapper-->
                        <div class="swiper-scrollbar"></div>
                    </div><!--.swiper-container-->
                </div><!--.pr__slider-->
                <div class="pr__detail col-xs-12 col-md-8 col-md-push-2">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-push-2">
                            {!! $resultApp->detail !!}
                        </div>
                    </div><!--.row-->
                    @if($resultApp->change)
                        <div class="row">
                            <div class="pr__info pr__toggle">
                                <div class="col-xs-12 col-md-10 col-md-push-1">
                                    <h4 class="pr__title">مشابه</h4>
                                    {!! $resultApp->change !!}
                                </div>
                            </div><!--.pr__info-->
                        </div><!--.row-->
                        <div class="col-xs-12 pr__drop text-center" data-for=".pr__toggle">
                            <i class="glyphicon glyphicon-menu-down pr__drop-icon"></i>
                        </div>
                    @endif
                </div><!--.pr__detail-->
                <div class="clearfix"></div>
            </div><!--.pr-wrapp-->
        </section><!--.container-->

        <section id="pr__similar" class="pr__fullSlider">
            <div class="pr__fullSlider-top"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="product-wrapp more-product">
                            <header class="product-wrapp__header">
                                <div class="product-wrapp__title pull-right">
                                    @if ($appCategoryName)
                                    <h2>{{$appCategoryName->title}}</h2>
                                    @endif
                                </div>
                                <div class="product-wrapp__more pull-left">
                                    <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$resultApp->category}}">سجّل</a>
                                </div>
                                <div class="clearfix"></div>
                            </header>
                            <?php
                            $rowCatView = 0;
                            ?>
                            @foreach($appCategory as $item)
                                <article class="col-xs-6 col-sm-4 col-md-2">
                                    <div class="product">
                                        <figure>
                                            <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">
                                                <span class="product__overlay"></span>
                                                <img src="{{$LocalhostPath.'/uploads/App/'.$item->image}}"
                                                     class="img-responsive" title="{{$item->title}}"
                                                     alt="{{$item->title}}">
                                            </a>
                                        </figure>
                                        <div class="product__detail">
                                            <h3 class="text-center">
                                                <a href="{{$LocalhostPath.'/App/Web/PageView/'.$item->id}}">{{$item->title}}</a>
                                            </h3>
                                            <ul class="text-center product__rate">
                                                @if ($btw_RateCntCategory[$rowCatView] == '0')
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '0.5')
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '1')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '1.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '2')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '2.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '3')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '3.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '4')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '4.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$rowCatView] == '5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                @endif
                                            </ul>
                                        </div><!--.product__detail-->
                                    </div><!--.product-->
                                </article>
                                <?php
                                $rowCatView++;
                                ?>
                            @endforeach
                            <div class="clearfix"></div>
                        </div><!--.product-wrapp-->
                    </div>
                </div><!--.row-->
            </div><!--.container-->
            <div class="pr__fullSlider-btm"></div>
        </section><!--.pr__fullSlider-->

        <section class="container">
            <div class="pr-wrapp row">
                <div id="pr__rating" class="pr__rating col-xs-12 col-md-6 col-md-push-3">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 text-center">
                            <h5 class="pr__rating-num">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $averageRate = 0;
                                }
                                echo $averageRate;
                                ?>
                            </h5>
                            <div class="rating">
                                @if ($averageRate == '0')
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '0.5')
                                    <span class="fa fa-star-half-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '1')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '1.5')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-half-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '2')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '2.5')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-half-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '3')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '3.5')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-half-o"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '4')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                @endif
                                @if ($averageRate == '4.5')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-half-o"></span>
                                @endif
                                @if ($averageRate == '5')
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                @endif
                            </div><!--.rating-->
                            <span class="pr__rating-total">{{$totalRateCnt}}</span>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <div class="row rating-desc">
                                <div class="rating-desc-detail">
                                    <?php
                                    if ($totalRateCnt == 0) {
                                        $percentage_4_5 = ($btw4_5RateCnt * 100) / ($totalRateCnt + 1);
                                    } else {
                                        $percentage_4_5 = ($btw4_5RateCnt * 100) / $totalRateCnt;
                                    }
                                    ?>
                                    <div class="col-xs-2"><span>{{$btw4_5RateCnt}}</span></div>
                                    <div class="col-xs-5">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar5" role="progressbar"
                                                 aria-valuenow="{{$percentage_4_5}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$percentage_4_5}}%">
                                            </div>
                                        </div><!--.progress-->
                                    </div>
                                    <div class="col-xs-5">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div><!--.rating-desc-detail-->
                                <div class="rating-desc-detail">
                                    <?php
                                    if ($totalRateCnt == 0) {
                                        $percentage_3_4 = ($btw3_4RateCnt * 100) / ($totalRateCnt + 1);
                                    } else {
                                        $percentage_3_4 = ($btw3_4RateCnt * 100) / $totalRateCnt;
                                    }
                                    ?>
                                    <div class="col-xs-2"><span>{{$btw3_4RateCnt}}</span></div>
                                    <div class="col-xs-5">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar4" role="progressbar"
                                                 aria-valuenow="{{$percentage_3_4}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$percentage_3_4}}%">
                                            </div>
                                        </div><!--.progress-->
                                    </div>
                                    <div class="col-xs-5">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div><!--.rating-desc-detail-->
                                <div class="rating-desc-detail">
                                    <?php
                                    if ($totalRateCnt == 0) {
                                        $percentage_2_3 = ($btw2_3RateCnt * 100) / ($totalRateCnt + 1);
                                    } else {
                                        $percentage_2_3 = ($btw2_3RateCnt * 100) / $totalRateCnt;
                                    }
                                    ?>
                                    <div class="col-xs-2"><span>{{$btw2_3RateCnt}}</span></div>
                                    <div class="col-xs-5">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar3" role="progressbar"
                                                 aria-valuenow="{{$percentage_2_3}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$percentage_2_3}}%">
                                            </div>
                                        </div><!--.progress-->
                                    </div>
                                    <div class="col-xs-5">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div><!--.rating-desc-detail-->
                                <div class="rating-desc-detail">
                                    <?php
                                    if ($totalRateCnt == 0) {
                                        $percentage_1_2 = ($btw1_2RateCnt * 100) / ($totalRateCnt + 1);
                                    } else {
                                        $percentage_1_2 = ($btw1_2RateCnt * 100) / $totalRateCnt;
                                    }
                                    ?>
                                    <div class="col-xs-2"><span>{{$btw1_2RateCnt}}</span></div>
                                    <div class="col-xs-5">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar2" role="progressbar"
                                                 aria-valuenow="{{$percentage_1_2}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$percentage_1_2}}%">
                                            </div>
                                        </div><!--.progress-->
                                    </div>
                                    <div class="col-xs-5">
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div><!--.rating-desc-detail-->
                                <div class="rating-desc-detail">
                                    <?php
                                    if ($totalRateCnt == 0) {
                                        $percentage_0_1 = ($btw0_1RateCnt * 100) / ($totalRateCnt + 1);
                                    } else {
                                        $percentage_0_1 = ($btw0_1RateCnt * 100) / $totalRateCnt;
                                    }
                                    ?>
                                    <div class="col-xs-2"><span>{{$btw0_1RateCnt}}</span></div>
                                    <div class="col-xs-5">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar1" role="progressbar"
                                                 aria-valuenow="{{$percentage_0_1}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$percentage_0_1}}%">
                                            </div>
                                        </div><!--.progress-->
                                    </div>
                                    <div class="col-xs-5">
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div><!--.rating-desc-detail-->
                            </div><!--.rating-desc-->
                        </div>
                    </div><!--.row-->
                </div><!--.pr__rating-->
            </div><!--.pr-wrapp-->

            <div class="pr__comment col-xs-12 col-md-8 col-md-push-2">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-push-2">
                        <ul>
                            <?php
                            $commentRowV = 0;
                            ?>
                            @foreach($comment as $item)
                                <li id="{{$item->id}}">
                                    <figure class="col-xs-2">
                                        @if ($clientInfo[$commentRowV]->image == 'null')
                                            <img class="img-responsive"
                                                 src="{{ asset('Assets/Master/Web/images/avatar.png') }}"
                                                 alt="{{$clientInfo[$commentRowV]->name.' '.$clientInfo[$commentRowV]->family}}"
                                                 title="{{$clientInfo[$commentRowV]->name.' '.$clientInfo[$commentRowV]->family}}"
                                                 style="border-radius: 100%;">
                                        @else
                                            <img class="img-responsive"
                                                 src="{{$LocalhostPath.'/uploads/avatars/'.$clientInfo[$commentRowV]->image}}"
                                                 alt="{{$clientInfo[$commentRowV]->name.' '.$clientInfo[$commentRowV]->family}}"
                                                 title="{{$clientInfo[$commentRowV]->name.' '.$clientInfo[$commentRowV]->family}}"
                                                 style="border-radius: 100%;">
                                        @endif
                                    </figure>
                                    <div class="col-xs-10">
                                        <ul class="pr__comment-detail">
                                            <li>
                                                <h6 class="pr__comment-title">{{$clientInfo[$commentRowV]->name.' '.$clientInfo[$commentRowV]->family}}</h6>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                    @if($appRate[$commentRowV] == 0)
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 0.5)
                                                        <span class="fa fa-star-half-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 1)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 1.5)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-half-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 2)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 2.5)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-half-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 3)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 3.5)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-half-o"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 4)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 4.5)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-half-o"></span>
                                                    @endif
                                                    @if($appRate[$commentRowV] == 5)
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                    @endif
                                                </div><!--.rating-->
                                                <time datetime="{{$item->created_at}}">{{$item->created_at}}</time>
                                                <div class="clearfix"></div>
                                            </li>
                                            <li>
                                                <p>
                                                    {{$item->comment}}
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <?php
                                $commentRowV++;
                                ?>
                            @endforeach
                        </ul>
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="pr__comment-more pr__toggleComment">
                        <div class="col-xs-12 col-md-8 col-md-push-2">
                            <ul>
                                <?php
                                $comment2RowV = 0;
                                ?>
                                @foreach($comment_2 as $item_2)
                                    <li id="{{$item_2->id}}">
                                        <figure class="col-xs-2">
                                            @if ($clientInfo_2[$comment2RowV]->image == 'null')
                                                <img class="img-responsive"
                                                     src="{{ asset('Assets/Master/Web/images/avatar.png') }}"
                                                     alt="{{$clientInfo_2[$comment2RowV]->name.' '.$clientInfo_2[$comment2RowV]->family}}"
                                                     title="{{$clientInfo_2[$comment2RowV]->name.' '.$clientInfo_2[$comment2RowV]->family}}"
                                                     style="border-radius: 100%;">
                                            @else
                                                <img class="img-responsive"
                                                     src="{{$LocalhostPath.'/uploads/avatars/'.$clientInfo_2[$comment2RowV]->image}}"
                                                     alt="{{$clientInfo_2[$comment2RowV]->name.' '.$clientInfo_2[$comment2RowV]->family}}"
                                                     title="{{$clientInfo_2[$comment2RowV]->name.' '.$clientInfo_2[$comment2RowV]->family}}"
                                                     style="border-radius: 100%;">
                                            @endif
                                        </figure>
                                        <div class="col-xs-10">
                                            <ul class="pr__comment-detail">
                                                <li>
                                                    <h6 class="pr__comment-title">{{$clientInfo_2[$comment2RowV]->name.' '.$clientInfo_2[$comment2RowV]->family}}</h6>
                                                </li>
                                                <li>
                                                    <div class="rating">
                                                        @if($appRate_2[$comment2RowV] == 0)
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 0.5)
                                                            <span class="fa fa-star-half-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 1)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 1.5)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-half-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 2)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 2.5)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-half-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 3)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 3.5)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-half-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 4)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 4.5)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-half-o"></span>
                                                        @endif
                                                        @if($appRate_2[$comment2RowV] == 5)
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        @endif
                                                    </div><!--.rating-->
                                                    <time datetime="{{$item_2->created_at}}">{{$item_2->created_at}}</time>
                                                    <div class="clearfix"></div>
                                                </li>
                                                <li>
                                                    <p>
                                                        {{$item_2->comment}}
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                    <?php
                                    $comment2RowV++;
                                    ?>
                                @endforeach
                            </ul>
                        </div>
                    </div><!--.pr__comment-more-->
                </div><!--.row-->
                <div class="col-xs-12 pr__dropComment text-center" data-for=".pr__toggleComment">
                    <i class="glyphicon glyphicon-menu-down pr__icon-comment"></i>
                </div><!--.pr__dropComment-->
            </div><!--.pr__comment-->
            <div class="clearfix"></div>
        </section><!--.container-->
    </section>
@stop