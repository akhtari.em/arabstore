<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\UserCategory\Admin\Controllers\UserCategoryManager;
use App\Http\Controllers\CoreCommon;
use App\Modules\App\Admin\Controllers\AppManager;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	

	//Views(Get) Routes
	Route::get('Add','AppManager@AddView')->name('AddAppView');
	Route::get('List','AppManager@ListView')->name('AppListView');

	
	Route::get('Ajax_GetSubCategories','AjaxHandler@SubCategoryList')->name('SubCategoryList');


	Route::get('Ajax_GetList','AjaxHandler@ListView');

	
	Route::get('Edit/{id}', 'AppManager@EditView')->name('EditAppView');
//	Route::get('Edit/{id}',function($id){
//		$acm= new AppManager();
//		return $acm->EditView($id);
//	})->name('EditAppView');

	//Create(Post) Routes
	Route::post('Add','AppManager@Add')->name('AddApp');
	Route::post('Edit/{id}','AppManager@Edit')->name('EditApp');

	//Update(Post) Routes

	//Delete(Delete) Routes
	Route::get('Delete/{id}','AppManager@Delete')->name('App.Delete');

});


//These Routes Use When User Not Logged In

