@extends('Master_Admin::MasterPage')

@section('title')
افزودن اپلیکشن 
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop


@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Add')}}</h2>
             </div>
        </div>
   </div>
</div>
<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post" onsubmit="return checkApp()" name="form" action="{{route('AddApp')}}" enctype="multipart/form-data" id="form">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">نام اپلیکیشن <span>الزامی است*</span> </label>
                                    <input class="input-style" value="{{old('title')}}" type="text" id="title" name="title">
                                </div>
                            </div>
                        </div>
	                    
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">دسته بندی<span>الزامی است*</span></label>
                                    <select    class="form-control selectpicker wide _category required" name="category" id="category" data-live-search="true">
                                        <option value="-1">انتخاب کنید</option>
                                        @foreach($categories as $index=>$value)
                                            <option value="{{$index}}"{{$index == old('category')}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">توسعه دهنده<span>الزامی است*</span></label>
                                    <select   class="form-control selectpicker wide _developer required" name="developer" id="developer" data-live-search="true">
                                        <option value="">انتخاب کنید</option>
                                        @foreach($developers as $index=>$value)
                                            <option value="{{$index}}" {{$index == (old('developer')? 'selected': '')}}>{{$value}}</option>
                                        @endforeach
                                    </select>                                                   
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form" >
                                    <label><span>الزامی است</span>ایکون (فایل مجاز jpg)</label>
                                    <div class="col-lg-8" style="width: 100%">
                                        <input id="avatar" name="avatar" type="file" multiple class="file-loading" accept="image/*" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form" >
                                    <label>دسته بندی عکس  (فایل مجاز jpg) <span>الزامی است*</span></label>
                                    <div class="col-lg-8" style="width: 100%">
                                        <input id="imageicon" name="imageicon" type="file" multiple class="file-loading" accept="image/*" >
                                    </div>
                                </div>
                            </div>
                        </div>
	
	                    <div class="row row-form">
		                    <div class="col-md-12 float-right">
			                    <div class="inline-form" >
				                    <label> ویدئو  (فایل مجاز mp4)</label>
				                    <div class="col-lg-8" style="width: 100%">
					                    <input id="appvideo" name="appvideo" type="file" multiple class="file-loading" accept="video/*"  >
				                    </div>
			                    </div>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">کمپانی </label>
                                    <input class="input-style" value="{{old('company')}}" type="text" name="company">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">وب سایت </label>
                                    <input class="input-style" placeholder="http://www.paraxco.com" value="{{old('website')}}" type="text" name="website" dir="ltr">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">ایمیل</label>
                                    <input class="input-style" value="{{old('email')}}" type="email" name="email" dir="ltr">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">تلفن ثابت</label>
                                    <input class="input-style" value="{{old('phone')}}" type="number" name="phone" dir="ltr">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">همراه</label>
                                    <input class="input-style" value="{{old('mobile')}}" type="number" name="mobile" dir="ltr">
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
	                        <div class="col-sm-12 col-md-8 col-lg-6">
		                        <label class=" pull-right">وضعیت</label>
		                        <select class="form-control" name="isFree" id="isFree">
			                        <option value="1" {{(old('status') == '1')?'selected':''}}>رایگان
			                        </option>
			                        <option value="0" {{(old('status') == '0')?'selected':''}}>پولی
			                        </option>
		                        </select>
	                        </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">قیمت</label>
                                    <input class="input-style" value="{{old('price')}}" type="number" name="price">
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">رده سنی</label>
                                    <select class="form-control" name="age" id="age" data-live-search="true">
                                        {{--<option value="0">انتخاب نشده</option>--}}
	                                    <option value="16" {{(old('age') == '16')?'selected':''}}>16 </option>
	                                    <option value="3" {{(old('age') == '3')? 'selected' : ''}}>3</option>
	                                    <option value="7" {{(old('age') == '7')?'selected':''}}> 7</option>
	                                    <option value="12" {{(old('age') == '12')?'selected':''}}> 12</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
		                                <label class=" pull-right">جنسیت</label>
		                                <select class="form-control" name="sex" id="sex">
			                                <option value="male" {{(old('sex') == 'male')?'selected':''}}>پسرانه
			                                </option>
			                                <option value="female" {{(old('sex') == 'female')?'selected':''}}>دخترانه
			                                </option>
		                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <div class="form-group">
			                            <label class="pull-right">ساخت شده برای</label>
			                            <select class="form-control" name="made" id="made">
				                            <option value="iranian" {{(old('made') == 'iranian')?'selected':''}}>ایرانی
				                            </option>
				                            <option value="foreign" {{(old('made') == 'foreign')?'selected':''}}>خارجی
				                            </option>
			                            </select>
	                            </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <div class="form-group">
			                            <label class="pull-right">vip</label>
			                            <select class="form-control" name="vip" id="vip">
				                            <option value="0" {{(old('vip') == '0')?'selected':''}}>نیست
				                            </option>
				                            <option value="1" {{(old('vip') == '1')?'selected':''}}>هست
				                            </option>
			                            </select>
	                            </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
                                    <label>جزئیات :<span>الزامی است*</span></label>
                                    <div class="form-editor">
                                      <textarea name="detail" id="detail" class="ckeditor"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
	                    <div class="row row-form">
		                    <div class="col-md-12 float-right">
			                    <div class="inline-form">
				                    <label>تغییرات ورژن جدید :</label>
				                    <div class="form-editor">
					                    <textarea name="change" id="change" class="ckeditor"></textarea>
				                    </div>
			                    </div>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number"  name="order" value="0" dir="ltr">
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
	                        <div class="col-sm-12 col-md-8 col-lg-6">
		                        <label class="pull-right">وضعیت</label>
		                        <select class="form-control" name="status" id="status">
			                        <option value="1" {{(old('status') == '1')?'selected':''}}>فعال
			                        </option>
			                        <option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال
			                        </option>
		                        </select>
	                        </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
</div>
@stop
@section('js')
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('Assets/App/Admin/js/Add.js')}}"></script>
	<script src="{{asset('Assets/App/Admin/js/checkAppValidate.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#imageicon").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#appvideo").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["mp4"],
			minImageWidth: 50,
			minImageHeight: 50,
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop