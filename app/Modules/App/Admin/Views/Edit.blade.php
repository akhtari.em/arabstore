@extends('Master_Admin::MasterPage')

@section('title')
ویرایش اپلیکشن 
@stop

@section('css')
    <link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Edit')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post" onsubmit="return checkEditApp()"  name="form" action="{{route('EditApp', ['id' => $id->id])}}" enctype="multipart/form-data" id="form">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">نام اپلیکیشن <span>الزامی است*</span> </label>
                                    <input class="input-style" type="text" id="title" name="title" value="{{$id->title}}">
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label"><span>الزامی است*</span>دسته بندی</label>
                                    <select class="form-control selectpicker wide _category" name="category" id="category" data-live-search="true">
	                                    @foreach($categories as $index=> $value)
		                                    <option value="{{$index}}" {{($index == $id->category)? 'selected' : ''}}>{{$value}}</option>
	                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label"><span>الزامی است*</span>توسعه دهنده</label>
                                    <select class="form-control selectpicker wide _developer" name="developer" id="developer" data-live-search="true">
                                        @foreach($developers as $index=>$value)
											<option value="{{$index}}" {{($index == $id->developer)? 'selected': ''}}>{{$value}}</option>
										@endforeach
                                    </select>                                                   
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
	                            <div class="inline-form">
		                            <label><span>الزامی است*</span>تصویر فایل (فایل مجاز jpg)</label>
		                            <div class="col-lg-8" style="width: 100%">
			                            <img src="{{ $id->getGravatar() }}" alt="{{ $id->name }}"  style="width:150px;height:150px;">
			                            <input id="avatar" name="avatar" type="file" multiple class="file-loading"
			                                   accept="image/*">
		                            </div>
		                            <div class="clear"></div>
	                            </div>
                            </div>
 
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
	                            <div class="inline-form">
		                            <label>تصویر فایل (فایل مجاز jpg)<span>الزامی است*</span></label>
		                            <div class="col-lg-8" style="width: 100%">
			                            <img src="{{ $id->getImage() }}" alt="{{ $id->name }}"  style="width:150px;height:150px;">
			                            <input id="imageicon" name="imageicon" type="file" multiple class="file-loading"
			                                   accept="image/*">
		                            </div>
		                            <div class="clear"></div>
	                            </div>
                            </div>
                        </div>
	                    <div class="row row-form">
		                    <div class="col-md-12 float-right">
			                    <div class="inline-form" >
				                    <label> ویدئو  (فایل مجاز mp4,mkv)</label>
				                    <div class="col-lg-8" style="width: 100%" >
					                    <video style="width:300px;height:300px;" src="{{$id->getVideo()}}" controls></video>
					                    <input id="appvideo" name="appvideo" type="file" multiple class="file-loading" accept="video/*">
				                    </div>
			                    </div>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">کمپانی </label>
                                    <input class="input-style" type="text" name="company" value="{{$id->company}}">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">وب سایت </label>
                                    <input class="input-style" type="text" name="website" dir="ltr" value="{{$id->website}}">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">ایمیل</label>
                                    <input class="input-style" type="text" name="email" dir="ltr" value="{{$id->email}}">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">تلفن ثابت</label>
                                    <input class="input-style" type="number" name="phone" dir="ltr" value="{{$id->phone}}">
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">همراه</label>
                                    <input class="input-style" type="number" name="mobile" dir="ltr" value="{{$id->mobile}}">
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <div class="toggle-setting">
		                            <label class="pull-right">وضعیت</label>
		                            <select class="form-control" name="isFree" id="isFree">
			                            <option value="1" {{($id->isFree == '1')?'selected':''}}>رایگان
			                            </option>
			                            <option value="0" {{($id->isFree == '0')?'selected':''}}>پولی
			                            </option>
		                            </select>
	                            </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">قیمت</label>
                                    <input class="input-style" type="number" name="price" value="{{$id->price}}">
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">رده سنی</label>
                                    <select class="form-control selectpicker wide _age" name="age" id="age" data-live-search="true">
	                                    <option value="16" {{($id->age == '16')? 'selected' : ''}}>16</option>
	                                    <option value="3" {{($id->age == '3')?'selected':''}}>3 </option>
	                                    <option value="7" {{($id->age == '7')?'selected':''}}> 7</option>
	                                    <option value="12" {{($id->age == '12')?'selected':''}}>12 </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">جنسیت</label>
                                    <select class="form-control selectpicker wide _sex" name="sex" id="sex" data-live-search="true">
		                                    <option value="male" {{($id->sex == 'male')?'selected':''}}>پسرانه</option>
		                                    <option value="female" {{($id->sex == 'female')?'selected':''}}>دخترانه</option>
	                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">ساخت</label>
                                    <select class="form-control selectpicker wide _made" name="made" id="made" data-live-search="true">
	                                    <option value="iranian" {{($id->made == 'iranian')?'selected':''}}>ایرانی
	                                    </option>
	                                    <option value="foreign" {{($id->made == 'foreign')?'selected':''}}>خارجی
	                                    </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
	                                <label class="form-label">vip</label>
	                                <select class="form-control" name="vip" id="vip">
		                                <option value="0" {{($id->vip == '0')?'selected':''}}>نیست
		                                </option>
		                                <option value="1" {{($id->vip == '1')?'selected':''}}>هست
		                                </option>
	                                </select>
                                </div>
                            </div>
                        </div>


                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
                                    <label><span>الزامی است*</span>جزئیات :</label>
                                    <div class="form-editor">
                                      <textarea name="detail" id="detail" class="ckeditor">{{$id->detail}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
	                    <div class="row row-form">
		                    <div class="col-md-12 float-right">
			                    <div class="inline-form">
				                    <label>تغییرات ورژن جدید :</label>
				                    <div class="form-editor">
					                    <textarea name="change" id="change" class="ckeditor">{{$id->change}}</textarea>
				                    </div>
			                    </div>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number" name="order" value="{{$id->order}}" dir="ltr">
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                 <div class="form-group">
                                    <label class="form-label">وضعیت</label>
	                                <select class="form-control" name="status" id="status">
		                                <option value="1" {{($id->status == '1')?'selected':''}}>فعال
		                                </option>
		                                <option value="0" {{($id->status == '0')?'selected':''}}>غیرفعال
		                                </option>
	                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit">ویرایش </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

 
</div>
 
@stop
@section('js')
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('Assets/App/Admin/js/Edit.js')}}"></script>
	<script src="{{asset('Assets/App/Admin/js/checkEditAppValidate .js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#imageicon").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#appvideo").fileinput({
			uploadUrl: "{{ route('AddApp') }}",
			allowedFileExtensions: ["mp4"],
			minImageWidth: 50,
			minImageHeight: 50,
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop