<?php


namespace App\Modules\App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CoreCommon;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\App\Admin\Models\App;


class AjaxHandler extends Controller
{	


	public function GetModuleName(){
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		return $moduleName;
	}

	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}



	//Just Return Views
	
	public function SubCategoryList(){
		
		return AppCategory::GetChilds(Input::get('categoryId'));
	}


	public function ListView()
	{	
		$limit=15;
		$count=App::Count(Input::get('search'));
		
		
		if(is_int($count/$limit))
		{
			$pagesCount=($count/$limit);	
		}
		else
		{
			$pagesCount=((int)($count/$limit))+1;
		}
		
		if(!empty(Input::get('pageNumber')))
		{	
			$offset=(Input::get('pageNumber')-1)*$limit;
			$apps=App::Search(Input::get('search'),$offset,$limit);
		}
		else
		{
			$apps=App::Search(Input::get('search'),0,$limit);
		}

		return view($this->GetPreView().'Ajax_List')->with('data',['apps'=>$apps,'pageInfo'=>['curPage'=>Input::get('pageNumber'),'pagesCount'=>$pagesCount]]);
	}


	//Controllers That Do Somethings
	

	 





	

}
