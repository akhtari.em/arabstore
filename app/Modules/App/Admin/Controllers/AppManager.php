<?php


namespace App\Modules\App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\App\Admin\Models\App;
use App\Modules\Developer\Admin\Models\Developer;
use App\Http\Controllers\CoreCommon;

use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
class AppManager extends Controller
{
	private $appService;
	public function __construct(AppServices $appService)
	{
		$this->appService = $appService;
	}
	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}
	

	//Just Return Views
	public function AddView()
	{	
		$developers=Developer::GetAll();
		$categories=AppCategory::GetParents();
		return view($this->GetPreView().'Add', compact('developers', 'categories'));
	}

	public function ListView()
	{
		return view($this->GetPreView().'List');
	}

	public function EditView($id)
	{	

		$id=App::GetWithId($id);

		$developers=Developer::GetAll();
		$categories=AppCategory::GetParents();
		return view($this->GetPreView().'Edit', compact('categories', 'developers'))->with(['id'=>$id]);

	}

	//Controllers That Do Somethings
	public function Add(AppRequest $request)
	{
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		if ($request->hasFile('imageicon') && $request->file('imageicon')->isValid())
		{
			$filenameIcon = $this->appService->uploadAvatar($request->file('imageicon'));
			$request->request->add(['image' => $filenameIcon]);
		}
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appService->uploadAvatar2($request->file('avatar'));
			$request->request->add(['icon' => $filename]);
		}
		if ($request->hasFile('appvideo') && $request->file('appvideo')->isValid())
		{
			$filename = $this->appService->uploadVideo($request->file('appvideo'));
			
			$request->request->add(['video' => $filename]);
		}
		App::create($request->all());
		return redirect(route('AppListView'));
	}

	public function Delete($id)
	{
		$app = App::findOrFail($id);
		$app->delete();
		// delete app in vitrineItem
		$vitrineItems = VitrineItem::where('app_id', $id)->get();
		foreach ($vitrineItems as $vitrineItem)
		{
			$vitrineItem->delete();
		}
		// remove app in favorite
		$favorites = Favorite::where('item_id', $id)->get();
		foreach ($favorites as $favorite)
		{
			$favorite->delete();
		}
		
		// remove app in comment
		$comments = Comment::where('item_id', $id)->get();
		foreach ($comments as $comment)
		{
			$comment->delete();
		}
		
		// remove in app_apk
		$appApks = AppApk:: where('app_id', $id)->get();
		foreach ($appApks as $appApk)
		{
			$appApk->delete();
		}
		
		//remove in app_media
		$appMedias = AppApk::where('app_id', $id)->get();
		foreach ($appMedias as $appMedia)
		{
			$appMedia->delete();
		}
		
		$wishLists = WishList::where('app_id', $id)->get();
		foreach ($wishLists as $wishList)
		{
			$wishList->delete();
		}
		
		// remove u=in app_rate
		$appRates = AppRate::where('app_id',$id)->get();
		foreach ($appRates as $appRate)
		{
			$appRate->delete();
		}
		return redirect(route('AppListView'));
	}

	public function Edit(AppRequest $request, $id)
	{	
		$app = App::findOrFail($id);
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		if ($request->hasFile('imageicon') && $request->file('imageicon')->isValid())
		{
			$filenameIcon = $this->appService->uploadAvatar($request->file('imageicon'));
			$request->request->add(['image' => $filenameIcon]);
		}
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->appService->uploadAvatar($request->file('avatar'));
			$request->request->add(['icon' => $filename]);
		}
		if ($request->hasFile('appvideo') && $request->file('appvideo')->isValid())
		{
			$filename = $this->appService->uploadVideo($request->file('appvideo'));
			
			$request->request->add(['video' => $filename]);
		}
		$app->update($request->all());
		return redirect(route('AppListView'));
	}



}
