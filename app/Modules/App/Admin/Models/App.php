<?php

namespace App\Modules\App\Admin\Models;

use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use App\Http\Controllers\CoreCommon;
use function Zend\Diactoros\normalizeUploadedFiles;

class App extends Model
{
	protected $table='app';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected  $fillable = [
		'title','age','lang','sex','made','package',
		'detail','shortDetail','manifest','category','developer','icon',
		'image','video','company','website','email','phone',
		'mobile','isFree','price','inAppPayment','owner','downloadCount',
		'installCount','viewCount','rateAvarage','commentCount','favoriteCount','status',
		'publishRequest','publishTime','hideRequest','vip','order','userid','change'
	];
	
	protected $appends = [
		'icon_path',
		'app_apk',
		'app_media',
		'image_path',
		'video_path',
		'apk_url',
		'wish_list_id',
		'share_url'
	];
	
	public function appMedia()
	{
		return $this->hasMany(AppMedia::class)->where('status', 1);
	}
	public static function  getApp()
	{
		return App::where('status', 1)->oldest('title')->pluck('title', 'id');
	}
	
	public function appApk()
	{
		return $this->hasMany(AppApk::class)->where('status', 1);
	}
	
	public static function GetWithId($id)
	{
		if(App::where('id',$id)->exists())
		{
			return App::where('id',$id)->get()[0];
		}
		
	}
	
	public static function Search($title,$offset,$limit)
	{
		return App::where('app.title','like','%'.$title.'%')->join('app_category','app.category','=','app_category.id')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['app.id','app.title','app.image','app.created_at','app_category.title as categoryTitle','app.status']);
	}
	
	public static function Remove($id)
	{
		if(App::where('id',$id)->exists())
		{
			App::where('id','=',$id)->delete();
		}
	}
	
	public static function CheckExists($id)
	{
		if(App::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function Count($searchFiled)
	{
		return App::where('title','like','%'.$searchFiled.'%')->count();
	}
	
	public function vitrineItems()
	{
		return $this->belongsTo(VitrineItem::class);
	}
	
	public function comments()
	{
		return $this->hasMany(Comment::class, 'item_id');
	}
	
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	public function favorites()
	{
		return $this->hasMany(Favorite::class, 'item_id');
	}
	
	public function appCategories()
	{
		return $this->hasMany(App::class, 'category');
	}
	
	public function getGravatar($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->icon)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->icon");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->title)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getImage($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->image)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->image");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->title)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getVideo($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->video)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->video");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->title)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getIconPathAttribute()
	{
		return AppServices::getAvatarDisplayPath("/uploads/App/" . $this->icon);
	}
	
	public function getShareUrlAttribute()
	{
		$location = CoreCommon::getLocalhostPath("");
		
		return $location.'/App/Web/PageView/'.$this->id;
	}
	
	public function getImagePathAttribute()
	{
		return AppServices::getAvatarDisplayPath("/uploads/App/" . $this->image);
	}
	
	public function getVideoPathAttribute()
	{
		return AppServices::getAvatarDisplayPath("/uploads/App/" . $this->video);
	}
	
	public function getAppApkAttribute()
	{
		
		if ($this->appApk()->latest('id')->first())
		{
			return array_only($this->appApk()->latest('id')->first()->toArray(), [ 'versionName', 'versionCode', 'package_name']);
		}
		return null;
	}
	
	public function getApkUrlAttribute()
	{
		
		if ($fiel = $this->appApk()->latest('id')->first())
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$fiel->file");
		}
		return "";
	}
	
	public function getAppMediaAttribute()
	{
		if ($this->appMedia()->latest()->first())
		{
			return array_only($this->appMedia()->latest('id')->first()->toArray(), ['id','app_id', 'url','videoImage']);
		}
		return null;
	}
	
	public function getWishListIdAttribute()
	{
		
		if ($this->wishLists()->latest()->first())
		{
			// check user has wishList or not
			return  array_only($this->wishLists()->first()->toArray(), ['id']);
		}
		return [];
		// check user login
//		$user = Auth('api')->user();
//		if ($this->wishLists()->latest()->first())
//		{
//			// check user has wishList or not
//			$user = $this->wishLists()->where('client_id',$user->id)->first();
//			if ($user)
//			{
//				return  array_only($this->wishLists()->first()->toArray(), ['id']);
//
//			}
//			else
//			{
//				return null;
//			}
//
//		}
//		return [];
	}
	
	public function clientDownloads()
	{
		return $this->hasMany(App::class);
	}
	
	public function clientViews()
	{
		return $this->hasMany(App::class);
	}
	
	public function wishLists()
	{
		return $this->hasMany(WishList::class, 'app_id');
	}
	
	public function appRate()
	{
		return $this->hasMany(App::class);
	}
}