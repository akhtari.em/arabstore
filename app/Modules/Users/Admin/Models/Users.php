<?php

namespace App\Modules\Users\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\AppMedia\Admin\Models\back;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Confirmation\Confirmation;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Kavenegar;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Carbon\Carbon;
class Users extends Model
{
	protected $table='users';
	public $timestamps=true;
	protected $primaryKey = 'id';
	
	public function appMedia()
	{
		return $this->hasMany(back::class);
	}
	
	public static function Get($id)
	{
		if(Users::where('id',$id)->exists())
		{
			return Users::where('id',$id)->get()[0];
		}
	}
	public static function Add($name,$family,$publicName,$username,$password,$address,$userCategory,$permissions,$status)
{
	if(!empty($publicName) && !empty($username) && !empty($password) && !empty($userCategory) && !empty($userCategory) )
	{
		
		$users=new Users();
		$users->name=$name;
		$users->family=$family;
		$users->publicName=$publicName;
		$users->username=$username;
		$users->password= Hash::make($password);
		$users->address=$address;
		$users->user_category_id=$userCategory;
		$users->permissions=json_encode($permissions);
		$users->status=$status;
		$users->save();
	}
}

	public static function GetUsers($count)
	{
		return Users::take($count)->get();
	}

	public static function SearchUsers($publicNameOrUsername,$offset,$limit)
	{	
		return Users::where('publicName','like','%'.$publicNameOrUsername.'%')->orWhere('username','like','%'.$publicNameOrUsername.'%')->join('user_category', 'users.user_category_id', '=', 'user_category.id')->offset($offset)->take($limit)->get(['users.id','users.publicName','users.username','users.created_at','user_category.title','users.status']);
	}

	public static function GetUserWithCategoryDetails($id)
	{
		return Users::where('users.id',$id)->join('user_category', 'users.user_category_id', '=', 'user_category.id')->get(['users.id','users.publicName','users.username','users.created_at','user_category.title','users.status']);
	}


	public static function Remove($id)
	{
		if(Users::where('id',$id)->exists())
		{
			Users::where('id','=',$id)->delete();
		}		
	}


	public static function CheckUsersExists($id)
	{
		if(Users::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function UpToDate($id,$name,$family,$publicName,$username,$password,$address,$userCategory,$permissions,$status)
	{
		if(!empty($publicName) && !empty($username)  && !empty($userCategory) && !empty($userCategory) )
		{
			if(Users::where('id',$id)->exists())
			{	
				$users= Users::where('id',$id)->get()[0];
				$users->name=$name;
				$users->family=$family;
				$users->publicName=$publicName;
				$users->username=$username;
				if(!empty($password))
				{
					$users->password= Hash::make($password);
				}
				$users->address=$address;
				$users->user_category_id=$userCategory;
				$users->permissions=json_encode($permissions);
				$users->status=$status;
				$users->save();
			}
		}
	}

	public static function UsersCount($searchFiled)
	{
		return Users::where('publicName','like','%'.$searchFiled.'%')->orWhere('username','like','%'.$searchFiled.'%')->count();
	}
}