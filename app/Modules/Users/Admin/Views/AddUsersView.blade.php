@extends('Master_Admin::MasterPage')
@section('title')
	 کابر جدید
@endsection

@section('css')
@stop



@section('content')
    <div style="padding-top: 100px">
        @foreach($errors->all() as $error)
            <div class="text-dark">{{$error}}</div>
        @endforeach
    
    </div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('lang.addUsers')}}</h2>
             </div>
        </div>
   </div>
</div>

<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" name="form" onsubmit="return AddUserValidate()" method="post"  action="{{route('AddUsers')}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-3">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام خانوادگی</label>
                                            <input class="input-style" type="text" name="family">
                                        </div>
                                    </div>
                                </div>
                            </div>
	                        <div class="col-sm-12 col-md-6 col-lg-3">
		                        <div class="row frst-row-form">
			                        <div class="col-md-12">
				                        <div class="inline-form">
					                        <label class="c-label">نام</label>
					                        <input class="input-style" type="text" name="name">
				                        </div>
			                        </div>
		                        </div>
	                        </div>
                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام نمایشی <span>*</span></label>
                                            <input class="input-style" type="text" id="publicName" name="publicName">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
	                        <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام کاربری <span>*</span></label>
                                            <input class="input-style" type="text" id="username" name="username">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-5">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">رمز ورود <span>*</span></label>
                                            <input class="input-style"  type="password" id="password" name="password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearsp"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-9 col-lg-9">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">آدرس</label>
                                            <textarea name="address"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearsp"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-9 col-lg-9">
                                <div class="row row-form">
                                    <div class="col-md-12 sec">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('lang.selectUserCategory')}}</label>
                                            <select class="form-control selectpicker wide _category" name="userCategory" id="userCategory" data-live-search="false">
                                                <option ></option>
                                            @foreach($userCategories as $userCategory)
                                                <option value="{{$userCategory['id']}}">{{$userCategory['title']}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form" >
                            <div class="col-md-12 fill-set">
                                <div class="toggle-setting">
                                    <div class="row" id="modules_user">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="toggle-setting">
                                    <span class="status-title">وضعیت</span>
                                    <label class="toggle-switch">
                                        <input type="checkbox" name="status" checked="">
                                        <span data-unchecked="غیر فعال" data-checked="فعال"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')
    <script src="{{asset('Assets/Users/Admin/js/AddUsers.js')}}"></script>
    <script src="{{asset('Assets/Users/Admin/js/checkUserAddValidate.js')}}"></script>
@stop