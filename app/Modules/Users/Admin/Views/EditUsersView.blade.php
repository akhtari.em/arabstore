@extends('Master_Admin::MasterPage')


@section('title')
ویرایش کاربر
@stop

@section('css')
@stop

@section('js')
<script src="{{asset('Assets/Users/Admin/js/EditUsers.js')}}"></script>
<script src="{{asset('Assets/Users/Admin/js/checkUserAddValidate.js')}}"></script>
@stop

@section('content')
    <div style="padding-top: 100px">
        @foreach($errors->all() as $error)
            <div class="text-dark">{{$error}}</div>
        @endforeach
    </div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('lang.editUsers')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" name="form" onsubmit="return EditUserValidate()" method="post"  action="{{route('EditUsers')}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                    <input type="hidden" value="{{ $data['id'] }}" name="id" >
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 col-lg-3">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام</label>
                                            <input class="input-style" value="{{$data['user']['name']}}" id="name" type="text" name="name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-3">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام خانوادگی</label>
                                            <input class="input-style" value="{{$data['user']['family']}}"  id="family" type="text" name="family">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام نمایشی <span>*</span></label>
                                            <input class="input-style" value="{{$data['user']['publicName']}}" id="publicName" type="text" name="publicName">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">نام کاربری <span>*</span></label>
                                            <input class="input-style" value="{{$data['user']['username']}}" type="text" id="username" name="username">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-5">
                                <div class="row frst-row-form">
                                    <div class="col-md-12" >
                                        <div class="inline-form">
                                            <label class="c-label">رمز ورود جدید <span>*</span></label>
                                            <input class="input-style"  type="password" id="password" name="password" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearsp"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-9 col-lg-9">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">آدرس</label>
                                            <textarea id="address" name="address">{{$data['user']['address']}}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="clearsp"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-9 col-lg-9">
                                <div class="row row-form">
                                    <div class="col-md-12 sec">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('lang.selectUserCategory')}}</label>
                                            <select required class="form-control selectpicker wide _category" name="userCategory" id="userCategory" data-live-search="false">
                                                <option ></option>
                                            @foreach($data['userCategories'] as $userCategory)
                                                <option value="{{$userCategory['id']}}" @if($userCategory['id']==$data['user']['user_category_id']) {{'selected'}} @endif>{{$userCategory['title']}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form" >
                            <div class="col-md-12 fill-set">
                                <div class="toggle-setting">
                                    <div class="row" id="modules_user">

@foreach(json_decode($data['userCategory']['permissions'],true)['admin'] as $moduleName =>$module)
<div class="col-md-12 text-center" style="border-bottom: 1px solid #e1e1e1;margin-bottom: 10px;">
        <div class="col-md-2 text-center">
        <span class="status-title module_title">{{trans('modules.'.$moduleName)}}</span>
            <div class="module_label baseModuleClick">
                <label class="toggle-switch">
                    <input type="checkbox" name="modules[]" value="{{$moduleName}}" 
                     @if(!empty($data['userPermissions'][$moduleName])) {{'checked'}} @endif>
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
    </div>
    <div class="col-md-2 text-center">
        <span class="status-title module_title">{{trans('lang.permToAll')}}</span>
            <div class="module_label">
                <label class="toggle-switch">
                    <input type="checkbox" name="{{$moduleName}}_permAll" 
                   @if(!empty($data['userPermissions'][$moduleName]['permAll']))
                    @if($data['userPermissions'][$moduleName]['permAll']=='yes')
                     {{'checked'}} 
                    @endif
                   @endif  > 
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
    </div>
     

    @foreach($module['permissions'] as $perm)
    <div class="col-md-2 text-center">
        <span class="status-title module_title">{{trans('modules.'.key(json_decode($perm,true)))}}</span>
            <div class="module_label">
                <label class="toggle-switch">
              
                    <input type="checkbox" name="{{$moduleName}}[]" value="{{$perm}}"
            @if(!empty($data['userPermissions'][$moduleName]['permissions']))
                @foreach( $data['userPermissions'][$moduleName]['permissions'] as $p )
                    @if(   key(json_decode($p,true)) == key(json_decode($perm,true))   ) 
                            {{'checked'}}
                    @endif
                @endforeach 
            @endif >
            
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
    </div>
    @endforeach
</div>
@endforeach

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="toggle-setting">
                                    <span class="status-title">وضعیت</span>
                                    <label class="toggle-switch">
                                        <input type="checkbox" name="status" 
                                        @if($data['user']['status']==1)
                                        {{'checked'}}
                                        @endif>

                                        
                                        <span data-unchecked="غیر فعال" data-checked="فعال"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit">ویرایش
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
