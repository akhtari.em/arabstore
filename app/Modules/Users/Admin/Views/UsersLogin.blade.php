<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>:: ﭘﻨﻞ ﻧﮕﺎﺭﺵ ::</title>
    <meta charset="utf-8">
    <!--    <meta name="description" content="">-->
    <!--    <meta name="keywords" content="">-->
    <meta name="author" content="bitlers">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/bootstrap.min.css')}}">
    <!-- Our Website CSS Styles -->

    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/login.css')}}">
    <style type="text/css">

    </style>

</head>
<body>

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<input type="hidden" value="{{ csrf_token() }}" id="nekot">
<!-- Our Website Content Goes Here -->
<div class="account-user-sec login-total">
    <div class="account-sec">
        <div class="account-top-bar">
            <div class="container">
                <div class="logo">
                    <a href="" title="ParaxWeb">
                        <img src="{{asset('Assets/Master/Admin/images/Group 15172.png')}}" alt="ParaxWeb">
                    </a>
                </div>
            </div>
        </div>
        <div class="acount-sec" style="padding: 0 !important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="contact-sec">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="widget-title">
                                        <h3>سامانه مدیریت</h3>
                                    </div><!-- Widget title -->
                                    <div class="account-form">
                                        <div class="login-form">
                                            <div class="row">
                                                <div class="feild col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <input type="text" name="username" id="username"
                                                           placeholder="ﻧﺎﻡ ﮐﺎﺭﺑﺮﯼ">
                                                </div>
                                                <div class="feild col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <input type="password" name="password" id="password"
                                                           placeholder="رمز عبور">
                                                </div>

                                                <div class="feild col-md-12">
                                                    <input id="submit" name="submit" type="submit" value="ﺑﺮﻭ!">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <span id="smsg" style="display: none;">
                                                    <i class="fa fa-check-circle-o"></i>
                                                    ﺧﻮﺵ ﺁﻣﺪﯾﺪ! ﺷﻤﺎ ﺑﻪ ﺑﺮﮔﻪ ﻧﺨﺴﺖ ﺗﺮاﺑﺮﺩﻩ(ﻣﻨﺘﻘﻞ) ﻣﯽ ﺷﻮﯾﺪ
                                                </span>
                                                    <span id="msg" style="display: none;color:darkred">
                                                    <i class="fa fa-close"></i>
                                                    ﺩاﺩﻩ ﻫﺎﯼ ﻭاﺭﺩ ﺷﺪﻩ ﻧﺎﺩﺭﺳﺖ ﻫﺴﺘﻨﺪ
                                                </span>
                                                    <span id="capg" style="display: none;color:darkred">
                                                    <i class="fa fa-close"></i>
                                                    کد امنیتی نادرست می باشد.
                                                </span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="setAjax" id="setAjax" value="login">
                                            <span id="loader" style="display: none;">
     <center><img src="{{asset('Assets/Master/Admin/images/loading-cylon-red.svg.png')}}"></center>
    </span>
                                            <div class="row">
                                                <div class="col-md-12">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <p>© 2017 Copyright ParaxWeb</p>
            </div>
        </footer>
    </div><!-- Account Sec -->
</div>

</body>
<script src="{{asset('Assets/Master/Admin/js/jquery-2.1.3.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/bootstrap.min.js')}}"></script>

<!-- Our Website Javascripts -->
<script src="{{asset('Assets/Master/Admin/js/app.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/common.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/home1.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/login.js')}}"></script>
</html>

