
@foreach($modules as $key => $module)
<div class="col-md-12 text-center" style="    border-bottom: 1px solid #e1e1e1;margin-bottom: 10px;">
		<div class="col-md-2 text-center">
		<span class="status-title module_title">{{trans('modules.'.$key)}}</span>
            <div class="module_label baseModuleClick">
                <label class="toggle-switch">
                    <input type="checkbox" name="modules[]" value="{{$key}}" checked >
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
	</div>
    <div class="col-md-2 text-center">
        <span class="status-title module_title">{{trans('lang.permToAll')}}</span>
            <div class="module_label">
                <label class="toggle-switch">
                    <input type="checkbox" name="{{$key}}_permAll" checked >
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
    </div>
	@foreach($module['permissions'] as $perm)

	<div class="col-md-2 text-center">
		<span class="status-title module_title">{{trans('modules.'.key(json_decode($perm,true)))}}</span>
            <div class="module_label">
                <label class="toggle-switch">
                    <input type="checkbox" name="{{$key}}[]" value="{{$perm}}" checked >
                    <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                </label>
                <div class="clear"></div>
            </div>
	</div>
	@endforeach
</div>
@endforeach