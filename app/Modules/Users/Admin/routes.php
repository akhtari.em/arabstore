<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\Users\Admin\Controllers\UsersManager;
use App\Http\Controllers\CoreCommon;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web'] ], function ()
{	

		Route::post('UsersLogin','UsersAuthenticateManager@UsersLogin')->name('UsersLogin');
		Route::get('UsersLogout','UsersAuthenticateManager@UsersLogout')->name('UsersLogout');
		Route::get('UsersLogin','UsersAuthenticateManager@UsersLoginView')->name('UsersLoginView');
		//Ajax
		Route::get('Ajax_GetUserCategoryPermission','AjaxHandler@GetUserCategoryPermission');
		Route::get('Ajax_GetUsersList','AjaxHandler@GetUsersList');
		Route::get('DeleteUsers/{id}','UsersManager@DeleteUsers')->name('DeleteUsers');
		Route::get('EditUsers/{id}',function($id){
			$um= new UsersManager();
			return $um->EditUsersView($id);
		})->name('EditUsersView');

		Route::post('EditUsers','UsersManager@EditUsers')->name('EditUsers');
		
});


//These Routes Use When User Not Logged In
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{
	//Views(Get) Routes
	//Route::get('UsersLogin','UsersAuthenticateManager@UsersLoginView')->name('UsersLoginView');
	
	Route::get('AddUsers','UsersManager@AddUsersView')->name('AddUsersView');
	Route::get('Users','UsersManager@UsersView')->name('UsersListView');


	

	//Create&Others (Post) Routes
	Route::post('AddUsers','UsersManager@AddUsers')->name('AddUsers');


	

	//Update(Put) Routes

	//Delete(Delete) Routes
	
});

