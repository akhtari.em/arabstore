<?php


namespace App\Modules\Users\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CoreCommon;


class UsersAuthenticateManager extends Controller
{	


	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}
		else if(substr(strrchr(__DIR__,"Web".$dd), 5))
		{
			$side='Web';
		}
		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}

	//Just Return Views
	public function UsersLoginView()
	{

		return view($this->GetPreView().'UsersLogin');
	}

	//Controllers That Do Somethings
	public function UsersLogin(Request $request)
	{
		if(Auth::guard('web')->attempt(['username' => Input::get('username'), 'password' => Input::get('password')]))
		{
			return ['status'=>'true','url'=>route('MasterPage')];
		}
		else
		{
			return ['status'=>'false'];
		}
	}

	public function UsersLogout(){
		Auth::guard('web')->logout();
		return redirect(route('UsersLoginView'));
	}

	 





	

}
