<?php


namespace App\Modules\Users\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\Users\Admin\Models\Users;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Http\Controllers\CoreCommon;



class UsersManager extends Controller
{	
	public function GetModuleName(){
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		return $moduleName;
	}

	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if (substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}
		else if(substr(strrchr(__DIR__,"Developer".$dd), 5))
		{
			$side='Developer';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}

	//Just Return Views
	public function UsersView()
	{
		return view($this->GetPreView().'UsersView');
	}

	public function AddUsersView()
	{	
		$userCategories=UserCategory::GetAll();
		return view($this->GetPreView().'AddUsersView')->with('userCategories',$userCategories);
	}


	//Controllers That Do Somethings
	public function AddUsers(UserRequest $request)
	{
		$info=[];
		if(!empty(Input::get('modules')))
		{
			foreach(Input::get('modules') as $moduleName)
			{
				if(!empty(Input::get($moduleName)))
				{
					$temp=[];
					if(!empty(Input::get($moduleName.'_permAll')))
					{
						$temp['permAll']='yes';
					}
					else
					{
						$temp['permAll']='no';
					}
					$temp['permissions']=Input::get($moduleName);
					$info[$moduleName]=$temp;
				}
			}
		}
		if(!empty($info) && Input::get('userCategory')!= 0)
		{
			$permissions=['admin'=>$info];
			if(!empty(Input::get('status')))
			{
				$status=1;
			}else{
				$status=0;
			}
			Users::Add(Input::get('name'),Input::get('family'),Input::get('publicName'),Input::get('username'),Input::get('password'),Input::get('address'),Input::get('userCategory'),$permissions,$status);
		}
		return redirect(route('UsersListView'));

	}

	public function DeleteUsers($id)
	{
		if(CoreCommon::UserHaveAllPermission($this->GetModuleName()))
		{
			$user = Users::findOrFail($id);
			$user->delete();
			// remove app and all subcategory app in all tables
			$apps = App::where('userid', $id)->get();
			
			foreach ($apps as $app)
			{
				$app->delete();
				
				$comments = Comment::where('item_id',$app->id)->get();
				foreach ($comments as $comment)
				{
					$comment->delete();
				}
				
				$vitrineItems = VitrineItem::where('app_id', $app->id)->get();
				foreach ($vitrineItems as $vitrineItem)
				{
					$vitrineItem->delete();
				}
				// remove app in favorite
				$favorites = Favorite::where('item_id', $app->id)->get();
				foreach ($favorites as $favorite)
				{
					$favorite->delete();
				}
				// remove in app_apk
				$appApks = AppApk:: where('app_id', $app->id)->get();
				foreach ($appApks as $appApk)
				{
					$appApk->delete();
				}
				
				//remove in app_media
				$appMedias = AppMedia::where('app_id', $app->id)->get();
				foreach ($appMedias as $appMedia)
				{
					$appMedia->delete();
				}
				
				$wishLists = WishList::where('app_id', $app->id)->get();
				foreach ($wishLists as $wishList)
				{
					$wishList->delete();
				}
				
				// remove u=in app_rate
				$appRates = AppRate::where('app_id',$app->id)->get();
				foreach ($appRates as $appRate)
				{
					$appRate->delete();
				}
			}
			
			// remove app_apk
			$appApks = AppApk::where('user_id', $id)->get();
			foreach ($appApks as $appApk)
			{
				$appApk->delete();
			}
			
			// remove app_media
			$appMedias = AppMedia::where('user', $id)->get();
			foreach ($appMedias as $appMedia)
			{
				$appMedia->delete();
			}
			
			return redirect(route('UsersListView'));
			
		}
		
	}

	public function EditUsersView($id)
	{	if(CoreCommon::UserHaveAllPermission($this->GetModuleName()))
		{
			$userPermissions=json_decode(Auth::guard('web')->user()->permissions,true)['admin'];
			$userCategories=UserCategory::GetAll();
			$user=Users::Get($id);
			$userCategory=UserCategory::GetWithId($user['user_category_id']);
			return view($this->GetPreView().'EditUsersView')->with('data',['userCategories'=>$userCategories , 'user'=>$user,'userPermissions'=>$userPermissions,'userCategory'=>$userCategory ,'id'=>$id]);
		}
		else
		{
			return redirect('/Admin');
		}
		
	}

	public function EditUsers(UserRequest $request)
	{	
		
		if(CoreCommon::UserHaveAllPermission($this->GetModuleName()))
		{
			
			$info=[];
			if(!empty(Input::get('modules')))
			{
				foreach(Input::get('modules') as $moduleName)
				{
					if(!empty(Input::get($moduleName)))
					{
						$temp=[];
						if(!empty(Input::get($moduleName.'_permAll')))
						{
							$temp['permAll']='yes';
						}
						else
						{
							$temp['permAll']='no';
						}
						$temp['permissions']=Input::get($moduleName);
						$info[$moduleName]=$temp;
					}
				}
			}
			if(!empty($info) && Input::get('userCategory')!= 0)
			{
				$permissions=['admin'=>$info];
				if(!empty(Input::get('status')))
				{
					$status=1;
				}else{
					$status=0;
				}
				
				Users::UpToDate(Input::get('id'),Input::get('name'),Input::get('family'),Input::get('publicName'),Input::get('username'),Input::get('password'),Input::get('address'),Input::get('userCategory'),$permissions,$status);
				
			}
		}
		return redirect(route('UsersListView'));

	}


}
