<?php
namespace App\Modules\Users\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\Users\Admin\Models\Users;
use App\Http\Controllers\CoreCommon;


class AjaxHandler extends Controller
{	


	public function GetModuleName(){
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		return $moduleName;
	}

	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}
		else if(substr(strrchr(__DIR__,"Developer".$dd), 5))
		{
			$side='Developer';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}


	//Just Return Views
	public function GetUserCategoryPermission()
	{	
		$permissions_json=UserCategory::GetWithId(Input::get('userCategoryId'))['permissions'];
	    $permissions=json_decode($permissions_json,true);
		return view($this->GetPreView().'Ajax_UserCategoryPermission')->with('modules',$permissions['admin']);
	}

	public function GetUsersList(){
		if(CoreCommon::UserHaveAllPermission($this->GetModuleName()))
		{	
			$limit=15;
			if(empty(Input::get('search')))
			{	
				$countOfAllUsers=Users::UsersCount(Input::get('search'));
			}
			else
			{
				$countOfAllUsers=Users::UsersCount(Input::get('search'));
			}
			
			if(is_int($countOfAllUsers/$limit))
			{
				$pagesCount=($countOfAllUsers/$limit);	
			}
			else
			{
				$pagesCount=((int)($countOfAllUsers/$limit))+1;
			}
			
			if(!empty(Input::get('pageNumber')))
			{	
				$offset=(Input::get('pageNumber')-1)*$limit;
				$users=Users::SearchUsers(Input::get('search'),$offset,$limit);
			}
			else
			{
				$users=Users::SearchUsers(Input::get('search'),0,$limit);
			}
		}
		else
		{
			$users=Users::GetUserWithCategoryDetails(Auth::user()->id);
		}
		return view($this->GetPreView().'Ajax_UsersList')->with('data',['users'=>$users,'pageInfo'=>['curPage'=>Input::get('pageNumber'),'pagesCount'=>$pagesCount]]);
	}


	//Controllers That Do Somethings
	

	 





	

}
