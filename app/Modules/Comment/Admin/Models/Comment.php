<?php

namespace App\Modules\Comment\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Users\Admin\Models\Users;
use App\Services\UserService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{	
	protected  $fillable =[
		'client_id', 'item_id', 'comment', 'type', 'status'
	];
	protected $table='comment';
	public $timestamps=true;
	protected $hidden = [
		'type', 'status', 'updated_at', 'item_id'
	];
	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id');
	}
	
	public function app()
	{
		return $this->belongsTo(App::class, 'item_id');
	}
//	public function getClientAttribute()
//	{
//		return array_only($this->client()->latest()->first()->toArray(), ['name', 'image']);
//	}
//
//	public function getClientInfoAttribute()
//	{
//		return $this->client();
//	}
	public function client_info()
	{
		return $this->client;
	}
	public static function Count($searchFiled)
	{
		return Comment::where('item_id','like','%'.$searchFiled.'%')->count();
	}
	
	public static function Search($title,$offset,$limit)
	{
		$search = App::where('title', 'like','%'.$title.'%')->pluck('id')->toArray();
		return Comment::whereIn('comment.item_id',$search)->join('app','comment.item_id','=','app.id')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['comment.id','comment.item_id', 'comment.comment','comment.client_id','comment.created_at']);
	}
}
