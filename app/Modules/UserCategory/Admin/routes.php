<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\UserCategory\Admin\Controllers\UserCategoryManager;
use App\Http\Controllers\CoreCommon;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	

	//Views(Get) Routes
	Route::get('AddUserCategory','UserCategoryManager@AddUserCategoryView')->name('AddUserCategoryView');
	Route::get('UserCategory','UserCategoryManager@UserCategoryView')->name('UserCategoryListView');
	Route::get('EditUserCategory','UserCategoryManager@EditUserCategoryView')->name('EditUserView');

	Route::get('Ajax_GetUserCategoryList','AjaxHandler@GetUsersCategoryList');


	Route::get('EditUserCategoryView/{id}',function($id){
		$um= new UserCategoryManager();
		return $um->EditUserCategoryView($id);
	})->name('EditUserCategoryView');

	//Create(Post) Routes
	Route::post('AddUserCategory','UserCategoryManager@AddUserCategory')->name('AddUserCategory');
	Route::post('EditUserCategory','UserCategoryManager@EditUserCategory')->name('EditUserCategory');
	//Update(Put) Routes

	//Delete(Delete) Routes
	Route::get('DeleteUserCategory/{id}','UserCategoryManager@DeleteUserCategory')->name('DeleteUserCategory');
});




