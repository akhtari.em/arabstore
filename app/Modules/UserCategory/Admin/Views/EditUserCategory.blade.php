@extends('Master_Admin::MasterPage')


@section('css')
@stop

@section('js')
<script src="{{asset('Assets/UserCategory/Admin/js/AddUserCategory.js')}}"></script>
<script src="{{asset('Assets/UserCategory/Admin/js/checkValidate.js')}}"></script>
@stop

@section('content')
    <div>
        @foreach($errors->all() as $error)
            <div class="has-error" style="font-size: medium" >{{$error}}</div>
        @endforeach
    </div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Edit')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">

                    <form class="form container-fluid" method="post" name="form" onsubmit="return Validate()" action="{{route('EditUserCategory')}}" enctype="multipart/form-data">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                    <input type="hidden" value="{{$data['id']}}" name="id" >

                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="row frst-row-form">
                                    <div class="col-md-12">
                                        <div class="inline-form">
                                            <label class="c-label">عنوان</label>
                                            <input class="input-style" value="{{$data['userCategory']['title']}}" type="text" id="title" name="title">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php $perms=json_decode($data['userCategory']['permissions'] ,true)['admin'];
                         ?>
                        <div class="row row-form" id="modules_user">
                            <div class="col-md-12 fill-set">
                                <div class="toggle-setting">
                                    <div class="row">

                                    @foreach($data['modules'] as $module)

                                    <div class="col-md-12 text-center" style="    border-bottom: 1px solid #e1e1e1;margin-bottom: 10px;">
                                        <div class="col-md-2 text-center">
                                            <span class="status-title module_title">{{trans('modules.'.$module['moduleName'])}}</span>
                                                <div class="module_label baseModuleClick">
                                                    <label class="toggle-switch">
                                                        <input type="checkbox" name="modules[]" id="permissions" value="{{$module['moduleName']}}"
                                                        @if(!empty($perms[$module['moduleName']]))
                                                            {{'checked'}}
                                                        @endif >
                                                        <span data-unchecked="غیر فعال" data-checked="فعال" 
                                                        ></span>
                                                    </label>
                                                    <div class="clear"></div>
                                                </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="status-title module_title">{{trans('lang.permToAll')}}</span>
                                                <div class="module_label">
                                                    <label class="toggle-switch">
                                                        <input type="checkbox" name="{{$module['moduleName']}}_allPerm" 
                                                        @if(!empty($perms[$module['moduleName']]['permAll']))
                                                        @if($perms[$module['moduleName']]['permAll']=='yes')
                                                        {{'checked'}}
                                                        @endif
                                                        
                                                        @endif >
                                                        <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                                                    </label>
                                                    <div class="clear"></div>
                                                </div>
                                        </div>
                                        @foreach($module['permissions'] as $perm)
                                        
                                        <div class="col-md-2 text-center">
                                            <span class="status-title module_title">{{trans('modules.'. key($perm))}}</span>
                                                <div class="module_label">
                                                    <label class="toggle-switch">
                                                        <input type="checkbox" name="{{$module['moduleName']}}[]" value="{{json_encode($perm)}}"
                        @if(!empty($perms[$module['moduleName']]['permissions']))                    
                        @foreach($perms[$module['moduleName']]['permissions'] as $p )
                       
                            @if(   key(json_decode($p,true)) == key($perm)   ) 
                            {{'checked'}}
                            @endif
                        @endforeach
                        @endif >
                                                        <span data-unchecked="غیر فعال" data-checked="فعال"  ></span>
                                                    </label>
                                                    <div class="clear"></div>
                                                </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                
                                    </div>
                                 </div>
                            </div>
                        </div>

                        
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="toggle-setting">
                                    <span class="status-title">وضعیت</span>
                                    <label class="toggle-switch">
                                        <input type="checkbox" name="status"
                                        @if($data['userCategory']['status']==1)
                                        {{'checked'}}
                                        @endif>
                                        <span data-unchecked="غیر فعال" data-checked="فعال"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit">ویرایش
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
