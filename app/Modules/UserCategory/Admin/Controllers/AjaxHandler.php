<?php


namespace App\Modules\UserCategory\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\Users\Admin\Models\Users;
use App\Http\Controllers\CoreCommon;


class AjaxHandler extends Controller
{	


	public function GetModuleName(){
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		return $moduleName;
	}

	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}



	//Just Return Views
	public function GetUsersCategoryList(){
		$limit=15;
		if(empty(Input::get('search')))
		{	
			$countOfAllUserCategory=UserCategory::UserCategoryCount(Input::get('search'));
		}
		else
		{
			$countOfAllUserCategory=UserCategory::UserCategoryCount(Input::get('search'));
		}
		
		if(is_int($countOfAllUserCategory/$limit))
		{
			$pagesCount=($countOfAllUserCategory/$limit);	
		}
		else
		{
			$pagesCount=((int)($countOfAllUserCategory/$limit))+1;
		}
		
		if(!empty(Input::get('pageNumber')))
		{	
			$offset=(Input::get('pageNumber')-1)*$limit;
			$userCategories=UserCategory::SearchUserCategory(Input::get('search'),$offset,$limit);
		}
		else
		{
			$userCategories=UserCategory::SearchUserCategory(Input::get('search'),0,$limit);
		}

		return view($this->GetPreView().'Ajax_UserCategoryList')->with('data',['userCategories'=>$userCategories,'pageInfo'=>['curPage'=>Input::get('pageNumber'),'pagesCount'=>$pagesCount]]);
	}


	//Controllers That Do Somethings
	

	 

}
