<?php


namespace App\Modules\UserCategory\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCategoryRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\Users\Admin\Models\Users;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Http\Controllers\CoreCommon;
class UserCategoryManager extends Controller
{	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}


	//Just Return Views
	public function AddUserCategoryView(){

		$modules=CoreCommon::GetAdminModulesPermissions();

		return view($this->GetPreView().'AddUserCategory')->with('modules',$modules);
	}

	public function UserCategoryView(){
		return view($this->GetPreView().'UserCategory');
	}



	//Controllers That Do Somethings
	public function AddUserCategory(UserCategoryRequest $request)
	{
		$info=[];
		if(!empty(Input::get('modules')))
		{
			foreach(Input::get('modules') as $moduleName)
			{
				if(!empty(Input::get($moduleName)))
				{	
					$temp=[];
					if(!empty(Input::get($moduleName.'_allPerm')))
					{
						$temp['permAll']='yes';
					}
					else
					{
						$temp['permAll']='no';
					}
					$temp['permissions']=Input::get($moduleName);
					$info[$moduleName]=$temp;
				}
			}
		}
		if(!empty($info))
		{
			$permissions=['admin'=>$info];
			if(!empty(Input::get('status')))
			{
				$status=1;
			}else{
				$status=0;
			}
			UserCategory::Add(Input::get('title'),$permissions,$status);
		}
		return redirect(route('UserCategoryListView'));
	}


	public function DeleteUserCategory($id)
	{
		$userCategory = UserCategory::findOrFail($id);
		$userCategory->delete();
		$user = Users::where('user_category_id', $id)->get();
		foreach ($user as $value)
		{
			$value->delete();
			$apps = App::where('userid', $value->id)->get();
			foreach ($apps as $app)
			{
				$app->delete();
				$comments = Comment::where('item_id',$app->id)->get();
				foreach ($comments as $comment)
				{
					$comment->delete();
				}
				
				$vitrineItems = VitrineItem::where('app_id', $app->id)->get();
				foreach ($vitrineItems as $vitrineItem)
				{
					$vitrineItem->delete();
				}
				// remove app in favorite
				$favorites = Favorite::where('item_id', $app->id)->get();
				foreach ($favorites as $favorite)
				{
					$favorite->delete();
				}
				// remove in app_apk
				$appApks = AppApk:: where('app_id', $app->id)->get();
				$appApkUsers = AppApk:: where('user_id', $value->id)->get();
				foreach ($appApkUsers as $apkUser)
				{
					$apkUser->delete();
				}
				foreach ($appApks as $appApk)
				{
					$appApk->delete();
				}
				
				//remove in app_media
				$appMedias = AppMedia::where('app_id', $app->id)->get();
				$appMediaUsers = AppMedia::where('user', $value->id)->get();
				foreach ($appMediaUsers as $appMediaUser)
				{
					$appMediaUser->delete();
				}
				foreach ($appMedias as $appMedia)
				{
					$appMedia->delete();
				}
				
				$wishLists = WishList::where('app_id', $app->id)->get();
				foreach ($wishLists as $wishList)
				{
					$wishList->delete();
				}
				
				// remove u=in app_rate
				$appRates = AppRate::where('app_id',$app->id)->get();
				foreach ($appRates as $appRate)
				{
					$appRate->delete();
				}
			}
			
		}
		return redirect(route('UserCategoryListView'));
	}

	public function EditUserCategoryView($id)
	{	
		$modules=CoreCommon::GetAdminModulesPermissions();
		$userCategory=UserCategory::GetWithId($id);
		if(!empty($userCategory))
		{
			return view($this->GetPreView().'EditUserCategory')->with('data',['userCategory'=>$userCategory,'modules'=> $modules,'id'=>$id]);
		}
		else{
			return redirect('/');
		}

		
	}


	public function EditUserCategory(UserCategoryRequest $request)
	{	
		$info=[];
		if(!empty(Input::get('modules')))
		{
			foreach(Input::get('modules') as $moduleName)
			{
				if(!empty(Input::get($moduleName)))
				{	
					$temp=[];
					if(!empty(Input::get($moduleName.'_allPerm')))
					{
						$temp['permAll']='yes';
					}
					else
					{
						$temp['permAll']='no';
					}
					$temp['permissions']=Input::get($moduleName);
					$info[$moduleName]=$temp;
				}
			}
		}

		if(!empty($info))
		{
			$permissions=['admin'=>$info];
			if(!empty(Input::get('status')))
			{
				$status=1;
			}else{
				$status=0;
			}
			UserCategory::Edit(Input::get('id'),Input::get('title'),$permissions,$status);
		}
		
		return redirect(route('UserCategoryListView'));	

	}

}
