<?php

namespace App\Modules\UserCategory\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserCategory extends Model
{
	protected $table='user_category';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected $fillable = [
		'title', 'permissions', 'status'
	];
	
	public static function GetWithId($id){
		if(UserCategory::where('id',$id)->exists())
		{
			return UserCategory::where('id',$id)->get()[0];
		}
		else
		{
			return false;
		}	
	}

	public static function GetAll()
	{
		return UserCategory::all()->where('status',1);
	}

	public static function Add($title,$permissions,$status)
	{
		if(!empty($title) && !empty($permissions))
		{
			$category=new UserCategory();
			$category->title=$title;
			$category->permissions=json_encode($permissions);
			$category->status=$status;
			$category->save();
		}
	}

	public static function SearchUserCategory($title,$offset,$limit)
	{
		return UserCategory::where('title','like','%'.$title.'%')->offset($offset)->take($limit)->get();
	}

	public static function Remove($id)
	{
		if(UserCategory::where('id',$id)->exists())
		{
			UserCategory::where('id','=',$id)->delete();
		}		
	}

	public static function CheckUserCategoryExists($id)
	{
		if(UserCategory::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function Edit($id,$title,$permissions,$status){

		if(!empty($title) && !empty($permissions) )
		{	
			if(UserCategory::where('id',$id)->exists())
			{	
				$category=UserCategory::where('id',$id)->get()[0];
				$category->title=$title;
				$category->permissions=json_encode($permissions);
				$category->status=$status;
				$category->save();
			}
		}
	}

	public static function UserCategoryCount($searchFiled)
	{
		return UserCategory::where('title','like','%'.$searchFiled.'%')->count();
	}
	
	public static function GetUserCategory()
	{
		return  UserCategory::select('id', 'title')->latest('id')->where('id', '<>', 1)->where('status',1)->get();
	}
	
}