<?php

namespace App\Modules\Search\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\App\Admin\Models\App;

class SearchManager extends Controller
{

    public function GetPreView()
    {
        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        }

        $preView = $moduleName . '_' . $side . '::';


        return $preView;
    }

    public function index($keyWord)
    {
//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");


        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

        $resultSearch = App::where('title','like','%'.$keyWord.'%')
            ->latest('id')
            ->skip(0)
            ->take(30)
            ->get();

        $rowCat = 0;
        foreach ($resultSearch as $item)
        {
            $btw_RateCnt[$rowCat] = AppRate::where('app_id' , $item->id)
                ->count();
            $rowCat++;
        }

        $resultSearch_2 = App::where('title','like','%'.$keyWord.'%')
            ->latest('id')
            ->skip(30)
            ->take(100000000)
            ->get();


        $rowCat_2 = 0;
        foreach ($resultSearch_2 as $item_2)
        {
            $btw_RateCnt_2[$rowCat_2] = AppRate::where('app_id' , $item_2->id)
                ->count();
            $rowCat_2++;
        }

        return view($this->GetPreView() . 'PageView', compact('moduleName','LocalhostPath','resultSearch','resultSearch_2','btw_RateCnt','btw_RateCnt_2'))->with(['keyWord' => $keyWord]);
    }
}