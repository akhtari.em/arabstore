<?php

namespace App\Modules\Vitrine\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use Illuminate\Database\Eloquent\Model;

class Vitrine extends Model
{
	protected $table='vitrine';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected $hidden = [
		'status', 'created_at', 'updated_at', 'vitrine_items'
	];
	protected $fillable = [
		'title','action', 'itemid','type','order','status', 'type_of_show'
	];
	
	protected $appends = [
		'vitrine_items_list'
	];
	
	public static function GetWithId($id)
	{
		if(Vitrine::where('id',$id)->exists())
		{
			return Vitrine::where('id',$id)->get()[0];
		}	
	}

	public static function GetAll()
	{
		return Vitrine::where('status',1)->oldest('title')->pluck('title', 'id');
	}


	public static function Search($title,$offset,$limit)
	{
		$vitrines=Vitrine::where('title','like','%'.$title.'%')->offset($offset)->take($limit)->orderBy('id', 'desc')->get();

		return $vitrines;
	}

	public static function Remove($id)
	{
		if(Vitrine::where('id',$id)->exists())
		{
			$vitrine=Vitrine::where('id','=',$id)->delete();
			
		}		
	}

	public static function CheckExists($id)
	{
		if(Vitrine::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	public static function Count($searchFiled)
	{
		return Vitrine::where('title','like','%'.$searchFiled.'%')->count();
	}
	
	public function vitrineItems()
	{
		return $this->hasMany(VitrineItem::class);
	}
	
	public function getVitrineItemsListAttribute()
	{
		return $this->vitrineItems()->take(5)->get();
//		return $this->vitrineItems;
	}
	
}
