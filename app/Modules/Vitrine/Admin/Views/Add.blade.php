@extends('Master_Admin::MasterPage')

@section('title')
افزودن ویترین
@stop

@section('css')
@stop


@section('content')
    
    @foreach($errors->all() as $error)
        <div class="text-danger">{{$error}}</div>
     @endforeach
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Add')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid"  id="form" method="post" name="form" onsubmit="return addVitrineValidate()"
                          action="{{route('AddVitrine')}}" enctype="multipart/form-data">
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">نام ویترین</label>
                                    <input class="input-style" type="text" id="title" name="title" >
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">اکشن </label>
                                    <input class="input-style" type="text" name="action" >
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Item Id (دستبندی اپ ها)</label>
                                    <select    class="form-control selectpicker wide _itemid" name="itemid" id="itemid">
                                        <option value="-1">انتخاب کنید</option>
                                        @foreach($AppCategory as $index=>$value)
                                            <option value="{{$value->id}}"@if($value->id==old('itemid')) {{' selected'}} @endif>{{$value->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">نوع نمایش</label>
                                    <select  class="form-control selectpicker wide _type" name="type" id="type" data-live-search="true">
                                        <option value="">انتخاب  کنید</option>
                                        <option value="scroll" {{(old('type') == 'scroll')?'selected' : ''}}>scroll</option>
	                                    <option value="slider" {{(old('type') == 'slider')? 'selected': ''}}>slider</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number" id="order" name="order" value="0" dir="ltr" >
                                </div>
                            </div>
                        </div>
	                    <div class="row row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <label class="pull-right">محل نمایش </label>
			                    <select class="form-control" name="type_of_show" id="type_of_show">
				                    <option value="home" {{(old('type_of_show') == 'home')?'selected':''}}>صفحه داخلی اپ
				                    </option>
				                    <option value="game" {{(old('type_of_show') == 'game')?'selected':''}}>بازی
				                    </option>
			                    </select>
		                    </div>
	                    </div>
	                    <div class="row row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <label class="pull-right">وضعیت</label>
			                    <select class="form-control" name="status" id="status">
				                    <option value="1" {{(old('status') == '1')?'selected':''}}>فعال
				                    </option>
				                    <option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال
				                    </option>
			                    </select>
		                    </div>
	                    </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
 
@stop
@section('js')
	<script src="{{asset('Assets/Vitrine/Admin/js/checkVitrineValidate.js')}}"></script>
@stop