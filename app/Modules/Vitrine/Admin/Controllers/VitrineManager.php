<?php


namespace App\Modules\Vitrine\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\VitrineRequest;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\Vitrine\Admin\Models\Vitrine;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;


class VitrineManager extends Controller
{

    public function GetPreView()
    {
        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 6)) {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Site" . $dd), 5)) {
            $side = 'Site';
        }

        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }


    //Just Return Views
    public function AddView()
    {
        $AppCategory = AppCategory::where('status', 1)
            ->latest('id')
            ->get();
        return view($this->GetPreView() . 'Add', compact('AppCategory'));
    }

    public function ListView()
    {

        return view($this->GetPreView() . 'List');
    }

    public function EditView($id)
    {
        $AppCategory = AppCategory::where('status', 1)
            ->latest('id')
            ->get();
        $id = Vitrine::findOrFail($id);
        return view($this->GetPreView() . 'Edit', compact('AppCategory'))->with(['id' => $id]);
    }

    //Controllers That Do Somethings
    public function Add(VitrineRequest $request)
    {
        Vitrine::create($request->all());
        return redirect(route('VitrineListView'));
    }

    public function Delete($id)
    {
        $vitrine = Vitrine::findOrFail($id);
        $vitrine->delete();
        $vitrineItems = VitrineItem::where('vitrine_id', $id)->get();
        foreach ($vitrineItems as $vitrineItem) {
            $vitrineItem->delete();
        }
        return redirect(route('VitrineListView'));
    }

    public function Edit(VitrineRequest $request, $id)
    {
        $vitrine = Vitrine::findOrFail($id);
        $vitrine->update($request->all());
        return redirect(route('VitrineListView'));
    }
}
