<?php
namespace App\Modules\Developer\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\Developer\Developer\Models\Developer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class DeveloperManager extends Controller
{
    use AuthenticatesUsers;

    public function GetPreView()
    {
        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        } else if (substr(strrchr(__DIR__, "Developer" . $dd), 0, 9) == 'Developer') {
            $side = 'Developer';
        }
        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }

    public function index()
    {

        if (Auth::guard('developer')->check() == false) {
            return view($this->GetPreView() . 'LoginView');
        } else {
            return redirect('/Dashboard/Developer/PageView');
//            return view($this->GetPreView() . 'LoginView');
        }

    }

    public function store(Request $request)
    {
        $findUser = Developer::where('email', $request->get('email'))
            ->first();
        if ($findUser) {
            $this->validate($request, [
                'email' => 'required',
                'password' => 'required'
            ]);

            if (Auth::guard('developer')->attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
                Auth::guard('developer')->login($findUser);
                return redirect('/Dashboard/Developer/PageView')->with('DeveloperSuccessLogin', 'توسعه دهنده گرامی (' . $findUser->nickName . '). خوش آمدید.');
            } else {
                return redirect('Developer/Developer/LoginView')->with('error', 'رمز عبور اشتباه می باشد');
            }
        } else {
            return redirect('Developer/Developer/LoginView')->with('error', 'ایمیل وارد شده در سامانه ثبت نشده است');
        }
    }
}