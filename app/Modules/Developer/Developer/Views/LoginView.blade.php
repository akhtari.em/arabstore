<!DOCTYPE html>
<html lang="fa-IR" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Index Page Twannas</title>
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="description" content=" توضیحات در رابطه با صفحه ">
    <meta name="keywords" content=" کلمات کلیدی مرتبط با صفحه ">
    <link href="{{ asset('Assets/Master/Web/images/favicon.png') }}" rel="shortcut icon">

    <link rel="stylesheet" type="text/css" href="{{ asset('Assets/Master/Web/css/bootstrap-css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('Assets/Master/Web/css/bootstrap-css/bootstrap-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('Assets/Master/Web/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('Assets/Master/Web/css/green.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('Assets/Master/Web/css/font-awesome.min.css') }}">
</head>
<body class="single-login">

<!--[if lt IE 9]>
<script src="{{ asset('Assets/Master/Web/js/html5.js') }}"></script>
<script src="{{ asset('Assets/Master/Web/js/css3-mediaqueries.js') }}"></script>
<![endif]-->

<script src="{{ asset('Assets/Master/Web/js/jquery-core.min.js') }}"></script>
<script src="{{ asset('Assets/Master/Web/js/bootstrap-js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Assets/Master/Web/js/select-js/countrySelect.min.js') }}"></script>
<script src="{{ asset('Assets/Master/Web/js/select-js/colorSelect.js') }}"></script>

<script src="{{ asset('Assets/Master/Web/js/custom.js') }}"></script>
<section class="container-fluid full-height-container">

    <div class="row full-height-row">
        <div class="login-wrapp col-xs-12">
            <br>
            @include('errors')
            <div class="row">
                <div class="logo col-xs-8 col-sm-6">
                    <h1 class="logo-img">
                        <a href="#"><img class="img-responsive" src="{{ asset('Assets/Master/Web/images/footer-logo.png') }}" alt="Twannas"
                                         title="Twannas"></a>
                    </h1>
                    <h2 class="logo-slogan">تؤنّس</h2>
                </div>
                <!--.logo-->
                <div class="col-xs-6"></div>
                <div class="clearfix"></div>
                <div class="col-xs-10 col-sm-6 login__form">
                    <form action="{{ route('LoginPost') }}" method="post" class="col-xs-12 col-md-7 col-md-push-2" enctype="multipart/form-data" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="ایمیل" autocomplete="off">
                        </div><!--.form-group-->
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="رمز عبور" autocomplete="off">
                        </div><!--.form-group-->
                        <div class="form-group">
                            <input type="submit" name="login" class="login__submit" value="ارسال">
                        </div><!--.form-group-->
                        <div class="login__or">
                            <hr>
                            <span>یا</span>
                            <hr>
                            <div class="clearfix"></div>
                        </div><!--.login__or-->
                        <div class="login__google">
                            <a href="#">
                                <span>ثبت نام از طریق</span>
                                <span class="fa fa-google"></span>
                            </a>
                        </div><!--.login__google-->
                        <div class="login__agreement">
                            <label class='checkbox-inline'><input type='checkbox' name='visto'
                                                                  id='visto' value=''>
                                <button type="button" data-toggle="modal" data-target="#agreementModal">مطالعه و تایید
                                    قوانین تونس
                                </button>
                            </label>
                        </div><!--.login__agreement-->
                    </form>
                </div><!--.login__form-->
                <div class="clearfix"></div>
                <footer class="login__footer">
                    <div class="login__copyright">
                        Twannas &copy;2018
                    </div>
                    <ul>
                        <li><a href="">درباره تونس</a></li>
                        <li><a href="">حریم خصوصی</a></li>
                        <li><a href="">شرایط خدمات سایت</a></li>
                        <li><span>تغییر زبان : </span>
                            <select class="form-control">
                                <option value="العربی">العربی</option>
                                <option value="ترکی">ترکی</option>
                                <option value="روسی">روسی</option>
                            </select>
                        </li>
                    </ul>
                </footer>
            </div><!--.row-->
        </div><!--.login-wrapp-->
    </div><!--.row-->
</section><!--.container-fluid-->
<div id="agreementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">و مدن سكان بأيدي, جدول معقل ابت</h4>
            </div>
            <div class="modal-body">
                <div class="scrollbar">
                    <div class="force-overflow">
                        <p>و مدن سكان بأيدي, جدول معقل ابتدعها قام كل, هناك بقسوة ٣٠ هذه. عن عملية انتباه وتنصيب أخر, إذ
                            نفس فبعد المبرمة ولاتّساع. فبعد الأحمر يتم ما, يتم التخطيط الإقتصادي ثم. فصل لم الطرفين
                            البشريةً التكاليف, تصرّف التحالف الإتفاقية بـ بال. بـ ذات ألمّ ألمانيا الإحتفاظ, قد وبغطاء
                            والروسية الأوروبي الا, عل ٠٨٠٤ القوى التحالف ولم. دون وفرنسا بالرّغم التكاليف عل. يكن كُلفة
                            تشيكوسلوفاكيا عن. من نهاية العدّ الشهير هذا, بـ إحتار بالولايات أضف, العصبة مقاطعة وتنامت ما
                            به،. كما وقامت الثانية التكاليف أي, أوسع واستمر عدم مع, أن هذه هُزم شعار. حين عن الربيع،
                            ايطاليا،. عالمية الصعداء العالمية لم حتى, لغزو لمحاكم الربيع، مع تحت. بحق لإعلان الجنوب
                            استطاعوا إذ, بعد أي اللازمة الأهداف. أدنى الدول الإنزال أضف ٣٠. المبرمة استعملت أخذ و.</p>
                        <p>و مدن سكان بأيدي, جدول معقل ابتدعها قام كل, هناك بقسوة ٣٠ هذه. عن عملية انتباه وتنصيب أخر, إذ
                            نفس فبعد المبرمة ولاتّساع. فبعد الأحمر يتم ما, يتم التخطيط الإقتصادي ثم. فصل لم الطرفين
                            البشريةً التكاليف, تصرّف التحالف الإتفاقية بـ بال. بـ ذات ألمّ ألمانيا الإحتفاظ, قد وبغطاء
                            والروسية الأوروبي الا, عل ٠٨٠٤ القوى التحالف ولم. دون وفرنسا بالرّغم التكاليف عل. يكن كُلفة
                            تشيكوسلوفاكيا عن. من نهاية العدّ الشهير هذا, بـ إحتار بالولايات أضف, العصبة مقاطعة وتنامت ما
                            به،. كما وقامت الثانية التكاليف أي, أوسع واستمر عدم مع, أن هذه هُزم شعار. حين عن الربيع،
                            ايطاليا،. عالمية الصعداء العالمية لم حتى, لغزو لمحاكم الربيع، مع تحت. بحق لإعلان الجنوب
                            استطاعوا إذ, بعد أي اللازمة الأهداف. أدنى الدول الإنزال أضف ٣٠. المبرمة استعملت أخذ و.</p>
                    </div>
                </div>
            </div>
        </div><!--.modal-content-->

    </div><!--.modal-dialog-->
</div><!--#agreementModal-->
<script>
    $(document).ready(function () {
        $('.login__submit').prop('disabled', true);

        $('#visto').click(function () {
            if ($(this).is(':checked')) {
                $('.login__submit').prop('disabled', false);
            } else {
                $('.login__submit').prop('disabled', true);
            }
        });


        $('#visto').click(function () {
            if ($(this).is(':checked')) {
                $('.login__google').addClass("activeGoogle");
            } else {
                $('.login__google').removeClass("activeGoogle");
            }
        });
    });
</script>
</body>
</html>