<?php

namespace App\Modules\Developer\Developer\Models;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Developer extends Authenticatable
{
    use Notifiable;

    protected $table = 'developer';

    protected $fillable = ['email',  'password'];

    protected $hidden = ['password'];
}