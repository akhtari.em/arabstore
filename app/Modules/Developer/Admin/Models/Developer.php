<?php

namespace App\Modules\Developer\Admin\Models;

use App\Modules\AppCategory\Admin\Models\AppCategory;
use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{	

	

	public static function GetWithId($id)
	{
		if(AppCategory::where('id',$id)->where('remove',0)->exists())
		{
			return AppCategory::where('id',$id)->where('remove',0)->get()[0];
		}
		else
		{
			return false;
		}	
	}

	public static function GetAll()
	{
		return Developer::where('status',1)->oldest('name')->pluck('name', 'id');
	}

	public static function Add($parent,$title,$pic1,$pic2,$order,$status,$userId)
	{
		
		
	}

	public static function Search($title,$offset,$limit)
	{
		$appCategories=AppCategory::where('title','like','%'.$title.'%')->where('remove',0)->offset($offset)->take($limit)->orderBy('id', 'desc')->get();

		foreach($appCategories as $appCategory)
		{	
			if($appCategory['parent'] != 0)
			{
				$title=AppCategory::where('id',$appCategory['parent'])->get(['title'])[0]['title'];
				$appCategory['parentTitle']=$title;
			}
	
			
		}

		return $appCategories;
		
	}

	public static function Remove($id)
	{
		if(AppCategory::where('id',$id)->exists())
		{
			$appCategory=AppCategory::where('id','=',$id)->get()[0];
			$appCategory->remove=time();
			$appCategory->save();
		}		
	}

	public static function CheckExists($id)
	{
		if(AppCategory::where('id',$id)->where('remove',0)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	public static function Edit($id,$title,$parent,$pic1,$pic2,$order,$status)
	{
		if(!empty($title))
		{	
			if(AppCategory::where('id',$id)->exists())
			{	
				$category=AppCategory::where('id',$id)->get()[0];
				$category->title=$title;
				$category->parent=$parent;
				if(!empty($pic1))
				{
					$category->icon=$pic1;
				}
				if(!empty($pic2))
				{
					$category->icon2=$pic2;
				}
				$category->order=$order;
				$category->status=$status;
				$category->save();
			}
		}
	}

	public static function Count($searchFiled)
	{
		return AppCategory::where('title','like','%'.$searchFiled.'%')->where('remove',0)->count();
	}
	
	protected $table='developer';
    public $timestamps=true;
    protected $primaryKey = 'id';
}