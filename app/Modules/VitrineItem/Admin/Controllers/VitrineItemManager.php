<?php


namespace App\Modules\VitrineItem\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\VItrineItemRequest;
use App\Http\Requests\VitrineRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\Vitrine\Admin\Models\Vitrine;
use App\Services\AppServices;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\VitrineItemService;


class VitrineItemManager extends Controller
{
	private $vitrineItemService;
	public function __construct(VitrineItemService $vitrineItemService)
	{
		$this->vitrineItemService = $vitrineItemService;
	}
	
	public function GetPreView()
	{
		$dd = CoreCommon::osD();
		$manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
		$manifest = json_decode($manifestJson, true);
		$moduleName = $manifest['title'];
		if (substr(strrchr(__DIR__, "Admin" . $dd), 6))
		{
			$side = 'Admin';
		}
		else if (substr(strrchr(__DIR__, "Site" . $dd), 5))
		{
			$side = 'Site';
		}
		
		$preView = $moduleName . '_' . $side . '::';
		return $preView;
	}
	
	
	//Just Return Views
	public function AddView()
	{
		$vitrines = Vitrine::GetAll();
		$apps = App::getApp();
		return view($this->GetPreView() . 'Add', compact('apps', 'vitrines'));
	}
	
	public function ListView()
	{
		return view($this->GetPreView() . 'List');
	}
	
	public function EditView($id)
	{
		$id = VitrineItem::findOrFail($id);
		$vitrines = Vitrine::GetAll();
		$apps = App::getApp();
		return view($this->GetPreView() . 'Edit', compact('vitrines', 'apps'))->with(['id' => $id]);
	}
	
	
	//Controllers That Do Somethings
	public function Add(VItrineItemRequest $request)
	{
		$user = Auth::user();
		$request->merge(['user_id' => $user->id]);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->vitrineItemService->uploadVitrine($request->file('avatar'));
			$request->request->add(['media' => $filename]);
		}
		VitrineItem::create($request->all());
		return redirect(route('VitrineItemListView'));
	}
	
	public function Delete($id)
	{
		$vitrineItems = VitrineItem::findOrFail($id);
		$vitrineItems->delete();
		return redirect(route('VitrineItemListView'));
	}
	
	public function Edit(VItrineItemRequest $request, $id)
	{
		$vitrineItem = VitrineItem::findOrFail($id);
		$user = Auth::user();
		$request->merge(['user_id' => $user->id]);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->vitrineItemService->uploadVitrine($request->file('avatar'));
			$request->request->add(['media' => $filename]);
		}
		$vitrineItem->update($request->all());
		return redirect(route('VitrineItemListView'));
	}
}
