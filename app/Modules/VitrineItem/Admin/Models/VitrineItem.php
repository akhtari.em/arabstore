<?php

namespace App\Modules\VitrineItem\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Vitrine\Admin\Models\Vitrine;
use Illuminate\Database\Eloquent\Model;
use App\Services\VitrineItemService;
class VitrineItem extends Model
{
	
	protected $table='vitrine_item';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected  $hidden = [
		'user_id','status', 'created_at', 'updated_at', 'fromdate', 'todate', 'order', 'action', 'media', 'title', 'id', 'app'
	];
	
	protected $fillable = [
		'vitrine_id','title','media','action','app_id',
		'fromdate','todate','order','status','user_id','url', 'type'
	];
	protected $appends = [
		'app_info'
	];

	public static function GetWithId($id){
		if(VitrineItem::where('id',$id)->exists())
		{
			return VitrineItem::where('id',$id)->get()[0];
		}	
	}


	public static function Search($title,$offset,$limit)
	{	
		return VitrineItem::where('vitrine_item.title','like','%'.$title.'%')->join('vitrine', 'vitrine_item.vitrine_id', '=', 'vitrine.id')->offset($offset)->take($limit)->orderBy('vitrine_item.id', 'desc')->get(['vitrine_item.id','vitrine_item.title','vitrine_item.status','vitrine.title as vitrineTitle','vitrine_item.created_at']);

	}

	public static function Remove($id)
	{
		if(VitrineItem::where('id',$id)->exists())
		{
			$vitrine=VitrineItem::where('id','=',$id)->delete();
			
		}		
	}

	public static function CheckExists($id)
	{
		if(VitrineItem::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}	
	}



	public static function Count($searchFiled)
	{
		return VitrineItem::where('vitrine_item.title','like','%'.$searchFiled.'%')->join('vitrine', 'vitrine_item.vitrine_id', '=', 'vitrine.id')->count();

	}
	
	public function vitrine()
	{
		return $this->belongsTo(Vitrine::class);
	}
	
	public function app()
	{
		return $this->belongsTo(App::class);
	}
	
	public function categories()
	{
		return $this->app()->category;
	}
	
	public function getGravatar($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->media)
		{
			return VitrineItemService::getAvatarDisplayPath("/uploads/vitrineItem/$this->media");
		}
	else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->email)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getAppInfoAttribute()
	{
		return $this->app;
//		return array_only($this->app->toArray(), ['title', 'icon_path', 'rateAvarage']);
	}
}
