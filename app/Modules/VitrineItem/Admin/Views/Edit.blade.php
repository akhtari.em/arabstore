@extends('Master_Admin::MasterPage')

@section('title')
ویرایش آیتم ویترین
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Edit')}}</h2>
             </div>
        </div>
   </div>
</div>
<div class="main-content-area container-fluid">
    <div class="row">
	    @include('errors')
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post" name="form" onsubmit="return EditVitrineItemValidate()"
                          action="{{route('EditVitrineItem', ['id' => $id->id])}}" id="form" enctype="multipart/form-data">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">نام آیتم ویترین</label>
                                    <input class="input-style" type="text" id="title" name="title" value="{{$id->title}}" >
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">ویترین</label>
                                    <select class="form-control selectpicker wide _vitrin" name="vitrine_id" id="vitrine_id" data-live-search="true">
                                        @foreach($vitrines as $key=>$index)
                                            <option value=""></option>
                                            <option value="{{$key}}" {{($key == $id->vitrine_id)? 'selected': ''}}>{{$index}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
	                                <div class="col-lg-8" style="width: 100%">
		                                <img src="{{ $id->getGravatar() }}" alt="{{ $id->name }}" style="width:150px;height:150px;">
		                                <input id="avatar" name="avatar" type="file" multiple class="file-loading"
		                                       accept="image/*">
	                                </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">اکشن </label>
                                    <input class="input-style" type="text" name="action" value="{{$id->action}}" >
                                </div>
                            </div>
                        </div>

                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <select class="form-control selectpicker wide _app" name="app_id" id="app_id" data-live-search="true">
	                            @foreach($apps as $key=>$index)
		                            <option value=""></option>
		                            <option value="{{$key}}" {{($key == $id->app_id)? 'selected': ''}}>{{$index}}</option>
	                            @endforeach
	                            </select>
                            </div>
                        </div>
	                    <div class="row frst-row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <div class="inline-form">
				                    <label class="c-label">آدرس سایت  </label>
				                    <input class="input-style" placeholder="http://www.paraxco.com" value="{{$id->url}}" type="text" name="url" id="url" >
			                    </div>
		                    </div>
	                    </div>
	                    <div class="row row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <div class="toggle-setting">
				                    <label class="pull-right">نوع نمایش</label>
				                    <select class="form-control" name="type" id="type">
					                    <option value="url" {{($id->type == 'url')?'selected':''}}>آدرس سایت
					                    </option>
					                    <option value="app" {{($id->type == 'app')?'selected':''}}>اپلیکیشن
					                    </option>
				                    </select>
				                    </label>
			                    </div>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number" name="order" dir="ltr" value="{{$id->order}}" >
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="toggle-setting">
		                                <label class="pull-right">وضعیت</label>
		                                <select class="form-control" name="status" id="status">
			                                <option value="1" {{($id->status == '1')?'selected':''}}>فعال
			                                </option>
			                                <option value="0" {{($id->status == '0')?'selected':''}}>غیرفعال
			                                </option>
		                                </select>
	                                </label>
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit">ویرایش </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
 
@stop
@section('js')
	<script src="{{asset('Assets/VitrineItem/Admin/js/checkVitrineItemValidate.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddVitrineItem') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop