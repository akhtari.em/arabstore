@extends('Master_Admin::MasterPage')

@section('title')
افزودن آیتم ویترین
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop

@section('content')
	<div style="padding-top: 30px">
		@foreach($errors->all() as $error)
			<div class="text-danger">{{$error}}</div>
		@endforeach
	</div>
	
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>{{trans('modules.Add')}}</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post" name="form" onsubmit="return AddVitrineItemValidate()"
                          action="{{route('AddVitrineItem')}}" id="form" enctype="multipart/form-data">
                      {{csrf_field()}}
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">عنوان</label>
                                    <input class="input-style" value="{{old('title')}}" type="text" id="title" name="title" >
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">ویترین</label>
                                    <select class="form-control selectpicker wide _vitrin" name="vitrine_id" id="vitrine_id" data-live-search="true">
                                        @foreach($vitrines as $key=>$index)
		                                    <option value=""></option>
		                                    <option value="{{$key}}" {{$key == old('vitrine_id')? 'selected': ''}}>{{$index}}</option>
                                          @endforeach
	                                      
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
                                    <label>فایل (فایل مجاز jpg)</label>
	                                <div class="col-lg-8" style="width: 100%">
		                                <input id="avatar" name="avatar" type="file" multiple class="file-loading" accept="image/*" >
	                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">اکشن </label>
                                    <input class="input-style" type="text" name="action" >
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <label>دسته بندی اپ</label>
	                            <select class="form-control selectpicker wide _app" name="app_id" id="app_id" data-live-search="true">
		                            @foreach($apps as $key=>$index)
			                            <option value=""></option>
			                            <option value="{{$key}}" {{$key == old('app_id')? 'selected': ''}}>{{$index}}</option>
		                            @endforeach
	
	                            </select>
                            </div>
                        </div>
	                    <div class="row frst-row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <div class="inline-form">
				                    <label class="c-label">آدرس سایت  </label>
				                    <input class="input-style" placeholder="http://www.paraxco.com" value="{{old('url')}}" type="text" name="url" id="url" >
			                    </div>
		                    </div>
	                    </div>
	                    <div class="row row-form">
		                    <div class="col-sm-12 col-md-8 col-lg-6">
			                    <label class="pull-right">نوع نمایش </label>
			                    <select class="form-control" name="type" id="type">
				                    <option value="url" {{(old('type') == 'url')?'selected':''}}>آدرس سایت
				                    </option>
				                    <option value="app" {{(old('type') == 'app')?'selected':''}}>اپلیکیشن
				                    </option>
			                    </select>
			                    </label>
		                    </div>
	                    </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="inline-form">
                                    <label class="c-label">اولویت</label>
                                    <input class="input-style" type="number" id="order" name="order" value="0" dir="ltr" >
                                </div>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
		                            <label class="pull-right">وضعیت</label>
		                            <select class="form-control" name="status" id="status">
			                            <option value="1" {{(old('status') == '1')?'selected':''}}>فعال
			                            </option>
			                            <option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال
			                            </option>
		                            </select>
	                            </label>
                            </div>
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <button class="btn green-bg pull-right" type="submit"><i class="fa fa-plus-square send-i"></i>ساختن </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('js')
	<script src="{{asset('Assets/VitrineItem/Admin/js/checkVitrineItemValidate.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddVitrineItem') }}",
			allowedFileExtensions: ["jpg","png","jpeg"],
			uploadExtraData: function() {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop