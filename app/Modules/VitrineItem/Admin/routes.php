<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\VitrineItem\Admin\Controllers\VitrineItemManager;
use App\Modules\AppCategory\Admin\Controllers\AppCategoryManager;
use App\Http\Controllers\CoreCommon;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	
	//Views(Get) Routes
	Route::get('Add','VitrineItemManager@AddView')->name('AddVitrineItemView');
	Route::get('List','VitrineItemManager@ListView')->name('VitrineItemListView');
	Route::get('Ajax_GetList','AjaxHandler@ListView');
	Route::get('Edit/{id}', 'VitrineItemManager@EditView')->name('EditVitrineItemView');
	// //Create(Post) Routes
	Route::post('Add','VitrineItemManager@Add')->name('AddVitrineItem');
				
	//Update(Put) Routes
	Route::post('Edit/{id}','VitrineItemManager@Edit')->name('EditVitrineItem');
		
	// //Delete(Delete) Routes
	Route::get('Delete/{id}','VitrineItemManager@Delete')->name('VitrineItem.Delete');
});








//These Routes Use When User Not Logged In

