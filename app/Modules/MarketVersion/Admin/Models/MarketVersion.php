<?php

namespace App\Modules\MarketVersion\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Users\Admin\Models\Users;
use App\Services\AppServices;
use App\Services\UserService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class MarketVersion extends Model
{	
	protected  $fillable =[
		'title','version_code','version_name','status',
		'note','file','force_status'
	];
	protected $table='market_version';
	public $timestamps=true;
	protected $hidden = [
		 'updated_at',
	];
	protected  $appends = [
		'file_path'
	];
	public function getFilePathAttribute()
	{
		return AppServices::getAvatarDisplayPath("/uploads/App/" . $this->file);
	}
}
