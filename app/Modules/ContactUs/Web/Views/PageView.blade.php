@extends('Master_Web::IndexPage')
@section('body')
    <section class="wrapper firstBlur">
        <div class="container">
            @include('errors')
        </div>
        <div class="main-title">
            <h1 class="container">ارتباط با ما</h1>
        </div><!--.main-title-->
        <section class="container">
            <div class="wrapper__content row">
                <div class="contact__detail col-xs-12 col-sm-6">
                    {!! $resultContactUs->address !!}
                </div><!--.contact__detail-->
                <div class="contact__form col-xs-12 col-sm-6">
                    <form method="post" name="form" action="{{route('ContactPost')}}" enctype="multipart/form-data" id="form">
                        <div class="form-group">
                            {!! Form::text('name', '', ['class' => 'form-control' ,'id' => 'name' ,'placeholder' => 'Your Name']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::email('email', '', ['class' => 'form-control' ,'id' => 'email' ,'placeholder' => 'Your Email']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::textarea('message', '', ['class' => 'form-control' ,'id' => 'message' ,'placeholder' => 'Your Text']) !!}
                        </div><!--.form-group-->
                        <div class="form-group">
                            {!! Form::submit('ارسال', ['class' => 'form__submit pull-left']) !!}
                        </div><!--.form-group-->
                        <div class="clearfix"></div>
                    </form>

                </div><!--.contact__form-->
                <div class="clearfix"></div>
            </div><!--.wrapper__content-->
        </section>
    </section>
    <div class="contact__map">
        <img class="img-responsive" src="{{ asset('Assets/Master/Web/images/snazzy-image.png') }}" title="" alt="">
    </div><!--.contact__map-->
@stop