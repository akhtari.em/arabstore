<?php
namespace App\Modules\ContactUs\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ContactForm\Admin\Models\ContactForm;
use Illuminate\Http\Request;



class ContactFormManager extends Controller
{
    public function creat()
    {
        return ('/ContactUs/Web/PageView');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required'
        ]);
        ContactForm::create($request->all());
        return redirect('/ContactUs/Web/PageView')->with('success' , 'فرم شما با موفقیت ارسال شد.');
    }
}