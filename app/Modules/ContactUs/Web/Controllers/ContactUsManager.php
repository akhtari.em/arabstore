<?php
namespace App\Modules\ContactUs\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\ContactUs\Admin\Models\ContactUs;

class ContactUsManager extends Controller
{
    public function GetPreView()
    {

        $dd = CoreCommon::osD();

        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");

        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        }

        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }

    public function index()
    {

//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");

        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

        $resultContactUs = ContactUs::where('id', 1)
            ->first();

        return view($this->GetPreView() . 'PageView', compact('moduleName','LocalhostPath','resultContactUs'));
    }
}