<?php

namespace App\Modules\ContactUs\Admin\Models;

use Illuminate\Database\Eloquent\Model;
class ContactUs extends Model
{
	protected $table='contact_us';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected  $fillable = [
		'lat', 'lan', 'address'
	];
	
	public static function Search($title,$offset,$limit)
	{
		return ContactUs::where('contact_us.address','like','%'.$title.'%')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['contact_us.id','contact_us.lat','contact_us.lon','contact_us.address']);
	}

	public static function Count($searchFiled)
	{
		return ContactUs::where('address','like','%'.$searchFiled.'%')->count();
	}
}