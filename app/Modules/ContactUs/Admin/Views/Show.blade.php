@extends('Master_Admin::MasterPage')

@section('title')
مشاهده پیام
@stop

@section('css')
    <link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-3 column">
             <div class="heading-profile">
                  <h2>مشاهده</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget with-padding">
                <div class="wizard-form-h">
                    <form class="form container-fluid" method="post"  name="form"  enctype="multipart/form-data" id="form">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">طول جغرافیایی   </label>
                                    <input class="input-style" type="text" id="lat" name="lat" value="{{$contactUs->lat}}">
                                </div>
                            </div>
                        </div>
                        <div class="row frst-row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
                                <div class="inline-form">
                                    <label class="c-label">عرض جغرافیایی</label>
                                    <input class="input-style" type="text" name="lon" dir="lat" value="{{$contactUs->lon}}">
                                </div>
                            </div>
                        </div>

                        <div class="row row-form">
                            <div class="col-md-12 float-right">
                                <div class="inline-form">
                                    <label><span>الزامی است*</span>پیام :</label>
                                    <div class="form-editor">
                                      <textarea name="message" id="message" class="ckeditor">{{$contactUs->address}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
	                 
                        
                        </div>
                        <div class="row row-form">
                            <div class="col-sm-12 col-md-8 col-lg-6">
	                            <a class="btn green-bg pull-right" href="{{route('ContactUsListView')}}">بازگشت</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

 
</div>
 
@stop
@section('js')
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
@stop