<?php

namespace App\Modules\ClientView\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Users\Admin\Models\Users;
use App\Services\UserService;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ClientView extends Model
{	
	protected  $fillable =[
		'client_id', 'app_id', 'device'
	];
	protected $table='client_view';
	public $timestamps=true;
	protected $hidden = [
		 'updated_at', 'created_at'
	];
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	public function app()
	{
		return $this->belongsTo(App::class, 'app_id');
	}
}
