@extends('Master_Admin::MasterPage')

@section('title')
	افزودن فایل نصب
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
	<link href="{{asset('packages/persiandate/css/jquery.Bootstrap-PersianDateTimePicker.css')}}" rel="stylesheet"/>
@stop
@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
	<div class="main-title-sec">
		<div class="row">
			<div class="col-md-3 column">
				<div class="heading-profile">
					<h2>{{trans('modules.Add')}}</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content-area container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="widget with-padding">
					<div class="wizard-form-h">
						<form class="form container-fluid" method="post" onsubmit="return AddAppApkValidate()"
						      name="form"
						      action="{{route('AddAppApk')}}" enctype="multipart/form-data" id="form">
							{{csrf_field()}}
							<div class="row frst-row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<div class="form-group">
										<label class="form-label">اپلیکیشن<span>الزامی است*</span></label>
										<select class="form-control selectpicker wide _category " name="app_id"
										        id="app_id" data-live-search="true">
											<option value=""></option>
											@foreach($apps as $index=>$value)
												<option value="{{$index}}"{{$index == old('app_id')}}>{{$value}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-md-12 float-right">
									<div class="inline-form">
										<label>ایکون (فایل مجاز jpg)</label>
										<div class="col-lg-8" style="width: 100%">
											<input id="avatar" name="avatar" type="file" multiple
											       class="file-loading" accept="image/*">
										</div>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-md-12 float-right">
									<div class="inline-form">
										<label><span>الزامی است*</span> فایل اپلیکیشن</label>
										<div class="col-lg-8" style="width: 100%">
											<input id="apkfile" name="apkfile" type="file" multiple
											       class="file-loading" accept="apk/*">
										</div>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									
									<div class="form-group">
										<label for="birthday">  تاریخ انتشار<span>الزامی است*</span></label>
										<div class="input-group">
											<input type="text" class="form-control" readonly data-englishnumber="true"
											       name="publishDate"
											       value="{{(old('publishDate'))}}"
											       id="publishDate" data-mddatetimepicker="true" data-trigger="click"
											       data-targetselector="#publishDate" data-groupid="group1"
											       data-enabletimepicker="true"
											       data-placement="bottom">
											<span class="input-group-addon" data-mddatetimepicker="true"
											      data-englishnumber="true"
											      data-trigger="click" data-targetselector="#publishDate"
											      data-groupid="group1"
											      data-enabletimepicker="true" data-placement="bottom"><i
														class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<div class="inline-form">
										<label class="c-label">تعداد دانلود </label>
										<input class="input-style" value="{{old('download')}}" type="number"
										       placeholder="0"
										       id="download" name="download">
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<label class="pull-right">وضعیت</label>
									<select class="form-control" name="status" id="status">
										<option value="1" {{(old('status') == '1')?'selected':''}}>فعال
										</option>
										<option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال
										</option>
									</select>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<button class="btn green-bg pull-right" type="submit"><i
												class="fa fa-plus-square send-i"></i>ساختن
									</button>
								</div>
							
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('js')
	<script src="{{url('packages/persiandate/js/jalaali.js')}}"></script>
	<script src="{{url('packages/persiandate/js/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('Assets/AppApk/Admin/js/checkAppApkValidate.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddAppApk') }}",
			allowedFileExtensions: ["jpg", "png", "jpeg"],
			uploadExtraData: function () {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#apkfile").fileinput({
			uploadUrl: "{{ route('AddAppApk') }}",
			allowedFileExtensions: ["apk"],
			minImageWidth: 50,
			minImageHeight: 50,
			uploadExtraData: function () {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>

@stop