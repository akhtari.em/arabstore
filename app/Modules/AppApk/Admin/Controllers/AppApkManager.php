<?php


namespace App\Modules\AppApk\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppApkRequest;
use App\Http\Requests\AppMediaRequest;
use App\Http\Requests\AppRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Services\CheckFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
use Morilog\Jalali\jDateTime;

class AppApkManager extends Controller
{
	private $appService;
	use CheckFile;
	public function __construct(AppServices $appService)
	{
		$this->appService = $appService;
	}
	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}


	//Just Return Views
	public function AddView()
	{
		$apps= App::getApp();
		return view($this->GetPreView().'Add', compact( 'apps'));
	}

	public function ListView()
	{
		
		return view($this->GetPreView().'List');
	}

	public function EditView($id)
	{
		
		$id=AppApk::findOrFail($id);
		$apps= App::getApp();
		return view($this->GetPreView().'Edit', compact('apps'))->with(['id'=>$id]);
	}

	public function Add(AppApkRequest $request)
	{
		$user = Auth::user();
		$request->merge(['user_id' => $user->id]);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filenameIcon = $this->appService->uploadAvatar($request->file('avatar'));
			$request->request->add(['changeLog' => $filenameIcon]);
		}
		if ($request->hasFile('apkfile') && $request->file('apkfile')->isValid())
		{
			if (!$this->checkUploadFile($request->file('apkfile')))
			{
				return redirect()->back()->with('message', 'فرمت فایل اشتباه می باشد')->withInput();
			}
			
			$filename = $this->appService->uploadVideo($request->file('apkfile'));
			$path = $this->appService->getUploadPath('/uploads/App/') . $filename;
//			$path = str_replace('/', '\\', $path);
			
			// decompile app apk
			$apk = new \ApkParser\Parser($path);
			$manifest = $apk->getManifest();
			
			$request->merge(['versionName' => $manifest->getVersionName()]);
			$request->merge(['versionCode' => $manifest->getVersionCode()]);
			
			$request->merge(['package_name' => $manifest->getPackageName()]);
			$request->merge(['permission' => $manifest->getPermissions()]);
			
			$request->merge(['minSDK' => $manifest->getMinSdk()]);
		}
		
		$request->request->add(['file' => $filename]);
		
		// update package_name in app table
		$findApp = App::where('id', $request->app_id)->first();
		$findApp->package = $manifest->getPackageName();
		$findApp->update($request->all());
		
		// get volume file automatic
		$request->merge(['volume' => $request->file('apkfile')->getClientSize()]);
		$request->merge(['publishDate' => jDateTime::createDatetimeFromFormat('Y/m/d H:i:s', $request['publishDate'])->format("Y-m-d H:i:s")]);
		AppApk::create($request->all());
		return redirect(route('AppApkListView'));
	}

	public function Delete($id)
	{	
		//Delete Selected App Media
		$appApk = AppApk::findOrFail($id);
		$appApk->delete();
		return redirect(route('AppApkListView'));
	}

	public function Edit(AppApkRequest $request, $id)
	{
		$appApk = AppApk::findOrFail($id);
		$user = Auth::user();
		$request->merge(['user_id' => $user->id]);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filenameIcon = $this->appService->uploadAvatar($request->file('avatar'));
			$request->request->add(['changeLog' => $filenameIcon]);
		}
		if ($request->hasFile('apkfile') && $request->file('apkfile')->isValid())
		{
			if (!$this->checkUploadFile($request->file('apkfile')))
			{
				return redirect()->back()->with('message', 'فرمت فایل اشتباه می باشد')->withInput();
			}
			$filename = $this->appService->uploadVideo($request->file('apkfile'));
			$path = $this->appService->getUploadPath('/uploads/App/') . $filename;
//			$path = str_replace('/', '\\', $path);
			
			 // decompile app apk
			$apk = new \ApkParser\Parser($path);
			$manifest = $apk->getManifest();
			$request->merge(['versionName' => $manifest->getVersionName()]);
			$request->merge(['versionCode' => $manifest->getVersionCode()]);
			$request->merge(['package_name' => $manifest->getPackageName()]);
			$request->merge(['permission' => $manifest->getPermissions()]);
			$request->merge(['minSDK' => $manifest->getMinSdk()]);
			$request->request->add(['file' => $filename]);
			
			// update package_name in app table
			$findApp = App::where('id', $request->app_id)->first();
			$findApp->package = $manifest->getPackageName();
			$findApp->update($request->all());
		}
		
		
		// get volume file automatic
		if (!empty($request->file('apkfile')))
		{
			$request->merge(['volume' => $request->file('apkfile')->getClientSize()]);
		}
		$request->merge(['publishDate' => jDateTime::createDatetimeFromFormat('Y/m/d H:i:s', $request['publishDate'])->format("Y-m-d H:i:s")]);
		$appApk->update($request->all());
		return redirect(route('AppApkListView'));
	}
	
	

}
