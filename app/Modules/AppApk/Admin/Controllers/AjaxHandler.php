<?php


namespace App\Modules\AppApk\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\AppApk\Admin\Models\AppApk;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CoreCommon;


class AjaxHandler extends Controller
{	


	public function GetModuleName(){
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		return $moduleName;
	}

	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}

		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}
	
	public function ListView()
	{
		$limit=15;
		$count=AppApk::get()->count();
		
		if(is_int($count/$limit))
		{
			$pagesCount=($count/$limit);
		}
		else
		{
			$pagesCount=((int)($count/$limit))+1;
		}
		
		if(!empty(Input::get('pageNumber')))
		{
			$offset=(Input::get('pageNumber')-1)*$limit;
			$apps=AppApk::Search(Input::get('search'),$offset,$limit);
		}
		else
		{
			$apps=AppApk::Search(Input::get('search'),0,$limit);
		}
		
		return view($this->GetPreView().'Ajax_List', compact('apps', 'show'))->with('data',['pageInfo'=>['curPage'=>Input::get('pageNumber'),'pagesCount'=>$pagesCount]]);
	}

}
