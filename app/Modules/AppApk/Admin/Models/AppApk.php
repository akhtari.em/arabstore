<?php

namespace App\Modules\AppApk\Admin\Models;

use App\Modules\App\Admin\Models\App;
use Illuminate\Database\Eloquent\Model;
use App\Services\AppServices;
class AppApk extends Model
{
	protected $table='app_apk';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected $fillable =[
		'app_id','file','versionName','versionCode','permission',
		'changeLog','minSDK','publishDate','volume','download',
		'user_id','status','package_name'
	];
	
	protected $casts = [
		'permission' => 'array',
		'minSDK' => 'array'
	];
	public static function GetNewestApk($appId)
	{	
		//if  Version code was assignted to apk Max VersionCOde Will Be Choose In other case maxID will be choose
		if(AppApk::where('app_id',$appId)->exists())
		{	
			$maxVersionCode=AppApk::where('app_id',$appId)->where('remove',0)->max('versionCode');
			$maxId=AppApk::where('app_id',$appId)->where('remove',0)->max('id');
			if(!empty($maxVersionCode))
			{
				if(AppApk::where('app_id',$appId)->where('remove',0)->where('versionCode','=',$maxVersionCode)->exists())
				{
					return AppApk::where('app_id',$appId)->where('remove',0)->where('versionCode','=',$maxVersionCode)->get()[0];
				}
			}
			else if(!empty($maxId))
			{
				
				if(AppApk::where('app_id',$appId)->where('remove',0)->where('id','=',$maxId)->exists())
				{
					return AppApk::where('app_id',$appId)->where('remove',0)->where('id','=',$maxId)->get()[0];
				}
			}
			
		}
		 
	}
	
	public function app()
	{
		return $this->belongsTo(App::class, 'app_id');
	}
	
	public function getImage($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->changeLog)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->changeLog");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->versionName)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getFile($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->file)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->file");
		}
	}
	
	public static function Count($searchFiled)
	{
		return AppApk::where('app_id','like','%'.$searchFiled.'%')->count();
	}
	
	public static function Search($title,$offset,$limit)
	{
		$search = App::where('title', 'like','%'.$title.'%')->pluck('id')->toArray();
		return AppApk::whereIn('app_apk.app_id',$search)->join('app','app_apk.app_id','=','app.id')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['app_apk.id','app_apk.app_id','app_apk.versionCode', 'app_apk.versionName','app_apk.created_at','app_apk.status']);
	}
	
	public static function Remove($id)
	{
		if(AppApk::where('id',$id)->exists())
		{
			AppApk::where('id','=',$id)->delete();
		}
	}
	
	public static function CheckExists($id)
	{
		if(AppApk::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}