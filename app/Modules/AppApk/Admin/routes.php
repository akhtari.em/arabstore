<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\UserCategory\Admin\Models\UserCategory;
use App\Modules\UserCategory\Admin\Controllers\UserCategoryManager;
use App\Http\Controllers\CoreCommon;
use App\Modules\App\Admin\Controllers\AppManager;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	

	//Views(Get) Routes
	Route::get('Add','AppApkManager@AddView')->name('AddAppApkView');
	Route::get('List','AppApkManager@ListView')->name('AppApkListView');
	Route::get('Ajax_GetList','AjaxHandler@ListView');
	
	Route::get('Edit/{id}', 'AppApkManager@EditView')->name('EditAppApkView');
	//Create(Post) Routes
	Route::post('Add','AppApkManager@Add')->name('AddAppApk');
	Route::post('Edit/{id}','AppApkManager@Edit')->name('EditAppApk');

	Route::get('Delete/{id}','AppApkManager@Delete')->name('AppApk.Delete');

});


//These Routes Use When User Not Logged In

