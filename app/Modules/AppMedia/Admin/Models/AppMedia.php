<?php

namespace App\Modules\AppMedia\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Users\Admin\Models\Users;
use Illuminate\Database\Eloquent\Model;
use App\Services\AppServices;
class AppMedia extends Model
{
    protected  $fillable =[
	    'app_id', 'file', 'type',
	    'videoImage', 'url', 'user',
	    'status', 'isImportant'
    ];

	protected $table = 'app_media';
	
	public function app()
	{
		return $this->belongsTo(App::class);
	}
	
	public function user()
	{
		return $this->belongsTo(Users::class);
	}
	
	public static function Count($searchFiled)
	{
		return AppMedia::where('app_id','like','%'.$searchFiled.'%')->count();
	}
	
	public static function Search($title,$offset,$limit)
	{
		$search = App::where('title', 'like','%'.$title.'%')->pluck('id')->toArray();
		return AppMedia::whereIn('app_media.app_id',$search)->join('app','app_media.app_id','=','app.id')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['app_media.id','app_media.app_id','app_media.type','app_media.created_at','app_media.status']);
	}
	
	public function getImage($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->videoImage)
		{
			$images = [];
			$temps = explode('|',$this->videoImage);
			foreach ($temps as $temp)
			{
				$images[] = AppServices::getAvatarDisplayPath("/uploads/App/$temp");
			}
			return $images;
		}
		else
			{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->title)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function getVideo($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->file)
		{
			return AppServices::getAvatarDisplayPath("/uploads/App/$this->file");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->title)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public static function Remove($id)
	{
		if(AppMedia::where('id',$id)->exists())
		{
			AppMedia::where('id','=',$id)->delete();
		}
	}
	
	public static function CheckExists($id)
	{
		if(AppMedia::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
