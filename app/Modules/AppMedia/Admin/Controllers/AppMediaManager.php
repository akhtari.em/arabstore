<?php


namespace App\Modules\AppMedia\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppMediaRequest;
use App\Http\Requests\AppRequest;
use App\Modules\App\Admin\Models\App;
use App\Services\CheckFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;

class AppMediaManager extends Controller
{
	private $appService;
	use CheckFile;
	
	public function __construct(AppServices $appService)
	{
		$this->appService = $appService;
	}
	
	public function GetPreView()
	{
		$dd = CoreCommon::osD();
		$manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
		$manifest = json_decode($manifestJson, true);
		$moduleName = $manifest['title'];
		if (substr(strrchr(__DIR__, "Admin" . $dd), 6))
		{
			$side = 'Admin';
		}
		else if (substr(strrchr(__DIR__, "Site" . $dd), 5))
		{
			$side = 'Site';
		}
		
		$preView = $moduleName . '_' . $side . '::';
		return $preView;
	}
	
	
	//Just Return Views
	public function AddView()
	{
		$apps = App::getApp();
		return view($this->GetPreView() . 'Add', compact('apps'));
	}
	
	public function ListView()
	{
		
		return view($this->GetPreView() . 'List');
	}
	
	public function EditView($id)
	{
		$id = AppMedia::findOrFail($id);
		$apps = App::getApp();
		return view($this->GetPreView() . 'Edit', compact('apps'))->with(['id' => $id]);
	}
	
	public function Add(AppMediaRequest $request)
	{
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		
		// array media input
		$temp = [];
		if ($request->hasFile('avatar'))
		{
			foreach ($request->avatar as $key)
			{
				if (!$this->checkMediaFile($key))
				{
					return redirect()->back()->with('message', 'فرمت فایل اشتباه می باشد')->withInput();
				}
				$temp[] = $this->appService->uploadVideo($key);
			}
			
			$request->request->add(['videoImage' => implode('|', $temp)]);
		}
		if ($request->hasFile('appvideo') && $request->file('appvideo')->isValid())
		{
			$filename = $this->appService->uploadVideo($request->file('appvideo'));
			$request->request->add(['file' => $filename]);
		}
		AppMedia::create($request->all());
		return redirect(route('AppMediaListView'));
	}
	
	public function Delete($id)
	{
		//Delete Selected App Media
		$appMedia = AppMedia::findOrFail($id);
		$appMedia->delete();
		return redirect(route('AppMediaListView'));
	}
	
	public function Edit(AppMediaRequest $request, $id)
	{
		$appMedia = AppMedia::findOrFail($id);
		$user = Auth::user();
		$request->merge(['user' => $user->id]);
		
		// array media input
		$temp = [];
		if ($request->hasFile('avatar'))
		{
			foreach ($request->avatar as $key)
			{
				if (!$this->checkMediaFile($key))
				{
					return redirect()->back()->with('message', 'فرمت فایل اشتباه می باشد')->withInput();
				}
				$temp[] = $this->appService->uploadVideo($key);
			}
			
			$request->request->add(['videoImage' => implode('|', $temp)]);
		}
		
		if ($request->hasFile('appvideo') && $request->file('appvideo')->isValid())
		{
			$filename = $this->appService->uploadVideo($request->file('appvideo'));
			$request->request->add(['file' => $filename]);
		}
		
		$appMedia->update($request->all());
		return redirect(route('AppMediaListView'));
	}
	
	
}
