@extends('Master_Admin::MasterPage')

@section('title')
	ویرایش مدیا
@stop

@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
	<div class="main-title-sec">
		<div class="row">
			<div class="col-md-3 column">
				<div class="heading-profile">
					<h2>{{trans('modules.Add')}}</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="main-content-area container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="widget with-padding">
					<div class="wizard-form-h">
						<form class="form container-fluid" method="post" onsubmit="return checkEditAppMedia()"
						      name="form" action="{{route('EditAppMedia', ['id' => $id->id])}}"
						      enctype="multipart/form-data" id="form">
							{{csrf_field()}}
							<div class="row frst-row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<div class="form-group">
										<label class="form-label">اپلیکیشن<span>الزامی است*</span></label>
										<select required class="form-control selectpicker wide _category required"
										        name="app_id" id="app_id" data-live-search="true">
											<option value=""></option>
											@foreach($apps as $index=>$value)
												<option value="{{$index}}" {{($index == $id->app_id)? 'selected': ''}}>{{$value}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-md-12 float-right">
									<div class="inline-form">
										<label><span>الزامی است*</span>تصویر فایل (فایل مجاز jpg)</label>
										<div class="col-lg-8" style="width: 100%">
											@foreach($id->getImage() as $value)
												
												@if(substr($value,strpos($value, '.')) == '.mp4')
													<video style="width:200px;height:200px;" src="{{$value}}"
													       controls></video>
												@else
													<img src="{{ $value }}" style="width:150px;height:150px;">
												
												@endif
											@endforeach
											<input  id="avatar" name="avatar[]" type="file" multiple
											       class="file-loading" accept="image/*">
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
							
							<div class="row row-form">
								<div class="col-md-12 float-right">
									<div class="inline-form">
										<label><span>الزامی است*</span> ویدئو (فایل مجاز mp4,mkv)</label>
										<div class="col-lg-8" style="width: 100%">
											<video style="width:200px;height:200px;" src="{{$id->getVideo()}}"
											       controls></video>
											<input id="appvideo" name="appvideo" type="file" multiple
											       class="file-loading" accept="video/*">
										</div>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<label class="pull-right">وضعیت</label>
									<select class="form-control" name="type" id="type">
										<option value="video" {{($id->type == 'video')?'selected':''}}>ویدئو</option>
										<option value="image" {{($id->type == 'image')?'selected':''}}>عکس
										</option>
									</select>
								</div>
							</div>
							
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<label class="pull-right">وضعیت</label>
									<select required class="form-control" name="status" id="status">
										<option value="1" {{($id->status == '1')?'selected':''}}>فعال
										</option>
										<option value="0" {{($id->status == '0')?'selected':''}}>غیرفعال
										</option>
									</select>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<label class="pull-right">نمایش داده شود</label>
									<select class="form-control" name="isImportant" id="isImportant">
										<option value="0" {{($id->isImportant == '0')?'selected':''}}>خیر
										</option>
										<option value="1" {{($id->isImportant == '1')?'selected':''}}>بلی
										</option>
									</select>
								</div>
							</div>
							<div class="row frst-row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<label class="pull-right">لینک </label>
									<div class="col-md-12">
										<div class="inline-form">
											<input class="input-style" value="{{old('url')}}" type="text" id="url"
											       placeholder="http://www.paraxco.com" name="url">
										</div>
									</div>
								</div>
							</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-8 col-lg-6">
									<button class="btn green-bg pull-right" type="submit"><i
												class="fa fa-plus-square send-i"></i>ساختن
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('js')
	<script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
	<script src="{{asset('Assets/AppMedia/Admin/js/Add.js')}}"></script>
	{{--<script src="{{asset('Assets/AppMedia/Admin/js/checkAppMediaValidate.js')}}"></script>--}}
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddAppMedia') }}",
			allowedFileExtensions: ["jpg", "png", "jpeg"],
			uploadExtraData: function () {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
	<script>
		$("#appvideo").fileinput({
			uploadUrl: "{{ route('AddAppMedia') }}",
			allowedFileExtensions: ["mp4"],
			minImageWidth: 50,
			minImageHeight: 50,
			uploadExtraData: function () {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>

@stop