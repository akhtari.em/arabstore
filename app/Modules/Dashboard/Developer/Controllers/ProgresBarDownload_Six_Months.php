<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarDownload_Six_Months extends Controller
{
    public function progresDownload_Six_Months(Request $request)
    {
        $progressColorDlSix_Months = $request->get('progressColorDlSix_Months');
        $results_download_Six_Months = $request->get('results_download_Six_Months');
        $outputProgressDlSix_Months = '<div class="pie_progress__Download_Six_Months" role="progressbar" data-goal="'.$results_download_Six_Months.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorDlSix_Months.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>۶ ماه گذشته</p></span><script type="text/javascript">$(".pie_progress__Download_Six_Months").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Download_Six_Months").asPieProgress("start");</script>';
        echo $outputProgressDlSix_Months;
    }
}
