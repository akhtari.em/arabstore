<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AppServices;
use DB;
use Illuminate\Http\Request;

class Chart extends Controller {
    public function appDl(Request $request) {

        $date = date("Y-m-d");
        $twelve_month_before = date('Y-m-d H:i:s', strtotime($date . ' -12 month'));
        $eleven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -11 month'));
        $ten_month_before = date('Y-m-d H:i:s', strtotime($date . ' -10 month'));
        $nine_month_before = date('Y-m-d H:i:s', strtotime($date . ' -9 month'));
        $eight_month_before = date('Y-m-d H:i:s', strtotime($date . ' -8 month'));
        $seven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -7 month'));
        $six_month_before = date('Y-m-d H:i:s', strtotime($date . ' -6 month'));
        $five_month_before = date('Y-m-d H:i:s', strtotime($date . ' -5 month'));
        $four_month_before = date('Y-m-d H:i:s', strtotime($date . ' -4 month'));
        $three_month_before = date('Y-m-d H:i:s', strtotime($date . ' -3 month'));
        $tow_month_before = date('Y-m-d H:i:s', strtotime($date . ' -2 month'));
        $one_month_before = date('Y-m-d H:i:s', strtotime($date . ' -1 month'));
        $Now = date('Y-m-d H:i:s', strtotime($date));

        $DevDllId = $request->get('DevDllId');

        $appDllId = $request->get('appDllId');

        $AppId = DB::table('app')
            ->where('developer', $DevDllId)
            ->get();

        $AppName = DB::table('app')
            ->where('id', $appDllId)
            ->first();

        foreach ($AppId as $item) {
            $id[] = $item->id;
        }
            $listId = implode(',' , $id);

        $countDl_one_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$one_month_before, $Now])
            ->count();

        $sumDl_one_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$one_month_before, $Now])
            ->count();



        $countDl_tow_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$tow_month_before, $one_month_before])
            ->count();

        $sumDl_tow_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$tow_month_before, $one_month_before])
            ->count();

        $countDl_three_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$three_month_before, $tow_month_before])
            ->count();
        $sumDl_three_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$three_month_before, $tow_month_before])
            ->count();

        $countDl_four_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$four_month_before, $three_month_before])
            ->count();
        $sumDl_four_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$four_month_before, $three_month_before])
            ->count();

        $countDl_five_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$five_month_before, $four_month_before])
            ->count();
        $sumDl_five_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$five_month_before, $four_month_before])
            ->count();

        $countDl_six_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$six_month_before, $five_month_before])
            ->count();
        $sumDl_six_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$six_month_before, $five_month_before])
            ->count();

        $countDl_seven_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$seven_month_before, $six_month_before])
            ->count();
        $sumDl_seven_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$seven_month_before, $six_month_before])
            ->count();

        $countDl_eight_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
            ->count();
        $sumDl_eight_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
            ->count();

        $countDl_nine_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
            ->count();
        $sumDl_nine_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
            ->count();

        $countDl_ten_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
            ->count();
        $sumDl_ten_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
            ->count();

        $countDl_eleven_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
            ->count();
        $sumDl_eleven_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
            ->count();

        $countDl_twelve_month = DB::table('client_download')
            ->where('app_id', $appDllId)
            ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
            ->count();
        $sumDl_twelve_month = DB::table('client_download')
            ->whereIn('app_id', explode(',', $listId))
            ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
            ->count();

        if ($appDllId == 0) {
            $outputChart = '<script type="text/javascript">$(document).ready(function () {var chart = new CanvasJS.Chart("chartDownload", {theme: "light2",animationEnabled: true,title: {text: ""},axisX: {interval: 1,intervalType: "month",valueFormatString: "MMM YYYY"},axisY: {title: "تعداد دانلود ها",fontFamily: "Al-Jazeera-Arabic-Bold-font",includeZero: false,prefix: "",lineThickness: 1,tickThickness: 1,interval: 1,valueFormatString: "#0"},data: [{type: "column",markerSize: 12,xValueFormatString: "MMM, YYYY",yValueFormatString: "###.#",dataPoints: [{ x: new Date('.date('Y, m',strtotime( $date . ' -12 month' )).'), y: '.$sumDl_twelve_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -11 month' )).'), y: '.$sumDl_eleven_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -10 month' )).'), y: '.$sumDl_ten_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -9 month' )).'), y: '.$sumDl_nine_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -8 month' )).'), y: '.$sumDl_eight_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -7 month' )).'), y: '.$sumDl_seven_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -6 month' )).'), y: '.$sumDl_six_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -5 month' )).'), y: '.$sumDl_five_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -4 month' )).'), y: '.$sumDl_four_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -3 month' )).'), y: '.$sumDl_three_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -2 month' )).'), y: '.$sumDl_tow_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -1 month' )).'), y: '.$sumDl_one_month.' }]}]});chart.render();$(".DlCartTitle").text("تمامی اپلیکیشن               ها");});</script><div id="chartDownload" style="height: 370px; width: 100%;"></div>';
        } else {
            $outputChart = '<script type="text/javascript">$(document).ready(function () {var chart = new CanvasJS.Chart("chartDownload", {theme: "light2",animationEnabled: true,title: {text: "",fontSize: 30},axisX: {interval: 1,intervalType: "month",valueFormatString: "MMM YYYY"},axisY: {title: "تعداد دانلود ها",fontFamily: "Al-Jazeera-Arabic-Bold-font",includeZero: false,prefix: "",lineThickness: 1,tickThickness: 1,interval: 1,valueFormatString: "#0"},data: [{type: "column",markerSize: 12,xValueFormatString: "MMM, YYYY",yValueFormatString: "###.#",dataPoints: [{ x: new Date('.date('Y, m',strtotime( $date . ' -12 month' )).'), y: '.$countDl_twelve_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -11 month' )).'), y: '.$countDl_eleven_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -10 month' )).'), y: '.$countDl_ten_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -9 month' )).'), y: '.$countDl_nine_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -8 month' )).'), y: '.$countDl_eight_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -7 month' )).'), y: '.$countDl_seven_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -6 month' )).'), y: '.$countDl_six_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -5 month' )).'), y: '.$countDl_five_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -4 month' )).'), y: '.$countDl_four_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -3 month' )).'), y: '.$countDl_three_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -2 month' )).'), y: '.$countDl_tow_month.' },{ x: new Date('.date('Y, m',strtotime( $date . ' -1 month' )).'), y: '.$countDl_one_month.' }]}]});chart.render();$(".DlCartTitle").text("'.$AppName->title.'");});</script><div id="chartDownload" style="height: 370px; width: 100%;"></div>';
        }



        echo $outputChart;
        
    }
}