<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AppServices;
use DB;
use Illuminate\Http\Request;

class AppInfoUpdate extends Controller
{
    private $appFileUpload;

    public function __construct(AppServices $appFileUpload)
    {
        $this->appFileUpload = $appFileUpload;
    }

    public function appUpdate(Request $request)
    {
        $pathIcon = $this->appFileUpload->getUploadPath('/uploads/App/');
        $pathimage = $this->appFileUpload->getUploadPath('/uploads/App/');
        $pathVideo = $this->appFileUpload->getUploadPath('/uploads/App/');

        if ($_FILES['icon']) {
            $imgIcon = $_FILES['icon']['name'];
            $tmpIcon = $_FILES['icon']['tmp_name'];

            $imgimage = $_FILES['image']['name'];
            $tmpimage = $_FILES['image']['tmp_name'];



            $imgVideo = $_FILES['video']['name'];
            $tmpVideo = $_FILES['video']['tmp_name'];

            $extIcon = strtolower(pathinfo($imgIcon, PATHINFO_EXTENSION));
            $extVideo = strtolower(pathinfo($imgVideo, PATHINFO_EXTENSION));

            $final_imgIcon = rand(1000, 1000000) . $imgIcon;
            $pathIcon = $pathIcon . strtolower($final_imgIcon);

            if ($imgimage) {
                $final_imgimage = rand(1000, 1000000) . $imgimage;
                $pathimage = $pathimage . strtolower($final_imgimage);
            } else {
                $final_imgimage = '';
            }
            if ($imgVideo) {
                $final_imgVideo = rand(1000, 1000000) . $imgVideo;
                $pathVideo = $pathVideo . strtolower($final_imgVideo);
            } else {
                $final_imgVideo = '';
            }
            if ($request->file('video') && $request->file('image')) {
                $baseFromImage = $_POST["imageCrop"];
                $base_to_Image = explode(',', $baseFromImage);
                $dataImage = base64_decode($base_to_Image[1]);
                file_put_contents($pathimage, $dataImage);
                move_uploaded_file($tmpVideo, $pathVideo);
            } elseif ($request->file('video')) {
                move_uploaded_file($tmpVideo, $pathVideo);
            } elseif ($request->file('image')) {
                $baseFromImage = $_POST["imageCrop"];
                $base_to_Image = explode(',', $baseFromImage);
                $dataImage = base64_decode($base_to_Image[1]);
                file_put_contents($pathimage, $dataImage);
            }
            if (move_uploaded_file($tmpIcon, $pathIcon)) {
                // echo "<img src='$pathIcon' />";
                $appID = $request->get('appID');
                $developerID = $request->get('developerID');
                $title = $request->get('title');
                $made = $request->get('made');
                $age = $request->get('age');
                $sex = $request->get('sex');
                $detail = $request->get('detail');

                DB::table('app')
                    ->where('id', $appID)
                    ->where('developer', $developerID)
                    ->update(['title' => $title,
                        'made' => $made,
                        'age' => $age,
                        'sex' => $sex,
                        'detail' => $detail,
                        'icon' => $final_imgIcon,
                        'image' => $final_imgimage,
                        'video' => $final_imgVideo,
                        'status' => 0,
                    ]);
            }
        }
    }

    public function appCancel(Request $request)
    {
        $appID = $request->get('appID');
        $developerID = $request->get('developerID');
        DB::table('app')
            ->where('id', $appID)
            ->where('developer', $developerID)
            ->delete();

        DB::table('app_apk')
            ->where('app_id', $appID)
            ->delete();
    }

}
