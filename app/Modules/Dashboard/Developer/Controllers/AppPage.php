<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AppServices;
use App\Http\Controllers\CoreCommon;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use DB;
use Illuminate\Http\Request;

class AppPage extends Controller
{
    private $appFileUpload;

    public function __construct(AppServices $appFileUpload)
    {
        $this->appFileUpload = $appFileUpload;
    }

    public function appEdit(Request $request)
    {
        $appID = $request->get('appID');
        $title = $request->get('title');
        $made = $request->get('made');
        $age = $request->get('age');
        $sex = $request->get('sex');
        $detail = $request->get('detail');
        DB::table('app')
            ->where('id', $appID)
            ->update(['title' => $title,
                'made' => $made,
                'age' => $age,
                'sex' => $sex,
                'detail' => $detail,
                'status' => 0,
            ]);
    }

    public function appIconEdit(Request $request)
    {

        $appIdIcon = $request->get('appIdIcon');

        $imgIcon = $_FILES['icon']['name'];
        $tmpIcon = $_FILES['icon']['tmp_name'];

        $pathIcon = $this->appFileUpload->getUploadPath('/uploads/App/');
        $extIcon = strtolower(pathinfo($imgIcon, PATHINFO_EXTENSION));
        if ($imgIcon) {
            $final_imgIcon = rand(1000, 1000000) . $imgIcon;
            $pathIcon = $pathIcon . strtolower($final_imgIcon);
        } else {
            $final_imgIcon = '';
        }

        move_uploaded_file($tmpIcon, $pathIcon);
        DB::table('app')
            ->where('id', $appIdIcon)
            ->update(['icon' => $final_imgIcon]);

        $LocalhostPath = CoreCommon::getLocalhostPath("");

        $script = '<script>$(".icon_img").attr("src", "' . $LocalhostPath . '/uploads/App/' . $final_imgIcon . '");notif({msg: "تصوير آيکن با موفقيت آپلود شد.پس از تاييد تغييرات از طرف اوپراتور مربوطه اپليکيشن شما در سامانه قرار ميگيرد.",type: "success",width : 400,height : 60,position : "right",autohide : true,opacity : 1,multiline: 0,fade: 0,bgcolor: "",color: "",timeout: 5000,zindex: null,offset: 0,callback: null,clickable: false,animation: "slide"});</script>';
        return $script;


    }

    public function appMedia(Request $request)
    {

        $appID = $request->get('appIDMedia');



        $developerID = $request->get('developerIDMedia');
        $LocalhostPath = CoreCommon::getLocalhostPath("");
        $videoName = $request->get('videoName');
        $imageName = $request->get('imageName');
        $imgimage = $_FILES['imageSr']['name'];
        $tmpimage = $_FILES['imageSr']['tmp_name'];
        $pathimage = $this->appFileUpload->getUploadPath('/uploads/App/');


        if ($imgimage) {
            $final_imgimage = rand(1000, 1000000) . $imgimage;
            $pathimage = $pathimage . strtolower($final_imgimage);
        } else {
            $final_imgimage = $imageName;
        }
        $imgVideo = $_FILES['video']['name'];
        $tmpVideo = $_FILES['video']['tmp_name'];
        $pathVideo = $this->appFileUpload->getUploadPath('/uploads/App/');
        if ($imgVideo) {
            $final_imgVideo = rand(1000, 1000000) . $imgVideo;
            $pathVideo = $pathVideo . strtolower($final_imgVideo);
        } else {
            $final_imgVideo = $videoName;
        }

        $extVideo = strtolower(pathinfo($imgVideo, PATHINFO_EXTENSION));
        if ($request->file('video') && $request->file('imageSr')) {
            $baseFromImage = $_POST["imageCrop"];
            $base_to_Image = explode(',', $baseFromImage);
            $dataImage = base64_decode($base_to_Image[1]);
            file_put_contents($pathimage, $dataImage);
            move_uploaded_file($tmpVideo, $pathVideo);
        } elseif ($request->file('video')) {
            move_uploaded_file($tmpVideo, $pathVideo);
        } elseif ($request->file('imageSr')) {
            $baseFromImage = $_POST["imageCrop"];
            $base_to_Image = explode(',', $baseFromImage);
            $dataImage = base64_decode($base_to_Image[1]);
            file_put_contents($pathimage, $dataImage);
        }

        DB::table('app')
            ->where('id', $appID)
            ->where('developer', $developerID)
            ->update([
                'image' => $final_imgimage,
                'video' => $final_imgVideo,
                'status' => 0,
            ]);
        $Ps = $request->get('Ps');
        if ($_FILES['imageMedia']['name']['1']) {
            $row = 1;
            foreach ($Ps as $item) {
                $imgimageMedia = $_FILES['imageMedia']['name'][$row];
                $tmpimageMedia = $_FILES['imageMedia']['tmp_name'][$row];

                $final_imgimageMedia = rand(1000, 1000000) . $imgimageMedia;
                $pathimage = $this->appFileUpload->getUploadPath('/uploads/App/ScreenShot/') . strtolower($final_imgimageMedia);


                $baseFromImageMedia = $_POST["croppedImgMedia"][$row];
                $base_to_ImageMedia = explode(',', $baseFromImageMedia);
                $dataImageMedia = base64_decode($base_to_ImageMedia[1]);
                file_put_contents($pathimage, $dataImageMedia);

                AppMedia::create([
                    'app_id' => $appID,
                    'type' => 'image',
                    'videoImage' => $final_imgimageMedia,
                    'user' => $developerID,
                    'status' => 1,
                    'isImportant' => 0,
                ]);
                $row++;
            }
        }


    }

}