<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarDownload_Three_Months extends Controller
{
    public function progresDownload_Three_Months(Request $request)
    {
        $progressColorDlThree_Months = $request->get('progressColorDlThree_Months');
        $results_download_Three_Months = $request->get('results_download_Three_Months');
        $outputProgressDlThree_Months = '<div class="pie_progress__Download_Three_Months" role="progressbar" data-goal="'.$results_download_Three_Months.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorDlThree_Months.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>۳ ماه گذشته</p></span><script type="text/javascript">$(".pie_progress__Download_Three_Months").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Download_Three_Months").asPieProgress("start");</script>';
        echo $outputProgressDlThree_Months;
    }
}
