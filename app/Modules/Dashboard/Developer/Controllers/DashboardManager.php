<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\ClientDownload\Admin\Models\ClientDownload;
use App\Modules\ClientView\Admin\Models\ClientView;
use App\Modules\Developer\Admin\Models\Developer;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Users\Admin\Models\Users;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use Illuminate\Http\Request;
use App\Modules\AppApk\Admin\Models\AppApk;
use DB;
use URL;
use Illuminate\Support\Facades\Auth;

class DashboardManager extends Controller
{

    public function GetPreView()
    {
        $dd = CoreCommon::osD();

        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        } else if (substr(strrchr(__DIR__, "Developer" . $dd), 0, 9) == 'Developer') {
            $side = 'Developer';
        }

        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }


    public function appindex($id)
    {
        //        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");


        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

        $url = URL::current();
        $explodedUrl = explode('/', $url);
        $page = count($explodedUrl);
        $pageName = $explodedUrl[$page - 2];




        $resultApp = App::where('id', $id)
            ->first();
        $resultComment = Comment::where('item_id', $id)
            ->get();

        $userForComment = [];
        $Commentrow = 0;
        foreach ($resultComment as $item) {
            $userForComment[$Commentrow] = Users::Where('id', $item->item_id)
                ->first();

            $clients[$Commentrow] = DB::table('client')
                ->where('id', $item->client_id)
                ->first();
            $apps[$Commentrow] = DB::table('app')
                ->where('id', $item->item_id)
                ->first();
            $Commentrow++;
        }

        $appCategory = App::where('category', $resultApp->category)
            ->take(6)
            ->get();


        $appCategoryName = AppCategory::where('id', $resultApp->category)
            ->first();

        $date = date("Y-m-d");

        // $twelve_month_before = date( 'Y, m', strtotime( $date . ' -12 month' ) );
        $twelve_month_before = date('Y-m-d H:i:s', strtotime($date . ' -12 month'));
        $eleven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -11 month'));
        $ten_month_before = date('Y-m-d H:i:s', strtotime($date . ' -10 month'));
        $nine_month_before = date('Y-m-d H:i:s', strtotime($date . ' -9 month'));
        $eight_month_before = date('Y-m-d H:i:s', strtotime($date . ' -8 month'));
        $seven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -7 month'));
        $six_month_before = date('Y-m-d H:i:s', strtotime($date . ' -6 month'));
        $five_month_before = date('Y-m-d H:i:s', strtotime($date . ' -5 month'));
        $four_month_before = date('Y-m-d H:i:s', strtotime($date . ' -4 month'));
        $three_month_before = date('Y-m-d H:i:s', strtotime($date . ' -3 month'));
        $tow_month_before = date('Y-m-d H:i:s', strtotime($date . ' -2 month'));
        $one_month_before = date('Y-m-d H:i:s', strtotime($date . ' -1 month'));
        $Now = date('Y-m-d H:i:s', strtotime($date));


        $count_one_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$one_month_before, $Now])
            ->count();
        $countDl_one_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$one_month_before, $Now])
            ->count();
        $count_tow_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$tow_month_before, $one_month_before])
            ->count();
        $countDl_tow_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$tow_month_before, $one_month_before])
            ->count();
        $count_three_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$three_month_before, $tow_month_before])
            ->count();
        $countDl_three_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$three_month_before, $tow_month_before])
            ->count();
        $count_four_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$four_month_before, $three_month_before])
            ->count();
        $countDl_four_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$four_month_before, $three_month_before])
            ->count();
        $count_five_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$five_month_before, $four_month_before])
            ->count();
        $countDl_five_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$five_month_before, $four_month_before])
            ->count();
        $count_six_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$six_month_before, $five_month_before])
            ->count();
        $countDl_six_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$six_month_before, $five_month_before])
            ->count();
        $count_seven_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$seven_month_before, $six_month_before])
            ->count();
        $countDl_seven_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$seven_month_before, $six_month_before])
            ->count();
        $count_eight_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
            ->count();
        $countDl_eight_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
            ->count();
        $count_nine_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
            ->count();
        $countDl_nine_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
            ->count();
        $count_ten_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
            ->count();
        $countDl_ten_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
            ->count();
        $count_eleven_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
            ->count();
        $countDl_eleven_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
            ->count();
        $count_twelve_month = DB::table('client_view')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
            ->count();
        $countDl_twelve_month = DB::table('client_download')
            ->where('app_id', $resultApp->id)
            ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
            ->count();
        $resultAppMedia = AppMedia::where('app_id', $resultApp->id)
            ->get();


        $totalRate = AppRate::where('app_id' , $resultApp->id)
            ->get();

        $totalRateCnt = AppRate::where('app_id' , $resultApp->id)
            ->count();

        $btw4_5RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',4)
            ->where('rate','<=',5)
            ->count();

        $btw3_4RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',3)
            ->where('rate','<=',4)
            ->count();

        $btw2_3RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',2)
            ->where('rate','<=',3)
            ->count();

        $btw1_2RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>',1)
            ->where('rate','<=',2)
            ->count();

        $btw0_1RateCnt = AppRate::where('app_id' , $resultApp->id)
            ->where('rate','>=',0)
            ->where('rate','<=',1)
            ->count();

        $rowCat = 0;
        foreach ($appCategory as $item)
        {
            $btw_RateCntCategory[$rowCat] = AppRate::where('app_id' , $item->id)
                ->count();
            $rowCat++;
        }

        $sumRate =0;
        foreach ($totalRate as $item)
        {
            $sumRate += $item->rate;
        }
        if ($totalRateCnt==0) {
            $averageRate = $sumRate / 1;
        } else {
            $averageRate = $sumRate / $totalRateCnt;
        }

        $resultAppApk = AppApk::where('app_id', $resultApp->id)
            ->orderBy('id', 'desc')
            ->get();
        $lastAppApk = AppApk::where('app_id', $resultApp->id)
            ->orderBy('id', 'desc')
            ->first();
        $lastAppApkAccepted = AppApk::where('app_id', $resultApp->id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->first();




        return view($this->GetPreView() . 'PageApp', compact('moduleName', 'LocalhostPath', 'resultApp', 'resultComment', 'userForComment', 'clients', 'apps', 'count_one_month', 'count_tow_month', 'count_three_month', 'count_four_month', 'count_five_month', 'count_six_month', 'count_seven_month', 'count_eight_month', 'count_nine_month', 'count_ten_month', 'count_eleven_month', 'count_twelve_month', 'countDl_one_month', 'countDl_tow_month', 'countDl_three_month', 'countDl_four_month', 'countDl_five_month', 'countDl_six_month', 'countDl_seven_month', 'countDl_eight_month', 'countDl_nine_month', 'countDl_ten_month', 'countDl_eleven_month', 'countDl_twelve_month', 'date', 'pageName', 'resultAppMedia','totalRateCnt','appCategory','appCategoryName','btw_RateCntCategory','btw4_5RateCnt','btw3_4RateCnt','btw2_3RateCnt','btw1_2RateCnt','btw0_1RateCnt','appRate','appRate_2','averageRate','resultAppApk','lastAppApk','lastAppApkAccepted'))->with(['id' => $id]);
    }

    public function index()
    {
//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");

        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);

        $moduleName = $manifest['title'];

        $DeveloperId = Auth::guard('developer')->id();
        $Developer = Developer::where('id', $DeveloperId)
            ->first();

        $now = date('Y-m-d H:i:s', time());
        $Week = date('Y-m-d H:i:s', strtotime('-7 days'));
        $Three_Months = date('Y-m-d H:i:s', strtotime('-90 days'));
        $Six_Months = date('Y-m-d H:i:s', strtotime('-180 days'));

//        All App Downloads
        $download_All = ClientDownload::where('created_at', '<', $now)
            ->get();
        $dl_All_row = 1;
        foreach ($download_All as $item) {
            $download_All_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($download_All_App as $item_2) {
                $dl_All_row++;
            }
        }

//        A Week App Downloads
        $download_Week = ClientDownload::where('created_at', '<', $now)
            ->where('created_at', '>', $Week)
            ->get();
        $dl_Week_row = 1;
        foreach ($download_Week as $item) {
            $download_Week_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($download_Week_App as $item_2) {
                $dl_Week_row++;
            }
        }

//        $results_download_Week = (($dl_All_row - $dl_Week_row) / $dl_Week_row) *100;
        $results_download_Week = ($dl_Week_row * 100) / $dl_All_row;

//        Three Months App Downloads
        $download_Three_Months = ClientDownload::where('created_at', '<', $now)
            ->where('created_at', '>', $Three_Months)
            ->get();
        $dl_Three_Months_row = 1;
        foreach ($download_Three_Months as $item) {
            $download_Three_Months_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($download_Three_Months_App as $item_2) {
                $dl_Three_Months_row++;
            }
        }
        $results_download_Three_Months = ($dl_Three_Months_row * 100) / $dl_All_row;

//        Six Months App Downloads
        $download_Six_Months = ClientDownload::where('created_at', '<', $now)
            ->where('created_at', '>', $Six_Months)
            ->get();
        $dl_Six_Months_row = 1;
        foreach ($download_Six_Months as $item) {
            $download_Six_Months_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($download_Six_Months_App as $item_2) {
                $dl_Six_Months_row++;
            }
        }
        $results_download_Six_Months = ($dl_Six_Months_row * 100) / $dl_All_row;

//        All App Views
        $view_All = ClientView::where('created_at', '<', $now)
            ->get();
        $view_All_row = 1;
        foreach ($view_All as $item) {
            $view_All_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($view_All_App as $item_2) {
                $view_All_row++;
            }
        }

//        A Week App Views
        $view_Week = ClientView::where('created_at', '<', $now)
            ->where('created_at', '>', $Week)
            ->get();
        $view_Week_row = 1;
        foreach ($view_Week as $item) {
            $view_Week_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($view_Week_App as $item_2) {
                $view_Week_row++;
            }
        }
        $results_view_Week = ($view_Week_row * 100) / $view_All_row;

//        Three Months App Views
        $view_Three_Months = ClientView::where('created_at', '<', $now)
            ->where('created_at', '>', $Three_Months)
            ->get();
        $view_Three_Months_row = 1;
        foreach ($view_Three_Months as $item) {
            $view_Three_Months_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($view_Three_Months_App as $item_2) {
                $view_Three_Months_row++;
            }
        }
        $results_view_Three_Months = ($view_Three_Months_row * 100) / $view_All_row;

//        Six Months App Views
        $view_Six_Months = ClientView::where('created_at', '<', $now)
            ->where('created_at', '>', $Six_Months)
            ->get();
        $view_Six_Months_row = 1;
        foreach ($view_Six_Months as $item) {
            $view_Six_Months_App = App::where('id', $item->app_id)
                ->where('developer', $Developer->id)
                ->get();
            foreach ($view_Six_Months_App as $item_2) {
                $view_Six_Months_row++;
            }
        }

        $results_view_Six_Months = ($view_Six_Months_row * 100) / $view_All_row;

        // Remove Incomplete Apps
        $emptyApp = App::where('icon', NULL)
            ->where('developer', $Developer->id)
            ->first();
        if ($emptyApp) {
            DB::table('app')
                ->where('id', $emptyApp->id)
                ->delete();

            DB::table('app_apk')
                ->where('app_id', $emptyApp->id)
                ->delete();
        }

        $appCarousel = App::where('developer', $Developer->id)
            ->orderBy('id', 'DESC')
            ->get();

        $strtotime = time();
        $date = date("Y-m-d");

        // $twelve_month_before = date( 'Y, m', strtotime( $date . ' -12 month' ) );
        $twelve_month_before = date('Y-m-d H:i:s', strtotime($date . ' -12 month'));
        $eleven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -11 month'));
        $ten_month_before = date('Y-m-d H:i:s', strtotime($date . ' -10 month'));
        $nine_month_before = date('Y-m-d H:i:s', strtotime($date . ' -9 month'));
        $eight_month_before = date('Y-m-d H:i:s', strtotime($date . ' -8 month'));
        $seven_month_before = date('Y-m-d H:i:s', strtotime($date . ' -7 month'));
        $six_month_before = date('Y-m-d H:i:s', strtotime($date . ' -6 month'));
        $five_month_before = date('Y-m-d H:i:s', strtotime($date . ' -5 month'));
        $four_month_before = date('Y-m-d H:i:s', strtotime($date . ' -4 month'));
        $three_month_before = date('Y-m-d H:i:s', strtotime($date . ' -3 month'));
        $tow_month_before = date('Y-m-d H:i:s', strtotime($date . ' -2 month'));
        $one_month_before = date('Y-m-d H:i:s', strtotime($date . ' -1 month'));
        $Now = date('Y-m-d H:i:s', strtotime($date));

        $Cntrow = 0;
        $sum_one_month = 0;
        $sum_tow_month = 0;
        $sum_three_month = 0;
        $sum_four_month = 0;
        $sum_five_month = 0;
        $sum_six_month = 0;
        $sum_seven_month = 0;
        $sum_eight_month = 0;
        $sum_nine_month = 0;
        $sum_ten_month = 0;
        $sum_eleven_month = 0;
        $sum_twelve_month = 0;


        $sumDl_one_month = 0;
        $sumDl_tow_month = 0;
        $sumDl_three_month = 0;
        $sumDl_four_month = 0;
        $sumDl_five_month = 0;
        $sumDl_six_month = 0;
        $sumDl_seven_month = 0;
        $sumDl_eight_month = 0;
        $sumDl_nine_month = 0;
        $sumDl_ten_month = 0;
        $sumDl_eleven_month = 0;
        $sumDl_twelve_month = 0;
        $appDeveloper = [];


        foreach ($appCarousel as $item) {

            $count_one_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$one_month_before, $Now])
                ->count();
            $sum_one_month += $count_one_month[$Cntrow];

            $countDl_one_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$one_month_before, $Now])
                ->count();

            $sumDl_one_month += $countDl_one_month[$Cntrow];


            $count_tow_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$tow_month_before, $one_month_before])
                ->count();
            $sum_tow_month += $count_tow_month[$Cntrow];

            $countDl_tow_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$tow_month_before, $one_month_before])
                ->count();
            $sumDl_tow_month += $countDl_tow_month[$Cntrow];


            $count_three_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$three_month_before, $tow_month_before])
                ->count();
            $sum_three_month += $count_three_month[$Cntrow];

            $countDl_three_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$three_month_before, $tow_month_before])
                ->count();
            $sumDl_three_month += $countDl_three_month[$Cntrow];


            $count_four_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$four_month_before, $three_month_before])
                ->count();
            $sum_four_month += $count_four_month[$Cntrow];

            $countDl_four_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$four_month_before, $three_month_before])
                ->count();
            $sumDl_four_month += $countDl_four_month[$Cntrow];


            $count_five_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$five_month_before, $four_month_before])
                ->count();
            $sum_five_month += $count_five_month[$Cntrow];

            $countDl_five_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$five_month_before, $four_month_before])
                ->count();
            $sumDl_five_month += $countDl_five_month[$Cntrow];


            $count_six_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$six_month_before, $five_month_before])
                ->count();
            $sum_six_month += $count_six_month[$Cntrow];

            $countDl_six_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$six_month_before, $five_month_before])
                ->count();
            $sumDl_six_month += $countDl_six_month[$Cntrow];


            $count_seven_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$seven_month_before, $six_month_before])
                ->count();
            $sum_seven_month += $count_seven_month[$Cntrow];

            $countDl_seven_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$seven_month_before, $six_month_before])
                ->count();
            $sumDl_seven_month += $countDl_seven_month[$Cntrow];


            $count_eight_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
                ->count();
            $sum_eight_month += $count_eight_month[$Cntrow];

            $countDl_eight_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$eight_month_before, $seven_month_before])
                ->count();
            $sumDl_eight_month += $countDl_eight_month[$Cntrow];


            $count_nine_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
                ->count();
            $sum_nine_month += $count_nine_month[$Cntrow];

            $countDl_nine_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$nine_month_before, $eight_month_before])
                ->count();
            $sumDl_nine_month += $countDl_nine_month[$Cntrow];


            $count_ten_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
                ->count();
            $sum_ten_month += $count_ten_month[$Cntrow];

            $countDl_ten_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$ten_month_before, $nine_month_before])
                ->count();
            $sumDl_ten_month += $countDl_ten_month[$Cntrow];


            $count_eleven_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
                ->count();
            $sum_eleven_month += $count_eleven_month[$Cntrow];

            $countDl_eleven_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$eleven_month_before, $ten_month_before])
                ->count();
            $sumDl_eleven_month += $countDl_eleven_month[$Cntrow];


            $count_twelve_month[$Cntrow] = DB::table('client_view')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
                ->count();
            $sum_twelve_month += $count_twelve_month[$Cntrow];

            $countDl_twelve_month[$Cntrow] = DB::table('client_download')
                ->where('app_id', $item->id)
                ->whereBetween('created_at', [$twelve_month_before, $eleven_month_before])
                ->count();
            $sumDl_twelve_month += $countDl_twelve_month[$Cntrow];


            $appDeveloper[] = $item->id;
            $Cntrow++;
        }

        $appDeveloperImp = implode(",", $appDeveloper);
        $comments = DB::table('comment')
            ->whereIn('item_id', explode(',', $appDeveloperImp))
            ->orderBy('created_at', 'DESC')
            ->get()
            ->take(10);
        $CommRow = 0;
        foreach ($comments as $item) {
            $clients[$item->id] = DB::table('client')
                ->where('id', $item->client_id)
                ->first();
            $apps[$item->id] = DB::table('app')
                ->where('id', $item->item_id)
                ->first();
            $CommRow++;
        }

        $developerName = $Developer->name;
        $developerFamily = $Developer->family;
        $developerNik = $Developer->nickName;
        $developerEmail = $Developer->email;
        $developerMCart = $Developer->melliCart;
        $developerPCode = $Developer->postalCode;
        $developerAddress = $Developer->address;


        return view($this->GetPreView() . 'PageView', compact('moduleName', 'LocalhostPath', 'results_download_Week', 'dl_Week_row', 'results_download_Three_Months', 'dl_Three_Months_row', 'results_download_Six_Months', 'dl_Six_Months_row', 'results_view_Week', 'view_Week_row', 'results_view_Three_Months', 'view_Three_Months_row', 'results_view_Six_Months', 'view_Three_Six_row', 'view_Six_Months_row', 'appCarousel', 'DeveloperId', 'date', 'count_one_month', 'count_tow_month', 'count_three_month', 'count_four_month', 'count_five_month', 'count_six_month', 'count_seven_month', 'count_eight_month', 'count_nine_month', 'count_ten_month', 'count_eleven_month', 'count_twelve_month', 'countDl_one_month', 'countDl_tow_month', 'countDl_three_month', 'countDl_four_month', 'countDl_five_month', 'countDl_six_month', 'countDl_seven_month', 'countDl_eight_month', 'countDl_nine_month', 'countDl_ten_month', 'countDl_eleven_month', 'countDl_twelve_month', 'sum_one_month', 'sum_tow_month', 'sum_three_month', 'sum_four_month', 'sum_five_month', 'sum_six_month', 'sum_seven_month', 'sum_eight_month', 'sum_nine_month', 'sum_ten_month', 'sum_eleven_month', 'sum_twelve_month', 'sumDl_one_month', 'sumDl_tow_month', 'sumDl_three_month', 'sumDl_four_month', 'sumDl_five_month', 'sumDl_six_month', 'sumDl_seven_month', 'sumDl_eight_month', 'sumDl_nine_month', 'sumDl_ten_month', 'sumDl_eleven_month', 'sumDl_twelve_month', 'comments', 'clients', 'apps', 'Now','developerName','developerFamily','developerNik','developerEmail','developerMCart','developerPCode','developerAddress'));
    }

    public function appImageDelete(Request $request)
    {
        $appID = $request->get('appId');
        App::where('id', $appID)->update(
            array(
                'image' => ''
            )
        );
    }

    public function appVideoDelete(Request $request)
    {
        $appID = $request->get('appId');
        App::where('id', $appID)->update(
            array(
                'video' => ''
            )
        );
    }
    public function appMediaDelete(Request $request)
    {
        $appMediaId = $request->get('appMediaId');

        AppMedia::where('id', $appMediaId)
        ->delete();

    }
}
