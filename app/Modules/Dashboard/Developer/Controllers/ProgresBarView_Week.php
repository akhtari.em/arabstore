<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarView_Week extends Controller
{
    public function progresView_Week(Request $request)
    {
        $progressColorViewWeek = $request->get('progressColorViewWeek');
        $results_view_Week = $request->get('results_view_Week');
        $outputProgressViewWeek = '<div class="pie_progress__Views_Week" role="progressbar" data-goal="'.$results_view_Week.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorViewWeek.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>هفته گذشته</p></span><script type="text/javascript">$(".pie_progress__Views_Week").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Views_Week").asPieProgress("start");</script>';
        echo $outputProgressViewWeek;
    }
}