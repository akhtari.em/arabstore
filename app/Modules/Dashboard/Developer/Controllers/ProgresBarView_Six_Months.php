<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarView_Six_Months extends Controller
{
    public function progresView_Six_Months(Request $request)
    {
        $progressColorViewSix_Months = $request->get('progressColorViewSix_Months');
        $results_view_Six_Months = $request->get('results_view_Six_Months');
        $outputProgressViewSix_Months = '<div class="pie_progress__Views_Six_Months" role="progressbar" data-goal="'.$results_view_Six_Months.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorViewSix_Months.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>۶ ماه گذشته</p></span><script type="text/javascript">$(".pie_progress__Views_Six_Months").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Views_Six_Months").asPieProgress("start");</script>';
        echo $outputProgressViewSix_Months;
    }
}