<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\App\Admin\Models\App;
use App\Modules\Dashboard\Developer\Models\Dashboard;
use App\Modules\Developer\Admin\Models\Developer;
use App\Services\AppServices;
use App\Services\CheckFile;
use Illuminate\Http\Request;

class FileController extends Controller {
    use CheckFile;
    private $appFileUpload;
    public function __construct(AppServices $appFileUpload) {
        $this->appFileUpload = $appFileUpload;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUpload() {
        return view('fileUpload');
    }

    public function fileUploadPost(Request $request) {

        if ($request->file('file')->isValid()) {
            if ($this->checkUploadFile($request->file('file'))) {
                $filename = $this->appFileUpload->uploadAppName($request->file('file')->getClientOriginalName(), $request->file('file'));
                $path = $this->appFileUpload->getUploadPath('/uploads/App/') . $filename;

                // decompile app apk
                $apk = new \ApkParser\Parser($path);

                $manifest = $apk->getManifest();
                $request->merge(['versionName' => $manifest->getVersionName()]);
                $request->merge(['versionCode' => $manifest->getVersionCode()]);
                $request->merge(['package_name' => $manifest->getPackageName()]);
                $request->merge(['permission' => $manifest->getPermissions()]);
                $request->merge(['minSDK' => $manifest->getMinSdk()]);

                $request->request->add(['file' => $filename]);
                $maxApp = App::where('status', '1')
                    ->orWhere('status', '0')
                    ->orderBy('created_at', 'desc')->first();
                $maxAppId = $maxApp->id;
                $maxAppId++;

                $maxAppApk = AppApk::where('status', '1')
                    ->orWhere('status', '0')
                    ->orderBy('created_at', 'desc')->first();
                $request->merge(['volume' => $request->file('file')->getClientSize()]);

                AppApk::create([
                    'app_id' => $maxAppId,
                    'file' => $filename,
                    'user_id' => $request['developerId'],
                    'versionName' => $request->versionName,
                    'versionCode' => $request->versionCode,
                    'package_name' => $request->package_name,
                    'volume' => $request->volume,
                    'minSDK' => $request->minSDK,
                    'permission' => $request->permission,
                    'publishDate' => date('Y-m-d H:i:s', $request['publishDate']),
                    'created_at' => date('Y-m-d H:i:s', $request['publishDate']),
                    'updated_at' => date('Y-m-d H:i:s', $request['publishDate']),
                ]);

                Dashboard::create([
                    'id' => $maxAppId,
                    'title' => 'Your Application Name',
                    'isFree' => '0',
                    'developer' => $request['developerId'],
                    'package' => $request->package_name,
                ]);

                $scriptControl = "<script>$('#developerID').val('".$request['developerId']."');$('#appID').val('".$maxAppId."');</script>";
                return $scriptControl;
            }

        }

    }
}