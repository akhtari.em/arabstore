<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarDownload_Week extends Controller
{
    public function progresDownload_Week(Request $request)
    {
        $progressColorDlWeek = $request->get('progressColorDlWeek');
        $results_download_Week = $request->get('results_download_Week');
        $outputProgressDlWeek = '<div class="pie_progress__Download_Week" role="progressbar" data-goal="'.$results_download_Week.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorDlWeek.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>هفته گذشته</p></span><script type="text/javascript">$(".pie_progress__Download_Week").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Download_Week").asPieProgress("start");</script>';
        echo $outputProgressDlWeek;
    }
}
