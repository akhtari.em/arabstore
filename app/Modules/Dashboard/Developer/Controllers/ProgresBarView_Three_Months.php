<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgresBarView_Three_Months extends Controller
{
    public function progresView_Three_Months(Request $request)
    {
        $progressColorViewThree_Months = $request->get('progressColorViewThree_Months');
        $results_view_Three_Months = $request->get('results_view_Three_Months');
        $outputProgressViewThree_Months = '<div class="pie_progress__Views_Three_Months" role="progressbar" data-goal="'.$results_view_Three_Months.'" data-barsize="20" data-stroke="25" data-barcolor="'.$progressColorViewThree_Months.'"><div class="pie_progress__number">0%</div></div><span><h5>مقدار رشد</h5><p>۳ ماه گذشته</p></span><script type="text/javascript">$(".pie_progress__Views_Three_Months").asPieProgress({namespace: "pie_progress"});$(".pie_progress__Views_Three_Months").asPieProgress("start");</script>';
        echo $outputProgressViewThree_Months;
    }
}