<?php

namespace App\Modules\Dashboard\Developer\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Hash;
use App\Modules\Developer\Admin\Models\Developer;
use App\Services\AppServices;
use Illuminate\Http\Request;
use DB;
use URL;
use Illuminate\Support\Facades\Auth;

class ProfilePage extends Controller
{
    private $appFileUpload;

    public function __construct(AppServices $appFileUpload)
    {
        $this->appFileUpload = $appFileUpload;
    }

    public function GetPreView()
    {
        $dd = CoreCommon::osD();

        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 0, 4) == 'Admin') {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Web" . $dd), 0, 3) == 'Web') {
            $side = 'Web';
        } else if (substr(strrchr(__DIR__, "Developer" . $dd), 0, 9) == 'Developer') {
            $side = 'Developer';
        }

        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }

    public function profile()
    {
        //        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");

        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);

        $moduleName = $manifest['title'];

        $DeveloperId = Auth::guard('developer')->id();
        $Developer = Developer::where('id', $DeveloperId)
            ->first();
        return view($this->GetPreView() . 'Profile', compact('moduleName', 'LocalhostPath', 'DeveloperId', 'Developer'));
    }


    public function profileLogoEdit(Request $request)
    {
        $DevId = $request->get('DevId');
        $imgLogo = $_FILES['logo']['name'];
        $tmpLogo = $_FILES['logo']['tmp_name'];

        $pathLogo = $this->appFileUpload->getUploadPath('/uploads/avatars/');
        $extLogo = strtolower(pathinfo($imgLogo, PATHINFO_EXTENSION));
        if ($imgLogo) {
            $final_imgLogo = rand(1000, 1000000) . $imgLogo;
            $pathLogo = $pathLogo . strtolower($final_imgLogo);
        } else {
            $final_imgLogo = '';
        }
        move_uploaded_file($tmpLogo, $pathLogo);
        DB::table('developer')
            ->where('id', $DevId)
            ->update(['logo' => $final_imgLogo]);
        $LocalhostPath = CoreCommon::getLocalhostPath("");
        $script = '<script>$(".logo_img").attr("src", "' . $LocalhostPath . '/uploads/avatars/' . $final_imgLogo . '");$(".developer-header-avatar").attr("src", "' . $LocalhostPath . '/uploads/avatars/' . $final_imgLogo . '");notif({msg: "تصویر پروفایل با موفقیت آپلود شد",type: "success",width : 400,height : 60,position : "right",autohide : true,opacity : 1,multiline: 0,fade: 0,bgcolor: "",color: "",timeout: 5000,zindex: null,offset: 0,callback: null,clickable: false,animation: "slide"});</script>';
        return $script;
    }

    public function profileEdit(Request $request)
    {
        $DeveloperId = $request->get('DevId');
        $name = $request->get('name');
        $family = $request->get('family');
        $nickName = $request->get('nickName');
        $email = $request->get('email');
        $password = $request->get('password');
        if ($request->get('mobile')) {
            $mobile = $request->get('mobile');
        } else {
            $mobile = '';
        }
        if ($request->get('phone')) {
            $phone = $request->get('phone');
        } else {
            $phone = '';
        }
        if ($request->get('fax')) {
            $fax = $request->get('fax');
        } else {
            $fax = '';
        }
        $melliCart = $request->get('melliCart');
        $postalCode = $request->get('postalCode');
        $address = $request->get('address');

        if ($password) {
            $pass = Hash::make($password);
            DB::table('developer')
                ->where('id', $DeveloperId)
                ->update(['name' => $name,
                    'family' => $family,
                    'nickName' => $nickName,
                    'email' => $email,
                    'password' => $pass,
                    'mobile' => $mobile,
                    'phone' => $phone,
                    'fax' => $fax,
                    'melliCart' => $melliCart,
                    'postalCode' => $postalCode,
                    'address' => $address,
                ]);
        } else {
            DB::table('developer')
                ->where('id', $DeveloperId)
                ->update(['name' => $name,
                    'family' => $family,
                    'nickName' => $nickName,
                    'email' => $email,
                    'mobile' => $mobile,
                    'phone' => $phone,
                    'fax' => $fax,
                    'melliCart' => $melliCart,
                    'postalCode' => $postalCode,
                    'address' => $address,
                ]);
        }

    }
}