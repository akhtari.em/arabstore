<?php
use App\Http\Controllers\CoreCommon;
use App\Post;

$dd = CoreCommon::osD();
$manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . 'Manifest.json', "r");
$manifest = json_decode($manifestJson, true);
$moduleName = $manifest['title'];
$moduleSide = substr(strrchr(__DIR__, $dd), 1);

Route::group(['prefix' => $moduleName . '/' . $moduleSide, 'namespace' => 'App\Modules\\' . $moduleName . '\\' . $moduleSide . '\Controllers', 'middleware' => ['web', 'guest:developer', 'checkPermissions']], function () {
    Route::get('PageView', 'DashboardManager@index');

    Route::get('PageApp/{id}', 'DashboardManager@appindex');

    Route::get('Profile', 'ProfilePage@profile');
    Route::post('/profileLogoEdit', 'ProfilePage@profileLogoEdit')->name('Dashboard.profileLogoEdit');
    Route::post('/profileEdit', 'ProfilePage@profileEdit')->name('Dashboard.profileEdit');

    Route::post('/appUpdate', 'AppInfoUpdate@appUpdate')->name('Dashboard.appUpdate');
    Route::post('/appCancel', 'AppInfoUpdate@appCancel')->name('Dashboard.appCancel');
    Route::post('/appEdit', 'AppPage@appEdit')->name('Dashboard.appEdit');
    Route::post('/appIconEdit', 'AppPage@appIconEdit')->name('Dashboard.appIconEdit');
    Route::post('/appMedia', 'AppPage@appMedia')->name('Dashboard.appMedia');

    Route::post('/appImageDelete', 'DashboardManager@appImageDelete')->name('Dashboard.appImageDelete');

    Route::post('/appVideoDelete', 'DashboardManager@appVideoDelete')->name('Dashboard.appVideoDelete');

    Route::post('/appMediaDelete', 'DashboardManager@appMediaDelete')->name('Dashboard.appMediaDelete');

    Route::post('/appDlChart', 'Chart@appDl')->name('Dashboard.appDlChart');

    Route::post('/progresDownload_Week', 'ProgresBarDownload_Week@progresDownload_Week')->name('Dashboard.progresDownload_Week');
    Route::post('/progresView_Week', 'ProgresBarView_Week@progresView_Week')->name('Dashboard.progresView_Week');

    Route::post('/progresDownload_Three_Months', 'ProgresBarDownload_Three_Months@progresDownload_Three_Months')->name('Dashboard.progresDownload_Three_Months');
    Route::post('/progresView_Three_Months', 'ProgresBarView_Three_Months@progresView_Three_Months')->name('Dashboard.progresView_Three_Months');

    Route::post('/progresDownload_Six_Months', 'ProgresBarDownload_Six_Months@progresDownload_Six_Months')->name('Dashboard.progresDownload_Six_Months');
    Route::post('/progresView_Six_Months', 'ProgresBarView_Six_Months@progresView_Six_Months')->name('Dashboard.progresView_Six_Months');

    Route::get('file-upload', 'FileController@fileUpload');
    Route::post('file-upload', 'FileController@fileUploadPost')->name('fileUploadPost');

});