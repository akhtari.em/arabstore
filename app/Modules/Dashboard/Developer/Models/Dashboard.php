<?php

namespace App\Modules\Dashboard\Developer\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model {
    protected $table = 'app';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'id', 'title', 'isFree', 'developer', 'package',
    ];
}