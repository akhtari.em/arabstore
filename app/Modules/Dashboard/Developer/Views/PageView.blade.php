@extends('Master_Web::IndexPage')
@section('body')



    <input type="hidden" name="DevId" id="DevId"
           value="{{Auth::guard('developer')->id()}}">
    <input type="hidden" name="EditdeveloperID" id="EditdeveloperID"
           value="0">
    @include('errors')
    <div class="wrapper firstBlur">
        @include('Dashboard.Developer.Views.inc.index.AppInfoForm')
        <section class="section-carousel">
            <div class="container-fluid">
                <div class="align-sides">
                    @include('Dashboard.Developer.Views.inc.index.AppCarousel')
                </div>
            </div>
        </section>
        <section class="section-cards">
            <div class="container-fluid">
                <div class="align-sides">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <div class="card card-img-holder">
                                @include('Dashboard.Developer.Views.inc.index.AppUpload')
                            </div>
                        </div>
                        @include('Dashboard.Developer.Views.inc.index.AppPercentRate')
                    </div>
                </div>
            </div>
        </section>
        <section class="section-stat">
            <div class="container-fluid">
                <div class="align-sides">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-3">
                            <div class="card">
                                @include('Dashboard.Developer.Views.inc.index.Comments')
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-9">
                            <div class="card card-chart">
                                @include('Dashboard.Developer.Views.inc.index.ChartView')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-stat">
            <div class="container-fluid">
                <div class="align-sides">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="card card-chart">
                                @include('Dashboard.Developer.Views.inc.index.ChartDownload')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="result"></div>

    <script type="text/javascript" charset="utf-8"
            src="{{ asset('Assets/Developer/js/jquery.form.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#modalMenu").attr('aria-hidden', "true");
            $(".footer-card_view_Week").css("display", "none");
            $(".footer-card_download_Week").css("display", "block");
            $('header').removeClass('header-zindex');
            $('header').removeClass('zindex-on');
            $(".Views_Btn_Week").on('click', function () {
                $(this).addClass('active_Week');
                $(".Download_Btn_Week").removeClass('active_Week');
                $("#View_Week").css("display", "block");
                $("#Download_Week").css("display", "none");
                $(".footer-card_view_Week").css("display", "block");
                $(".footer-card_download_Week").css("display", "none");
            });
            $(".Download_Btn_Week").on('click', function () {
                $(this).addClass('active_Week');
                $(".Views_Btn_Week").removeClass('active_Week');
                $("#View_Week").css("display", "none");
                $("#Download_Week").css("display", "block");
                $(".footer-card_view_Week").css("display", "none");
                $(".footer-card_download_Week").css("display", "block");
            });

            $(".footer-card_view_Three_Months").css("display", "none");
            $(".footer-card_download_Three_Months").css("display", "block");
            $(".Views_Btn_Three_Months").on('click', function () {
                $(this).addClass('active_Three_Months');
                $(".Download_Btn_Three_Months").removeClass('active_Three_Months');
                $("#View_Three_Months").css("display", "block");
                $("#Download_Three_Months").css("display", "none");
                $(".footer-card_view_Three_Months").css("display", "block");
                $(".footer-card_download_Three_Months").css("display", "none");
            });
            $(".Download_Btn_Three_Months").on('click', function () {
                $(this).addClass('active_Three_Months');
                $(".Views_Btn_Three_Months").removeClass('active_Three_Months');
                $("#View_Three_Months").css("display", "none");
                $("#Download_Three_Months").css("display", "block");
                $(".footer-card_view_Three_Months").css("display", "none");
                $(".footer-card_download_Three_Months").css("display", "block");
            });


            $(".footer-card_view_Six_Months").css("display", "none");
            $(".footer-card_download_Six_Months").css("display", "block");
            $(".Views_Btn_Six_Months").on('click', function () {
                $(this).addClass('active_Six_Months');
                $(".Download_Btn_Six_Months").removeClass('active_Six_Months');
                $("#View_Six_Months").css("display", "block");
                $("#Download_Six_Months").css("display", "none");
                $(".footer-card_view_Six_Months").css("display", "block");
                $(".footer-card_download_Six_Months").css("display", "none");
            });
            $(".Download_Btn_Six_Months").on('click', function () {
                $(this).addClass('active_Six_Months');
                $(".Views_Btn_Six_Months").removeClass('active_Six_Months');
                $("#View_Six_Months").css("display", "none");
                $("#Download_Six_Months").css("display", "block");
                $(".footer-card_view_Six_Months").css("display", "none");
                $(".footer-card_download_Six_Months").css("display", "block");
            });

            changeProgressBar();
            $("#colorselector").on('change', function () {
                var pr_color = $(this).val();
                if (pr_color == 'green') {
                    percentageColor = '#0f9d58'
                } else if (pr_color == 'purple') {
                    percentageColor = '#6B2852'
                } else if (pr_color == 'blue') {
                    percentageColor = '#283C6B'
                } else if (pr_color == 'yellow') {
                    percentageColor = '#FFAA2C'
                } else {
                    percentageColor = '#0f9d58'
                }
                $("#percentageColor").val(percentageColor);
                changeProgressBar();
                return false;
            });

            function changeProgressBar() {
                var pr_color = $("#percentageColor").val();
                var results_download_Week = $("#results_download_Week").val();
                var results_view_Week = $("#results_view_Week").val();
                var results_download_Three_Months = $("#results_download_Three_Months").val();
                var results_view_Three_Months = $("#results_view_Three_Months").val();
                var results_download_Six_Months = $("#results_download_Six_Months").val();
                var results_view_Six_Months = $("#results_view_Six_Months").val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresDownload_Week') }}',
                    data: {progressColorDlWeek: pr_color, results_download_Week: results_download_Week},
                    success: function (Download_Week) {
                        $('#Download_Week').html(Download_Week);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresView_Week') }}',
                    data: {progressColorViewWeek: pr_color, results_view_Week: results_view_Week},
                    success: function (View_Week) {
                        $('#View_Week').html(View_Week);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresDownload_Three_Months') }}',
                    data: {
                        progressColorDlThree_Months: pr_color,
                        results_download_Three_Months: results_download_Three_Months
                    },
                    success: function (Download_Three_Months) {
                        $('#Download_Three_Months').html(Download_Three_Months);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresView_Three_Months') }}',
                    data: {
                        progressColorViewThree_Months: pr_color,
                        results_view_Three_Months: results_view_Three_Months
                    },
                    success: function (View_Three_Months) {
                        $('#View_Three_Months').html(View_Three_Months);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresDownload_Six_Months') }}',
                    data: {
                        progressColorDlSix_Months: pr_color,
                        results_download_Six_Months: results_download_Six_Months
                    },
                    success: function (Download_Six_Months) {
                        $('#Download_Six_Months').html(Download_Six_Months);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('Dashboard.progresView_Six_Months') }}',
                    data: {progressColorViewSix_Months: pr_color, results_view_Six_Months: results_view_Six_Months},
                    success: function (View_Six_Months) {
                        $('#View_Six_Months').html(View_Six_Months);
                    }
                });
            }
        });

        $('.progress').hide();
        $('.footer-card-Uploader-title').show();
        $('#poster').change(function () {
            $('#sendApp').submit();
            $('.progress').show();
            $('.footer-card-Uploader-title').hide();
        });

        function validate(formData, jqForm, options) {
            var form = jqForm[0];
            if (!form.file.value) {
                alert('File not found');
                return false;
            }
        }

        (function () {
            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');
            $('form').ajaxForm({
                beforeSubmit: validate,
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    var posterValue = $('input[name=file]').fieldValue();
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function () {
                    var percentVal = 'Wait, Saving';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                complete: function (xhr) {
                    $(".result").html('').html(xhr.responseText);
                    status.html(xhr.responseText);
                    $('.progress').hide();
                    $('.footer-card-Uploader-title').show();
                    var ext = $('#poster').val().split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['apk']) == -1) {
                        notif({
                            msg: "فایل ابلیکیشن حتما باید با پسوند 'apk' باشد.",
                            type: "error",
                            // width : 400,
                            // height : 60,
                            position : "right",
                            autohide : true,
                            opacity : 1,
                            multiline: 0,
                            fade: 0,
                            bgcolor: "",
                            color: "",
                            timeout: 5000,
                            zindex: null,
                            offset: 0,
                            callback: null,
                            clickable: false,
                            animation: "slide",
                        });
                        $('#poster').val("");
                        a = 0;
                        $('.uploader-text').text('افزودن برنامه');
                    } else {
                        // Show Modal For add info For App
                        $('#modalSucsessUploadApp').modal('show');
                        $('header').addClass('header-zindex');
                        // $(".header__menu--btn").removeAttr("data-target");
                        // $(".header__menu--btn").removeAttr("data-toggle");
                        // $(".header__menu--btn").attr('disabled', true);
                        // $(".header__logo a").removeAttr('href');
                        // $(".login__setting").removeClass('logout-btn');

                        $('.modal-backdrop').addClass('blackModal');
                        $('.section-carousel').addClass('position-relative');
                        $('.section-carousel').addClass('zindex-on');
                        $('.header').addClass('zindex-on');
                        $('.firstBlur').addClass('zindex-off');

                        $('input[name=file]').val('');
                        $('.uploader-text').text('افزودن برنامه');


                        var picsize = (this.files[0].size);
                        if (picsize > 200000000) {
                            notif({
                                msg: "حداکثر حجم ابلیکیشن ۲۰۰ مگابایت می باشد.",
                                type: "error",
                                // width : 400,
                                // height : 60,
                                position : "right",
                                autohide : true,
                                opacity : 1,
                                multiline: 0,
                                fade: 0,
                                bgcolor: "",
                                color: "",
                                timeout: 5000,
                                zindex: null,
                                offset: 0,
                                callback: null,
                                clickable: false,
                                animation: "slide",
                            });
                            $('#poster').val("");
                            a = 0;
                        } else {
                            a = 1;

                        }

                    }
                },
            });
        })();

        $(".header__menu--btn").click(function () {
            $('#modalSucsessUploadApp').modal('hide');
            $('.header').removeClass('header-zindex');
            $('.header').removeClass('zindex-on');
            $('.section-carousel').removeClass('zindex-on');
            $('.section-carousel').removeClass('position-relative');
            var developerID = $("#developerID").val();
            var appID = $("#appID").val();
            $.ajax({
                type: 'POST',
                url: "{{ route('Dashboard.appCancel') }}",
                data: {developerID: developerID, appID: appID},
                success: function (appCancel) {
                    $('#appInfo').html(appCancel);
                }
            });
        });
        $("#logout-btn").click(function () {
            $('#modalSucsessUploadApp').modal('hide');
            $('.header').removeClass('header-zindex');
            $('.header').removeClass('header-zindex');
            $('.header').removeClass('zindex-on');
            $('.section-carousel').removeClass('zindex-on');
            $('.section-carousel').removeClass('position-relative');
            var developerID = $("#developerID").val();
            var appID = $("#appID").val();
            $.ajax({
                type: 'POST',
                url: "{{ route('Dashboard.appCancel') }}",
                data: {developerID: developerID, appID: appID},
                success: function (appCancel) {
                    $('#appInfo').html(appCancel);
                }
            });
        });

        $(document).ready(function () {
            var _URL = window.URL || window.webkitURL;
            $('#icon').change(function () {
                var icon = $(this)[0].files[0];
                img = new Image();
                var imgwidth = 142;
                var imgheight = 142;
                var maxwidth = 142;
                var maxheight = 142;
                img.src = _URL.createObjectURL(icon);
                img.onload = function () {
                    imgwidth = this.width;
                    imgheight = this.height;
                    $("#width").text(imgwidth);
                    $("#height").text(imgheight);
                    if (imgwidth == maxwidth && imgheight == maxheight) {
                        $("#secStepAppInfoNext").attr('disabled', false);
                        // $("#modalMenu").removeAttr("aria-hidden");

                        $("#response").text("");
                        var formData = new FormData();
                        formData.append('fileToUpload', $('#file')[0].files[0]);
                    } else {
                        $('#icon').val("");
                        $('.uploader-text-icon').text('');
                        $("#secStepAppInfoNext").attr('disabled', true);
                        $("#response").text("آیکون آپلود شده باید در اندازه های " + maxwidth + "X" + maxheight + " باشد");
                    }
                };
                img.onerror = function () {
                    $('#icon').val("");
                    $('.uploader-text-icon').text('');
                    $("#secStepAppInfoNext").attr('disabled', true);
                    $("#response").text("not a valid file: " + icon.type);
                }
            });
        });
    </script>
    <script type="text/javascript"
            src="{{ asset('Assets/Developer/js/custom-file-input.js') }}"></script>
@stop