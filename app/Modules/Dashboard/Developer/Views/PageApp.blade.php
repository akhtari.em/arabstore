@extends('Master_Web::IndexPage')
@section('body')
    <script type="text/javascript" charset="utf-8"
            src="{{ asset('Assets/Developer/js/parent/jquery.multifield.js') }}"></script>
    <div class="wrapper firstBlur">
        <section class="section-cards developerTotal">
            <div id="loadingPageApp">
                <div class="loader loader--style8" title="7">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                         xml:space="preserve">
                        <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s"
                                     dur="0.6s"
                                     repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s"
                                     dur="0.6s"
                                     repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                        <rect x="8" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                        <rect x="16" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                    </svg>
                </div>
            </div>
            <div class="container pageAppTotal">
                <div class="pageAppIcon">
                    <input type="file" name="icon" id="icon" class="inputfile inputfile-6"
                           value="{{$resultApp->icon}}" rel="{{$resultApp->id}}"/>
                    <label for="icon" id="iconLable">
                        @if ($resultApp->status == 0)
                            <i class="statusBtn" style="background:#F9BE67"></i>
                        @elseif ($resultApp->status == 1)
                            <i class="statusBtn" style="background:#0F9D58"></i>
                        @elseif ($resultApp->status == 2)
                            <i class="statusBtn" style="background:#bbff1c"></i>
                        @elseif ($resultApp->status == 3)
                            <i class="statusBtn" style="background:#ff1322"></i>
                        @endif
                        <img src="{{$LocalhostPath.'/uploads/App/'.$resultApp->icon}}" class="icon_img">
                    </label>
                </div>
                <div class="pageAppTitle">جزييات</div>
                <span class="changeAPP editTlt">ويرايش</span>
                <span class="changeAPPHide"></span>
                <a href="{{$LocalhostPath.'/Dashboard/Developer/PageView'}}" class="pageAppBack">داشبورد<i
                            class="fa fa-angle-left"></i></a>
                <div class="tabTotal">
                    <script type="text/javascript" charset="utf-8"
                            src="{{ asset('Assets/Developer/js/jquery-ui.js') }}"></script>
                    <script type="text/javascript">
                        $(function () {
                            $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
                            $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
                        });
                        $(document).ready(function () {
                            $('.z-tabs').tooltip({
                                tooltipClass: "uitooltip",
                                position: {
                                    my: "left top",
                                    at: "right+25 top-5"
                                },
                                show: {
                                    effect: "slideDown",
                                },
                                hide: {
                                    effect: "slideUp",
                                }
                            });
                        });
                    </script>


                    <div class="z-tabs" id="tabs">
                        <ul>
                            <li title="جزييات" class="INFOtlt AppTitle" id="tooltip-1">
                                <a href="#tabs-1"></a></li>
                            <li title="APK" class="ApkTitle" id="tooltip-2">
                                <a href="#tabs-2"></a></li>
                            <li title="مديا" class="MediaTitle" id="tooltip-3">
                                <a href="#tabs-3"></a></li>
                            <li title="کامنت ها" class="CommentTitle" id="tooltip-4">
                                <a href="#tabs-4"></a></li>
                            <li title="آمار بازديد" class="StatTitle" id="tooltip-5">
                                <a href="#tabs-5" id="viewChartTab"></a></li>
                            <li title="آمار دانلود" class="DownloadTitle" id="tooltip-6">
                                <a href="#tabs-6" id="viewDownloadTab"></a></li>
                        </ul>
                        <div id="tabs-1">
                            @include('Dashboard.Developer.Views.inc.app.EditApp')
                        </div>
                        <div id="tabs-2">
                            <div class="row uploadAppRow padding-bottom image-app-edit" style="border: none">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h5 style="padding: 2%;color: #ffab00;font-size: 18px;">ليست APK ها</h5>
                                </div>
                                <ul class="col-lg-12 col-md-12 col-sm-12">
                                    @foreach($resultAppApk as $item)
                                        @if ($item->id == $lastAppApk->id)
                                            @if ($item->status == 0)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #FFAA2C;">
                                                            <b>وضعيت : </b><i>در انتظار تاييد</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @elseif ($item->status == 1)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #0F9D58">
                                                            <b>وضعيت : </b><i>تاييد شده</i>
                                                        @if ($item->id == $lastAppApkAccepted->id)
                                                                <b style="width: auto;padding: 0 30px 0 0;">فايل بارگذاري شده در سايت</b>
                                                        @endif
                                                            <div class="clearfix"></div>
                                                        </li>

                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @elseif ($item->status == 2)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #ff1733;">
                                                            <b>وضعيت : </b><i>تاييد نشده</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                        @else
                                            @if ($item->status == 0)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #FFAA2C;">
                                                            <b>وضعيت : </b><i>در انتظار تاييد</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @elseif ($item->status == 1)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #0F9D58;">
                                                            <b>وضعيت : </b><i>تاييد شده</i>
                                                            @if ($item->id == $lastAppApkAccepted->id)
                                                                <b style="width: auto;padding: 0 30px 0 0;">فايل بارگذاري شده در سايت</b>
                                                            @endif
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @elseif ($item->status == 2)
                                                <li class="ApkItems">
                                                    <ul class="ApkItemsUl">
                                                        <li style="color: #ff1733;">
                                                            <b>وضعيت : </b><i>تاييد نشده</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Version Name : </b><i>{{$item->versionName}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>Package Name : </b><i>{{$item->package_name}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <li>
                                                            <b>تاريخ آپلود : </b><i>{{$item->created_at}}</i>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <form class="row uploadAppRow image-app-edit" id="appApkForm" name="appApkForm" method="POST" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="col-lg-5 col-md-6 col-sm-12">
                                    <input type="file" name="apk" id="apk" class="inputfile inputfile-6"/>
                                    <label for="apk" id="apkLable"><strong> آپلود ورژن جدید (apk.)
                                            <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg"
                                                 width="20"
                                                 height="17" viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                        </strong>
                                        <span class="uploader-text-icon uploader-text-iconapk"></span>
                                    </label>
                                </div>
                                <input name="submit" type="submit" value="ثبت" id="app-Media-submit">
                                <div class="clearfix"></div>
                            </form>
                            <script>
                                $("#appApkForm").on('submit', function (xhr) {
                                    $(".result").html('').html(xhr.responseText);
                                    status.html(xhr.responseText);
                                    $('.progress').hide();
                                    $('.footer-card-Uploader-title').show();
                                    var ext = $('#apk').val().split('.').pop().toLowerCase();
                                    if ($.inArray(ext, ['apk']) == -1)
                                    {
                                        alert('notOK');
                                        notif({
                                            msg: "فایل ابلیکیشن حتما باید با پسوند 'apk' باشد.",
                                            type: "error",
                                            // width : 400,
                                            // height : 60,
                                            position : "right",
                                            autohide : true,
                                            opacity : 1,
                                            multiline: 0,
                                            fade: 0,
                                            bgcolor: "",
                                            color: "",
                                            timeout: 5000,
                                            zindex: null,
                                            offset: 0,
                                            callback: null,
                                            clickable: false,
                                            animation: "slide",
                                        });
                                        $('#apk').val("");
                                        a = 0;
                                        $('.uploader-text').text('افزودن برنامه');
                                    }
                                    else
                                    {
                                        // Show Modal For add info For App
                                        $('#modalSucsessUploadApp').modal('show');
                                        $('header').addClass('header-zindex');
                                        // $(".header__menu--btn").removeAttr("data-target");
                                        // $(".header__menu--btn").removeAttr("data-toggle");
                                        // $(".header__menu--btn").attr('disabled', true);
                                        // $(".header__logo a").removeAttr('href');
                                        // $(".login__setting").removeClass('logout-btn');

                                        $('.modal-backdrop').addClass('blackModal');
                                        $('.section-carousel').addClass('position-relative');
                                        $('.section-carousel').addClass('zindex-on');
                                        $('.header').addClass('zindex-on');
                                        $('.firstBlur').addClass('zindex-off');

                                        $('input[name=file]').val('');
                                        $('.uploader-text').text('افزودن برنامه');


                                        var picsize = (this.files[0].size);
                                        if (picsize > 200000000) {
                                            notif({
                                                msg: "حداکثر حجم ابلیکیشن ۲۰۰ مگابایت می باشد.",
                                                type: "error",
                                                // width : 400,
                                                // height : 60,
                                                position : "right",
                                                autohide : true,
                                                opacity : 1,
                                                multiline: 0,
                                                fade: 0,
                                                bgcolor: "",
                                                color: "",
                                                timeout: 5000,
                                                zindex: null,
                                                offset: 0,
                                                callback: null,
                                                clickable: false,
                                                animation: "slide",
                                            });
                                            $('#apk').val("");
                                            a = 0;
                                        } else {
                                            alert('OK');

                                        }

                                    }
                                });


                            </script>
                        </div>
                        <div id="tabs-3">
                            @include('Dashboard.Developer.Views.inc.app.MediaApp')
                        </div>
                        <div id="tabs-4">
                            @include('Dashboard.Developer.Views.inc.app.CommentApp')
                        </div>
                        <div id="tabs-5">
                            @include('Dashboard.Developer.Views.inc.app.ChartViewApp')
                        </div>
                        <div id="tabs-6">
                            @include('Dashboard.Developer.Views.inc.app.ChartDownloadApp')
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="resultShow"></div>
    <div class="cropHolder">
        <div id="cropWrapper">
            <img id="inputimage" src="">
        </div>
        <div class="cropInputs">
            <div class="inputtools">
                <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/horizontal.png') }}">
                    </span>
                    <span>افقي</span>
                </p>
                <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
            </div>
            <div class="inputtools">
                <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/vertical.png') }}">
                    </span>
                    <span>عمودي</span>
                </p>
                <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
            </div>
            <br>
            <button class="cropButtons" id="zplus">
                <img src="{{ asset('Assets/Developer/images/add.png') }}">
            </button>
            <button class="cropButtons" id="zminus">
                <img src="{{ asset('Assets/Developer/images/minus.png') }}">
            </button>
            <br>
            <button id="cropSubmit" class="cropSubmit">انجام</button>
            <button id="closeCrop">بستن</button>
        </div>
    </div>

    <div id="appInfo"></div>




    <script type="text/javascript">
        $("#imageSr").finecrop({
            viewHeight: 500,
            cropWidth: 697,
            cropHeight: 340,
            cropInput: 'inputimage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });
        $(document).on("click", ".mediaAppDelete", function () {
            var appMediaId = $(this).attr('rel');
            bootbox.confirm({
                title: "حذف اسکرين شات",
                message: "آيا از حذف اين اسکرين شات اطمينان داريد؟",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> خير'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> بله'
                    }
                },
                callback: function (resultDeletMedia) {
                    console.log('This was logged in the callback: ' + resultDeletMedia);
                    if (resultDeletMedia == true) {
                        $.ajax({
                            type: 'POST',
                            data: {appMediaId: appMediaId},
                            url: '{{ route('Dashboard.appMediaDelete') }}',
                            success: function () {
                                $(".mediaAppDelete_"+appMediaId).parent().remove();
                            }
                        });
                    }

                }
            });
            return false;
        });
        if(window.location.hash) {
            var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
            if (hash == 'tabs-3') {
                $(".changeAPP").fadeOut("fast");
                $(".changeAPPHide").css('display', 'inline-block');
                $('html,body').animate({
                    scrollTop: $(".firstBlur").offset().top
                }, 'slow');
                notif({
                    msg: "ويرايش مديا با موفقيت انجام شد. پس از تاييد تغييرات از طرف اوپراتور مربوطه اپليکيشن شما در سامانه قرار ميگيرد.",
                    type: "success",
                    // width : 400,
                    // height : 60,
                    position : "right",
                    autohide : true,
                    opacity : 1,
                    multiline: 0,
                    fade: 0,
                    bgcolor: "",
                    color: "",
                    timeout: 5000,
                    zindex: null,
                    offset: 0,
                    callback: null,
                    clickable: false,
                    animation: "slide",

                });
                window.location.hash = "";
                $('.MediaTitle').addClass('ui-tabs-active');
                $('.MediaTitle').addClass('ui-state-active');
                $('.INFOtlt').removeClass('ui-tabs-active');
                $('.INFOtlt').removeClass('ui-state-active');
                $(".pageAppTitle").text("مديا");
            }
        }
        $("#appMediaForm").on('submit', function (e) {
            e.preventDefault();
            var file_dataImageCrop = $("#croppedImg").attr("src");
            var file_dataImage = $("#imageSr").prop("files")[0];
            var file_dataVideo = $("#video").prop("files")[0];
            var form_data = new FormData(this);
            form_data.append("imageSr", file_dataImage);
            form_data.append("imageCrop", file_dataImageCrop);
            form_data.append("video", file_dataVideo);
            $.ajax({
                url: "{{ route('Dashboard.appMedia') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                },
                success: function (appInfo) {
                    $('.statusBtn').css({ 'background': '#F9BE67'});
                    $('.app-info-status i').css({ 'color': '#F9BE67'});
                    $('.app-info-status p').text("اپليکيشن شما با موفقيت بارگذاري شد. براي انتشار کارشناسان مربوطه در اسرع وقت بررسي خواهند کرد.");
                    $('#appInfo').html(appInfo);
                    window.location.hash = $('.MediaTitle a').attr("href");
                    location.reload();
                }
            });
        });


        // $(".changeAPP").fadeIn();
        $(".changeAPP").css('display', 'inline-block');
        $(document).on("click", "#tooltip-1 a", function () {
            // $(".changeAPP").fadeIn("fast");
            $(".changeAPP").css('display', 'inline-block');
            // $(".changeAPPHide").fadeOut("fast");
            $(".changeAPPHide").css('display', 'none');
        });
        $(document).on("click", "#tooltip-2 a", function () {
            // $(".changeAPP").fadeOut("fast");
            $(".changeAPP").css('display', 'none');
            // $(".changeAPPHide").fadeIn("fast");
            $(".changeAPPHide").css('display', 'inline-block');
        });
        $(document).on("click", "#tooltip-3 a", function () {
            // $(".changeAPP").fadeOut("fast");
            $(".changeAPP").css('display', 'none');
            // $(".changeAPPHide").fadeIn("fast");
            $(".changeAPPHide").css('display', 'inline-block');
        });
        $(document).on("click", "#tooltip-4 a", function () {
            // $(".changeAPP").fadeOut("fast");
            $(".changeAPP").css('display', 'none');
            // $(".changeAPPHide").fadeIn("fast");
            $(".changeAPPHide").css('display', 'inline-block');
        });
        $(document).on("click", ".editTlt", function () {
            $("#appEditForm").fadeIn("slow");
            $("#appInfo").css('display', 'none');
            // $("#appInfo").fadeOut("fast");
            $(".changeAPP").removeClass("editTlt");
            $(".changeAPP").addClass("showTlt");
            $(".changeAPP").text("جزييات");
            $(".pageAppTitle").text("ويرايش");
            $(".INFOtlt").addClass("appINFO");
            $(".INFOtlt").removeClass("AppTitle");
            $(".appINFO").attr("title", "ويرايش");
        });
        $(document).on("click", ".showTlt", function () {
            $("#appInfo").fadeIn("slow");
            $("#appEditForm").css('display', 'none');
            // $("#appEditForm").fadeOut("fast");
            $(".changeAPP").removeClass("showTlt");
            $(".changeAPP").addClass("editTlt");
            $(".changeAPP").text("ويرايش");
            $(".pageAppTitle").text("جزييات");
            $(".INFOtlt").addClass("AppTitle");
            $(".INFOtlt").removeClass("appINFO");
            $(".AppTitle").attr("title", "جزييات");

        });
        $(document).on("click", ".appINFO a", function () {
            $(".pageAppTitle").text("ويرايش");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".AppTitle a", function () {
            $(".pageAppTitle").text("جزييات");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".CommentTitle a", function () {
            $(".pageAppTitle").text("کامنت ها");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".ApkTitle a", function () {
            $(".pageAppTitle").text("APK");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".MediaTitle a", function () {
            $(".pageAppTitle").text("مديا");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".StatTitle a", function () {
            $(".pageAppTitle").text("آمار بازديد");
            if (location.hash) {
                location.hash = '';
            }
        });
        $(document).on("click", ".DownloadTitle a", function () {
            $(".pageAppTitle").text("آمار دانلود");
            if (location.hash) {
                location.hash = '';
            }
        });
        $('#icon').bind('change', function () {
            var _URL = window.URL || window.webkitURL;
            var appIdIcon = $("#imageAppDelete").attr('rel');
            var file_data = $("#icon").prop("files")[0];   // Getting the properties of file from file field
            var form_data = new FormData();                  // Creating object of FormData class
            form_data.append("icon", file_data);
            form_data.append("appIdIcon", appIdIcon);

            var ext = $('#icon').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png']) == -1) {
                notif({
                    msg: "تصوير پروفايل حتما بايد با پسوند 'png' باشد.",
                    type: "error",
                    // width : 400,
                    // height : 60,
                    position: "right",
                    autohide: true,
                    opacity: 1,
                    multiline: 0,
                    fade: 0,
                    bgcolor: "",
                    color: "",
                    timeout: 5000,
                    zindex: null,
                    offset: 0,
                    callback: null,
                    clickable: false,
                    animation: "slide",
                });
                $('#icon').val("");
                a = 0;
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 1000000) {
                    notif({
                        msg: "حداکثر حجم تصوير ۱ مگابايت مي باشد.",
                        type: "error",
                        // width : 400,
                        // height : 60,
                        position: "right",
                        autohide: true,
                        opacity: 1,
                        multiline: 0,
                        fade: 0,
                        bgcolor: "",
                        color: "",
                        timeout: 5000,
                        zindex: null,
                        offset: 0,
                        callback: null,
                        clickable: false,
                        animation: "slide",
                    });
                    $('#icon').val("");
                    a = 0;
                } else {
                    a = 1;

                }

                if (a == 1) {
                    var icon = $(this)[0].files[0];
                    img = new Image();
                    var imgwidth = 512;
                    var imgheight = 512;
                    var maxwidth = 512;
                    var maxheight = 512;
                    img.src = _URL.createObjectURL(icon);
                    img.onload = function () {
                        imgwidth = this.width;
                        imgheight = this.height;
                        $("#width").text(imgwidth);
                        $("#height").text(imgheight);
                        if (imgwidth == maxwidth && imgheight == maxheight) {
                            $.ajax({
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: form_data,                         // Setting the data attribute of ajax with file_data
                                type: 'post',
                                url: "{{ route('Dashboard.appIconEdit') }}",
                                success: function (results) {
                                    $('#results').html(results);
                                }
                            });
                            $("#response").text("");
                        } else {
                            $('#icon').val("");
                            $('.uploader-text-icon').text('');

                            notif({
                                msg: "تصوير آپلود شده بايد در اندازه هاي " + maxwidth + "X" + maxheight + " باشد",
                                type: "error",
                                // width : 400,
                                // height : 60,
                                position: "right",
                                autohide: true,
                                opacity: 1,
                                multiline: 0,
                                fade: 0,
                                bgcolor: "",
                                color: "",
                                timeout: 5000,
                                zindex: null,
                                offset: 0,
                                callback: null,
                                clickable: false,
                                animation: "slide",
                            });
                        }
                    };
                    img.onerror = function () {
                        $('#icon').val("");
                        $('.uploader-text-icon').text('');
                        $("#response").text("not a valid file: " + icon.type);
                    };
                }
            }
        });


        $(document).ready(function () {
            $.validator.setDefaults({
                submitHandler: function () {
                    var data = new FormData($('form#appEditForm')[0]);
                    $.ajax({
                        url: "{{route('Dashboard.appEdit')}}",
                        type: "POST",
                        data: data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //$("#preview").fadeOut();
                            // $("#err").fadeOut();
                        },
                        success: function (data) {
                            $('.statusBtn').css({ 'background': '#F9BE67'});
                            $('.app-info-status i').css({ 'color': '#F9BE67'});
                            $('.app-info-status p').text("اپليکيشن شما با موفقيت بارگذاري شد. براي انتشار کارشناسان مربوطه در اسرع وقت بررسي خواهند کرد.");
                            $("#loadingPageApp").fadeOut("slow");
                            $(".resultShow").html('').html(data);
                            $('html,body').animate({
                                scrollTop: $(".firstBlur").offset().top
                            }, 'slow');
                            notif({
                                msg: "ويرايش شما با موفقيت انجام شد. پس از تاييد تغييرات از طرف اوپراتور مربوطه اپليکيشن شما در سامانه قرار ميگيرد.",
                                type: "success",
                                // width: 400,
                                height: 60,
                                position: "right",
                                autohide: true,
                                opacity: 1,
                                multiline: 0,
                                fade: 0,
                                bgcolor: "",
                                color: "",
                                timeout: 5000,
                                zindex: null,
                                offset: 0,
                                callback: null,
                                clickable: false,
                                animation: "slide",
                            });

                        },
                        error: function (e) {

                        }
                    });
                }
            });
            $().ready(function () {
                $("#appEditForm").validate({
                    rules: {
                        title: "required",
                        made: "required",
                        age: "required",
                        sex: "required",
                        detail: "required",
                    },
                    messages: {
                        title: "لطفا عنوان ابليکيشن خود را وارد نماييد",
                        made: "لطفا فيلد ساخت را انتخاب نماييد",
                        age: "لطفا فيلد رده سني را انتخاب نماييد",
                        sex: "لطفا فيلد جنسيت را انتخاب نماييد",
                        detail: "لطفا توضيحات خود را وارد نماييد",
                    }
                });
            });


            // Configure/customize these variables.
            var showChar = 110;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "<i class='fa fa-angle-left'></i>";
            var lesstext = "<i class='fa fa-angle-right'></i>";
            $('.more').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });
            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });

        // $("#loadingPageApp").fadeIn("slow");
        $('[data-fancybox="images"]').fancybox({

            afterLoad: function (instance, current) {
                var pixelRatio = window.devicePixelRatio || 1;

                if (pixelRatio > 1.5) {
                    current.width = current.width / pixelRatio;
                    current.height = current.height / pixelRatio;
                }
            },
            beforeLoad: function() {

            },
            beforeClose: function() {

            },
        });
        $(document).ready(function () {
            $(".fancybox").fancybox({
                afterShow: function () {
                    // After the show-slide-animation has ended - play the vide in the current slide
                    this.content.find('video').trigger('play')

                    // Attach the ended callback to trigger the fancybox.next() once the video has ended.
                    this.content.find('video').on('ended', function () {
                        $.fancybox.next();
                    });


                },

            });

        });
        $("#colorselector").on('change', function () {
            var color = $(this).val();
            if (color == 'green') {
                $(".video_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/playButtonGreen.png'}}");
                $(".image_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/glass_green.png'}}");
            }
            if (color == 'blue') {
                $(".video_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/playButtonBlue.png'}}");
                $(".image_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/glass_blue.png'}}");
            }
            if (color == 'purple') {
                $(".video_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/playButtonpurple.png'}}");
                $(".image_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/glass_purple.png'}}");
            }
            if (color == 'yellow') {
                $(".video_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/playButtonYellow.png'}}");
                $(".image_img").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/glass_yellow.png'}}");
            }
            return false;
        });

        $('#image').bind('change', function () {
            var ext = $('#image').val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['jpg', 'jpeg']) == -1) {

                notif({
                    msg: "تصوير ابليکيشن حتما بايد با پسوند 'jpg' يا 'jpeg' باشد.",
                    type: "error",
                    width: 400,
                    height: 60,
                    position: "right",
                    autohide: true,
                    opacity: 1,
                    multiline: 0,
                    fade: 0,
                    bgcolor: "",
                    color: "",
                    timeout: 5000,
                    zindex: null,
                    offset: 0,
                    callback: null,
                    clickable: false,
                    animation: "slide",
                });

                $('#image').val("");
                a = 0;
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 1000000) {
                    notif({
                        msg: " حداکثر حجم تصوير ۱ مگابايت مي باشد.",
                        type: "error",
                        width: 400,
                        height: 60,
                        position: "right",
                        autohide: true,
                        opacity: 1,
                        multiline: 0,
                        fade: 0,
                        bgcolor: "",
                        color: "",
                        timeout: 5000,
                        zindex: null,
                        offset: 0,
                        callback: null,
                        clickable: false,
                        animation: "slide",
                    });
                    $('#image').val("");
                    a = 0;
                } else {
                    a = 1;
                }

            }
        });

        $('#video').bind('change', function () {
            var ext = $('#video').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['mp4', 'mkv']) == -1) {
                notif({
                    msg: " ويديو ابليکيشن حتما بايد با پسوند 'mp4' يا 'mkv' باشد.",
                    type: "error",
                    width: 400,
                    height: 60,
                    position: "right",
                    autohide: true,
                    opacity: 1,
                    multiline: 0,
                    fade: 0,
                    bgcolor: "",
                    color: "",
                    timeout: 5000,
                    zindex: null,
                    offset: 0,
                    callback: null,
                    clickable: false,
                    animation: "slide",
                });
                $('#video').val("");
                a = 0;
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 10000000) {
                    notif({
                        msg: "حداکثر حجم ويديو ۱۰ مگابايت مي باشد.",
                        type: "error",
                        width: 400,
                        height: 60,
                        position: "right",
                        autohide: true,
                        opacity: 1,
                        multiline: 0,
                        fade: 0,
                        bgcolor: "",
                        color: "",
                        timeout: 5000,
                        zindex: null,
                        offset: 0,
                        callback: null,
                        clickable: false,
                        animation: "slide",
                    });
                    $('#video').val("");
                    a = 0;
                } else {
                    a = 1;

                }

            }
        });



        $(document).on("click", "#imageAppDelete", function () {
            var appId = $(this).attr('rel');
            bootbox.confirm({
                title: "حذف مديا",
                message: "آيا از حذف اين تصوير اطمينان داريد؟",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> خير'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> بله'
                    }
                },
                callback: function (resultDeletImage) {
                    console.log('This was logged in the callback: ' + resultDeletImage);
                    if (resultDeletImage == true) {
                        $.ajax({
                            type: 'POST',
                            data: {appId: appId},
                            url: '{{ route('Dashboard.appImageDelete') }}',
                            success: function () {
                                $("#image").val('');
                                $("#imageName").val('');
                                $(".uploader-text-iconImage").text('');
                                $(".removeImagePart").fadeOut('slow');
                                $("#croppedImg").removeAttr('src');
                                $(".middleImage").remove();
                                $("#imageAppDelete").remove();
                                $("#videoImage").attr("src", "{{$LocalhostPath.'/Assets/Developer/images/videoAvatar.jpg'}}");
                            }
                        });
                    }

                }
            });
            return false;
        });
        $("#icon").change(function () {
            var emptyIcon = false;
            $('#icon').each(function () {
                if ($(this).val() == '') {
                    emptyIcon = true;
                }
            });
            if (emptyIcon) {
                // $('#secStepAppInfoNext').attr('disabled', 'disabled');
            } else {
                // $('#secStepAppInfoNext').removeAttr('disabled');
            }
        });


        $(document).on("click", "#videoAppDelete", function () {
            var appId = $(this).attr('rel');
            bootbox.confirm({
                title: "حذف مديا",
                message: "آيا از حذف اين ويديو اطمينان داريد؟",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> خير'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> بله'
                    }
                },
                callback: function (resultDeletImage) {
                    console.log('This was logged in the callback: ' + resultDeletImage);
                    if (resultDeletImage == true) {
                        $.ajax({
                            type: 'POST',
                            data: {appId: appId},
                            url: '{{ route('Dashboard.appVideoDelete') }}',
                            success: function () {
                                $("#video").val('');
                                $("#videoName").val('');
                                $(".uploader-text-iconVideo").text('');
                                $(".removeVideoPart").fadeOut('slow');
                            }
                        });
                    }
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript"
            src="{{ asset('Assets/Developer/js/custom-file-input.js') }}"></script>
    <div id="results"></div>
@stop

