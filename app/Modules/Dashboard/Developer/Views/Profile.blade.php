@extends('Master_Web::IndexPage')
@section('body')
    <div class="wrapper firstBlur">
        <section class="section-cards developerTotal">
            <div id="loadingPageApp">
                <div class="loader loader--style8" title="7">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                         xml:space="preserve">
                        <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s"
                                     dur="0.6s"
                                     repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s"
                                     dur="0.6s"
                                     repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                        <rect x="8" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                        <rect x="16" y="10" width="4" height="10" fill="#333" opacity="0.2">
                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s"
                                     dur="0.6s" repeatCount="indefinite"/>
                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s"
                                     repeatCount="indefinite"/>
                        </rect>
                    </svg>
                </div>
            </div>
            <div class="container pageAppTotal">
                <div class="pageAppIcon">
                    <input type="file" name="logo" id="logo" class="inputfile inputfile-6"
                           value="" rel=""/>
                    <label for="logo" id="logoLable"><img
                                src="{{$LocalhostPath.'/uploads/avatars/'.$Developer->logo}}" class="logo_img"></label>
                </div>
                <div class="pageAppTitle">ويرايش پروفايل</div>
                <a href="{{$LocalhostPath.'/Dashboard/Developer/PageView'}}" class="pageAppBack">داشبورد <i
                            class="fa fa-angle-left"></i></a>
                <div class="tabTotal" id="ProfTotla">
                    <form id="profileForm" name="profileForm" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="row tab-app-edit">
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <div class="input-group-addon">نام</div>
                                    <input type="text" name="name" id="name" value="{{$Developer->name}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">نام خانوادگي</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="text" name="family" id="family" value="{{$Developer->family}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">نام نمايشي</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="text" name="nickName" id="nickName" value="{{$Developer->nickName}}">
                                </div>
                            </div>
                        </div>
                        <div class="row tab-app-edit">
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">ايميل</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="email" autocomplete="off" style="text-align: right" name="email"
                                           id="email" value="{{$Developer->email}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">رمز عبور جديد</div>
                                    @if (!$Developer->password)
                                        <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    @endif
                                    <input type="password" autocomplete="new-password" name="password" id="password">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    @if (!$Developer->password)
                                        <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    @endif
                                    <div class="input-group-addon">تکرار رمز عبور</div>
                                    <input id="confirm_password" name="confirm_password" autocomplete="new-password"
                                           type="password">
                                </div>
                            </div>
                        </div>
                        <div class="row tab-app-edit">
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">تلفن همراه</div>
                                    <input type="text" style="text-align: right" name="mobile" id="mobile"
                                           value="{{$Developer->mobile}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">تلفن تماس</div>
                                    <input type="text" style="text-align: right" name="phone" id="phone"
                                           value="{{$Developer->phone}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">فکس</div>
                                    <input type="text" style="text-align: right" name="fax" id="fax"
                                           value="{{$Developer->fax}}">
                                </div>
                            </div>
                        </div>
                        <div class="row tab-app-edit">
                            <div class="col-sm-12 col-md-6 col-lg-6 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">کد ملي</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="text" style="text-align: right" name="melliCart" id="melliCart"
                                           value="{{$Developer->melliCart}}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">کد پستي</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="text" style="text-align: right" name="postalCode" id="postalCode"
                                           value="{{$Developer->postalCode}}">
                                </div>
                            </div>
                        </div>
                        <div class="row tab-app-edit">
                            <div class="col-sm-12 col-md-12 col-lg-12 position-relative">
                                <div class="input-group">
                                    <div class="input-group-addon">آدرس</div>
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 25px;left: 25px;">*</i>
                                    <textarea name="address" id="address"
                                              cols="30"
                                              rows="10">{{$Developer->address}}</textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="{{$Developer->id}}" id="DevId" name="DevId">
                        <input name="submit" type="submit" value="ثبت" id="app-edit-submit">
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </section>
    </div>


    <script>

        $.validator.setDefaults({
            submitHandler: function () {
                $("#loadingPageApp").fadeIn("slow");
                var DevId = $('#DevId').val();
                var name = $('#name').val();
                var family = $('#family').val();
                var nickName = $('#nickName').val();
                var email = $('#email').val();
                var password = $('#password').val();
                var mobile = $('#mobile').val();
                var phone = $('#phone').val();
                var fax = $('#fax').val();
                var melliCart = $('#melliCart').val();
                var postalCode = $('#postalCode').val();
                var address = $('#address').val();
                $.ajax({
                    url: "{{route('Dashboard.profileEdit')}}",
                    type: "POST",
                    data: {
                        DevId: DevId,
                        name: name,
                        family: family,
                        nickName: nickName,
                        email: email,
                        password: password,
                        mobile: mobile,
                        phone: phone,
                        fax: fax,
                        melliCart: melliCart,
                        postalCode: postalCode,
                        address: address
                    },
                    success: function (data) {
                        $("#loadingPageApp").fadeOut("slow");
                        $('html,body').animate({
                            scrollTop: $(".firstBlur").offset().top
                        }, 'slow');
                        notif({
                            msg: "ويرايش شما با موفقيت انجام شد.",
                            type: "success",
                            width: 400,
                            height: 60,
                            position: "right",
                            autohide: true,
                            opacity: 1,
                            multiline: 0,
                            fade: 0,
                            bgcolor: "",
                            color: "",
                            timeout: 5000,
                            zindex: null,
                            offset: 0,
                            callback: null,
                            clickable: false,
                            animation: "slide",
                        });
                    },
                });
            }
        });

        $().ready(function () {
            $("#profileForm").validate({
                rules: {
                    name: "required",
                    family: "required",
                    nickName: "required",
                    address: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    mobile: {
                        number: true
                    },
                    phone: {
                        number: true
                    },
                    fax: {
                        number: true
                    },
                    melliCart: {
                        required: true,
                        number: true
                    },
                    postalCode: {
                        required: true,
                        number: true
                    },
                    password: {
                        @if (!$Developer->password)
                        required: true,
                        @endif
                        minlength: 5
                    },
                    confirm_password: {
                        @if (!$Developer->password)
                        required: true,
                        @endif
                        minlength: 5,
                        equalTo: "#password"
                    },


                },
                messages: {
                    name: "لطفا نام خود را وارد نماييد",
                    family: "لطفا نام خانوادگي خود را وارد نماييد",
                    nickName: "لطفا نام نمايشي خود را وارد نماييد",
                    address: "لطفا آدرس خود را وارد نماييد",
                    password: {
                        required: "لطفا رمز عبور خود را وارد نمایید.",
                        minlength: "رمز عبور حداقل ۵ حرف بايد باشد."
                    },
                    confirm_password: {
                        required: "لطفا تکرار رمز عبور را وارد نمایید.",
                        minlength: "رمز عبور حداقل ۵ حرف بايد باشد.",
                        equalTo: "لطفا رمز عبور يکسان را وارد نماييد"
                    },
                    melliCart: {
                        number: "لطفا براي کد ملي عدد وارد نماييد.",
                        required: "لطفا کد ملی خود را وارد نمایید.",
                    },
                    email: {
                        email: "لطفا يک ايميل معتبر وارد نماييد.",
                        required: "لطفا ايميل خود را وارد نمایید.",
                    },
                    mobile: {
                        number: "لطفا براي تلفن همراه عدد وارد نماييد.",
                    },
                    phone: {
                        number: "لطفا براي تلفن تماس عدد وارد نماييد.",
                    },
                    fax: {
                        number: "لطفا براي فکس عدد وارد نماييد.",
                    },

                }
            });


        });
        $("#logo").change(function () {
            var emptylogo = false;
            $('#logo').each(function () {
                if ($(this).val() == '') {
                    emptylogo = true;
                }
            });
            if (emptylogo) {

            } else {

            }
        });
        $('#logo').bind('change', function () {
            var _URL = window.URL || window.webkitURL;
            var DevId = $("#DevId").val();
            var file_data = $("#logo").prop("files")[0];   // Getting the properties of file from file field
            var form_data = new FormData();                  // Creating object of FormData class
            form_data.append("logo", file_data);
            form_data.append("DevId", DevId);

            var ext = $('#logo').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['jpg']) == -1) {
                notif({
                    msg: "تصوير پروفایل حتما بايد با پسوند 'jpg' باشد.",
                    type: "error",
                    // width : 400,
                    // height : 60,
                    position: "right",
                    autohide: true,
                    opacity: 1,
                    multiline: 0,
                    fade: 0,
                    bgcolor: "",
                    color: "",
                    timeout: 5000,
                    zindex: null,
                    offset: 0,
                    callback: null,
                    clickable: false,
                    animation: "slide",
                });
                $('#logo').val("");
                a = 0;
            } else {
                var picsize = (this.files[0].size);
                if (picsize > 1000000) {
                    notif({
                        msg: "حداکثر حجم تصوير ۱ مگابايت مي باشد.",
                        type: "error",
                        // width : 400,
                        // height : 60,
                        position: "right",
                        autohide: true,
                        opacity: 1,
                        multiline: 0,
                        fade: 0,
                        bgcolor: "",
                        color: "",
                        timeout: 5000,
                        zindex: null,
                        offset: 0,
                        callback: null,
                        clickable: false,
                        animation: "slide",
                    });
                    $('#logo').val("");
                    a = 0;
                } else {
                    a = 1;

                }

                if (a == 1) {
                    var logo = $(this)[0].files[0];
                    img = new Image();
                    var imgwidth = 512;
                    var imgheight = 512;
                    var maxwidth = 512;
                    var maxheight = 512;
                    img.src = _URL.createObjectURL(logo);
                    img.onload = function () {
                        imgwidth = this.width;
                        imgheight = this.height;
                        $("#width").text(imgwidth);
                        $("#height").text(imgheight);
                        if (imgwidth == maxwidth && imgheight == maxheight) {
                            $.ajax({
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: form_data,                         // Setting the data attribute of ajax with file_data
                                type: 'post',
                                url: "{{ route('Dashboard.profileLogoEdit') }}",
                                success: function (results) {
                                    $('#results').html(results);
                                }
                            });
                            $("#response").text("");
                        } else {
                            $('#logo').val("");
                            $('.uploader-text-logo').text('');

                            notif({
                                msg: "تصویر آپلود شده بايد در اندازه هاي " + maxwidth + "X" + maxheight + " باشد",
                                type: "error",
                                // width : 400,
                                // height : 60,
                                position: "right",
                                autohide: true,
                                opacity: 1,
                                multiline: 0,
                                fade: 0,
                                bgcolor: "",
                                color: "",
                                timeout: 5000,
                                zindex: null,
                                offset: 0,
                                callback: null,
                                clickable: false,
                                animation: "slide",
                            });
                        }
                    };
                    img.onerror = function () {
                        $('#logo').val("");
                        $('.uploader-text-logo').text('');
                        $("#response").text("not a valid file: " + logo.type);
                    };
                }
            }
        });
    </script>
    <div id="results"></div>
@stop