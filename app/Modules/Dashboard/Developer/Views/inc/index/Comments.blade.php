<div class="card-body card-comment-body">
    <h4>کامنت های اخیر</h4>
    <div class="scrollbar-ripe-comment">
        <div class="comment-body">
            <?php $CommRowView = 0; ?>
            @foreach($comments as $item)
                <div class="well well-sm comment-item">
                    <div class="comment-item-head">
                        <ul>
                            <li>
                                <div class="comment-item-pic">

                                    @if (!empty($clients[$item->id]->image))
                                        @if($clients[$item->id]->image == 'null')
                                            <img src="{{$LocalhostPath.'/Assets/Master/Web/images/avatar.png'}}">
                                        @elseif($clients[$item->id]->image == NULL)
                                            <img src="{{$LocalhostPath.'/Assets/Master/Web/images/avatar.png'}}">
                                        @else
                                            <img src="{{$LocalhostPath.'/uploads/avatars/'.$clients[$item->id]->image}}">
                                        @endif
                                    @endif

                                </div>
                            </li>
                            <li class="comment-item-head-li">
                                <div class="comment-item-title-icon">
                                    <div class="comment-item-title">
                                        @if (!empty($clients[$item->id]->name) && !empty($clients[$item->id]->family) )
                                            {{$clients[$item->id]->name}} {{$clients[$item->id]->family}}
                                        @endif
                                    </div>
                                    <div class="comment-item-icon">
                                        <img src="{{$LocalhostPath.'/uploads/App/'.$apps[$item->id]->icon}}">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <?php
                                $dateComment = explode(' ',$item->created_at);
                                ?>
                                <div class="comment-item-date">
                                    <span class="comment-item-time">{{$dateComment['1']}}</span>
                                    <span class="comment-item-day">{{$dateComment['0']}}</span>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                    <div class="comment-item-body more">
                        {{$item->comment}}
                    </div>
                </div>
                <?php  $CommRowView++; ?>
            @endforeach
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            // Configure/customize these variables.
            var showChar = 110;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "<i class='fa fa-angle-left'></i>";
            var lesstext = "<i class='fa fa-angle-right'></i>";


            $('.more').each(function() {
                var content = $(this).html();

                if(content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });

            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
</div>