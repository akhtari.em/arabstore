<h4>گزارش های دانلود</h4>
<h5 class="DlCartTitle">تمامی اپلیکیشن ها</h5>
<div class="resultappDlCart"></div>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
        @foreach($appCarousel as $item)
            <div class="appDlIcone">
                <a class="appDlHref" href="#" rel="{{$item->id}}">
                    <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon}}">
                </a>
            </div>
        @endforeach
        <div class="appDlIcone">
            <a class="appDlHref active" href="#" rel="0">
                <img src="{{ asset('Assets/Developer/images/app_icon.png') }}" alt="">
            </a>
        </div>
        <script>
            appDllist();
            $(document).on('click', '.appDlHref', function () {
                var appDllId = $(this).attr('rel');
                var DevDllId = $("#DevId").val();
                $("#EditdeveloperID").val(appDllId);
                $("#EditappID").val(DevDllId);
                $(".appDlHref").removeClass("active");
                $(this).addClass("active");
                appDllist();
                return false;
            });

            function appDllist() {
                var appDllId = $('#EditdeveloperID').val();
                var DevDllId = $('#DevId').val();
                $("#loadingPage").fadeIn();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('Dashboard.appDlChart') }}",
                    data: {DevDllId: DevDllId, appDllId: appDllId},
                    success: function (appDlCart) {
                        $(".resultappDlCart").html('').html(appDlCart);
                    }
                });
            }


        </script>
    </div>
</div>