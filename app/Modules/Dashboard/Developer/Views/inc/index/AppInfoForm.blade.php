<div class="modal fade" id="modalSucsessUploadApp" role="dialog" aria-labelledby="myModalLabel"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-content-app-info">
            <div class="modal-header-app-info">
                <div class="avatar-app-info">
                    <img src="{{ asset('Assets/Developer/image/app_icon.png') }}" alt="">
                </div>
                <h4 class="modal-title-app-info">مشخصات اپليکيشن</h4>
                <div class="clearfix"></div>
            </div>
            <div class="modal-body">
                <form id="fupForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="developerID" id="developerID">
                    <input type="hidden" name="appID" id="appID">
                    <div class="tab-app-info">
                        <button class="tablinks-app-info defaultOpen App_tab_1 active" id="defaultOpen_app-info"
                                disabled="disabled">
                            مشخصات کلي اب
                        </button>
                        <button class="tablinks-app-info App_tab_2" disabled="disabled">
                            مشخصات بصري
                        </button>
                    </div>
                    <div id="App_tab_1" class="tabcontent-app-info">
                        <div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6 position-relative">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="text" name="title" id="title" class="requiredInputFirstAppStep"
                                           placeholder="عنوان ابليکيشن">
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 position-relative app-info-select">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 17px;left: 40px;">*</i>
                                    <select id="made" name="made">
                                        <option value="iranian">ساخت ايران</option>
                                        <option value="foreign">خارجي</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6 app-info-select">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 17px;left: 40px;">*</i>
                                    <select id="age" name="age" class="requiredInputFirstAppStep">
                                        <option value="">رده سني</option>
                                        <option value="under5">زير ۵ سال</option>
                                        <option value="5-8">بين ۵ تا ۸ سال</option>
                                        <option value="above9">بالاي ۹ سال</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6 app-info-select">
                                    <select id="sex" name="sex">
                                        <option value="0">جنسيت</option>
                                        <option value="female">دختر</option>
                                        <option value="male">بسر</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 position-relative">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 25px;left: 25px;">*</i>
                                    <textarea name="detail" class="requiredInputFirstAppStep" id="detail" cols="30"
                                              rows="10" placeholder="توضيحات"></textarea>
                                </div>
                            </div>
                            <span class="tablinks-app-info tabbtn-app-info" id="firstStepAppInfo">
                                مرحله بعد
                            </span>
                            <span class="tablinks-app-info tabbtn-app-info close" data-dismiss="modal">انصراف</span>
                        </div>
                    </div>
                    <div id="App_tab_2" class="tabcontent-app-info">
                        <div>
                            <div class="row uploadAppRow">
                                <div class="col-sm-12 col-md-12 col-lg-12 position-relative">
                                    <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                                    <input type="file" name="icon" id="icon" class="inputfile inputfile-6"/>
                                    <label for="icon">
                                        <strong> آيکون (png.)
                                            <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg" width="20"
                                                 height="17" viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                        </strong>
                                        <span class="uploader-text-icon"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row uploadAppRow">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="file" name="image" id="image" class="inputfile inputfile-6">
                                            <label for="image">
                                                <strong> تصوير (jpg|.jpeg.)
                                                    <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg"
                                                         width="20"
                                                         height="17" viewBox="0 0 20 17">
                                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                                    </svg>
                                                </strong>
                                                <span class="uploader-text-icon"></span>
                                            </label>

                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <img id="croppedImg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row uploadAppRow">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <input type="file" name="video" id="video" class="inputfile inputfile-6"/>
                                    <label for="video"><strong> ويديو (mkv|.mp4.)
                                            <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg" width="20"
                                                 height="17" viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                            </svg>
                                        </strong>
                                        <span class="uploader-text-icon"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row uploadAppRow">
                                <p id="erroricon1" style="display:none; color:#FF0000;">
                                    تصوير آيکون حتما بايد با پسوند 'png' باشد.
                                </p>
                                <p id="erroricon2" style="display:none; color:#FF0000;">
                                    حداکثر حجم تصوير ۱ مگابايت مي باشد.
                                </p>
                                <p id="errorimage1" style="display:none; color:#FF0000;">
                                    تصوير ابليکيشن حتما بايد با پسوند 'jpg' يا 'jpeg' باشد.
                                </p>
                                <p id="errorimage2" style="display:none; color:#FF0000;">
                                    حداکثر حجم تصوير ۱ مگابايت مي باشد.
                                </p>
                                <p id="errorvideo1" style="display:none; color:#FF0000;">
                                    ويديو ابليکيشن حتما بايد با پسوند 'mp4' يا 'mkv' باشد.
                                </p>
                                <p id="errorvideo2" style="display:none; color:#FF0000;">
                                    حداکثر حجم ويديو ۱۰ مگابايت مي باشد.
                                </p>
                                <p id="response" style="color:#FF0000;"></p>
                            </div>
                            <span class="tablinks-app-info tabbtn-app-info" id="secStepAppInfoPrev"
                                  onclick="openAppInfo(event, 'App_tab_1')">
                                مرحله قبل
                            </span>
                            <button class="tablinks-app-info tabbtn-app-info upload-result" id="secStepAppInfoNext">
                                افزودن اطلاعات
                            </button>
                            {{--<input class="tablinks-app-info tabbtn-app-info" type="submit" value="افزودن اطلاعات" id="secStepAppInfoNext">--}}
                            <span class="tablinks-app-info tabbtn-app-info close" data-dismiss="modal">انصراف</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div><!--.modal-body-->
        </div>
    </div>

</div><!--.modal-->

<div id="appInfo"></div>
<div class="cropHolder">
    <div id="cropWrapper">
        <img id="inputimage" src="">
    </div>
    <div class="cropInputs">
        <div class="inputtools">
            <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/horizontal.png') }}">
                    </span>
                <span>افقي</span>
            </p>
            <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
        </div>
        <div class="inputtools">
            <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/vertical.png') }}">
                    </span>
                <span>عمودي</span>
            </p>
            <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
        </div>
        <br>
        <button class="cropButtons" id="zplus">
            <img src="{{ asset('Assets/Developer/images/add.png') }}">
        </button>
        <button class="cropButtons" id="zminus">
            <img src="{{ asset('Assets/Developer/images/minus.png') }}">
        </button>
        <br>
        <button id="cropSubmit">انجام</button>
        <button id="closeCrop">بستن</button>
    </div>
</div>
<script>
    $("#image").finecrop({
        viewHeight: 500,
        cropWidth: 697,
        cropHeight: 340,
        cropInput: 'inputimage',
        cropOutput: 'croppedImg',
        zoomValue: 50
    });
</script>
<script type="text/javascript">
    $("#fupForm").on('submit', function (e) {
        e.preventDefault();
        var file_dataIcon = $("#icon").prop("files")[0];
        var file_dataImageCrop = $("#croppedImg").attr("src");
        var file_dataImage = $("#image").prop("files")[0];
        var file_dataVideo = $("#video").prop("files")[0];
        var form_data = new FormData(this);
        form_data.append("icon", file_dataIcon);
        form_data.append("image", file_dataImage);
        form_data.append("imageCrop", file_dataImageCrop);
        form_data.append("video", file_dataVideo);
        $.ajax({
            url: "{{ route('Dashboard.appUpdate') }}",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            beforeSend: function () {
            },
            success: function (appInfo) {
                $('#appInfo').html(appInfo);
                location.reload();
            }
        });
    });
    $(".close").on('click', function () {
        $('#title').val('');
        $('#croppedImg').removeAttr('src');
        $('#detail').val('');
        $('#imageSpan').text('');
        $('.uploader-text-icon').text('');
        $('#icon').val('');
        $('.uploader-text-image').text('');
        $('#image').val('');
        $('.uploader-text-video').text('');
        $('#video').val('');
        $("#App_tab_1").css("display", "block");
        $("#App_tab_2").css("display", "none");
        $('#firstStepAppInfo').removeAttr('onclick');
        $('#firstStepAppInfo').attr('disabled', 'disabled');
        $('header').removeClass('header-zindex');
        $('header').removeClass('zindex-on');


        $('.modal-backdrop').removeClass('blackModal');
        $('.section-carousel').removeClass('position-relative');
        $('.section-carousel').removeClass('zindex-on');
        $('.header').removeClass('zindex-on');
        $('.firstBlur').removeClass('zindex-off');

        $("#logout-btn").addClass('logout-btn');
        $(".header__menu--btn").attr('disabled', false);
        $('.header__menu--btn').attr("data-toggle", "modal");
        $('.header__menu--btn').attr("data-target", "#modalMenu");
        $('.header__logo a').attr("href", "{{$LocalhostPath.'/Master/Web'}}");
        var developerID = $("#developerID").val();
        var appID = $("#appID").val();
        $.ajax({
            type: 'POST',
            url: "{{ route('Dashboard.appCancel') }}",
            data: {developerID: developerID, appID: appID},
            success: function (appCancel) {
                $('#appInfo').html(appCancel);
            }
        });
    });
</script>
<script>
    function openAppInfo(evt, cityName) {
        // Declare all variables
        var i, tabcontentAppInfo, tablinksAppInfo;

        // Get all elements with class="tabcontentAppInfo" and hide them
        tabcontentAppInfo = document.getElementsByClassName("tabcontent-app-info");
        for (i = 0; i < tabcontentAppInfo.length; i++) {
            tabcontentAppInfo[i].style.display = "none";
        }
        // Get all elements with class="tablinksAppInfo" and remove the class "active"
        tablinksAppInfo = document.getElementsByClassName("tablinks-app-info");
        for (i = 0; i < tablinksAppInfo.length; i++) {
            tablinksAppInfo[i].className = tablinksAppInfo[i].className.replace(" active", "");
        }
        // Show the current tab, and add an "active" class to the link that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
        $("." + cityName).addClass('active');
    }

    // document.getElementById("defaultOpen_app-info").click();
    $('#firstStepAppInfo').attr('disabled', 'disabled');
    $('body').on('input', ".requiredInputFirstAppStep", function () {
        var empty = false;
        $('.requiredInputFirstAppStep').each(function () {
            if ($(this).val() == '') {
                empty = true;
            }
        });
        if (empty) {
            $('#firstStepAppInfo').attr('disabled', 'disabled');
            $('#firstStepAppInfo').removeAttr('onclick');
        } else {
            $('#firstStepAppInfo').removeAttr('disabled');
            $('#firstStepAppInfo').attr('onclick', 'openAppInfo(event, "App_tab_2")');
        }
    });
    $("#age").change(function () {
        var age = $(this).val();
        if (age == '') {
            $('#firstStepAppInfo').attr('disabled', 'disabled');
        } else {
            var empty2 = false;
            $('.requiredInputFirstAppStep').each(function () {
                if ($(this).val() == '') {
                    empty2 = true;
                }
            });
            if (empty2) {
                $('#firstStepAppInfo').attr('disabled', 'disabled');
            } else {
                $('#firstStepAppInfo').removeAttr('disabled');
            }
        }
    });

    $('#secStepAppInfoNext').attr('disabled', 'disabled');
    $("#icon").change(function () {
        var emptyIcon = false;
        $('#icon').each(function () {
            if ($(this).val() == '') {
                emptyIcon = true;
            }
        });
        if (emptyIcon) {
            $('#secStepAppInfoNext').attr('disabled', 'disabled');
        } else {
            $('#secStepAppInfoNext').removeAttr('disabled');
        }
    });
    $('#secStepAppInfoNext').prop("disabled", true);
    $('#icon').bind('change', function () {
        if ($('#secStepAppInfoNext').attr('disabled', false)) {
            $('#secStepAppInfoNext').attr('disabled', true);
        }
        var ext = $('#icon').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png']) == -1) {
            $('#erroricon1').slideDown("slow");
            $('#erroricon2').slideUp("slow");
            $('#icon').val("");
            a = 0;
        } else {
            var picsize = (this.files[0].size);
            if (picsize > 1000000) {
                $('#erroricon2').slideDown("slow");
                $('#icon').val("");
                a = 0;
            } else {
                a = 1;
                $('#erroricon2').slideUp("slow");
            }
            $('#erroricon1').slideUp("slow");
            if (a == 1) {
                $('#secStepAppInfoNext').attr('disabled', false);
            }
        }
    });

    $('#video').bind('change', function () {
        var ext = $('#video').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['mp4', 'mkv']) == -1) {
            $('#errorvideo1').slideDown("slow");
            $('#errorvideo2').slideUp("slow");
            $('#video').val("");
            a = 0;
        } else {
            var picsize = (this.files[0].size);
            if (picsize > 10000000) {
                $('#errorvideo2').slideDown("slow");
                $('#video').val("");
                a = 0;
            } else {
                a = 1;
                $('#errorvideo2').slideUp("slow");
            }
            $('#errorvideo1').slideUp("slow");
        }
    });
</script>