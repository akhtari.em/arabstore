<div class="card-body card-body-Uploader">
    <script>(function (e, t, n) {
            var r = e.querySelectorAll("html")[0];
            r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
        })(document, window, 0);</script>
    @if (!$developerName || !$developerFamily || !$developerNik || !$developerEmail || !$developerMCart || !$developerPCode || !$developerAddress)
        <a href="#" style="display: block;" class="unCompPro">
        <form method="POST" id="sendApp">
            <div class="form-group">
                <input type="hidden" name="file" id="poster" class="inputfile inputfile-4"/>
                <label for="poster">
                    <figure>
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="uploader-text">افزودن برنامه</span>
                        <div class="clear"></div>
                    </figure>
                    <div class="footer-card footer-card-Uploader" style="display: block;">
                        <span class="footer-card-Uploader-title"> <i
                                    class="fa fa-clone"></i>  درگ کنيد اينجا </span>
                    </div>
                </label>
            </div>
        </form>
        </a>
        <script type="text/javascript">
            $(document).on("click", ".unCompPro", function (e) {
                bootbox.confirm({
                    title: "پروفايل کاربري",
                    message: "براي بارگذاري اپليکيشن ابتدا بايد پروفايل کاربري خود را تکميل نماييد. مايل هستيد وارد صفحه ي پروفايل خود شويد؟",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> خير'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> بله'
                        }
                    },
                    callback: function (result) {
                        console.log('This was logged in the callback: ' + result);
                        if (result == true) {
                            window.location.href = "{{$LocalhostPath.'/Dashboard/Developer/Profile'}}";
                        }

                    }
                });
                return false;
            });
        </script>
    @else
        <form method="POST" id="sendApp" name="sendApp"
              action="{{ route('fileUploadPost') }}" enctype="multipart/form-data">
            <div class="form-group">
                <input type="file" name="file" id="poster" class="inputfile inputfile-4"/>
                <label for="poster">
                    <figure>
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="uploader-text">افزودن برنامه</span>
                        <div class="clear"></div>
                    </figure>
                    <div class="footer-card footer-card-Uploader" style="display: block;">
                        <div class="progress">
                            <div class="bar"></div>
                            <div class="percent">0%</div>
                        </div>
                        <span class="footer-card-Uploader-title"> <i
                                    class="fa fa-clone"></i>  درگ کنيد اينجا </span>
                    </div>
                </label>
                <input type="hidden" name="publishDate" value="{{time()}}">
                <input type="hidden" name="developerId"
                       value="{{Auth::guard('developer')->id()}}">
            </div>
            {{-- <input type="submit"  value="Submit" class="btn btn-success"> --}}
        </form>
    @endif

</div>