<div class="col-sm-12 col-md-6 col-lg-3">
    <div class="card card-img-holder">
        <div class="card-body">
            <div class="tab">
                {{--<button class="tablinks tablinks_Week Sale_Btn"--}}
                {{--onclick="openCards_Week(event, 'Sale_Week')">فروش--}}
                {{--</button>--}}
                <button class="tablinks tablinks_Week Download_Btn Download_Btn_Week defaultOpen"
                        onclick="openCards_Week(event, 'Download_Week')"
                        id="defaultOpen_Week">دانلود
                </button>
                <button class="tablinks tablinks_Week Views_Btn Views_Btn_Week"
                        onclick="openCards_Week(event, 'Views_Week')">بازدید
                </button>
            </div>
            {{--<div id="Sale_Week" class="tabcontent tabcontent_Week">--}}
            {{----}}
            {{--</div>--}}
            <div id="Download_Week" class="tabcontent tabcontent_Week"></div>
            <div id="View_Week" class="tabcontent tabcontent_Week"></div>
            <input type="hidden" id="results_download_Week" value="{{$results_download_Week}}">
            <input type="hidden" id="results_view_Week" value="{{$results_view_Week}}">
            <script type="text/javascript">
                function openCards_Week(evt, cityName) {
                    var i, tabcontent_Week, tablinks_Week;
                    tabcontent_Week = document.getElementsByClassName("tabcontent_Week");
                    for (i = 0; i < tabcontent_Week.length; i++) {
                        tabcontent_Week[i].style.display = "none";
                    }
                    tablinks_Week = document.getElementsByClassName("tablinks_Week");
                    for (i = 0; i < tablinks_Week.length; i++) {
                        tablinks_Week[i].className = tablinks_Week[i].className.replace(" active_Week", "");
                    }
                    document.getElementById(cityName).style.display = "block";
                    evt.currentTarget.className += " active_Week";
                }

                // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen_Week").click();
            </script>
        </div>
        <div class="footer-card footer-card_download_Week">{{$dl_Week_row}}</div>
        <div class="footer-card footer-card_view_Week">{{$view_Week_row}}</div>
    </div>
</div>
<div class="col-sm-12 col-md-6 col-lg-3">
    <div class="card card-img-holder">
        <div class="card-body">
            <div class="tab">
                {{--<button class="tablinks tablinks_Three_Months Sale_Btn"--}}
                {{--onclick="openCards_Three_Months(event, 'Sale_Three_Months')"--}}
                {{--id="defaultOpen_Three_Months">فروش--}}
                {{--</button>--}}

                <button class="tablinks tablinks_Three_Months Download_Btn Download_Btn_Three_Months defaultOpen"
                        onclick="openCards_Three_Months(event, 'Download_Three_Months')"
                        id="defaultOpen_Three_Months">دانلود
                </button>
                <button class="tablinks tablinks_Three_Months Views_Btn Views_Btn_Three_Months"
                        onclick="openCards_Three_Months(event, 'Views_Three_Months')">بازدید
                </button>
            </div>
            {{--<div id="Sale_Three_Months" class="tabcontent tabcontent_Three_Months">--}}
            {{----}}
            {{--</div>--}}
            <div id="Download_Three_Months" class="tabcontent tabcontent_Three_Months"></div>
            <div id="View_Three_Months" class="tabcontent tabcontent_Three_Months"></div>

            <script type="text/javascript">
                function openCards_Three_Months(evt, cityName) {
                    var i, tabcontent_Three_Months, tablinks_Three_Months;
                    tabcontent_Three_Months = document.getElementsByClassName("tabcontent_Three_Months");
                    for (i = 0; i < tabcontent_Three_Months.length; i++) {
                        tabcontent_Three_Months[i].style.display = "none";
                    }
                    tablinks_Three_Months = document.getElementsByClassName("tablinks_Three_Months");
                    for (i = 0; i < tablinks_Three_Months.length; i++) {
                        tablinks_Three_Months[i].className = tablinks_Three_Months[i].className.replace(" active_Three_Months", "");
                    }
                    document.getElementById(cityName).style.display = "block";
                    evt.currentTarget.className += " active_Three_Months";
                }

                // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen_Three_Months").click();
            </script>
        </div>
        <div class="footer-card footer-card_download_Three_Months">{{$dl_Three_Months_row}}</div>
        <div class="footer-card footer-card_view_Three_Months">{{$view_Three_Months_row}}</div>
        <input type="hidden" id="results_download_Three_Months"
               value="{{$results_download_Three_Months}}">
        <input type="hidden" id="results_view_Three_Months"
               value="{{$results_view_Three_Months}}">
    </div>
</div>
<div class="col-sm-12 col-md-6 col-lg-3">
    <div class="card card-img-holder">
        <div class="card-body">
            <div class="tab">
                {{--<button class="tablinks tablinks_Six_Months Sale_Btn defaultOpen"--}}
                {{--onclick="openCards_Six_Months(event, 'Sale_Six_Months')"--}}
                {{-->فروش--}}
                {{--</button>--}}

                <button class="tablinks tablinks_Six_Months Download_Btn Download_Btn_Six_Months defaultOpen"
                        onclick="openCards_Six_Months(event, 'Download_Six_Months')"
                        id="defaultOpen_Six_Months">دانلود
                </button>
                <button class="tablinks tablinks_Six_Months Views_Btn Views_Btn_Six_Months"
                        onclick="openCards_Six_Months(event, 'Views_Six_Months')">بازدید
                </button>
            </div>
            {{--<div id="Sale_Six_Months" class="tabcontent tabcontent_Six_Months">--}}
            {{----}}
            {{--</div>--}}
            <div id="Download_Six_Months" class="tabcontent tabcontent_Six_Months"></div>
            <div id="View_Six_Months" class="tabcontent tabcontent_Six_Months"></div>
            <script type="text/javascript">
                function openCards_Six_Months(evt, cityName) {
                    var i, tabcontent_Six_Months, tablinks_Six_Months;
                    tabcontent_Six_Months = document.getElementsByClassName("tabcontent_Six_Months");
                    for (i = 0; i < tabcontent_Six_Months.length; i++) {
                        tabcontent_Six_Months[i].style.display = "none";
                    }
                    tablinks_Six_Months = document.getElementsByClassName("tablinks_Six_Months");
                    for (i = 0; i < tablinks_Six_Months.length; i++) {
                        tablinks_Six_Months[i].className = tablinks_Six_Months[i].className.replace(" active_Six_Months", "");
                    }
                    document.getElementById(cityName).style.display = "block";
                    evt.currentTarget.className += " active_Six_Months";
                }

                // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen_Six_Months").click();
            </script>
        </div>
        <div class="footer-card footer-card_download_Six_Months">{{$dl_Six_Months_row}}</div>
        <div class="footer-card footer-card_view_Six_Months">{{$view_Six_Months_row}}</div>
        <input type="hidden" id="results_download_Six_Months"
               value="{{$results_download_Six_Months}}">
        <input type="hidden" id="results_view_Six_Months" value="{{$results_view_Six_Months}}">
    </div>
</div>