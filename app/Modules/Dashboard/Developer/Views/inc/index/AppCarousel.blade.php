<div class="row">
    <section class="app-carousel ">
        <?php $row = 1; ?>
        @foreach($appCarousel as $item)
            <div class="app-item-carousel">
                <a class="appHref" href="{{$LocalhostPath.'/Dashboard/Developer/PageApp/'.$item->id}}"
                   rel="{{$item->id}}">
                    @if ($item->status == 0)
                        <i class="statusBtnCarousel" style="background:#F9BE67"></i>
                    @elseif ($item->status == 1)
                        <i class="statusBtnCarousel" style="background:#0F9D58"></i>
                    @elseif ($item->status == 2)
                        <i class="statusBtnCarousel" style="background:#bbff1c"></i>
                    @elseif ($item->status == 3)
                        <i class="statusBtnCarousel" style="background:#ff1322"></i>
                    @endif
                    <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon}}">
                </a>
            </div>
            <?php $row++; ?>
        @endforeach
        @if ($row == 1)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 2)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 3)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 4)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 5)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 6)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 7)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 8)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 9)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @elseif ($row == 10)
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
            <img src="{{ asset('Assets/Developer/images/app-item-carousel.jpg') }}" class="avatarImage">
        @endif
    </section>

    <script type="text/javascript">
        $(".app-carousel").slick({
            dots: false,
            infinite: false,
            slidesToShow: 10,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 2500,
                    settings: {
                        arrows: true,
                        centerMode: false,
                        slidesToShow: 10
                    }
                },
                {
                    breakpoint: 1025,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 520,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        slidesToShow: 2
                    }
                }
            ]
        });

    </script>
</div>