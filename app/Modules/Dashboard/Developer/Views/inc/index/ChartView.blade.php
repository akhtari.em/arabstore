<script>
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {

            exportEnabled: false,
            animationEnabled: true,
            theme: "light2",
            title: {
                text: ""
            },
            axisY: {
                title: "تعداد بازدید ها",
                fontFamily: "Al-Jazeera-Arabic-Bold-font",
                includeZero: true,
                prefix: "",
                lineThickness: 0,
                tickThickness: 0,
                interval: 1
            },
            toolTip: {
                shared: true
            },
            legend: {
                fontSize: 8,
                markerMargin: 0,
                itemWrap: true,
                dockInsidePlotArea: false,
                cursor: "pointer",
                fontFamily: "Al-Jazeera-Arabic-Bold-font",
                horizontalAlign: "right",
                itemclick: toggleDataSeries
            },
            data: [
                    <?php $Cntrow1 = 0; ?>
                    @foreach($appCarousel as $item)
                {
                    type: "splineArea",
                    showInLegend: true,
                    name: "{{$item->title}}",
                    yValueFormatString: "#,##0.##",
                    dataPoints: [
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -12 month' ))}}),
                            y: {{$count_twelve_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -11 month' ))}}),
                            y: {{$count_eleven_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -10 month' ))}}),
                            y: {{$count_ten_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -9 month' ))}}),
                            y: {{$count_nine_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -8 month' ))}}),
                            y: {{$count_eight_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -7 month' ))}}),
                            y: {{$count_seven_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -6 month' ))}}),
                            y: {{$count_six_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -5 month' ))}}),
                            y: {{$count_five_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -4 month' ))}}),
                            y: {{$count_four_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -3 month' ))}}),
                            y: {{$count_three_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -2 month' ))}}),
                            y: {{$count_tow_month[$Cntrow1]}} },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -1 month' ))}}),
                            y: {{$count_one_month[$Cntrow1]}} }
                    ]
                },
                    <?php $Cntrow1++; ?>
                    @endforeach
                {
                    type: "splineArea",
                    showInLegend: true,
                    name: "تماهی اپلیکیشنها",
                    yValueFormatString: "#,##0.##",
                    dataPoints: [
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -12 month' ))}}), y: {{$sum_twelve_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -11 month' ))}}), y: {{$sum_eleven_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -10 month' ))}}), y: {{$sum_ten_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -9 month' ))}}), y: {{$sum_nine_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -8 month' ))}}), y: {{$sum_eight_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -7 month' ))}}), y: {{$sum_seven_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -6 month' ))}}), y: {{$sum_six_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -5 month' ))}}), y: {{$sum_five_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -4 month' ))}}), y: {{$sum_four_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -3 month' ))}}), y: {{$sum_three_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -2 month' ))}}), y: {{$sum_tow_month}} },
                        {x: new Date({{date( 'Y, m', strtotime( $date . ' -1 month' ))}}), y: {{$sum_one_month}} }
                    ]
                },
            ],

        });
        chart.render();

        function toggleDataSeries(e) {
            if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else {
                e.dataSeries.visible = true;
            }
            e.chart.render();
        }
    }
</script>
<h4>آمار بازدید</h4>
<div id="chartContainer" style="height: 370px; max-width: 100%; margin: 0px auto;"></div>