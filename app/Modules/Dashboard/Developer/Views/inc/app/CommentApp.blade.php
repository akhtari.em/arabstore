<div class="scrollbar-ripe-comment scrollbar-AppView">
    <?php $CommRowView = 0; ?>
    @foreach ($resultComment as $item)
        <div class="well well-sm comment-item comment-item-AppView">
            <div class="comment-item-head">
                <ul>
                    <li>
                        <div class="comment-item-pic">
                            @if($clients[$CommRowView]->image == 'null')
                                <img src="{{$LocalhostPath.'/Assets/Master/Web/images/avatar.png'}}">
                            @elseif($clients[$CommRowView]->image == NULL)
                                <img src="{{$LocalhostPath.'/Assets/Master/Web/images/avatar.png'}}">
                            @else
                                <img src="{{$LocalhostPath.'/uploads/avatars/'.$clients[$CommRowView]->image}}">

                            @endif
                        </div>
                    </li>
                    <li class="comment-item-head-li">
                        <div class="comment-item-title-icon">
                            <div class="comment-item-title">
                                {{$clients[$CommRowView]->name}} {{$clients[$CommRowView]->family}}
                            </div>
                            <div class="comment-item-icon">
                                <img src="{{$LocalhostPath.'/uploads/App/'.$apps[$CommRowView]->icon}}">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                        $dateComment = explode(' ', $item->created_at);
                        ?>
                        <div class="comment-item-date">
                            <span class="comment-item-time">{{$dateComment['1']}}</span>
                            <span class="comment-item-day">{{$dateComment['0']}}</span>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </div>
            <div class="comment-item-body more">
                {{$item->comment}}
            </div>
        </div>
        <?php  $CommRowView++; ?>
    @endforeach
</div>