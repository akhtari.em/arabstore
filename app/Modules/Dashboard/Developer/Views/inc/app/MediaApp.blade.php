<form id="appMediaForm" name="appMediaForm" method="POST" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <input type="hidden" name="appIDMedia" id="appIDMedia" value="{{$resultApp->id}}">
    <input type="hidden" name="imageName" id="imageName" value="{{$resultApp->image}}">
    <input type="hidden" name="videoName" id="videoName" value="{{$resultApp->video}}">
    <input type="hidden" name="developerIDMedia" id="developerIDMedia"
           value="{{$resultApp->developer}}">
    <div class="row uploadAppRow padding-bottom image-app-edit">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12">
                    <input type="file" name="imageSr" id="imageSr"
                           class="inputfile inputfile-6">
                    <label for="imageSr">
                        <strong> تصوير (jpg|.jpeg.)
                            <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg"
                                 width="20"
                                 height="17" viewBox="0 0 20 17">
                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                            </svg>
                        </strong>
                        <span class="uploader-text-icon uploader-text-iconImage">{{$resultApp->image}}</span>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-7 text-center">
                    @if ($resultApp->image)
                        <a href="{{$LocalhostPath.'/uploads/App/'.$resultApp->image}}"
                           data-fancybox="images" data-width="2048" data-height="1365">
                            <img id="croppedImg"
                                 src="{{$LocalhostPath.'/uploads/App/'.$resultApp->image}}">
                            <div class="middle middleImage">
                                @if (Cookie::get('cookie_val') == 'green')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_green.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'purple')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_purple.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'blue')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_blue.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'yellow')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_yellow.png'}}"
                                         class="middle_img image_img">
                                @else
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_green.png'}}"
                                         class="middle_img image_img">
                                @endif
                            </div>
                        </a>
                        <br>

                        <a href="#" id="imageAppDelete" rel="{{$resultApp->id}}">حذف</a>
                    @else
                        <img id="croppedImg">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12">
                    <input type="file" name="video" id="video" class="inputfile inputfile-6"
                           value="{{$resultApp->video}}"/>
                    <label for="video" id="videoLable"><strong> ويديو (mkv|.mp4.)
                            <svg class="uploadIconSvg" xmlns="http://www.w3.org/2000/svg"
                                 width="20"
                                 height="17" viewBox="0 0 20 17">
                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                            </svg>
                        </strong>
                        <span class="uploader-text-icon uploader-text-iconVideo">{{$resultApp->video}}</span>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-7 text-center removeVideoPart"
                     id="VideoTotal"
                     @if (!$resultApp->video) style='display:none'@endif>
                    <div id="AppVideoTotal">
                        <a class="fancybox" data-fancybox data-width="640" data-height="360"
                           href="{{$LocalhostPath.'/uploads/App/'.$resultApp->video}}">
                            @if ($resultApp->image)
                                <img src="{{$LocalhostPath.'/uploads/App/'.$resultApp->image}}"
                                     class="videoAppImage" id="videoImage">
                            @else
                                <img src="{{$LocalhostPath.'/Assets/Developer/images/videoAvatar.jpg'}}"
                                     class="videoAppImage" id="videoImage">
                            @endif
                            <div class="middle middleVideo">
                                @if (Cookie::get('cookie_val') == 'green')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/playButtonGreen.png'}}"
                                         class="middle_img video_img">
                                @elseif (Cookie::get('cookie_val') == 'purple')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/playButtonpurple.png'}}"
                                         class="middle_img video_img">
                                @elseif (Cookie::get('cookie_val') == 'blue')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/playButtonBlue.png'}}"
                                         class="middle_img video_img">
                                @elseif (Cookie::get('cookie_val') == 'yellow')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/playButtonYellow.png'}}"
                                         class="middle_img video_img">
                                @else
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/playButtonGreen.png'}}"
                                         class="middle_img video_img">
                                @endif
                            </div>
                        </a>
                    </div>
                    <video width="640" height="320" controls id="myVideo"
                           style="display:none;">
                        <source src="{{$LocalhostPath.'/uploads/App/'.$resultApp->video}}"
                                type="video/mp4">
                        Your browser doesn't support HTML5 video tag.
                    </video>
                    <br>
                    <a href="#" id="videoAppDelete" rel="{{$resultApp->id}}">حذف</a>
                </div>
            </div>
        </div>
    </div>
    <div id="INFO" class="row uploadAppRow padding-bottom image-app-edit">
        <div class='containers'>
            <div class="row group">
                <div class="col-md-12 float-right">
                    <h5 style="padding: 2%;color: #ffab00;font-size: 18px;">اسکرين شات</h5>
                </div>
            </div>
            <br/>
            <div class="row group">
                @foreach($resultAppMedia as $item)
                    <div class="col-lg-2 col-md-3 col-sm-4 text-center mediaItem">
                        <a href="{{$LocalhostPath.'/uploads/App/ScreenShot/'.$item->videoImage}}" data-fancybox="images" data-width="2048" >
                            <img class="imageMediaItem" src="{{$LocalhostPath.'/uploads/App/ScreenShot/'.$item->videoImage}}">

                            <div class="middle middleImage middleImageMeida">
                                @if (Cookie::get('cookie_val') == 'green')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_green.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'purple')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_purple.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'blue')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_blue.png'}}"
                                         class="middle_img image_img">
                                @elseif (Cookie::get('cookie_val') == 'yellow')
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_yellow.png'}}"
                                         class="middle_img image_img">
                                @else
                                    <img src="{{$LocalhostPath.'/Assets/Developer/images/glass_green.png'}}"
                                         class="middle_img image_img">
                                @endif
                            </div>

                        </a>
                        <br>
                        <a href="#" class="mediaAppDelete mediaAppDelete_{{$item->id}}" rel="{{$item->id}}">حذف</a>
                    </div>
                @endforeach
            </div>
            <br/>
            <div class='element' id='div_1'>
                                            <span class='add btn green-bg bg-success'
                                                  style='margin: 10px;'>اضافه کردن</span>
                <input type='hidden' name='Ps[]' value='1'>
                <input type='hidden' name='croppedImgMedia[1]' id='croppedMedia1'>
                <div class='form-group col-xs-12'>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-9 col-md-8 col-sm-12">
                                <input type="file" name="imageMedia[1]" id="imageMedia1"
                                       class="inputfile inputfile-6">
                                <label for="imageMedia1">
                                    <strong> تصوير (jpg|.jpeg.)
                                        <svg class="uploadIconSvg"
                                             xmlns="http://www.w3.org/2000/svg"
                                             width="20"
                                             height="17" viewBox="0 0 20 17">
                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                    </strong>
                                    <span class="uploader-text-icon uploader-text-iconImageMedia1"></span>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-7 text-center croppedImgMediaImage">
                                <img id="croppedImgMedia1">
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <hr/>
                <br/>
                <div class="cropHolder">
                    <div id="cropWrapper1">
                        <img id="inputimageMedia1" src="">
                    </div>
                    <div class="cropInputs">
                        <div class="inputtools">
                            <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/horizontal.png') }}">
                    </span>
                                <span>افقي</span>
                            </p>
                            <input type="range" class="cropRange" name="xmove1" id="xmove1"
                                   min="0" value="0">
                        </div>
                        <div class="inputtools">
                            <p>
                    <span>
                        <img src="{{ asset('Assets/Developer/images/vertical.png') }}">
                    </span>
                                <span>عمودي</span>
                            </p>
                            <input type="range" class="cropRange" name="ymove1" id="ymove1"
                                   min="0" value="0">
                        </div>
                        <br>
                        <button class="cropButtons" id="zplus1">
                            <img src="{{ asset('Assets/Developer/images/add.png') }}">
                        </button>
                        <button class="cropButtons" id="zminus1">
                            <img src="{{ asset('Assets/Developer/images/minus.png') }}">
                        </button>
                        <br>
                        <button id="cropSubmit1" class="cropSubmit">انجام</button>
                        <button id="closeCrop1">بستن</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            // Add new element
            $(".add").click(function () {
                // Finding total number of elements added
                var total_element = $(".element").length;
                // last <div> with element class id
                var lastid = $(".element:last").attr("id");
                var split_id = lastid.split("_");
                var nextindex = Number(split_id[1]) + 1;
                var max = 5;
                // Check total number elements
                if (total_element < max) {
                    // Adding new div containers after last occurance of element class
                    $(".element:last").after("<div class='element' id='div_" + nextindex + "'></div>");
                    // Adding element to <div>
                    var StoreAddressNo = 'تصوير';
                    $("#div_" + nextindex).append("<input type='hidden' name='Ps[]' value='1'><input type='hidden' name='croppedImgMedia[" + nextindex + "]' id='croppedMedia" + nextindex + "'><div class='form-group col-xs-12'><div class='col-lg-6 col-md-12 col-sm-12'><div class='row'><div class='col-lg-9 col-md-8 col-sm-12'><input type='file' name='imageMedia[" + nextindex + "]' id='imageMedia" + nextindex + "' class='inputfile inputfile-6'><label for='imageMedia" + nextindex + "'><strong> تصوير (jpg|.jpeg.)<svg class='uploadIconSvg' xmlns='http://www.w3.org/2000/svg' width='20' height='17' viewBox='0 0 20 17'><path d='M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z'/></svg></strong><span class='uploader-text-icon uploader-text-iconImageMedia" + nextindex + "'></span></label></div><div class='col-lg-3 col-md-3 col-sm-7 text-center croppedImgMediaImage'><img id='croppedImgMedia" + nextindex + "'></div></div></div><span id='remove_" + nextindex + "' class='remove red-bg btn bg-danger'  style='margin: 10px;'>حذف</span></div><br/><hr/><br/><div class='cropHolder'><div id='cropWrapper" + nextindex + "'><img id='inputimageMedia" + nextindex + "' src=''></div><div class='cropInputs'><div class='inputtools'><p><span><img src='{{ asset('Assets/Developer/images/horizontal.png') }}'></span><span>افقي</span></p><input type='range' class='cropRange' name='xmove" + nextindex + "' id='xmove" + nextindex + "' min='0' value='0'></div><div class='inputtools'><p><span><img src='{{ asset('Assets/Developer/images/vertical.png') }}'></span><span>عمودي</span></p><input type='range' class='cropRange' name='ymove" + nextindex + "' id='ymove" + nextindex + "' min='0' value='0'></div><br><button class='cropButtons' id='zplus" + nextindex + "'><img src='{{ asset('Assets/Developer/images/add.png') }}'></button><button class='cropButtons' id='zminus" + nextindex + "'><img src='{{ asset('Assets/Developer/images/minus.png') }}'></button><br><button class='cropSubmit' id='cropSubmit" + nextindex + "'>انجام</button><button id='closeCrop" + nextindex + "'>بستن</button></div></div>");
                    $("#imageMedia" + nextindex).finecrop({
                        viewHeight: 500,
                        cropWidth: 340,
                        cropHeight: 340,
                        cropInput: 'inputimageMedia' + nextindex,
                        cropOutput: 'croppedImgMedia' + nextindex,
                        zoomValue: 50
                    });
                    $("#croppedImgMedia" + nextindex).on("load", function () {
                        var croppedMediaSrc1 = $("#croppedImgMedia" + nextindex).attr("src");
                        $("#croppedMedia"+ nextindex).attr("value", croppedMediaSrc1);
                    });
                }
            });
            // Remove element
            $('.containers').on('click', '.remove', function () {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];
                // Remove <div> with id
                $("#div_" + deleteindex).remove();
            });
        });
        $("#imageMedia1").finecrop({
            viewHeight: 500,
            cropWidth: 340,
            cropHeight: 340,
            cropInput: 'inputimageMedia1',
            cropOutput: 'croppedImgMedia1',
            zoomValue: 50,


        });
        $('#croppedImgMedia1').on('load', function () {
            var croppedMediaSrc1 = $("#croppedImgMedia1").attr("src");
            $("#croppedMedia1").attr("value", croppedMediaSrc1);
        });
    </script>
    <input name="submit" type="submit" value="ثبت" id="app-Media-submit">
    <div class="clearfix"></div>
</form>