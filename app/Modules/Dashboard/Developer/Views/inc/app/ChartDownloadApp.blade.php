<script type="text/javascript">
    $(document).on("click", "#viewDownloadTab", function () {
        var chart = new CanvasJS.Chart("chartDownload", {
                theme: "light2",
                animationEnabled: true,
                title: {text: "", fontSize: 30},
                axisX: {interval: 1, intervalType: "month", valueFormatString: "MMM YYYY"},
                axisY: {
                    title: "تعداد دانلود ها",
                    fontFamily: "Al-Jazeera-Arabic-Bold-font",
                    includeZero: false,
                    prefix: "",
                    lineThickness: 1,
                    tickThickness: 1,
                    interval: 1,
                    valueFormatString: "#0"
                },
                data: [{
                    type: "column",
                    markerSize: 12,
                    xValueFormatString: "MMM, YYYY",
                    yValueFormatString: "###.#",
                    dataPoints: [{
                        x: new Date({{date( 'Y, m', strtotime( $date . ' -12 month' ))}}),
                        y: {{$countDl_twelve_month}} ,
                    },
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -11 month' ))}}),
                            y: {{$countDl_eleven_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -10 month' ))}}),
                            y: {{$countDl_ten_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -9 month' ))}}),
                            y: {{$countDl_nine_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -8 month' ))}}),
                            y: {{$countDl_eight_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -7 month' ))}}),
                            y: {{$countDl_seven_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -6 month' ))}}),
                            y: {{$countDl_six_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -5 month' ))}}),
                            y: {{$countDl_five_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -4 month' ))}}),
                            y: {{$countDl_four_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -3 month' ))}}),
                            y: {{$countDl_three_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -2 month' ))}}),
                            y: {{$countDl_tow_month}} ,
                        }
                        ,
                        {
                            x: new Date({{date( 'Y, m', strtotime( $date . ' -1 month' ))}}),
                            y: {{$countDl_one_month}} ,
                        }
                    ]
                }]
            })
        ;chart.render();
        $(".DlCartTitle").text("'.$AppName->title.'");
    });
</script>
<div id="chartDownload" style="height: 370px; width: 100%;direction: ltr"></div>