<form id="appEditForm" name="appEditForm" method="POST" enctype="multipart/form-data" style="display: none;">
    {!! csrf_field() !!}
    <input type="hidden" name="appID" id="appID" value="{{$resultApp->id}}">
    <input type="hidden" name="developerID" id="developerID"
           value="{{$resultApp->developer}}">
    <div class="row tab-app-edit">
        <div class="col-sm-12 col-md-6 col-lg-6 position-relative">
            <div class="input-group">
                <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 13px;left: 25px;">*</i>
                <div class="input-group-addon">عنوان ابليکيشن</div>
                <input type="text" name="title" id="title" class="requiredInputFirstAppStep"
                       value="{{$resultApp->title}}">
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 position-relative app-info-select">
            <div class="input-group">
                <div class="input-group-addon">ساخت</div>
                <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 17px;left: 40px;">*</i>
                <select id="made" name="made">
                    <option value="" {{($resultApp->made == "")? 'selected' : ''}}>انتخاب نشده
                    </option>
                    <option value="iranian" {{($resultApp->made == "iranian")? 'selected' : ''}}>ايران</option>
                    <option value="foreign" {{($resultApp->made == "foreign")? 'selected' : ''}}>خارجي</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row tab-app-edit">
        <div class="col-sm-12 col-md-6 col-lg-6 app-info-select">
            <div class="input-group">
                <div class="input-group-addon">رده سني</div>
                <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 17px;left: 40px;">*</i>
                <select id="age" name="age" class="requiredInputFirstAppStep">
                    <option value="" {{($resultApp->age == "")? 'selected' : ''}}>انتخاب نشده
                    </option>
                    <option value="under5" {{($resultApp->age == "under5")? 'selected' : ''}}>
                        زير ۵ سال
                    </option>
                    <option value="5-8" {{($resultApp->age == "5-8")? 'selected' : ''}}>
                        بين ۵ تا ۸ سال
                    </option>
                    <option value="above9" {{($resultApp->age == "above9")? 'selected' : ''}}>
                        بالاي ۹ سال
                    </option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 app-info-select">
            <div class="input-group">
                <div class="input-group-addon">جنسيت</div>
                <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 17px;left: 40px;">*</i>
                <select id="sex" name="sex">
                    <option value="" {{($resultApp->sex == "0")? 'selected' : ''}}>انتخاب نشده
                    </option>
                    <option value="female" {{($resultApp->sex == "female")? 'selected' : ''}}>
                        دختر
                    </option>
                    <option value="male" {{($resultApp->sex == "male")? 'selected' : ''}}>
                        بسر
                    </option>
                </select>
            </div>
        </div>
    </div>
    <div class="row tab-app-edit">
        <div class="col-sm-12 col-md-12 col-lg-12 position-relative">
            <div class="input-group">
                <div class="input-group-addon">توضيحات</div>
                <i style="font-family: auto;font-size: 17px;position: absolute;color: #ff1411;min-height: auto;padding: 0;top: 25px;left: 25px;">*</i>
                <textarea name="detail" class="requiredInputFirstAppStep" id="detail"
                          cols="30"
                          rows="10" placeholder="توضيحات">{{$resultApp->detail}}</textarea>
            </div>
        </div>
    </div>
    <input name="submit" type="submit" value="ثبت" id="app-edit-submit">
    <div class="clearfix"></div>
</form>

<div id="appInfo">
    <div class="appInfo">
        <div class="row app-info-item app-info-title">
            <h4>{{$resultApp->title}}</h4>
        </div>
        <div class="row app-info-item app-info-status">
            @if ($resultApp->status == 0)
                <i class="fa fa-info-circle" style="color: #F9BE67" aria-hidden="true"></i><p>اپليکيشن شما با موفقيت
                    بارگذاري شد. براي انتشار کارشناسان مربوطه در اسرع وقت بررسي خواهند کرد.</p>
            @elseif ($resultApp->status == 1)
                <i class="fa fa-info-circle" style="color: #0F9D58" aria-hidden="true"></i><p>اپليکيشن شما پس از بررسي
                    مورد تاييد قرار گرفت و بارگذاري گرديد.</p>
            @elseif ($resultApp->status == 2)
                <i class="fa fa-info-circle" style="color: #bbff1c" aria-hidden="true"></i><p>اپليکيشن شما توسط کارشناس
                    مربوطه ديده شد و در حال بررسي مي باشد.</p>
            @elseif ($resultApp->status == 3)
                <i class="fa fa-info-circle" style="color: #ff1322" aria-hidden="true"></i><p>اپليکيشن شما تاييد نشد و
                    ليست مشکلات بزودي توسط کارشناس مربوطه ارسال خواهد شد.</p>
            @endif
        </div>
        <div class="row app-info-item app-info">
            <span class="app-infoTitle-span">مشخصات</span>
            <ul>
                <li>
                    <b>جنسيت</b>
                    <span>@if ($resultApp->sex == "female") دختر @elseif ($resultApp->sex == "male") پسر @else انتخاب
                        نشده @endif</span>
                </li>
                <li>
                    <b>رده سني</b>
                    <span>@if ($resultApp->age == "under5") زير ۵ سال @elseif ($resultApp->age == "5-8") بين ۵ تا ۸
                        سال @elseif ($resultApp->age == "above9") بالاي ۹ سال @else انتخاب نشده @endif</span>
                </li>
                <li>
                    <b>ساخت</b>
                    <span>@if ($resultApp->made == "iranian") ايران @elseif ($resultApp->made == "foreign") خارجي @else
                            انتخاب نشده @endif</span>
                </li>
                <li id="app-info-LastLi">
                    <b>تاريخ آپلود</b>
                    <span>{{$resultApp->created_at}}</span>
                </li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="pr-wrapp row">
            <div id="pr__rating" class="pr__rating col-xs-12 col-md-7 col-md-push-2">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 text-center">
                        <h5 class="pr__rating-num">
                            <?php
                            if ($totalRateCnt == 0) {
                                $averageRate = 0;
                            }
                            echo $averageRate;
                            ?>
                        </h5>
                        <div class="rating">
                            @if ($averageRate == '0')
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '0.5')
                                <span class="fa fa-star-half-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '1')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '1.5')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-half-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '2')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '2.5')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-half-o"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '3')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '3.5')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-half-o"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '4')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-o"></span>
                            @endif
                            @if ($averageRate == '4.5')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-half-o"></span>
                            @endif
                            @if ($averageRate == '5')
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            @endif
                        </div><!--.rating-->
                        <span class="pr__rating-total">{{$totalRateCnt}}</span>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <div class="row rating-desc">
                            <div class="rating-desc-detail">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $percentage_4_5 = ($btw4_5RateCnt * 100) / ($totalRateCnt + 1);
                                } else {
                                    $percentage_4_5 = ($btw4_5RateCnt * 100) / $totalRateCnt;
                                }
                                ?>
                                <div class="col-xs-2"><span>{{$btw4_5RateCnt}}</span></div>
                                <div class="col-xs-5">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar5" role="progressbar"
                                             aria-valuenow="{{$percentage_4_5}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$percentage_4_5}}%">
                                        </div>
                                    </div><!--.progress-->
                                </div>
                                <div class="col-xs-5">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="clearfix"></div>
                            </div><!--.rating-desc-detail-->
                            <div class="rating-desc-detail">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $percentage_3_4 = ($btw3_4RateCnt * 100) / ($totalRateCnt + 1);
                                } else {
                                    $percentage_3_4 = ($btw3_4RateCnt * 100) / $totalRateCnt;
                                }
                                ?>
                                <div class="col-xs-2"><span>{{$btw3_4RateCnt}}</span></div>
                                <div class="col-xs-5">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar4" role="progressbar"
                                             aria-valuenow="{{$percentage_3_4}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$percentage_3_4}}%">
                                        </div>
                                    </div><!--.progress-->
                                </div>
                                <div class="col-xs-5">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="clearfix"></div>
                            </div><!--.rating-desc-detail-->
                            <div class="rating-desc-detail">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $percentage_2_3 = ($btw2_3RateCnt * 100) / ($totalRateCnt + 1);
                                } else {
                                    $percentage_2_3 = ($btw2_3RateCnt * 100) / $totalRateCnt;
                                }
                                ?>
                                <div class="col-xs-2"><span>{{$btw2_3RateCnt}}</span></div>
                                <div class="col-xs-5">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar3" role="progressbar"
                                             aria-valuenow="{{$percentage_2_3}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$percentage_2_3}}%">
                                        </div>
                                    </div><!--.progress-->
                                </div>
                                <div class="col-xs-5">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="clearfix"></div>
                            </div><!--.rating-desc-detail-->
                            <div class="rating-desc-detail">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $percentage_1_2 = ($btw1_2RateCnt * 100) / ($totalRateCnt + 1);
                                } else {
                                    $percentage_1_2 = ($btw1_2RateCnt * 100) / $totalRateCnt;
                                }
                                ?>
                                <div class="col-xs-2"><span>{{$btw1_2RateCnt}}</span></div>
                                <div class="col-xs-5">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar2" role="progressbar"
                                             aria-valuenow="{{$percentage_1_2}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$percentage_1_2}}%">
                                        </div>
                                    </div><!--.progress-->
                                </div>
                                <div class="col-xs-5">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="clearfix"></div>
                            </div><!--.rating-desc-detail-->
                            <div class="rating-desc-detail">
                                <?php
                                if ($totalRateCnt == 0) {
                                    $percentage_0_1 = ($btw0_1RateCnt * 100) / ($totalRateCnt + 1);
                                } else {
                                    $percentage_0_1 = ($btw0_1RateCnt * 100) / $totalRateCnt;
                                }
                                ?>
                                <div class="col-xs-2"><span>{{$btw0_1RateCnt}}</span></div>
                                <div class="col-xs-5">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar1" role="progressbar"
                                             aria-valuenow="{{$percentage_0_1}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$percentage_0_1}}%">
                                        </div>
                                    </div><!--.progress-->
                                </div>
                                <div class="col-xs-5">
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="clearfix"></div>
                            </div><!--.rating-desc-detail-->
                        </div><!--.rating-desc-->
                    </div>
                </div><!--.row-->
            </div><!--.pr__rating-->
        </div><!--.pr-wrapp-->
    </div>

</div>