<?php

namespace App\Modules\Comment\Admin\Models;

use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{	
	protected  $fillable =[
		'client_id', 'item_id', 'status'
	];
	protected $table = "favorite";
	public $timestamps=true;
	
	public function client()
	{
		return $this->belongsTo(Client::class);
	}
	
	protected $hidden = [
		'created_at', 'updated_at', 'status', 'client_id'
	];
	
	public function app()
	{
		return $this->belongsTo(App::class);
	}
	
}
