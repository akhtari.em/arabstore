<?php
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use App\Modules\Vitrine\Admin\Models\Vitrine;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Http\Controllers\CoreCommon;
use App\Post;




$dd = CoreCommon::osD();
$manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . 'Manifest.json', "r");
$manifest = json_decode($manifestJson, true);
$moduleName = $manifest['title'];
$moduleSide = substr(strrchr(__DIR__, $dd), 1);


Route::group(['prefix' => $moduleName . '/' . $moduleSide, 'namespace' => 'App\Modules\\' . $moduleName . '\\' . $moduleSide . '\Controllers', 'middleware' => ['web']], function () {
    Route::get('/', function () {

//        For Image Path src
        $LocalhostPath = CoreCommon::getLocalhostPath("");

        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];

//        VITRIN(just in  page [Slider])
        $vitrinesItemSlider = [];
        $vitrinesSlider = Vitrine::where('type', 'slider')
            ->latest('id')
            ->get();
        $Sliderrow = 0;
        foreach ($vitrinesSlider as $itemSlider) {
            $vitrinesItemSlider[$Sliderrow] = VitrineItem::where('vitrine_id', $itemSlider->id)
                ->latest('id')
                ->get();
            $Sliderrow++;
        }

//        VITRIN(just in  page [Carousel])
        $vitrinesItemScroll = [];
        $vitrinesItemScroll_2 = [];
        $vitrinesScroll = Vitrine::where('type', 'scroll')
            ->skip(0)
            ->take(4)
            ->latest('id')
            ->get();
        $vitrinesScroll_2 = Vitrine::where('type', 'scroll')
            ->skip(4)
            ->take(100000)
            ->latest('id')
            ->get();


        $Scrollrow = 0;
        foreach ($vitrinesScroll as $itemScroll) {
            $vitrinesItemScroll[$Scrollrow] = VitrineItem::where('vitrine_id', $itemScroll->id)
                ->latest('id')
                ->take('6')->get();

            $ScrollrowRate = 0;
            foreach ($vitrinesItemScroll[$Scrollrow] as $itemRate) {
                $btw_RateCntCategory[$ScrollrowRate] = AppRate::where('app_id', $itemRate->id)
                    ->count();
                $ScrollrowRate++;
            }
            $Scrollrow++;
        }

        $Scrollrow_2 = 0;
        foreach ($vitrinesScroll_2 as $itemScroll_2) {
            $vitrinesItemScroll_2[$Scrollrow_2] = VitrineItem::where('vitrine_id', $itemScroll_2->id)
                ->latest('id')
                ->take('6')->get();

            $Scrollrow_2Rate = 0;
            foreach ($vitrinesItemScroll_2[$Scrollrow_2] as $itemRate_2) {
                $btw_RateCntCategory_2[$Scrollrow_2Rate] = AppRate::where('app_id', $itemRate_2->id)
                    ->count();
                $Scrollrow_2Rate++;
            }
            $Scrollrow_2++;
        }


        return View('Master_Web::IndexPage', compact( 'moduleName','LocalhostPath','vitrinesSlider', 'vitrinesScroll', 'vitrinesScroll_2', 'vitrinesItemSlider', 'vitrinesItemScroll', 'vitrinesItemScroll_2', 'btw_RateCntCategory', 'btw_RateCntCategory_2'));
    });

    //Autocomplate
    Route::post('/fetch', 'Autocomplate@fetch')->name('Web.fetch');

    Route::post('/set', 'CookieColor@set')->name('Web.set');

    Route::get('/events', 'CookieColor@events')->name('Web.events');

    Route::post('/logoutDeveloper', 'LogoutDeveloper@logout')->name('Web.logoutDeveloper');
});

