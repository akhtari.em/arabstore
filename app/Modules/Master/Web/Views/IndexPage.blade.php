<?php
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\Developer\Admin\Models\Developer;
use App\Modules\Menu\Admin\Models\Menu;
use Illuminate\Support\Facades\Auth;

$gameCategory = AppCategory::where('parent', 15)
    ->latest('id')
    ->get();

$homeCategory = AppCategory::where('parent', 37)
    ->latest('id')
    ->get();

//        FOOTER
$menuBottom = Menu::where('position', 'bottom')
    ->latest('_ord')
    ->get();

$url = url()->current();
$urlExp = explode('/', $url);
foreach ($urlExp as $item) {
    $finalPage = $item;
}
$AppCategory = AppCategory::where('id', $finalPage)
    ->first();

if (Auth::guard('developer')->check() == true) {
    $IndexDeveloperId = Auth::guard('developer')->id();
    $Indexdeveloper = Developer::where('id', $IndexDeveloperId)
        ->first();
}

?>
        <!DOCTYPE html>
@if ($moduleName == 'Dashboard')
    <html lang="fa-IR" dir="rtl" class="js">
    @else
        <html lang="fa-IR" dir="rtl" class="no-js">
        @endif

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--[if IE]>
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
            <title>تونس</title>
            <meta name="HandheldFriendly" content="true">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="description" content=" توضيحات در رابطه با صفحه ">
            <meta name="keywords" content=" کلمات کليدي مرتبط با صفحه ">
            <link href="{{ asset('Assets/Master/Web/images/favicon.png') }}" rel="shortcut icon">

            <link rel="stylesheet" type="text/css"
                  href="{{ asset('Assets/Master/Web/css/bootstrap-css/bootstrap.css') }}">
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('Assets/Master/Web/css/bootstrap-css/bootstrap-theme.css') }}">
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('Assets/Master/Web/css/style.css') }}">
            @if (Cookie::get('cookie_val') == 'green')
                <link rel="stylesheet" id="themeColor"
                      type="text/css" href="{{ asset('Assets/Master/Web/css/green.css') }}">
            @elseif(Cookie::get('cookie_val') == 'purple')
                <link rel="stylesheet" id="themeColor"
                      type="text/css" href="{{ asset('Assets/Master/Web/css/purple.css') }}">
            @elseif(Cookie::get('cookie_val') == 'blue')
                <link rel="stylesheet" id="themeColor"
                      type="text/css" href="{{ asset('Assets/Master/Web/css/blue.css') }}">
            @elseif(Cookie::get('cookie_val') == 'yellow')
                <link rel="stylesheet" id="themeColor"
                      type="text/css" href="{{ asset('Assets/Master/Web/css/yellow.css') }}">
            @else
                <link rel="stylesheet" id="themeColor"
                      type="text/css" href="{{ asset('Assets/Master/Web/css/green.css') }}">
            @endif
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('Assets/Master/Web/css/font-awesome.min.css') }}">

            @if ($moduleName == 'App')
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Master/Web/css/fancybox/jquery.fancybox.min.css') }}">
            @endif
            @if (Auth::guard('developer')->check() == true)
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/style.css') }}">
            @endif

            @if ($moduleName == 'Dashboard')
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/carousel/style.css') }}">
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/percentage/asPieProgress.min.css') }}">
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/tab/styles.css') }}">
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/fancybox/jquery.fancybox.min.css') }}">
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/notif/notifIt.css') }}">
                <link rel="stylesheet" type="text/css"
                      href="{{ asset('Assets/Developer/css/crop/fineCrop.css') }}">
            @endif

            <style>
                .search__result__detail {
                    word-wrap: break-word;
                }
            </style>
        </head>
        <body>
        <!--[if lt IE 9]>
        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/html5.js') }}"></script>
        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/css3-mediaqueries.js') }}"></script>
        <![endif]-->

        {{--@if ($moduleName == 'Dashboard')--}}
        {{--<script type="text/javascript" charset="utf-8"--}}
        {{--src="{{ asset('Assets/Developer/js/jquery-1.8.2.min.js') }}"></script>--}}
        {{--@else--}}
        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/jquery-core.min.js') }}"></script>




        {{--@endif--}}
        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/bootstrap-js/bootstrap.min.js') }}"></script>

        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/select-js/countrySelect.min.js') }}"></script>
        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/select-js/colorSelect.js') }}"></script>
        @if (Auth::guard('developer')->check() == true)
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/confirm/bootbox.min.js') }}"></script>

        @endif

        <script type="text/javascript" charset="utf-8"
                src="{{ asset('Assets/Master/Web/js/custom.js') }}"></script>

        @if ($moduleName == 'App')
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Master/Web/js/fancybox/jquery.fancybox.min.js') }}"></script>
        @endif

        @if ($moduleName == 'Dashboard')


            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/carousel/slick.js') }}"></script>
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/percentage/jquery-asPieProgress.js') }}"></script>
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/chart/canvasjs.min.js') }}"></script>
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/fancybox/jquery.fancybox.min.js') }}"></script>
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/jquery.validate.js') }}"></script>
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/notif/notifIt.js') }}"></script>


            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Developer/js/crop/fineCrop.js') }}"></script>

        @endif

        <span class="circleback--small"></span>
        <span class="circleback--large"></span>

        @if (Auth::guard('developer')->check() == true)
            <header class="header">
                <div class="row row_header">
                    <!-- start headr for developer login -->
                    <div class="header__developer">
                        <div class="col-xs-12 col-sm-3 header__logo">
                            <h1 class="header__logo-img">
                                <a href="{{$LocalhostPath.'/Master/Web'}}">
                                    <img class="img-responsive" src="{{ asset('Assets/Master/Web/images/logo.png') }}"
                                         alt="Twannas">
                                </a>
                            </h1>
                        </div>
                        <!--.header__logo-->
                        <div class="header__menu col-xs-6 col-sm-2 bg_mobile">
                            <button type="button" class="header__menu--btn" data-toggle="modal"
                                    data-target="#modalMenu">
                                جميع التطبيقات <span class="glyphicon glyphicon-menu-down"></span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <form class="header__search">
                                <input type="search" name="app" class="search__text" id="search__text_Login">
                                <span class="fa fa-search"></span>
                            </form>
                            <div id="appList_Login"></div>
                            {{ csrf_field() }}
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#appList_Login').fadeOut();
                                    $('body').on('input', "#search__text_Login", function () {
                                        var query = $("#search__text_Login").val();
                                        var _token = $('input[name = "_token"]').val();

                                        $.ajax({
                                            url: "{{ route('Web.fetch') }}",
                                            type: 'POST',
                                            data: {query: query, _token: _token},
                                            success: function (data_Login) {
                                                $('#appList_Login').fadeIn();
                                                $('#appList_Login').html(data_Login);
                                            }
                                        });
                                    });

                                });
                            </script>
                            <!--.header__search__result-->
                        </div>
                        <div class="col-xs-12 col-sm-3 header__login bg_mobile">
                            <div class="header__login1">
                                <div class="login__name">
                                    <p>{{$Indexdeveloper->nickName}}</p>
                                </div>
                                <div class="login__setting cogIcon">
                                    <a href="{{$LocalhostPath.'/Dashboard/Developer/Profile'}}" title="پروفايل"><i class="fa fa-cog"></i></a>
                                </div><!--.login__setting-->
                                <div class="login__setting cogIcon">
                                    <a href=""><i class="fa fa-envelope-o"></i></a>
                                </div><!--.login__setting-->
                                <div class="login__setting cogIcon logout-btn" id="logout-btn">
                                    <span><i class="fa fa-sign-out"></i></span>
                                </div><!--.login__setting-->
                                <script type="text/javascript">
                                    $(document).on("click", ".logout-btn", function (e) {
                                        bootbox.confirm({
                                            title: "خارج شدن از حساب کاربري",
                                            message: "براي خارج شدن از حساب کاربري خود اطمينان داريد؟",
                                            buttons: {
                                                cancel: {
                                                    label: '<i class="fa fa-times"></i> خير'
                                                },
                                                confirm: {
                                                    label: '<i class="fa fa-check"></i> بله'
                                                }
                                            },
                                            callback: function (result) {
                                                console.log('This was logged in the callback: ' + result);
                                                if (result == true) {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '{{ route('Web.logoutDeveloper') }}',
                                                        success: function () {
                                                            window.location.href = "{{$LocalhostPath.'/Developer/Developer/LoginView'}}";
                                                        }
                                                    });
                                                }

                                            }
                                        });
                                    });
                                </script>
                                <div class="color-wrapp">
                                    <form method="get" action="#">
                                        <select id="colorselector">
                                            <option value="green" data-color="#0f9d58"
                                                    @if ((Cookie::get('cookie_val') == 'green') || Cookie::get('cookie_val') == 'null' )  selected="selected" @endif>
                                                green
                                            </option>

                                            <option value="purple" data-color="#6b2852"
                                                    @if (Cookie::get('cookie_val') == 'purple')   selected="selected" @endif>
                                                purple
                                            </option>
                                            <option value="blue" data-color="#283c6b"
                                                    @if (Cookie::get('cookie_val') == 'blue')   selected="selected" @endif>
                                                blue
                                            </option>
                                            <option value="yellow" data-color="#ffaa2c"
                                                    @if (Cookie::get('cookie_val') == 'yellow')   selected="selected" @endif>
                                                yellow
                                            </option>
                                        </select>
                                    </form>
                                </div>
                                <div id="colorResult"></div>
                                <div id="setCook"></div>
                                <div id="setevents"></div>
                                @if (Cookie::get('cookie_val') == 'green')
                                    <input type="hidden" id="curColor" value="green">
                                    <input type="hidden" id="percentageColor" value="#0f9d58">
                                @elseif (Cookie::get('cookie_val') == 'purple')
                                    <input type="hidden" id="curColor" value="purple">
                                    <input type="hidden" id="percentageColor" value="#6B2852">
                                @elseif (Cookie::get('cookie_val') == 'blue')
                                    <input type="hidden" id="curColor" value="blue">
                                    <input type="hidden" id="percentageColor" value="#283C6B">
                                @elseif (Cookie::get('cookie_val') == 'yellow')
                                    <input type="hidden" id="curColor" value="yellow">
                                    <input type="hidden" id="percentageColor" value="#FFAA2C">
                                @else
                                    <input type="hidden" id="curColor" value="green">
                                    <input type="hidden" id="percentageColor" value="#0f9d58">
                                @endif

                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        siteTheme();
                                        $("#colorselector").on('change', function (e) {
                                            var color = $(this).val();
                                            $("#curColor").val(color);
                                            siteTheme();
                                            return false;
                                        });

                                        function siteTheme() {
                                            var color = $("#curColor").val();
                                            $.ajax({
                                                type: 'POST',
                                                url: '{{ route('Web.set') }}',
                                                data: {
                                                    cookie_name: 'theme',
                                                    cookie_val: color
                                                },
                                                success: function (response) {
                                                    $(".color-btn").removeAttr("title");
                                                    $('#setCook').html(response);
                                                    console.log('Response:', response);
                                                    if (color == 'blue') {
                                                        $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/blue.css') }}');
                                                    }
                                                    if (color == 'green') {
                                                        $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/green.css') }}');
                                                    }
                                                    if (color == 'purple') {
                                                        $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/purple.css') }}');
                                                    }
                                                    if (color == 'yellow') {
                                                        $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/yellow.css') }}');
                                                    }
                                                }
                                            });
                                            $.ajax({
                                                type: 'GET',
                                                url: '{{ route('Web.events') }}',
                                                success: function (response) {
                                                    $('#setevents').html(response);
                                                    console.log('Response:', response);
                                                }
                                            });
                                        }
                                    });

                                </script>

                                <div class="lang-wrapp">
                                    <form method="get" action="">
                                        <div class="form-item" dir="ltr">
                                            <input id="country_selector" type="text">
                                            <label for="country_selector" style="display:none;">Select a country
                                                here...</label>
                                        </div>
                                        <div class="form-item" style="display:none;">
                                            <input type="text" id="country_selector_code" name="country_selector_code"
                                                   data-countrycodeinput="1"
                                                   readonly="readonly"
                                                   placeholder="Selected country code will appear here"/>
                                            <label for="country_selector_code">...and the selected country code will be
                                                updated
                                                here</label>
                                        </div>
                                        <button type="submit" style="display:none;">Submit</button>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--.header__login1-->
                            <div class="header__login2">
                                <a href="{{$LocalhostPath.'/Dashboard/Developer/Profile'}}">
                                    <img src="{{$LocalhostPath.'/uploads/avatars/'.$Indexdeveloper->logo}}"
                                         class="img-responsive developer-header-avatar" title="پروفايل" alt="پروفايل">
                                </a>
                            </div><!--.header__login2-->
                        </div>
                        <!--.header__login-->
                        <div class="clearfix"></div>
                    </div>
                    <!-- end headr for developer login -->
                    <div class="clearfix"></div>
                </div><!--.row-->
            </header>
        @else
            <header class="header">
                <div class="row no-margin">
                    <!--.header__login-->
                    <div class="col-xs-12 col-sm-3 col-md-2 header__logo ">
                        <h1 class="header__logo-img">
                            <a href="{{$LocalhostPath.'/Master/Web'}}">
                                <img class="img-responsive" src="{{ asset('Assets/Master/Web/images/logo.png') }}"
                                     alt="Twannas"
                                     title="Twannas">
                            </a>
                        </h1>
                    </div>
                    <!--.header__logo-->

                    <div class="col-xs-12 col-sm-6 col-md-8 bg_mobile">
                        <div class="row">
                            <div class="header__menu col-xs-12 col-sm-4 col-md-3">
                                <button type="button" class="header__menu--btn" data-toggle="modal"
                                        data-target="#modalMenu">
                                    جميع التطبيقات <span class="glyphicon glyphicon-menu-down"></span>
                                </button>
                            </div><!--.header__menu-->
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <form class="header__search">
                                    <input type="search" name="app" class="search__text" id="search__text">
                                    <span class="fa fa-search"></span>
                                </form>
                                <div id="appList"></div>
                                {{ csrf_field() }}
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#appList').fadeOut();
                                    $('body').on('input', "#search__text", function () {
                                        var query = $("#search__text").val();
                                        var _token = $('input[name = "_token"]').val();

                                        $.ajax({
                                            url: "{{ route('Web.fetch') }}",
                                            type: 'POST',
                                            data: {query: query, _token: _token},
                                            success: function (data) {
                                                $('#appList').fadeIn();
                                                $('#appList').html(data);
                                            }
                                        });
                                    });

                                });
                            </script>
                            @if ($moduleName == 'AppCategory')
                                <div class="col-xs-12">
                                    <div class="cat-title-wrapp">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2"></div>
                                            <div class="cat-title col-xs-12 col-sm-6 col-md-8">
                                                <figure>
                                                    <img src="{{$LocalhostPath.'/uploads/App/'.$AppCategory->icon2}}"
                                                         alt=""
                                                         title="">
                                                </figure>
                                                <h2>
                                                    {{$AppCategory->title}}
                                                </h2>
                                            </div><!--.cat-title-->
                                            <div class="clearfix"></div>
                                        </div><!--.row-->
                                    </div><!--.cat-title-wrapp-->
                                </div>
                            @elseif($moduleName == 'Search')
                                <div class="col-xs-12">
                                    <div class="cat-title-wrapp">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 col-md-2"></div>
                                            <div class="cat-title col-xs-12 col-sm-6 col-md-8">
                                                <figure>
                                                    <img src="{{ asset('Assets/Master/Web/images/search.png') }}" alt=""
                                                         title="">
                                                </figure>
                                                <h2>
                                                    نتيجه جستجو براي : {{$finalPage}}
                                                </h2>
                                            </div><!--.cat-title-->
                                            <div class="clearfix"></div>
                                        </div><!--.row-->
                                    </div><!--.cat-title-wrapp-->
                                </div>

                            @endif
                            <div class="clearfix"></div>
                        </div><!--.row-->
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 header__login bg_mobile">
                        <div>
                            <div class="color-wrapp">
                                <form method="get" action="#">
                                    <div class="btn-group" data-toggle="buttons">

                                        <label class="btn btn-green @if ((Cookie::get('cookie_val') == 'green') || Cookie::get('cookie_val') == 'null' ) active @endif">
                                            <input type="radio" class="colorBtm" name="options[]" value="green"
                                                   @if ((Cookie::get('cookie_val') == 'green') || Cookie::get('cookie_val') == 'null' ) active @endif>
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </label>

                                        <label class="btn btn-purple @if (Cookie::get('cookie_val') == 'purple') active @endif">
                                            <input type="radio" class="colorBtm" name="options[]" value="purple"
                                                   @if (Cookie::get('cookie_val') == 'purple') checked @endif>
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </label>

                                        <label class="btn btn-blue @if (Cookie::get('cookie_val') == 'blue') active @endif">
                                            <input type="radio" class="colorBtm" name="options[]" value="blue"
                                                   @if (Cookie::get('cookie_val') == 'blue') checked @endif>
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </label>

                                        <label class="btn btn-yellow @if (Cookie::get('cookie_val') == 'yellow') active @endif">
                                            <input type="radio" class="colorBtm" name="options[]" value="yellow"
                                                   @if (Cookie::get('cookie_val') == 'yellow') checked @endif>
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>
                            <input type="hidden" id="curColor" value="">
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    siteTheme();
                                    $(".colorBtm").on('change', function (e) {
                                        var color = $(this).val();
                                        $("#curColor").val(color);
                                        siteTheme();
                                        return false;
                                    });

                                    function siteTheme() {
                                        var color = $("#curColor").val();
                                        $.ajax({
                                            type: 'POST',
                                            url: '{{ route('Web.set') }}',
                                            data: {
                                                cookie_name: 'theme',
                                                cookie_val: color
                                            },
                                            success: function (response) {
                                                $('#setCook').html(response);
                                                console.log('Response:', response);
                                                if (color == 'blue') {
                                                    $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/blue.css') }}');
                                                }
                                                if (color == 'green') {
                                                    $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/green.css') }}');
                                                }
                                                if (color == 'purple') {
                                                    $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/purple.css') }}');
                                                }
                                                if (color == 'yellow') {
                                                    $('#themeColor').attr('href', '{{ asset('Assets/Master/Web/css/yellow.css') }}');
                                                }

                                            }
                                        });
                                        $.ajax({
                                            type: 'GET',
                                            url: '{{ route('Web.events') }}',
                                            success: function (response) {
                                                $('#setevents').html(response);
                                                console.log('Response:', response);

                                            }
                                        });
                                    }
                                });
                            </script>
                            <div class="lang-wrapp">
                                <form method="get" action="">
                                    <div class="form-item" dir="ltr">
                                        <input id="country_selector" type="text">
                                        <label for="country_selector" style="display:none;">Select a country
                                            here...</label>
                                    </div>
                                    <div class="form-item" style="display:none;">
                                        <input type="text" id="country_selector_code" name="country_selector_code"
                                               data-countrycodeinput="1"
                                               readonly="readonly"
                                               placeholder="Selected country code will appear here"/>
                                        <label for="country_selector_code">...and the selected country code will be
                                            updated
                                            here</label>
                                    </div>
                                    <button type="submit" style="display:none;">Submit</button>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!--.row-->
                    </div>
                    <div class="clearfix"></div>
                </div><!--.row-->
            </header>
        @endif


        <div class="modal fade" id="modalMenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="tab-title">
                            <ul class="nav nav-tabs text-center" role="tablist">
                                <li class="active"><a href="#tab1" role="tab" data-toggle="tab">ألعاب</a></li>
                                <li><a href="#tab2" role="tab" data-toggle="tab">ألرئيسية</a></li>
                            </ul>
                            <hr>
                        </div><!--.tab-title-->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <ul class="row">
                                    @foreach($gameCategory as $item)
                                        <li class="col-xs-6 col-sm-3">
                                            <figure>
                                                <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon2}}" alt=""
                                                     title="">
                                            </figure>
                                            <h3>
                                                <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">{{$item->title}}</a>
                                            </h3>
                                        </li>
                                    @endforeach

                                </ul><!--.row-->
                            </div><!--.tab-pane-->
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <ul class="row">
                                    @foreach($homeCategory as $item)
                                        <li class="col-xs-6 col-sm-3">
                                            <figure>
                                                <img src="{{$LocalhostPath.'/uploads/App/'.$item->icon2}}" alt=""
                                                     title="">
                                            </figure>
                                            <h3>
                                                <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$item->id}}">{{$item->title}}</a>
                                            </h3>
                                        </li>
                                    @endforeach
                                </ul><!--.row-->
                            </div>
                        </div><!--.tab-content-->
                    </div><!--.modal-body-->
                </div>
            </div>
        </div><!--.modal-->
        @if ($moduleName == 'Master')
            <section class="wrapper firstBlur">

                <?php
                $Sliderrow = 0
                ?>
                @foreach($vitrinesSlider as $itemsSlider)
                    <section class="slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach($vitrinesItemSlider[$Sliderrow] as $Slider)
                                    <div class="swiper-slide">
                                        <a href="{{$LocalhostPath.'/App/Web/PageView/'.$Slider->app_id}}">
                                            <img src="{{$LocalhostPath.'/uploads/vitrineItem/'.$Slider->media}}"
                                                 class="img-responsive" alt="" title="">
                                        </a>
                                    </div><!--.swiper-slide-->
                                @endforeach
                            </div><!--.swiper-wrapper-->
                        </div><!--.swiper-container-->
                    </section><!--.slider-->
                    <?php
                    $Sliderrow++
                    ?>
                @endforeach
                <section class="container">
                    <div class="featured__area row">
                        <div class="col-sm-hidden col-md-3"></div>
                        <ul class="text-center col-xs-12 col-md-6">
                            <li class="col-xs-4"><a href="">أو انضم</a></li>
                            <li class="col-xs-4"><a href="">أو انضم</a></li>
                            <li class="col-xs-4"><a href="">أو انضم</a></li>
                        </ul>
                    </div><!--.featured__area-->
                    <div class="product-wrapp more-product  cat-product">
                        <?php
                        $Scrollrow = 0
                        ?>
                        @foreach($vitrinesScroll as $Scroll)
                            <header class="product-wrapp__header">
                                <div class="product-wrapp__title pull-right">
                                    <h2>{{$Scroll->title}}</h2>
                                </div>
                                <div class="product-wrapp__more pull-left">
                                    <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$Scroll->itemid}}">سجّل</a>
                                </div>
                                <div class="clearfix"></div>
                            </header>
                            <?php $ScrollrowRateView = 0;?>
                            @foreach($vitrinesItemScroll[$Scrollrow] as $itemScroll)
                                <article class="col-xs-6 col-sm-4 col-md-2">
                                    <div class="product">
                                        <figure>
                                            <a href="{{$LocalhostPath.'/App/Web/PageView/'.$itemScroll->app_id}}">
                                                <span class="product__overlay"></span>
                                                <img src="{{$LocalhostPath.'/uploads/vitrineItem/'.$itemScroll->media}}"
                                                     class="img-responsive" title="" alt=""></a></figure>
                                        <div class="product__detail">
                                            <h3 class="text-center"><a
                                                        href="{{$LocalhostPath.'/App/Web/PageView/'.$itemScroll->app_id}}">{{$itemScroll->title}}</a>
                                            </h3>
                                            <ul class="text-center product__rate">
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '0')
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '0.5')
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '1')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '1.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '2')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '2.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '3')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '3.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '4')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '4.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory[$ScrollrowRateView] == '5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                @endif
                                            </ul>
                                        </div><!--.product__detail-->
                                    </div><!--.product-->
                                </article>
                                <?php $ScrollrowRateView++;?>
                            @endforeach
                            <div class="clearfix"></div>
                            <?php $Scrollrow++?>
                        @endforeach
                    </div><!--.product-wrapp-->

                    <div class="product-wrapp more-product" style="display: none;">
                        <?php
                        $Scrollrow_2 = 0
                        ?>
                        @foreach($vitrinesScroll_2 as $Scroll_2)
                            <header class="product-wrapp__header">
                                <div class="product-wrapp__title pull-right">
                                    <h2>{{$Scroll_2->title}}</h2>
                                </div>
                                <div class="product-wrapp__more pull-left">
                                    <a href="{{$LocalhostPath.'/AppCategory/Web/PageView/'.$Scroll_2->itemid}}">سجّل</a>
                                </div>
                                <div class="clearfix"></div>
                            </header>
                            <?php $Scrollrow_2RateView = 0;?>
                            @foreach($vitrinesItemScroll_2[$Scrollrow_2] as $itemScroll_2)
                                <article class="col-xs-6 col-sm-4 col-md-2">
                                    <div class="product">
                                        <figure>
                                            <a href="{{$LocalhostPath.'/App/Web/PageView/'.$itemScroll_2->app_id}}">
                                                <span class="product__overlay"></span>
                                                <img src="{{$LocalhostPath.'/uploads/vitrineItem/'.$itemScroll_2->media}}"
                                                     class="img-responsive" title="" alt=""></a></figure>
                                        <div class="product__detail">
                                            <h3 class="text-center"><a
                                                        href="{{$LocalhostPath.'/App/Web/PageView/'.$itemScroll_2->app_id}}">{{$itemScroll_2->title}}</a>
                                            </h3>
                                            <ul class="text-center product__rate">
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '0')
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '0.5')
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '1')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '1.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '2')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '2.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '3')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '3.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '4')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '4.5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star-half-o"></li>
                                                @endif
                                                @if ($btw_RateCntCategory_2[$Scrollrow_2RateView] == '5')
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                    <li class="fa fa-star"></li>
                                                @endif
                                            </ul>
                                        </div><!--.product__detail-->
                                    </div><!--.product-->
                                </article>
                                <?php
                                $Scrollrow_2RateView++;
                                ?>
                            @endforeach
                            <div class="clearfix"></div>
                            <?php $Scrollrow_2++?>
                        @endforeach
                    </div><!--.product-wrapp-->
                    @if(!$vitrinesScroll_2->isEmpty())
                        <div class="loadMore">
                            <a href="#">أكثر
                            </a>
                        </div>
                    @endif
                </section>
            </section>
        @else
            @yield('body')
        @endif

        <footer class="footer text-center">
            <div class="footer__section__right footer__section1">
                <div class="footer__section__right footer__section2">
                    <div class="footer__section__right footer__section3 text-center">
                        <p>ولد رجل الأعمال الأوكراني من أصل سوري عدنان كيوان في قرية طفس (جنوب سوريا)، ثم انتقل .</p>
                        <ul>
                            @foreach($menuBottom as $item)
                                <?php
                                if ($item->module == 'app') {
                                    $module = 'App';
                                    $id = '/' . $item->item;
                                }
                                if ($item->module == 'app_category') {
                                    $module = 'AppCategory';
                                    $id = '/' . $item->item;
                                }
                                if ($item->module == 'content') {
                                    $module = 'Content';
                                    $id = '/' . $item->item;
                                }
                                if ($item->module == 'contact_us') {
                                    $module = 'ContactUs';
                                    $id = '';
                                }
                                ?>
                                <li>
                                    <a href="{{$LocalhostPath.'/'.$module.'/Web/PageView'.$id}}">
                                        {{$item->title}}
                                    </a>
                                </li>
                            @endforeach
                            @if (Auth::guard('developer')->check() == false)
                                <li>
                                    <a href="{{$LocalhostPath.'/Developer/Developer/LoginView'}}">
                                        ورود توسعه دهندگان
                                    </a>
                                </li>
                            @elseif(Auth::guard('developer')->check() == true)
                                <li>
                                    <a href="{{$LocalhostPath.'/Dashboard/Developer/PageView'}}">
                                        پنل کاربري توسعه دهنده
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div><!--.footer__section3-->
                </div><!--.footer__section2-->
            </div><!--.footer__section1-->
            <div class="footer__section__center">
                <figure>
                    <a href="{{$LocalhostPath.'/Master/Web'}}">
                        <img class="img-responsive" src="{{ asset('Assets/Master/Web/images/footer-logo.png') }}"
                             title=""
                             alt="">
                    </a>
                </figure>
                <div class="footer__section__centerbtn"></div><!--.footer__section__centerbtn-->
            </div><!--.footer__section__center-->
            <div class="footer__section__left footer__section4">
                <div class="footer__section__left footer__section5">
                    <div class="footer__section__left footer__section6 text-center">
                        <p>
                            سجّل الدخول أو انضم إلينا
                        </p>
                        <ul class="footer__social">
                            <li><a class="fa fa-twitter" href="#" title="twitter" target="_blank">&nbsp;</a></li>
                            <li><a class="fa fa-linkedin" href="#" title="Linkedin" target="_blank">&nbsp;</a></li>
                            <li><a class="fa fa-facebook" href="#" title="Facebook" target="_blank">&nbsp;</a></li>
                            <li><a class="fa fa-google-plus" href="#" title="Google Plus" target="_blank">&nbsp;</a>
                            </li>
                        </ul>
                        <!--.footer__social-->
                    </div><!--.footer__section4-->
                </div><!--.footer__section5-->
            </div><!--.footer__section6-->
            <div class="clearfix"></div>
            <div class="copyright">
                <p>همه حقوق براي تونس محفوظ است. &copy;1397 | &nbsp;<a href="#"
                                                                       title="پاراکس | طراح و توسعه دهنده اپليکيشن هاي موبايلي (اندرويد و آي او اس)| برنامه نويسي اختصاصي | انواع وب سايت و پرتال">طراحي
                        وب سايت : </a><a href="#"
                                         title="پاراکس | طراح و توسعه دهنده اپليکيشن هاي موبايلي (اندرويد و آي او اس)| برنامه نويسي اختصاصي | انواع وب سايت و پرتال">پاراکس</a>
                </p>
            </div><!--.copyright-->
        </footer>


        @if ($moduleName == 'App' || $moduleName == 'Master')
            <script type="text/javascript" charset="utf-8"
                    src="{{ asset('Assets/Master/Web/js/slider-js/swiper.js') }}"></script>
        @endif



        <script type="text/javascript">

            /*
            =featured__menu
            ---------------------------------------------*/
            $('a.page-scroll').on('click', function (e) {
                // ambil isi href
                var tujuan = $(this).attr('href');
                // tangkap elemen ybs
                var elemenTujuan = $(tujuan);
                // pindahkan scroll
                $('html, body').animate({
                    scrollTop: elemenTujuan.offset().top - 110
                }, 1250, 'swing');
                e.preventDefault();
            });
            /*
            =menu
            ---------------------------------------------*/
            (function () {
                //Show Modal
                $('#modalMenu').on('show.bs.modal', function (e) {
                    console.log('show');
                    $('.firstBlur').addClass('modalBlur');
                });

                //Remove modal
                $('#modalMenu').on('hide.bs.modal', function (e) {
                    console.log('hide');
                    $('.firstBlur').removeClass('modalBlur');
                })
            })();
            @if ($moduleName == 'App')
            /*
            =barcode
            ---------------------------------------------*/
            (function () {
                //Show Modal
                $('#barcodeModal').on('show.bs.modal', function (e) {
                    console.log('show');
                    $('.firstBlur').addClass('modalBlur');
                });
                //Remove modal
                $('#barcodeModal').on('hide.bs.modal', function (e) {
                    console.log('hide');
                    $('.firstBlur').removeClass('modalBlur');
                })
            })();
            @endif


            @if ($moduleName == 'App' || $moduleName == 'Master')
            /*
        =slider
        ---------------------------------------------*/
            var swiper = new Swiper('.swiper-container', {
                slidesPerView: 3,
                freeMode: true,
                breakpoints: {
                    980: {
                        slidesPerView: 3
                    },
                    768: {
                        slidesPerView: 2
                    },
                    640: {
                        slidesPerView: 2
                    },
                    480: {
                        slidesPerView: 1
                    }
                }
            });
            @endif

            /*
            =select language
            ---------------------------------------------*/
            $("#country_selector").countrySelect({
                defaultCountry: "sa",
                onlyCountries: ['sa', 'tr', 'ru']
            });


            @if ($moduleName == 'App')
            /*
            =slider screenshot
            ---------------------------------------------*/
            var swiper = new Swiper('.pr__slider .swiper-container', {
                slidesPerView: 3,
                spaceBetween: 15,
                scrollbar: {
                    el: '.swiper-scrollbar',
                    hide: true,
                }
            });

            /*
            =toggle product
            ---------------------------------------------*/
            $(document).ready(function () {
                var panels = $('.pr__info');
                var panelsButton = $('.pr__drop');
                panels.hide();

                //Click dropdown
                panelsButton.click(function () {
                    //get data-for attribute
                    var dataFor = $(this).attr('data-for');
                    var idFor = $(dataFor);

                    //current button
                    var currentButton = $(this);
                    idFor.slideToggle(400, function () {
                        //Completed slidetoggle
                        if (idFor.is(':visible')) {
                            currentButton.html('<i class="glyphicon glyphicon-menu-up pr__drop-icon"></i>');
                        } else {
                            currentButton.html('<i class="glyphicon glyphicon-menu-down pr__drop-icon"></i>');
                        }
                    })
                });


                $('[data-toggle="tooltip"]').tooltip();

            });
            /*
            =toggle comment
            ---------------------------------------------*/
            $(document).ready(function () {
                var panels = $('.pr__comment-more');
                var panelsButton = $('.pr__dropComment');
                panels.hide();

                //Click dropdown
                panelsButton.click(function () {
                    //get data-for attribute
                    var dataFor = $(this).attr('data-for');
                    var idFor = $(dataFor);

                    //current button
                    var currentButton = $(this);
                    idFor.slideToggle(400, function () {
                        //Completed slidetoggle
                        if (idFor.is(':visible')) {
                            currentButton.html('<i class="glyphicon glyphicon-menu-up pr__icon-comment"></i>');
                        } else {
                            currentButton.html('<i class="glyphicon glyphicon-menu-down pr__icon-comment"></i>');
                        }
                    })
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
            @endif

            $(document).ready(function () {

                $(".more-product").slice(0, 1).show();
                if ($(".product-wrapp:hidden").length != 0) {
                    $(".loadMore").show();
                }
                $(".loadMore").on('click', function (e) {
                    e.preventDefault();
                    $(".more-product:hidden").slice(0, 6).slideDown();
                    if ($(".more-product:hidden").length == 0) {
                        $(".loadMore").fadeOut('slow');
                    }
                });
            });
            @if ($moduleName == 'Content' || $moduleName == 'ContactUs')
            $('body').addClass('single-body');
            @endif

            @if ($moduleName == 'Dashboard')
            $('.circleback--large').remove();
            $('.circleback--small').remove();
            @endif

        </script>
        </body>
        </html>




