<?php

namespace App\Modules\Master\Web\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LogoutDeveloper extends Controller
{
    use AuthenticatesUsers;

    function logout(Request $request)
    {
        Auth::guard('developer')->logout();
    }
}