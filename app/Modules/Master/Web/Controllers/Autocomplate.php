<?php
namespace App\Modules\Master\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use Illuminate\Http\Request;
use App\Modules\App\Admin\Models\App;
use App\Services\AppServices;

class Autocomplate extends Controller
{
    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $LocalhostPath = CoreCommon::getLocalhostPath("");
            
            $query = $request->get('query');
            $data = App::where('title', 'like', '%' . $query . '%')
                ->limit(4)
                ->get();
            if (count($data)) {
                $output = '<div class="header__search__result"><ul>';
                $row = 1;
                foreach ($data as $item) {
                    $output .= '<li class="row"><figure class="col-xs-3"><a href="' . $LocalhostPath . '/App/Web/PageView/' . $item->id . '"><img class="img-responsive" src="' . $LocalhostPath . '/uploads/App/' . $item->image . '" alt="' . $item->title . '"></a></figure><div class="col-xs-9"><h4 class="search__result__title"><a href="' . $LocalhostPath . '/App/Web/PageView/' . $item->id . '">' . $item->title . '</a></h4><div class="search__result__detail">' . substr($item->detail, 0, 100) . '</div></div><div class="clearfix"></div></li>';
                    $row++;
                }
                if ($row > 4) {
                    $output .= '<li class="search__result__more"><a href="' . $LocalhostPath . '/Search/Web/PageView/' . $query . '">شاهد المزيد</a></li>';
                }
                $output .= '</ul></div>';
            } else {
                $output = '<div class="header__search__result"><p>موردي براي عبارت مورد نظر یافت نشد.</p></div>';
            }
            echo $output;
        }
    }
}
