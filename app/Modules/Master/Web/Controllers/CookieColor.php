<?php
namespace App\Modules\Master\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CoreCommon;
use Illuminate\Http\Request;
use App\Modules\App\Admin\Models\App;
use App\Services\AppServices;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Cookie\CookieJar;
class CookieColor extends Controller
{
    function set(Request $request)
    {
//        if ($request->get('cookie_val')) {
//            $cookieName = 'cookie_val';
//            $cookieVal = $request->get('color');
//            return response()->json(['previousCookieValue' => Cookie::get('cookie_val')])->withCookie(cookie($cookieName, $cookieVal, 10000));
//        }

        if ($request->get('cookie_val')) {
            $cookieName = 'cookie_val';
            $cookieVal = $request->get('cookie_val');
            return response()->json(['previousCookieValue' => Cookie::get('cookie_val')])->withCookie(Cookie::forever($cookieName, $cookieVal));
        }
    }
    public function events() {
        return response()->json([
            'cookieValue' => Cookie::get('cookie_val'),``
        ]);


    }
}
