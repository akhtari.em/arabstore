<?php
$permissions = json_decode(Auth::user()->permissions, true)['admin'];
$userId = Auth::user()->id;

$userAllSinglePermissions = [];
if (!empty($permissions)) {
    foreach ($permissions as $permission) {
        foreach ($permission['permissions'] as $perm_json) {
            $perm = json_decode($perm_json, true);
            foreach ($perm[key($perm)] as $access_name) {
                array_push($userAllSinglePermissions, $access_name);
            }
        }
    }
}

//dd(Auth::guard('web')->check())
?>

        <!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="description"
          content="Glade is a clean and powerful ready to use responsive AngularJs Admin Template based on Latest Bootstrap version and powered by jQuery, Glade comes with 3 amazing Dashboard layouts. Glade is completely flexible and user friendly admin template as it supports all the browsers and looks awesome on any device.">
    <meta name="keywords"
          content="admin, admin dashboard, angular admin, bootstrap admin, dashboard, modern admin, responsive admin, web admin, web app, bitlers">
    <meta name="author" content="bitlers">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/bootstrap.min.css')}}">
    <!-- Our Website CSS Styles -->

    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('Assets/Master/Admin/css/style.css')}}">
    <script src="{{asset('Assets/Master/Admin/js/jquery-2.1.3.js')}}"></script>
    @yield('css')
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Our Website Content Goes Here -->
<header class="simple-normal">
    <div class="top-bar">
        <div class="logo">
            <a href="index.html" title=""><i class="fa fa-bullseye"></i> Glade</a>
        </div>
        <div class="menu-options"><span class="menu-action"><i></i></span></div>
        <span class="open-panel">
                <a href="{{route('UsersLogout')}}" title="خروج">
                    <i class="fa fa-power-off"></i>
                </a>
            </span>

        <div class="top-bar-quick-sec">
            <span class="open-panel"><i class="fa fa-cog fa-spin"></i></span>
            <span id="toolFullScreen" class="full-screen-btn"><i class="fa fa-arrows-alt"></i></span>
            <div class="name-area">
                <a href="javascript:void(0)" title=""><img src="http://placehold.it/100x100" alt=""/> <strong>John
                        Doe</strong></a>
            </div>
        </div>
    </div><!-- Top Bar -->
    <div class="side-menu-sec" id="header-scroll">

        <div class="side-menus">
            <span style="font-size : 15px;">{{trans('lang.mainLinks')}}</span>
            <nav>
                <ul class="parent-menu">

                    @if(!empty($permissions['Users']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'Users'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddUsersView' || Route::currentRouteName()=='UsersListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddUsersView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddUsersView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("UsersListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('UsersListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['UserCategory']) || Auth::guard('web')->user()->id == 1)
                        <?php $moduleName = 'UserCategory'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='UserCategoryListView' || Route::currentRouteName()=='AddUserCategoryView' ) active @endif">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddUserCategoryView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddUserCategoryView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("UserCategoryListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('UserCategoryListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if(!empty($permissions['Menu']) || Auth::guard('web')->user()->id == 1)
                        <?php $moduleName = 'Menu'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='MenuListView' || Route::currentRouteName()=='AddMenuView' ) active @endif">
                            <a title=""><i class="ti-user"></i><span>نوارابزار</span></a>
                            <ul>
                                @if(in_array("AddMenuView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddMenuView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("MenuListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('MenuListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if(!empty($permissions['AppCategory']) || Auth::guard('web')->user()->id == 1)
                        <?php $moduleName = 'AppCategory'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AppCategoryListView' || Route::currentRouteName()=='AddAppCategoryView' ) active @endif">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddAppCategoryView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddAppCategoryView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("AppCategoryListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AppCategoryListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['App']) || Auth::guard('web')->user()->id == 1)
                        <?php $moduleName = 'App'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddAppView' || Route::currentRouteName()=='AppListView' ) active @endif">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddAppView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddAppView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("AppListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AppListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['Client']) || Auth::guard('web')->user()->id == 1)
                        <?php $moduleName = 'Client'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddClientView' || Route::currentRouteName()=='ClientListView' ) active @endif">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddClientView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddClientView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("ClientListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('ClientListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['Vitrine']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'Vitrine'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddVitrineView' || Route::currentRouteName()=='VitrineListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddVitrineView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddVitrineView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("VitrineListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('VitrineListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif


                    @if(!empty($permissions['VitrineItem']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'VitrineItem'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddVitrineItemView' || Route::currentRouteName()=='VitrineItemListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddVitrineItemView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddVitrineItemView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("VitrineItemListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('VitrineItemListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    {{-- rest of side bar place here --}}

                    @if(!empty($permissions['AppMedia']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'AppMedia'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddAppMediaView' || Route::currentRouteName()=='AppMediaListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddAppMediaView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddAppMediaView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("AppMediaListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AppMediaListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['AppApk']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'AppApk'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddAppApkView' || Route::currentRouteName()=='AppApkListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddAppApkView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddAppApkView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("AppApkListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AppApkListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    {{--@if(!empty($permissions['Comment']) || Auth::guard('web')->user()->id == 1 )--}}
                    {{--<?php $moduleName='Comment'; ?>--}}
                    {{--<li class="menu-item-has-children @if(  Route::currentRouteName()=='CommentListView' ) active @endif " >--}}
                    {{--<a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>--}}
                    {{--<ul>--}}
                    {{----}}
                    {{--@if(in_array("CommentListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)--}}
                    {{--<li><a href="{{route('CommentListView')}}">{{trans('modules.list')}}</a></li>--}}
                    {{--@endif--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endif--}}


                    @if(!empty($permissions['ContactForm']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'ContactForm'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='ContactFormListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>

                                @if(in_array("ContactFormListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('ContactFormListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if(!empty($permissions['ContactUs']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'ContactUs'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='ContactUsListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>

                                @if(in_array("ContactUsListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('ContactUsListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(!empty($permissions['Content']) || Auth::guard('web')->user()->id == 1 )
                        <?php $moduleName = 'Content'; ?>
                        <li class="menu-item-has-children @if( Route::currentRouteName()=='AddContentView' || Route::currentRouteName()=='ContentListView' ) active @endif ">
                            <a title=""><i class="ti-user"></i><span>{{trans('modules.'.$moduleName)}}</span></a>
                            <ul>
                                @if(in_array("AddContentView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('AddContentView')}}">{{trans('modules.add')}}</a></li>
                                @endif
                                @if(in_array("ContentListView",$userAllSinglePermissions) || Auth::guard('web')->user()->id == 1)
                                    <li><a href="{{route('ContentListView')}}">{{trans('modules.list')}}</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                </ul>
            </nav>
            <span class="footer-line">2017 Copyright Glade by <a title=""
                                                                 href="http://themeforest.net/user/bitlers/portfolio?ref=bitlers">bitlers</a></span>
        </div>
    </div>
</header>
<input type="hidden" value="{{ csrf_token() }}" id="nekot">
<div class="main-content">
    <div class="panel-content">
        @yield('content')
    </div><!-- Panel Content -->
</div>


<!-- Vendor: Javascripts -->


<script src="{{asset('Assets/Master/Admin/js/bootstrap.min.js')}}"></script>
<!-- Our Website Javascripts -->
<script src="{{asset('Assets/Master/Admin/js/app.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/common.js')}}"></script>
{{--<script src="{{asset('Assets/Master/Admin/js/home1.js')}}"></script>--}}
<script src="{{asset('Assets/Master/Admin/js/popper.min.js')}}"></script>
<script src="{{asset('Assets/Master/Admin/js/bootstrap-confirmation.js')}}"></script>


@yield('js')
</body>


</html>









