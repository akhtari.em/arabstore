<?php

namespace App\Modules\Menu\Admin\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Menu extends Model
{
    protected $table = 'menus';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'title', 'lang', '_ord', 'status', 'module', 'item', 'url', 'parent', 'position'
    ];

    public static function GetWithId($id)
    {
        if (Menu::where('id', $id)->exists()) {
            return Menu::where('id', $id)->get()[0];
        }

    }
    public static function Search($title, $offset, $limit)
    {
        return Menu::where('title', 'like', '%' . $title . '%')->offset($offset)->take($limit)->orderBy('id', 'desc')->get(['id', 'title', 'module', '_ord', 'position']);
    }

    public static function Remove($id)
    {
        if (Menu::where('id', $id)->exists()) {
            Menu::where('id', '=', $id)->delete();
        }
    }

    public static function CheckExists($id)
    {
        if (Menu::where('id', $id)->exists()) {
            return true;
        } else {
            return false;
        }
    }
    public static function Count($searchFiled)
    {
        return Menu::where('title', 'like', '%' . $searchFiled . '%')->count();
    }
}