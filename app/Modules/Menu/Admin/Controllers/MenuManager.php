<?php

namespace App\Modules\Menu\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use App\Modules\Menu\Admin\Models\Menu;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\CoreCommon;
use DB;

class MenuManager extends Controller
{


    //Just Return Views
    public function AddView()
    {
        $modules = CoreCommon::GetModulesName();

        view()->addNamespace('my_views', app_path('Modules'));
        // then:
        return view('my_views::Menu.Admin.Views.Add', compact('modules')); // /app/custom_views/some/view/name.blade.php
    }


    public function GetPreView()
    {
        $dd = CoreCommon::osD();
        $manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . '..' . $dd . 'Manifest.json', "r");
        $manifest = json_decode($manifestJson, true);
        $moduleName = $manifest['title'];
        if (substr(strrchr(__DIR__, "Admin" . $dd), 6)) {
            $side = 'Admin';
        } else if (substr(strrchr(__DIR__, "Site" . $dd), 5)) {
            $side = 'Site';
        }

        $preView = $moduleName . '_' . $side . '::';
        return $preView;
    }

    function fetch(Request $request)
    {
        $value = $request->get('value');
        $itemName = $request->get('itemName');
        $menuItem = $request->get('menuItem');

        //Chanege Modules Name to Database Name
        if ($value == 'App') {
            $value = 'app';
        } elseif ($value == 'AppCategory') {
            $value = 'app_category';
        } elseif ($value == 'Content') {
            $value = 'content';
        } elseif ($value == 'ContactUs') {
            $value = 'contact_us';
        }
        //End
        $data = DB::table($value)
            ->get();
        $output = '<label class="form-label">' . $itemName . '</label><select class="form-control selectpicker wide _' . $value . ' required" name="item" id="' . $value . '" data-live-search="true"><option value="-1">انتخاب کنید</option>';
        foreach ($data as $item) {
            if ($menuItem == $item->id) {
                $SelectResult = ' selected';
            } else {
                $SelectResult = ' ';
            }
            $output .= '<option value="' . $item->id . '" ' . $SelectResult . '>' . $item->title . '</option>';
        }
        echo $output;
    }

    public function ListView()
    {
        return view($this->GetPreView() . 'List');
    }

    public function EditView($id)
    {
        $modules = CoreCommon::GetModulesName();
        $id = Menu::GetWithId($id);
        view()->addNamespace('my_views', app_path('Modules'));
        // then:
        return view($this->GetPreView() . 'Edit', compact('modules'))->with(['id' => $id]);
    }

    //Controllers That Do Somethings
    public function Add(MenuRequest $request)
    {
        Menu::create($request->all());
        return redirect(route('MenuListView'));
    }

    public function Delete($id)
    {
        $menu = Menu::findOrFail($id);
        $menu->delete();
        return redirect(route('MenuListView'));
    }

    public function Edit(MenuRequest $request, $id)
    {
        $menu = Menu::findOrFail($id);

        $menu->update($request->all());
        return redirect(route('MenuListView'));
    }


}
