<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\CoreCommon;
use App\Modules\Menu\Admin\Controllers\MenuManager;

$dd = CoreCommon::osD();
$manifestJson = file_get_contents(__DIR__ . $dd . '..' . $dd . 'Manifest.json', "r");
$manifest = json_decode($manifestJson, true);
$moduleName = $manifest['title'];
$moduleSide = substr(strrchr(__DIR__, $dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName . '/' . $moduleSide, 'namespace' => 'App\Modules\\' . $moduleName . '\\' . $moduleSide . '\Controllers', 'middleware' => ['web', 'guest:web', 'checkPermissions']], function () {

    //Views(Get) Routes
    Route::get('Add', 'MenuManager@AddView')->name('AddMenuView');
    Route::get('List', 'MenuManager@ListView')->name('MenuListView');

    Route::get('Ajax_GetSubCategories', 'AjaxHandler@SubCategoryList')->name('SubCategoryList');


    Route::get('Ajax_GetList', 'AjaxHandler@ListView');
    Route::get('Edit/{id}', 'MenuManager@EditView')->name('EditMenuView');


    Route::post('Add', 'MenuManager@Add')->name('AddMenu');
    Route::post('/Add/fetch', 'MenuManager@fetch')->name('Getmodule.fetch');

    Route::post('Edit/{id}', 'MenuManager@Edit')->name('EditMenu');

    //Update(Post) Routes
    Route::get('Delete/{id}', 'MenuManager@Delete')->name('menu.Delete');

});


//These Routes Use When User Not Logged In

