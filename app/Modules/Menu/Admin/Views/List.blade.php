@extends('Master_Admin::MasterPage')

@section('title')
فهرست اپلیکشن ها
@stop
@section('css')
@stop

@section('js')
<script src="{{asset('Assets/Menu/Admin/js/List.js')}}"></script>
@stop

@section('content')
<div class="main-title-sec">
   <div class="row">
        <div class="col-md-6 column">
             <div class="heading-profile">
                  <h2>فهرست منو ها</h2>
             </div>
        </div>
   </div>
</div>


<div class="main-content-area container-fluid">

	{{-- search button --}}
	<div class="item trditem">
        <br>
        <div class="row row-form">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="inline-form">
                    <label class="c-label">جستجو (نام اپلیکیشن)</label>
                    <input class="input-style _search" type="text" name="search" id="search">
                </div>
            </div>
        </div>
    </div>
    {{-- new user button --}}
    <div class="col-md-3 float-left BtnTotal">
        <button type="button" class="btn btn-success btn-block" id="new-item-btn" onclick="location.href='{{route('AddMenu')}}'"><i class="fa fa-plus-square"></i>
            منوی تازه
        </button>
    </div>
    <div class="clear"></div>

    {{-- result table --}}
    <div class="result_total container-fluid">
        <div class="row result">
    <div class="col-md-12">
        <div class="streaming-table">
       			
               
        </div><!-- Streaming Table -->
    </div>
   
</div>
        <div id="loadingPage" style="display: none;">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                <rect x="0" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML" values="1; .2; 1" begin="0s" dur="0.6s" repeatCount="indefinite"></animate>
                </rect>
                <rect x="7" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML" values="1; .2; 1" begin="0.2s" dur="0.6s" repeatCount="indefinite"></animate>
                </rect>
                <rect x="14" y="0" width="4" height="20" fill="#333">
                    <animate attributeName="opacity" attributeType="XML" values="1; .2; 1" begin="0.4s" dur="0.6s" repeatCount="indefinite"></animate>
                </rect>
            </svg>
        </div>
    </div>

</div>
	
@stop
