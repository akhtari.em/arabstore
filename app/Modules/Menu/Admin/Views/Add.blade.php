@extends('Master_Admin::MasterPage')

@section('title')
    افزودن اپلیکشن
@stop
@section('css')
    <link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop

@section('content')
    <div style="padding-top: 50px">
        @include('errors')
    </div>
    <div class="main-title-sec">
        <div class="row">
            <div class="col-md-3 column">
                <div class="heading-profile">
                    <h2>{{trans('modules.Add')}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content-area container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="widget with-padding">
                    <div class="wizard-form-h">

                        <form class="form container-fluid" method="post" onsubmit="return checkMenu()" name="form"
                              action="{{route('AddMenu')}}" enctype="multipart/form-data" id="form">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="inline-form">
                                        <label class="c-label">عنوان <span>الزامی است*</span> </label>
                                        <input class="input-style" value="{{old('title')}}" type="text" id="title"
                                               name="title">
                                    </div>
                                </div>
                            </div>
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">ماژول</label>
                                        <select class="form-control selectpicker wide _module required" name="module"
                                                id="module" data-live-search="true">
                                            <option value="-1">انتخاب کنید</option>
                                            @foreach($modules as $item)
                                                @if ($item=='App' || $item=='AppCategory' || $item=='Content'  || $item=='ContactUs' )
                                                    @if ($item=='App')
                                                        {{$itemName = 'اپلیکیشن ها'}}
                                                        {{$itemAction = 'app'}}
                                                    @elseif ($item=='AppCategory')
                                                        {{$itemName = 'دستبندی های اپلیکیشن'}}
                                                        {{$itemAction = 'app_category'}}
                                                    @elseif ($item=='Content')
                                                        {{$itemName = 'محتوا'}}
                                                        {{$itemAction = 'content'}}
                                                    @elseif ($item=='ContactUs')
                                                        {{$itemName = 'تماس با ما'}}
                                                        {{$itemAction = 'contact_us'}}
                                                    @endif
                                                    <option value="{{$itemAction}}"{{$itemAction == old('models')}}>{{$itemName}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <div class="form-group" id="itemList"></div>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#itemList').hide();
                                    $("#module").change(function () {
                                        if ($("#module").val() != '-1') {
                                            var value = $(this).val();
                                            var item = document.getElementById("module").selectedIndex;
                                            var itemName = document.getElementsByTagName("option")[item].text;
                                            var _token = $('input[name="_token"]').val();
                                            $.ajax({
                                                method: "POST",
                                                url: "{{route('Getmodule.fetch')}}",
                                                data: {value: value, _token: _token, itemName: itemName},
                                                success: function (result) {
                                                    $("#itemList").html(result);
                                                    $('#itemList').show();
                                                }
                                            });
                                        } else {
                                            $('#itemList').hide();
                                        }
                                    });
                                });
                            </script>
                            <div class="row frst-row-form">
                                <div class="col-sm-12 col-md-2 col-lg-2">
                                    <div class="inline-form">
                                        <label class="c-label">اولویت</label>
                                        <input class="input-style" type="number" name="_ord" value="0" dir="ltr">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <label class="pull-right">وضعیت</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" {{(old('status') == '1')?'selected':''}}>فعال</option>
                                        <option value="0" {{(old('status') == '0')?'selected':''}}>غیرفعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row row-form">
                                <div class="col-sm-12 col-md-8 col-lg-6">
                                    <button class="btn green-bg pull-right" type="submit"><i
                                                class="fa fa-plus-square send-i"></i>ساختن
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('Ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('Assets/Menu/Admin/js/Add.js')}}"></script>
    <script src="{{asset('Assets/Menu/Admin/js/checkMenuValidate.js')}}"></script>
    <script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
@stop