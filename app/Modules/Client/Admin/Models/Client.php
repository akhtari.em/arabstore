<?php

namespace App\Modules\Client\Admin\Models;

use App\Http\Requests\ClientRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\Confirmation\Confirmation;
use App\Modules\WishList\Admin\Models\WishList;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
class Client extends Authenticatable
{
	use HasApiTokens, Notifiable;
	protected $table='client';
	public $timestamps=true;
	protected $primaryKey = 'id';
	protected $fillable = [
		'phone', 'email', 'name', 'family', 'sex','city','birthDate','image','bio','area_code','status', 'password',
		'loginType','level', 'follower','following','score','credit','type', 'parent_password','age'
	];
	protected $appends = [
		'image_path'
	];
	
	public static function GetWithId($id){
		if(Client::where('id',$id)->exists())
		{
			return Client::where('id',$id)->get()[0];
		}
		else
		{
			return false;
		}	
	}

	public static function GetAll()
	{
		return Client::all()->where('status',1);
	}

	public static function SearchClient($phone,$offset,$limit)
	{
		return Client::where('phone','like','%'.$phone.'%')->offset($offset)->take($limit)->get();
	}

	public static function Remove($id)
	{
		if(Client::where('id',$id)->exists())
		{
			Client::where('id','=',$id)->delete();
		}		
	}

	public static function CheckClientExists($id)
	{
		if(Client::where('id',$id)->exists())
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	
	public static function ClientCount($searchFiled)
	{
		return Client::where('phone','like','%'.$searchFiled.'%')->count();
	}
	
	public function confirmation()
	{
		return $this->hasMany(Confirmation::class)->where('expire_code', ">", Carbon::now() )->latest('id')->first();
	}
	public function ConfirmationCode()
	{
		$confirm = $this->hasMany(Confirmation::class)->where('expire_code', ">", Carbon::now() )->latest('id')->first();
		if(count($confirm))
		{
			return $confirm->code;
		}
		return null;
	}
	
	
	public function findForPassport($username)
	{
		return $this->where('id', $username)->first();
	}
	
	public function getGravatar($s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array() )
	{
		if ($this->image)
		{
			return url("/uploads/avatars/$this->image");
		}
		else
		{
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5(strtolower(trim($this->email)));
			$url .= "?s=$s&d=$d&r=$r";
			if ($img)
			{
				$url = '<img src="' . $url . '"';
				foreach ($atts as $key => $val)
					$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
	}
	
	public function comments()
	{
		return $this->hasMany(Comment::class, 'client_id');
	}
	
	public function apps()
	{
		return $this->hasMany(App::class, 'userid');
	}
	
	public function favorites()
	{
		return $this->hasMany(Favorite::class, 'client_id');
	}
	
	public function getImagePathAttribute()
	{
		return UserService::getAvatarDisplayPath("/uploads/avatars/". $this->image);
	}
	
	public function clientDownloads()
	{
		return $this->hasMany(Client::class);
	}
	
	public function clientViews()
	{
		return $this->hasMany(Client::class);
	}
	
	public function wishLists()
	{
		return $this->hasMany(WishList::class);
	}
	
	public function appRate()
	{
		return $this->hasMany(AppRate::class);
	}
}