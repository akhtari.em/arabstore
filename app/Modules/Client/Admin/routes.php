<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Modules\Client\Admin\Controllers\ClientManager;
use App\Http\Controllers\CoreCommon;

$dd=CoreCommon::osD();
$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'Manifest.json', "r");
$manifest=json_decode($manifestJson,true);
$moduleName=$manifest['title'];
$moduleSide=substr(strrchr(__DIR__,$dd), 1);


//These Routes Use When User Logged In And Have Permissions Of That Route
Route::group(['prefix' => $moduleName.'/'.$moduleSide, 'namespace' => 'App\Modules\\'.$moduleName.'\\'.$moduleSide.'\Controllers' ,'middleware'=>['web','guest:web','checkPermissions'] ], function ()
{	
	
	//Views(Get) Routes
	Route::get('Add','ClientManager@AddClientView')->name('AddClientView');
	Route::get('Clients','ClientManager@ClientView')->name('ClientListView');
//	Route::get('EditClient','UserCategoryManager@EditUserCategoryView')->name('EditUserView');
//
	Route::get('Ajax_GetClientList','AjaxHandler@GetClientList');

	Route::get('Edit/{id}', 'ClientManager@EditClientView')->name('EditClientView');

	//Create(Post) Routes
	Route::post('Add','ClientManager@AddClient')->name('AddClient');
	Route::post('Edit/{id}','ClientManager@EditClient')->name('EditClient');
	//Update(Put) Routes

	//Delete(Delete) Routes
	Route::get('DeleteClient/{id}','ClientManager@DeleteClient')->name('DeleteClient');
});







