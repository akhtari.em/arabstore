<?php use  App\Http\Controllers\CoreCommon; ?>
@extends('Master_Admin::MasterPage')

@section('title')
	ویرایش مشتری
@stop
@section('css')
	<link href="{{asset('backend/assets/slider/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
	<link href="{{asset('packages/persiandate/css/jquery.Bootstrap-PersianDateTimePicker.css')}}" rel="stylesheet"/>
@stop

@section('content')
	<div style="padding-top: 50px">
		@include('errors')
	</div>
	<div class="main-title-sec">
		<div class="row">
			<div class="col-md-3 column">
				<div class="heading-profile">
					<h2>{{trans('modules.Add')}}</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content-area container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="widget with-padding">
					<div class="wizard-form-h">
						<form class="form container-fluid" method="post" name="form"
						      onsubmit="return editClientValidate()" action="{{route('EditClient', ['id' => $id->id])}}"
						      enctype="multipart/form-data" id="form">
							<input type="hidden" value="{{ csrf_token() }}" name="_token">
							<div class="form-group">
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">تلفن<span>الزامی است</span></label>
												<input class="input-style" value="{{$id->phone}}" type="number"
												       id="phone" name="phone">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label"> پیش شماره<span>الزامی است</span></label>
												<input class="input-style" value="{{$id->area_code}}" type="number" id="area_code"
												       name="area_code">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">نام <span>الزامی است</span></label>
												<input class="input-style" value="{{$id->name}}" type="text" id="name"
												       name="name">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">نام خانوادگی <span>الزامی است</span> </label>
												<input class="input-style" value="{{$id->family}}" type="text"
												       id="family" name="family">
											</div>
										</div>
									</div>
								</div>
									<div class="col-sm-12 col-md-8 col-lg-4" style="margin-top: -5px">
										<div class="form-group">
											<label for="birthDate"> تاریخ تولد</label>
											<div class="input-group">
												<input type="text" class="form-control" readonly data-englishnumber="true"
												       name="birthDate"
												       value="{{($id->birthDate != '0000-00-00 00:00:00')?(str_replace('-','/',jDate::forge($id->birthDate)->format('datetime'))):''}}"
												       id="birthDate" data-mddatetimepicker="true" data-trigger="click"
												       data-targetselector="#birthDate" data-groupid="group1"
												       data-enabletimepicker="true"
												       data-placement="bottom">
												<span class="input-group-addon" data-mddatetimepicker="true"
												      data-englishnumber="true"
												      data-trigger="click" data-targetselector="#birthDate"
												      data-groupid="group1"
												      data-enabletimepicker="true" data-placement="bottom"><i
															class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">توضیحات در مرود خودتان</label>
												<input class="input-style" value="{{$id->bio}}" type="text" id="bio"
												       name="bio">
											</div>
										</div>
									</div>
								</div>
							
							</div>
							<div class="form-group">
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">نام شهر</label>
												<input class="input-style" value="{{$id->city}}" type="text" id="city"
												       name="city">
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="row frst-row-form">
										<div class="col-md-12">
											<div class="inline-form">
												<label class="c-label">ایمیل<span>الزامی است</span></label>
												<input class="input-style" value="{{$id->email}}" type="email"
												       id="email" name="email">
											</div>
										</div>
									</div>
								</div>
									<div class="col-sm-12 col-md-4 col-lg-4">
										<div class="row frst-row-form" style="padding-left: -30px;">
											<label class="pull-right">جنسیت</label>
											<select class="form-control" name="sex" id="sex">
												<option value="male" {{($id->status == 'male')?'selected':''}}>آقا
												</option>
												<option value="female" {{($id->status == 'female')?'selected':''}}>خانم
												</option>
											</select>
										</div>
									</div>
							</div>
							
									<div class="col-sm-12 col-md-4 col-lg-4">
										<div class="row frst-row-form" style="padding-left: 30px;">
											<label class="pull-right">وضعیت</label>
											<select class="form-control" name="status" id="status">
												<option value="1" {{($id->status == '1')?'selected':''}}>فعال
												</option>
												<option value="0" {{($id->status == '0')?'selected':''}}>غیرفعال
												</option>
											</select>
										</div>
									</div>
							<div class="row row-form">
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="form-group">
										<label class="form-label">رده سنی</label>
										<select class="form-control selectpicker wide _age" name="age" id="age" data-live-search="true">
											<option value="16" {{($id->age == '16')? 'selected' : ''}}>16</option>
											<option value="3" {{($id->age == '3')?'selected':''}}>3 </option>
											<option value="7" {{($id->age == '7')?'selected':''}}> 7</option>
											<option value="12" {{($id->age == '12')?'selected':''}}>12 </option>
										</select>
									</div>
								</div>
							</div>
								<div class="row frst-row-form">
									<div class="col-md-12 float-right">
										<div class="inline-form">
											<label><span>الزامی است*</span>تصویر فایل (فایل مجاز jpg)</label>
											<div class="col-lg-8" style="width: 100%">
												<img src="{{ $id->getGravatar() }}" alt="{{ $id->name }}"
												     style="width:150px;height:150px;">
												<input id="avatar" name="avatar" type="file" multiple class="file-loading"  accept="image/*">
											</div>
											<div class="clear"></div>
										</div>
									</div>
								</div>
								<div class="row row-form">
									<div class="col-sm-12 col-md-12 col-lg-6">
										<button class="btn green-bg pull-right" type="submit"><i
													class="fa fa-plus-square send-i"></i>ساختن
										</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop
@section('js')
	<script src="{{url('packages/persiandate/js/jalaali.js')}}"></script>
	<script src="{{url('packages/persiandate/js/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>
	<script src="{{asset('Assets/Client/Admin/js/checkClientValidate.js')}}"></script>
	<script src="{{asset('Assets/App/Admin/js/Add.js')}}"></script>
	<script src="{{asset('backend/assets/slider/js/fileinput.js')}}" type="text/javascript"></script>
	<script>
		$("#avatar").fileinput({
			uploadUrl: "{{ route('AddClient') }}",
			allowedFileExtensions: ["jpg", "png", "jpeg"],
			uploadExtraData: function () {
				return {
					_token: "{{csrf_token()}}",
				};
			}
		});
	</script>
@stop