<?php


namespace App\Modules\Client\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\Comment\Admin\Models\Favorite;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\CoreCommon;
use Illuminate\Validation\Rules\In;
use App\Modules\Client\Admin\Models\Client;
use Morilog\Jalali\jDateTime;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;
class ClientManager extends Controller
{
	private $userService;
	public function __construct(UserService $userService)
	{
		$this->userService = $userService;
	}
	
	public function GetPreView()
	{
		$dd=CoreCommon::osD();
		$manifestJson=file_get_contents(__DIR__.$dd.'..'.$dd.'..'.$dd.'Manifest.json', "r");
		$manifest=json_decode($manifestJson,true);
		$moduleName=$manifest['title'];
		if(substr(strrchr(__DIR__,"Admin".$dd), 6))
		{
			$side='Admin';
		}
		else if(substr(strrchr(__DIR__,"Site".$dd), 5))
		{
			$side='Site';
		}
		
		$preView=$moduleName.'_'.$side.'::';
		return $preView;
	}


	//Just Return Views
	public function AddClientView()
	{
		$modules=CoreCommon::GetAdminModulesPermissions();
		return view($this->GetPreView().'Add')->with('modules',$modules);
	}

	public function ClientView()
	{
		return view($this->GetPreView().'List');
	}

	//Controllers That Do Somethings
	public function AddClient(ClientRequest $request)
	{
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->userService->uploadAvatar($request->file('avatar'));
			$request->request->add(['image' => $filename]);
		}
		$request->merge(['birthDate' => jDateTime::createDatetimeFromFormat('Y/m/d H:i:s', $request['birthDate'])->format("Y-m-d H:i:s")]);
		$client = Client::create($request->all());
		$findClient = Client::where('id', $client->id)->first();
		$findClient->password = Hash::make($client->id);
		$findClient->update($request->all());
		return redirect(route('ClientListView'));
	}

	public function DeleteClient($id)
	{
		$client = Client::findOrFail($id);
		$client->delete();
		//remove app_rate
		$appRates = AppRate::where('client_id', $id)->get();
		foreach ($appRates as $appRate)
		{
			$appRate->delete();
		}
		// remove comment
		$comments = Comment::where('client_id', $id)->get();
		foreach ($comments as $comment)
		{
			$comment->delete();
		}
		
		// remove favorite
		$favorotes = Favorite::where('client_id', $id)->get();
		foreach ($favorotes as $favorote)
		{
			$favorote->delete();
		}
		
		// remove wish_list
		$wishLists = WishList::where('client_id', $id)->get();
		foreach ($wishLists as $wishList)
		{
			$wishList->delete();
		}
		
		return redirect(route('ClientListView'));
	}

	public function EditClientView($id)
	{
		$id = Client::findOrFail($id);
		return view($this->GetPreView().'Edit')->with(['id' => $id]);
	}

	public function EditClient(ClientRequest $request, $id)
	{
		$id = Client::findOrFail($id);
		$password = Hash::make($id->id);
		$request->merge(['password' => $password]);
		$request->merge(['birthDate' => jDateTime::createDatetimeFromFormat('Y/m/d H:i:s', $request['birthDate'])->format("Y-m-d H:i:s")]);
		if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
		{
			$filename = $this->userService->uploadAvatar($request->file('avatar'));
			$request->request->add(['image' => $filename]);
		}
		$id->update($request->all());
		return redirect(route('ClientListView'));
	}
	
}
