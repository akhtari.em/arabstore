<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Intervention\Image\Exception\NotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use  Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
 
	
	public function render($request, Exception $exception)
	{
		if ($exception instanceof ModelNotFoundException || $exception instanceof  NotFoundHttpException)
		{
			return $this->NotFoundHttpMessages($request, $exception);
		}
		
		if($exception instanceof HttpException && $exception->getStatusCode() == 429)
		{
			return $this->response429($request, $exception);
		}
		return parent::render($request, $exception);
	}
	
	public function NotFoundHttpMessages($request, Exception $exception)
	{
		if ($request->expectsJson()) {
			return response()->json([
				'error' => 'not found',
			], 404);
		} else {
			return parent::render($request, $exception);
		}
	}
	
	public function response429($request, Exception $exception)
	{
		if ($request->expectsJson()) {
			return response()->json([
				'error' => 'Too_Many_Attempts',
			], 429)->withHeaders($exception->getHeaders());
		}
		else
		{
			return parent::render($request, $exception);
		}
	}
	

}
