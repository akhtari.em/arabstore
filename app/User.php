<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {
	use Notifiable;

/**
 * The attributes that are mass assignable.
 *
 * @var array
 */
	protected $table = 'users';
	protected $fillable = [
		'username', 'password',
	];

/**
 * The attributes that should be hidden for arrays.
 *
 * @var array
 */
	protected $hidden = [
		'password', 'remember_token',
	];

//	public function setPasswordAttribute($value)
	//	{
	//		$this->attributes['password'] = Hash::make($value);
	//	}
}
