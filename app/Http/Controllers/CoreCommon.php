<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\Employee\Models\Message;
use Illuminate\Support\Facades\Auth;
class CoreCommon extends Controller
{

    public static function UserHaveAllPermission($moduleName)
    {
        $permAll=json_decode(Auth::user()->permissions,true)['admin'][$moduleName]['permAll'];
        if($permAll=='yes' ||  Auth::guard('web')->user()->id == 1)
        {
            return true;
        }else
        {
            return false;
        }
    }

  	public static function GetModulesName()
    {
        $dd=CoreCommon::osD();
    		$modules=[];
    		$dirs = array_filter(glob(app_path('Modules').$dd.'*'),'is_dir');
            foreach ($dirs as $dir) {
                if(file_exists($dir.$dd."Manifest.json")){
                   $manifest_json=file_get_contents($dir.$dd."Manifest.json", "r");
                   $manifest=json_decode($manifest_json,true);
                   if($manifest['status']=='Active'){
                     array_push($modules,substr(strrchr($dir,$dd), 1));
                   }
                }
            }
        return $modules;
  	}

  	public static function GetAdminModulesPermissions()
    {
        $dd=CoreCommon::osD();
      	$modules=[];
  		  $dirs = array_filter(glob(app_path('Modules').$dd.'*'),'is_dir');
          foreach ($dirs as $dir) {
              if(file_exists($dir.$dd."Manifest.json")  && is_dir($dir.$dd."Admin")){
                 $manifest_json=file_get_contents($dir.$dd."Manifest.json", "r");
                 $manifest=json_decode($manifest_json,true);
                 if($manifest['status']=='Active' && $manifest['type']=='public' ){
                   //array_push($modules,substr(strrchr($dir,"\'"), 1));
                 	 $temp=[];
                 	 $temp['moduleName']=$manifest['title'];
                 	 $temp['permissions']=$manifest['permissions']['admin'];
                 	 array_push($modules,$temp);
                 }
              }
          }
          return $modules;
  	}

    public static function osD()
    {
        $os_version=strtolower(php_uname());
        if(preg_match('*windows*',$os_version)){
          return '\\';
        }
        else if(preg_match('*linux*',$os_version))
        {
          return '/';
        }
    }


    public static function FileUploader($name,$size,$count,$module,$side,$perm,$initialPic=null)
    { 
        if(!empty($initialPic)){
            $database=$initialPic['database'];
            $field=$initialPic['field'];
            $type=$initialPic['type'];
            $identities=implode($initialPic['id'],',');
            echo view('FileUploader')->with('data',['name'=>$name,'size'=>$size,'count'=>$count,'module'=>$module,'side'=>$side,'permission'=>$perm,'database'=>$database,'field'=>$field,'id'=>$identities,'type'=>$type]);
        }
        else
        {
            echo view('FileUploader')->with('data',['name'=>$name,'size'=>$size,'count'=>$count,'module'=>$module,'side'=>$side,'permission'=>$perm]);
        }
    }
    /**
     * Get Avatar Display base path based on env : BASE_UPLOAD_PATH
     * @param $file_path
     * @return string
     */
    public static function getLocalhostPath($file_path)
{
    return env('APP_URL', 'http://localhost/ArabStore/public') . $file_path;
}
}