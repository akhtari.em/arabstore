<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ClientViewRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use App\Modules\AppMedia\Admin\Models\AppMedia;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\ClientDownload\Admin\Models\ClientDownload;
use App\Modules\ClientView\Admin\Models\ClientView;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\WishList\Admin\Models\WishList;
use App\Services\AppServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AppController extends Controller {
	
	public function index(Request $request)
	{
		$apps = App::select(['id', 'title'])->latest('id')->paginate(3)->makeHidden(['app_apk', 'app_media', 'icon_path', 'image_path', 'video_path', 'apk_url', 'wish_list_id']);
		$appsPaginate = App::select(['id', 'title'])->latest('id')->paginate(3);
//		$appData = App::select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->latest('id')->get()->makeHidden(['image_path', 'video_path', 'apk_url']);
		$url = url()->current() . '?page=' . $appsPaginate->currentPage();
		$page = $request->input('page');
		
		$message = [
			'result' => $apps,
			'paginateUrl' => $url,
		
		];
		return response()->json($message);
	}
	
	public function create()
	{
		//
	}
	
	
	public function store(Request $request)
	{
		//
	}
	
	public function show($id)
	{
	
	}
	
	
	public function edit($id)
	{
		//
	}
	
	
	public function update(Request $request, $id)
	{
		//
	}
	
	
	public function destroy($id)
	{
		//
	}
	
	// show app information such as category,... and users comments
	public function appInfo(Request $request)
	{
		$appId = $request->input('id');
		$findApp = App::find($appId);
		$validate = Validator::make($request->all(), [
			'id' => 'required|exists:app,id',
		]);
		if ($validate->fails())
		{
			$error = [
				'error' => $validate->errors()
			];
			return response()->json($error);
		}
		$user = Auth('api')->user();
		$wish = WishList::where('client_id', $user->id)->where('app_id', $findApp->id)->first();
		if ($wish)
		{
			$wishListId = $wish->id;
			// iterator app table
			$app = App::where('id', $findApp->id)->select(['id', 'title', 'video', 'age', 'company', 'price', 'icon', 'image', 'age',
				'downloadCount', 'installCount', 'rateAvarage', 'category', 'viewCount', 'detail', 'change'])->latest('id')->first()->makeHidden(['app_media', 'wish_list_id']);
			$similarApps = App::where('category', $app->category)->where('id', '<>', $app->id)->select(['id', 'title', 'icon', 'rateAvarage'])->latest('id')->take(2)->get()->makeHidden(['app_apk', 'app_media', 'image_path', 'video_path']);
//			$appDataInfo = App::where('category', $app->category)->where('status', 1)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->latest('id')->take(2)->get()->makeHidden(['image_path', 'video_path']);
			$appMedia = AppMedia::where('app_id', $findApp->id)->select(['videoImage', 'file'])->latest('id')->first();
			if (!empty($appMedia))
			{
				$images = [];
				if (!empty($appMedia->videoImage))
				{
					$temps = explode('|', $appMedia->videoImage);
					foreach ($temps as $temp)
					{
						$images[] = AppServices::getAvatarDisplayPath("/uploads/App/$temp");
					}
				}
				
				$video = [];
				if (!empty($appMedia->file))
				{
					$video[] = AppServices::getAvatarDisplayPath("/uploads/App/$appMedia->file");
				}
				
				// calculation appRate
				$category = AppCategory::where('id', $app->category)->select(['id', 'title', 'icon'])->latest('id')->first();
				
				// check exists  appRate
				$appRates = AppRate::where('app_id', $findApp->id)->get();
				if (!empty($appRates))
				{
					$sum = 0;
					$count = 0;
					foreach ($appRates as $appRate)
					{
						$sum = $sum + $appRate->rate;
//						$count = count($appRates);
					}
					$rate1 = AppRate::where('rate', 1)->where('app_id', $findApp->id)->get();
					$rate2 = AppRate::where('rate', 2)->where('app_id', $findApp->id)->get();
					$rate3 = AppRate::where('rate', 3)->where('app_id', $findApp->id)->get();
					$rate4 = AppRate::where('rate', 4)->where('app_id', $findApp->id)->get();
					$rate5 = AppRate::where('rate', 5)->where('app_id', $findApp->id)->get();
					if (count($appRates) > 0)
					{
						$avg = number_format(floatval($sum / ($appRates->count())), 1, '.', '');
						$findApp->rateAvarage = $avg;
						$findApp->update($request->all());
						$rate = [
							'Rate1' => count($rate1),
							'Rate2' => count($rate2),
							'Rate3' => count($rate3),
							'Rate4' => count($rate4),
							'Rate5' => count($rate5),
							'count' => count($appRates),
							'avg' => $avg
						];
						$app = [
							'app' => $app,
							'wish_list_id' => $wishListId,
							'category' => $category,
							'similarApps' => $similarApps,
							'Rate' => $rate,
							'appMedia' => array_merge($images, $video)
						];
						$result = [
							'result' => $app,
						];
						return response()->json($result);
					}
					else
					{
						$rate = [
							'Rate1' => count($rate1),
							'Rate2' => count($rate2),
							'Rate3' => count($rate3),
							'Rate4' => count($rate4),
							'Rate5' => count($rate5),
							'sum' => count($appRates),
							'avg' => 0
						];
						$app = [
							'app' => $app,
							'category' => $category,
							'wish_list_id' => $wishListId,
							'similarApps' => $similarApps,
							'Rate' => $rate,
							'appMedia' => array_merge($images, $video)
						];
						$result = [
							'result' => $app,
//							'appData' => $appDataInfo
						];
						return response()->json($result);
					}
				}
				else
				{
					$rate = [
						'Rate1' => 0,
						'Rate2' => 0,
						'Rate3' => 0,
						'Rate4' => 0,
						'Rate5' => 0,
						'count' => 0,
						'avg' => 0
					];
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => array_merge($images, $video)
					
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
				
			}
			else
			{
				// calculation appRate
				$category = AppCategory::where('id', $app->category)->select(['id', 'title', 'icon'])->latest('id')->first();
				$appRates = AppRate::where('app_id', $findApp->id)->select(['rate'])->get();
				$sum = 0;
				foreach ($appRates as $appRate)
				{
					$sum = $sum + $appRate->rate;
				}
				$rate1 = AppRate::where('rate', 1)->where('app_id', $findApp->id)->get();
				$rate2 = AppRate::where('rate', 2)->where('app_id', $findApp->id)->get();
				$rate3 = AppRate::where('rate', 3)->where('app_id', $findApp->id)->get();
				$rate4 = AppRate::where('rate', 4)->where('app_id', $findApp->id)->get();
				$rate5 = AppRate::where('rate', 5)->where('app_id', $findApp->id)->get();
				
				if (!empty($appRates->count()) > 0)
				{
					$avg = number_format(floatval($sum / ($appRates->count())), 1, '.', '');
					$findApp->rateAvarage = $avg;
					$findApp->update($request->all());
					$rate = [
						'Rate1' => count($rate1),
						'Rate2' => count($rate2),
						'Rate3' => count($rate3),
						'Rate4' => count($rate4),
						'Rate5' => count($rate5),
						'count' => count($appRates),
						'avg' => $avg
					];
					$images = [];
					$video = [];
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => array_merge($images, $video)
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
				else
				{
					$rate = [
						'Rate1' => count($rate1),
						'Rate2' => count($rate2),
						'Rate3' => count($rate3),
						'Rate4' => count($rate4),
						'Rate5' => count($rate5),
						'count' => count($appRates),
						'avg' => 0
					];
					$images = [];
					$video = [];
					
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => array_merge($images, $video)
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
			}
		}
		else
		{
			$wishListId = null;
			// iterator app table
			$app = App::where('id', $findApp->id)->select(['id', 'title', 'video', 'age', 'company', 'price', 'icon', 'image'
				, 'downloadCount', 'installCount', 'rateAvarage', 'category', 'viewCount', 'detail', 'change'])->latest('id')->first()->makeHidden(['app_media', 'wish_list_id']);
			$similarApps = App::where('category', $app->category)->where('id', '<>', $app->id)->select(['id', 'title', 'icon', 'rateAvarage'])->latest('id')->take(2)->get()->makeHidden(['app_apk', 'app_media', 'image_path', 'video_path']);
//			$appDataInfo = App::where('category', $app->category)->where('status', 1)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->latest('id')->take(2)->get()->makeHidden(['image_path', 'video_path']);
			$appMedia = AppMedia::where('app_id', $findApp->id)->select(['videoImage', 'file'])->latest('id')->first();
			if (!empty($appMedia))
			{
				$images = [];
				if (!empty($appMedia->videoImage))
				{
					$temps = explode('|', $appMedia->videoImage);
					foreach ($temps as $temp)
					{
						$images[] = AppServices::getAvatarDisplayPath("/uploads/App/$temp");
					}
				}
				
				$video = [];
				if (!empty($appMedia->file))
				{
					$video[] = AppServices::getAvatarDisplayPath("/uploads/App/$appMedia->file");
				}
				
				// calculation appRate
				$category = AppCategory::where('id', $app->category)->select(['id', 'title', 'icon'])->latest('id')->first();
				
				// check exists  appRate
				$appRates = AppRate::where('app_id', $findApp->id)->get();
				if (!empty($appRates))
				{
					$sum = 0;
					$count = 0;
					foreach ($appRates as $appRate)
					{
						$sum = $sum + $appRate->rate;
//						$count = count($appRates);
					}
					$rate1 = AppRate::where('rate', 1)->where('app_id', $findApp->id)->get();
					$rate2 = AppRate::where('rate', 2)->where('app_id', $findApp->id)->get();
					$rate3 = AppRate::where('rate', 3)->where('app_id', $findApp->id)->get();
					$rate4 = AppRate::where('rate', 4)->where('app_id', $findApp->id)->get();
					$rate5 = AppRate::where('rate', 5)->where('app_id', $findApp->id)->get();
					if (count($appRates) > 0)
					{
						$avg = number_format(floatval($sum / ($appRates->count())), 1, '.', '');
						$findApp->rateAvarage = $avg;
						$findApp->update($request->all());
						$rate = [
							'Rate1' => count($rate1),
							'Rate2' => count($rate2),
							'Rate3' => count($rate3),
							'Rate4' => count($rate4),
							'Rate5' => count($rate5),
							'count' => count($appRates),
							'avg' => $avg
						];
						$app = [
							'app' => $app,
							'wish_list_id' => $wishListId,
							'category' => $category,
							'similarApps' => $similarApps,
							'Rate' => $rate,
							'appMedia' => $images
						];
						$result = [
							'result' => $app,
//							'appData' => $appDataInfo
						];
						return response()->json($result);
					}
					else
					{
						$rate = [
							'Rate1' => count($rate1),
							'Rate2' => count($rate2),
							'Rate3' => count($rate3),
							'Rate4' => count($rate4),
							'Rate5' => count($rate5),
							'sum' => count($appRates),
							'avg' => 0
						];
						$app = [
							'app' => $app,
							'category' => $category,
							'wish_list_id' => $wishListId,
							'similarApps' => $similarApps,
							'Rate' => $rate,
							'appMedia' => array_merge($images, $video)
						];
						$result = [
							'result' => $app,
//							'appData' => $appDataInfo
						];
						return response()->json($result);
					}
				}
				else
				{
					$rate = [
						'Rate1' => 0,
						'Rate2' => 0,
						'Rate3' => 0,
						'Rate4' => 0,
						'Rate5' => 0,
						'count' => 0,
						'avg' => 0
					];
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => $images
					
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
				
			}
			else
			{
				// calculation appRate
				$category = AppCategory::where('id', $app->category)->select(['id', 'title', 'icon'])->latest('id')->first();
				$appRates = AppRate::where('app_id', $findApp->id)->select(['rate'])->get();
				$sum = 0;
				foreach ($appRates as $appRate)
				{
					$sum = $sum + $appRate->rate;
				}
				$rate1 = AppRate::where('rate', 1)->where('app_id', $findApp->id)->get();
				$rate2 = AppRate::where('rate', 2)->where('app_id', $findApp->id)->get();
				$rate3 = AppRate::where('rate', 3)->where('app_id', $findApp->id)->get();
				$rate4 = AppRate::where('rate', 4)->where('app_id', $findApp->id)->get();
				$rate5 = AppRate::where('rate', 5)->where('app_id', $findApp->id)->get();
				
				if (!empty($appRates->count()) > 0)
				{
					$avg = number_format(floatval($sum / ($appRates->count())), 1, '.', '');
					$findApp->rateAvarage = $avg;
					$findApp->update($request->all());
					$rate = [
						'Rate1' => count($rate1),
						'Rate2' => count($rate2),
						'Rate3' => count($rate3),
						'Rate4' => count($rate4),
						'Rate5' => count($rate5),
						'count' => count($appRates),
						'avg' => $avg
					];
					$images = [];
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => $images
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
				else
				{
					$rate = [
						'Rate1' => count($rate1),
						'Rate2' => count($rate2),
						'Rate3' => count($rate3),
						'Rate4' => count($rate4),
						'Rate5' => count($rate5),
						'count' => count($appRates),
						'avg' => 0
					];
					$images = [];
					$app = [
						'app' => $app,
						'category' => $category,
						'wish_list_id' => $wishListId,
						'similarApps' => $similarApps,
						'Rate' => $rate,
						'appMedia' => $images
					];
					$result = [
						'result' => $app,
//						'appData' => $appDataInfo
					];
					return response()->json($result);
				}
			}
		}
	}
	
	// continue appInfo method  and show app categories similar
	public function MoreAppCategorySimilar(Request $request)
	{
		$id = $request->input('id');
		$page = $request->input('page');
		$findApp = App::find($id);
		$user = auth('api')->user();
		if ($findApp)
		{
			if ($user->type == 'on')
			{
				$similarApps = App::where('category', $findApp->category)->where('age', $user->age)->where('id', '<>', $findApp->id)->select(['id', 'title', 'icon', 'rateAvarage'])->paginate(10)->makeHidden(['app_media', 'image_path', 'video_path', 'wish_list_id']);
				$similarAppsPaginate = App::where('category', $findApp->category)->where('id', '<>', $findApp->id)->paginate(10);
				$url = url()->current() . '?page=' . $similarAppsPaginate->currentPage();
				$result = [
					'result' => $similarApps,
					'paginateUrl' => $url,
				];
				return response()->json($result);
			}
			else
			{
				$similarApps = App::where('category', $findApp->category)->where('id', '<>', $findApp->id)->select(['id', 'title', 'icon', 'rateAvarage'])->paginate(10)->makeHidden(['app_media', 'image_path', 'video_path', 'wish_list_id']);
				$similarAppsPaginate = App::where('category', $findApp->category)->where('id', '<>', $findApp->id)->paginate(10);
				$url = url()->current() . '?page=' . $similarAppsPaginate->currentPage();
				$result = [
					'result' => $similarApps,
					'paginateUrl' => $url,
				];
				return response()->json($result);
			}
			
		}
		else
		{
			$error = [
				'error' => 'app_dose_not_exist'
			];
			return response()->json($error);
		}
	}


//	public function appComment(Request $request)
//	{
//		$appId = $request->input('id');
//		$page = $request->input('page');
//		$star = $request->input('star');
//		$findApp = App::find($appId);
//		$validate = Validator::make($request->all(),[
//			'id' =>'required|exists:app,id',
//			'star' => 'in:0,1,2,3,4,5'
//		]);
//		if ($validate->fails())
//		{
//			$error = [
//				'error' => $validate->errors()
//			];
//			return response()->json($error);
//		}
//		$user = Auth('api')->user();
//		if ($star == 0)
//		{
//			$findUserComment = null;
//			$userComment = $findApp->comments()->where('client_id', $user->id)->first();
//			$userRate = AppRate::where('client_id', $user->id)->where('app_id',$findApp->id)->first();
//
//			if ($userComment)
//			{
//				$findUserComment = [
//					'id' => $userComment->id,
//					'comment' => $userComment->comment,
//					'client_id' => $userComment->client_id,
//					'create_at' => $userComment->created_at->format('Y/m/d'),
//					'client' => $userComment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray(),
//					'rate' => $userRate->rate,
//				];
//			}
//			$comments_list = [];
//			$comments = $findApp->comments()->where('client_id', '<>', $user->id)->paginate(10);
//			foreach ($comments as $comment)
//			{
////				$userComment = $findApp->comments()->where('client_id', $user->id)->first();
//				$appRate = AppRate::where('client_id', $comment->client_id)->where('app_id',$findApp->id)->get();
//				foreach ($appRate as $app)
//				{
//					$comments_list[] =
//					[
//						'comment' => $comment->comment,
//						'client_id' => $comment->client_id,
//						'create_at' => $comment->created_at->format('Y/m/d'),
//						'client' => $comment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray(),
//						'rate' => $app->rate,
//					];
//				}
//			}
//			$url = url()->current() . '?page=' . $comments->currentPage();
//			$result = [
//				'result' => $comments_list,
//				'userComment' => $findUserComment,
//				'paginateUrl' => $url,
//			];
//			return response()->json($result);
//		}
//		else
//		{
//			$comments_list = [];
//			$comments = $findApp->comments()->where('client_id', '<>', $user->id)->paginate(10);
//			$findUserComment = null;
//			foreach ($comments as $comment)
//			{
//				$appRate = AppRate::where('client_id', $comment->client_id)->where('app_id',$findApp->id)->where('rate', $star)->get();
//				foreach ($appRate as $app)
//				{
//					$comments_list[] = [
//						'comment' => $comment->comment,
//						'client_id' => $comment->client_id,
//						'create_at' => $comment->created_at->format('Y/m/d'),
//						'client' => $comment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
//						'rate' => $app->rate,
//					];
//					$userComment = $findApp->comments()->where('client_id', $user->id)->first();
//					if ($userComment)
//					{
//						$findUserComment = [
//							'id' => $userComment->id,
//							'comment' => $userComment->comment,
//							'client_id' => $userComment->client_id,
//							'create_at' => $userComment->created_at->format('Y/m/d'),
//							'client' => $userComment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
//							'rate' => $app->rate,
//						];
//					}
//
//				}
//			}
//			$url = url()->current() . '?page=' . $comments->currentPage();
//			$result = [
//				'result' => $comments_list,
//				'userComment' => $findUserComment,
//				'paginateUrl' => $url,
//			];
//			return response()->json($result);
//		}
//	}
	public function appComment(Request $request)
	{
		$appId = $request->input('id');
		$page = $request->input('page');
		$star = $request->input('star');
		$findApp = App::find($appId);
		$validate = Validator::make($request->all(), [
			'id' => 'required|exists:app,id',
			'star' => 'in:0,1,2,3,4,5'
		]);
		if ($validate->fails())
		{
			$error = [
				'error' => $validate->errors()
			];
			return response()->json($error);
		}
		$user = auth('api')->user();
		if ($star == 0)
		{
			$findUserComment = null;
			$userComment = $findApp->comments()->where('client_id', $user->id)->first();
			
			$userRate = AppRate::where('client_id', $user->id)->where('app_id', $findApp->id)->first();
			
			// show user comment that login
			if ($userComment)
			{
				$findUserComment = [
					'id' => $userComment->id,
					'comment' => $userComment->comment,
					'client_id' => $userComment->client_id,
					'create_at' => $userComment->created_at->format('Y/m/d'),
					'client' => $userComment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
					'rate' => $userRate->rate,
				];
			
			}
			// show all users comment
			$comments_list = [];
			$comments = $findApp->comments()->where('client_id', '<>', $user->id)->paginate(10);
			foreach ($comments as $comment)
			{
//				$userComment = $findApp->comments()->where('client_id', $user->id)->first();
				$appRate = AppRate::where('client_id', $comment->client_id)->where('app_id', $findApp->id)->get();
				foreach ($appRate as $app)
				{
					$comments_list[] = [
						'comment' => $comment->comment,
						'client_id' => $comment->client_id,
						'create_at' => $comment->created_at->format('Y/m/d'),
						'client' => $comment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
//						'client' => $userComment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
						'rate' => $app->rate,
					];
				}
			}
			
			$url = url()->current() . '?page=' . $comments->currentPage();
			$result = [
				'result' => $comments_list,
				'userComment' => $findUserComment,
				'paginateUrl' => $url,
			];
			return response()->json($result);
			
		}
		else
		{
			$comments_list = [];
			$comments = $findApp->comments()->where('client_id', '<>', $user->id)->paginate(10);
			$findUserComment = null;
			foreach ($comments as $comment)
			{
				$appRate = AppRate::where('client_id', $comment->client_id)->where('app_id', $findApp->id)->where('rate', $star)->get();
				foreach ($appRate as $app)
				{
					$comments_list[] = [
						'comment' => $comment->comment,
						'client_id' => $comment->client_id,
						'create_at' => $comment->created_at->format('Y/m/d'),
						'client' => $comment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
						'rate' => $app->rate,
					];
					$userComment = $findApp->comments()->where('client_id', $user->id)->first();
					if ($userComment)
					{
						$findUserComment = [
							'id' => $userComment->id,
							'comment' => $userComment->comment,
							'client_id' => $userComment->client_id,
							'create_at' => $userComment->created_at->format('Y/m/d'),
							'client' => $userComment->client()->select(['id', 'name', 'family', 'image'])->get()->toArray()[0],
							'rate' => $app->rate,
						];
					}
					
				}
			}
			$url = url()->current() . '?page=' . $comments->currentPage();
			$result = [
				'result' => $comments_list,
				'userComment' => $findUserComment,
				'paginateUrl' => $url,
			];
			return response()->json($result);
		}
	}
	
	
	public function countAppDownload(Request $request)
	{
		$id = $request->input('id');
		$appId = App::find($id);
		$validate = Validator::make($request->all(), [
			'id' => 'required|exists:app,id'
		]);
		if ($validate->fails())
		{
			$error = [
				'error' => $validate->errors()
			];
			return response()->json($error);
		}
		$user = auth('api')->check();
		if ($user)
		{
			$userInfo = auth('api')->user();
			if ($appId)
			{
				// find app and update download count
				$downloadCount = $appId->downloadCount;
				$appId->downloadCount = $downloadCount + 1;
				$appId->update($request->all());
				
				// find app_id in AppApk and update download count
				$findAppApk = AppApk::where('app_id', $appId->id)->where('status', 1)->latest('id')->first();
				$appApkDownloadCount = $findAppApk->download;
				$findAppApk->download = $appApkDownloadCount + 1;
				$findAppApk->update($request->all());
				
				// insert new record in client_download table
				$clientDownload = new ClientDownload();
				$clientDownload->client_id = $userInfo->id;
				$clientDownload->app_id = $appId->id;
				$clientDownload->save();

//				$appData = App::where('id', $appId->id)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'apk_url']);
				$message = [
					'result' => 1,
//					'appData' => $appData
				];
				return response()->json($message);
			}
			else
			{
				$error = [
					'error' => 'app_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
		
	}
	
	public function appInstall(Request $request)
	{
		$appId = $request->input('id');
		$user = auth('api')->check();
		if ($user)
		{
			$findAppId = App::find($appId);
			if ($findAppId)
			{
				$validate = Validator::make($request->all(), [
					'id' => 'required|exists:app,id'
				]);
				if ($validate->fails())
				{
					$error = [
						'error' => $validate->errors()
					];
					return response()->json($error);
				}
				// update count installCount filed in app table
				$installCount = $findAppId->installCount;
				$findAppId->installCount = $installCount + 1;
				$findAppId->update($request->all());
//				$appData = App::where('id', $findAppId->id)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'apk_url']);
				
				$result = [
					'result' => 1,
//					'appData' => $appData
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'app_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	}
	
	public function appView(ClientViewRequest $request)
	{
		$appId = $request->input('app_id');
		$checkUser = auth('api')->check();
		if ($checkUser)
		{
			$findAppId = App::find($appId);
			$user = auth('api')->user();
			if ($findAppId)
			{
				// update count of appView field in app table
				$viewCount = $findAppId->viewCount;
				$findAppId->viewCount = $viewCount + 1;
				$findAppId->update($request->all());
				
				// insert new record in client_view table
				$request->merge(['client_id' => $user->id]);
				ClientView::create($request->all());
//				$appData = App::where('id', $findAppId->id)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'apk_url']);
				
				$result = [
					'result' => 1,
//					'appData' => $appData
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'app_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	}
	
	public function appUpdate(Request $request)
	{
		$user = auth('api')->check();
		if ($user)
		{
			$packages = $request->input('packages');
			$versions = $request->input('versions');
			
			$info = [];
			foreach ($packages as $key => $package)
			{
				$info[$package] = $versions[$key];
			}
			
			$findApps = App::whereIn('package', $packages)->select('id', 'title', 'package', 'icon', 'change')->get()->makeHidden(['app_apk', 'app_media', 'image_path', 'apk_url', 'wish_list_id']);
			$appIdes = [];
			foreach ($findApps as $findApp)
			{
				$appIdes[$findApp->id] = $findApp->toArray();
			}
			$appApks = AppApk::whereIn('app_id', array_keys($appIdes))->select('app_id', 'versionCode', 'versionName', 'file')->get();
			$needUpdates = [];
			$checkUpdate = 0;
			foreach ($appApks as $appApk)
			{
				
				if (!empty($info[$appIdes[$appApk->app_id]['package']]))
				{
					$checkUpdate = 0;
//					dd($info[$appIdes[$appApk->app_id]['package']]);
					if ($appApk->versionCode > $info[$appIdes[$appApk->app_id]['package']])
					{
						$checkUpdate = 1;
					}
					$needUpdates[] = [
						'app_id' => $appApk->app_id,
						'title' => $appIdes[$appApk->app_id]['title'],
						'change' => $appIdes[$appApk->app_id]['change'],
						'package_name' => $appIdes[$appApk->app_id]['package'],
						'icon' => $appIdes[$appApk->app_id]['icon_path'],
						'version_code' => $appApk->versionCode,
						'version_name' => $appApk->versionName,
						'check_update' => $checkUpdate,
						'apk_url' => AppServices::getAvatarDisplayPath("/uploads/App/$appApk->file"),
						
					];
				}
			}
			$message = [
				'result' => $needUpdates,
//				'che' => $checkUpdate
			];
			return response()->json($message);
		}
		else
		{
			$error = [
				'error' => 'user_has_not_login'
			];
			return response()->json($error);
		}
	}
	
	// show all info for apps according package name
	public function appsInfo(Request $request)
	{
		$ides = $request->input('ids');
		$findApps = App::whereIn('id', $ides)->select('id', 'title', 'package', 'icon', 'change')->get()->makeHidden(['app_apk', 'app_media', 'image_path', 'apk_url', 'wish_list_id']);
		$appIdes = [];
		foreach ($findApps as $findApp)
		{
			$appIdes[$findApp->id] = $findApp->toArray();
		}
		
		$appApks = AppApk::whereIn('app_id', array_keys($appIdes))->select('app_id', 'versionCode', 'versionName', 'file')->get();
		
		$apps = [];
		foreach ($appApks as $appApk)
		{
			$apps[] = [
				'app_id' => $appApk->app_id,
				'title' => $appIdes[$appApk->app_id]['title'],
				'change' => $appIdes[$appApk->app_id]['change'],
				'package_name' => $appIdes[$appApk->app_id]['package'],
				'icon' => $appIdes[$appApk->app_id]['icon_path'],
				'version_code' => $appApk->versionCode,
				'version_name' => $appApk->versionName,
				'apk_url' => AppServices::getAvatarDisplayPath("/uploads/App/$appApk->file")
			];
			
		}
		$message = [
			'result' => $apps
		];
		return response()->json($message);
	}
	
	public function appDownload(Request $request)
	{
		$app = $request->input('id');
		$user = auth('api')->check();
		if ($user)
		{
			$findApp = App::find($app);
			$validate = Validator::make($request->all(), [
				'app_id' => 'required|exists:app,id'
			]);
			if ($validate->fails())
			{
				$error = [
					'error' => $validate->errors()
				];
				return response()->json($error);
			}
			if ($findApp)
			{
//				$appData = App::where('id', $findApp->id)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'apk_url']);
				$appApk = AppApk::where('app_id', $findApp->id)->select(['id', 'file'])->latest('id')->first();
				$message = [
					'file' => AppServices::getAvatarDisplayPath("/uploads/App/" . $appApk->file)
				];
				$result = [
					'result' => $message,
//					'appData' => $appData
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'app_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_has_not_login'
			];
			return response()->json($error);
		}
	}
}
