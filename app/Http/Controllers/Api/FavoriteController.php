<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\FavoriteRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Comment\Admin\Models\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\AppServices;
class FavoriteController extends Controller
{
    
    public function index()
    {
        //
    }

 
    public function create()
    {
        //
    }

  
    public function store(FavoriteRequest $request)
    {
        $itemId = $request->input('item_id');
        $user = Auth::user();
        if ($user)
        {
        	$request->merge(['client_id' => $user->id]);
        	Favorite::create($request->all());
	        $result = [
		        'result' => '1'
	        ];
	        return response()->json($result, 200);
        }
        else
        {
	        $error = [
		        'error' => 'user_is_not_exist'
	        ];
	        return response()->json($error, 200);
        }
    }
	
    public function show($id)
    {
    
    }

 
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
	
	public function userListFavorites()
	{
		$user = Auth::user();
		$apps = [];
		$appIconUrl = [];
		
		if ($user)
		{
			$favorites = Favorite::where('client_id', $user->id)->get();
			foreach ($favorites as $favorite)
			{
				// iterator apps  that exists in favorite table
				$apps[] = App::where('id', $favorite->item_id)->select(['id','title', 'icon'])->get();
				$appIcons = App::where('id', $favorite->item_id)->get();
				foreach ($appIcons as $appIcone)
				{
					if ($appIcone->icon)
					{
						$appIconUrl[] = AppServices::getAvatarDisplayPath("/uploads/App/$appIcone->icon") . "/appId" . $appIcone->id;
						
					}
					else
					{
						$appIconUrl[] = "/$appIcone->id";
					}
				}
			}
			$result = [
				'apps' => $apps,
				'appIconUrl' => $appIconUrl,
			];
			return response()->json($result);
		
		}
		else
		{
			$error = [
				'error' => 'user_is_not_exist'
			];
			return response()->json($error, 200);
		}
	}

}
