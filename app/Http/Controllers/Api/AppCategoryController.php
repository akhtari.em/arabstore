<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\AppCategory\Admin\Models\AppCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AppServices;
class AppCategoryController extends Controller
{
   
    public function index()
    {
        $categories = AppCategory::select(['id', 'title', 'icon'])->where('parent', 0)->latest('id')->get();
	    $tempCategory = [];
	    $tc = 0;
	    foreach ($categories as $app)
	    {
		    $tempCategory[$tc] = $app;
		    $tc++;
	    }
	    $categories = $tempCategory;
	    $result = [
	        'result' => $categories,
	    ];
        return response()->json($result,200);
    }

 
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }

  
    public function show($id)
    {
	    $appCategory = AppCategory::find($id);
	    if ($appCategory)
	    {
		    $categories = AppCategory::select(['id', 'title', 'icon'])->where('parent', $appCategory->id)->latest('id')->get();
		    $tempCategory = [];
		    $tc = 0;
		    foreach ($categories as $app)
		    {
		    	$tempCategory[$tc] = $app;
		    	$tc++;
		    }
		    $categories = $tempCategory;
		    $result = [
			    'result' => $categories
		    ];
		    return response()->json($result);
	    }
	    else
	    {
		    $error = [
			    'error' => 'app_dose_not_exist'
		    ];
		    return response()->json($error);
	    }
    }

 
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
	
	public function more($id)
	{
		$appCategory = AppCategory::find($id);
		if ($appCategory)
		{
			$categories = AppCategory::select(['id', 'title', 'icon', 'icon2'])->where('parent', $id)->latest('id')->get();
			$icon1Url = [];
			$icon2Url = [];
			foreach ($categories as $app)
			{
				if ($app->icon)
				{
					$icon1Url[] = AppServices::getAvatarDisplayPath("/uploads/App/$app->icon") . "/appCategoryId" . $app->id;
				}
				else
				{
					$icon1Url[] = "appCategoryId".$app->id ;
				}
				if ($app->icon2)
				{
					$icon2Url[] = AppServices::getAvatarDisplayPath("/uploads/App/$app->icon2") . "/appCategoryId" . $app->id;
				}
				else
				{
					$icon2Url[] = "appCategoryId".$app->id ;
				}
			}
			$result = [
				'categories' => $categories,
				'icon1Url' => $icon1Url,
				'icon2Url' => $icon2Url
			];
			return response()->json($result,200);
		}
		else
		{
			$error = [
				'error' => 'app_dose_not_exist'
			];
			return response()->json($error);
		}

	}
	
	public function listAppCategory(Request $request)
	{
		$categoryId = $request->input('id');
		$page = $request->input('page');
		$findCategory = AppCategory::find($categoryId);
		$user = auth('api')->user();
		if ($findCategory)
		{
			if ($user->type == 'on')
			{
				$apps = App::where('category', $findCategory->id)->where('age', $user->age)->select(['id', 'title', 'rateAvarage', 'icon','age'])->paginate(10)->makeHidden(['app_media', 'image_path', 'video_path','wish_list_id']);
				$appsPaginate = App::where('category', $findCategory->id)->paginate(10);
				$url = url()->current().'?page='. $appsPaginate->currentPage();
				$page = $request->input('page');
				$message = [
					'result' => $apps,
					'paginateUrl' =>$url,
				
				];
				return response()->json($message);
			}
			else
			{
				$apps = App::where('category', $findCategory->id)->select(['id', 'title', 'rateAvarage', 'icon','age'])->paginate(10)->makeHidden(['app_media', 'image_path', 'video_path','wish_list_id']);
				$appsPaginate = App::where('category', $findCategory->id)->paginate(10);
				$url = url()->current().'?page='. $appsPaginate->currentPage();
				$page = $request->input('page');
				$message = [
					'result' => $apps,
					'paginateUrl' =>$url,
				
				];
				return response()->json($message);
			}
			
		}
		else
		{
			$error = [
				'error' => 'category_dose_not_exist'
			];
			return response()->json($error);
		}
	}
}
