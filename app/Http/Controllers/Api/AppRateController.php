<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AppRateRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppRate\Admin\Models\AppRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppRateController extends Controller
{
	public function store(AppRateRequest $request)
	{
		$app = $request->input('app_id');
		$rate = $request->input('rate');
		
		$user = Auth('api')->check();
		if ($user)
		{
			$userInfo = auth('api')->user();
			$find = AppRate::where('app_id', $app)->where('client_id', $userInfo->id)->latest('id')->first();
			if (!empty($find))
			{
				// find app
				$findApp = App::where('id', $app)->first();
				$find->rate = $rate;
				$find->update($request->all());
				// update app average
				$appRates = AppRate::where('app_id', $findApp->id)->select(['rate'])->get();
				$sum = 0;
				foreach ($appRates as $appRate)
				{
					$sum = $sum + $appRate->rate;
				}
				$avg = number_format(floatval($sum /($appRates->count())),1,'.', '');
				$findApp->rateAvarage = $avg;
				$findApp->update($request->all());
//				$appData = App::where('id', $app)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'video_path']);
				$result = [
					'result' => 1,
//					'appData' => $appData
				];
				return response()->json($result);
			}
			else
			{
				if ($userInfo)
				{
					$request->merge(['client_id' => $userInfo->id]);
					 AppRate::create($request->all());
//					$appData = App::where('id', $app)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'video_path']);
					// update app average
					$findApp = App::where('id', $app)->first();
					$appRates = AppRate::where('app_id', $findApp->id)->select(['rate'])->get();
					$sum = 0;
					foreach ($appRates as $appRate)
					{
						$sum = $sum + $appRate->rate;
					}
					$avg = number_format(floatval($sum /($appRates->count())),1,'.', '');
					$findApp->rateAvarage = $avg;
					$findApp->update($request->all());
					$result = [
						'result' => 1,
//						'appData' => $appData
					];
					return response()->json($result);
				}
				else
				{
					$error = [
						'error' => 'app_id_dose_not_exist'
					];
					return response()->json($error);
				}
			}
			
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	}
}
