<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\MarketVersion\Admin\Models\MarketVersion;
use App\Services\AppServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketVersionController extends Controller
{
	public function marketVersion(Request $request)
	{
		$versionCode = $request->input('version');
		$lastVersion = MarketVersion::select('version_code')->where('status', 1)->latest('id')->first();
		if ($lastVersion->version_code == $versionCode)
		{
			$message = [
				'result' => 'no_update'
			];
			return response()->json($message);
		}
		
		elseif($lastVersion->version_code <= $versionCode)
		{
			$message = [
				'result' => 'no_update'
			];
			return response()->json($message);
		}
		else
		{
			$version = MarketVersion::select('note', 'file', 'force_status')->latest('id')->first();
			$message = [
				'note' => $version->note,
				'force_status' => boolval($version->force_status),
				'file' => AppServices::getAvatarDisplayPath("/uploads/App/" . $version->file)
			];
			$appData = App::where('id', 0)->select(['id', 'title', 'icon', 'rateAvarage','package', 'isFree', 'inAppPayment'])->get()->makeHidden('image_path');
			$result = [
				'result' => $message,
				'appData' => $appData
			];
			return response()->json( $result);
		}
	}
}
