<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CommentRequest;
use App\Modules\App\Admin\Models\App;
use App\Modules\AppRate\Admin\Models\AppRate;
use App\Modules\Comment\Admin\Models\Comment;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use League\OAuth2\Server\Grant\AuthCodeGrant;

class CommentController extends Controller
{
    
    public function index()
    {

    }

    
    public function create()
    {
        //
    }

   
    public function store(CommentRequest $request)
    {
    	$comment = $request->input('comment');
    	$itemId = $request->input('item_id');
        $user = Auth::user();
        if ($user)
        {
	        $find = Comment::where('item_id', $itemId)->where('client_id', $user->id)->latest('id')->first();
			if ($find)
			{
				$find->comment = $comment;
				$find->update($request->all());
				$result = [
					'result' => '1'
				];
				return response()->json($result, 200);
			}
			else
			{
				$request->merge(['client_id' => $user->id]);
				Comment::create($request->all());
				$result = [
					'result' => '1'
				];
				return response()->json($result, 200);
			}
	      
        }
        else
        {
	        $error = [
		        'error' => 'user_is_exist'
	        ];
	        return response()->json($error, 200);
        }
    }

 
    public function show($id)
    {

    }

  
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
	
    // show if user has comment and rate for app
	public function userAppDetails(Request $request)
	{
		$appId = $request->input('id');
		$user = Auth('api')->user();
		$findApp = App::find($appId);
		if ($findApp)
		{
			if ($user)
			{
				// show user comment and rate for this app
				$userComment = Comment::where('client_id', $user->id)->where('item_id',$findApp->id)->first();
				$userRate = AppRate::where('client_id', $user->id)->where('app_id',$findApp->id)->first();
				$userWishList = WishList::where('client_id', $user->id)->where('app_id',$findApp->id)->first();
				// check user has comment for app
				if (!empty($userRate) )
				{
					// check user has rate for app
					if (!empty($userWishList))
					{
						$userAppDetails = [
							'comment' => $userComment->comment,
							'rate' => $userRate->rate,
							'wishList' => 1
						];
						$result = [
							'result' => $userAppDetails,
						];
						return response()->json($result);
					}
					else
					{
						$userAppDetails = [
							'comment' => $userComment->comment,
							'rate' => $userRate->rate,
							'wishList' => 0
						];
						$result = [
							'result' => $userAppDetails,
						];
						return response()->json($result);
					}
					
				}
				else
				{
					if (!empty($userWishList))
					{
						$userAppDetails = [
							'comment' => "",
							'rate' => "",
							'wishList' => 1
						];
						$result = [
							'result' => $userAppDetails,
						];
						return response()->json($result);
					}
					else
					{
						$userAppDetails = [
							'comment' => "",
							'rate' => "",
							'wishList' => 0
						];
						$result = [
							'result' => $userAppDetails,
						];
						return response()->json($result);
					}
				
				}
			}
			else
			{
				$error = [
					'error' => 'app_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'app_dose_not_exist'
			];
			return response()->json($error);
		}
		
	}
	
	public function userComment(Request $request)
	{
		$page = $request->input('page');
		$user = auth('api')->check();
		if ($user)
		{
			$commentsList = [];
//	        $appData = [];
			$userInfo = auth('api')->user();
			$comments = Comment::where('client_id', $userInfo->id)->select(['id', 'comment', 'item_id', 'created_at', 'client_id'])->paginate(10);
			foreach ($comments as $comment)
			{
				$appRate = AppRate::where('client_id', $comment->client_id)->where('app_id',$comment->item_id)->get();
				foreach ($appRate as $app)
				{
					$commentsList[] = [
						'id' => $comment->id,
						'comment' => $comment->comment,
						'create_at' => $comment->created_at->format('Y.m.d'),
						'app' => $comment->app()->select(['id', 'title','rateAvarage', 'icon'])->get()->makeHidden(['app_apk', 'app_media', 'image_path', 'wish_list_id', 'video_path', 'apk_url'])->toArray()[0],
					   'rate' => $app->rate,
					];
				}
			}
			
			$url = url()->current().'?page='. $comments->currentPage();
			$message = [
				'result' => $commentsList,
				'paginateUrl' =>$url,
//		        'appData' => $appData
			];
			return response()->json($message);
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	}
}
