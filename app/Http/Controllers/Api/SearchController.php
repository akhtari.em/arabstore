<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\AppApk\Admin\Models\AppApk;
use App\Services\AppServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller {
	
	public function index()
	{
	
	}
	
	
	public function create()
	{
		//
	}
	
	
	public function store(Request $request)
	{
		$title = $request->input('title');
		
		$lesson = App::paginate(10);
		$apps = App::where('title', 'like', '%' . $title . '%')
			->select('id','title', 'age', 'sex', 'made', 'isFree', 'inAppPayment', 'price', 'rateAvarage', 'icon')
			->paginate(10)->makeHidden(['app_apk', 'app_media', 'image_path', 'apk_url', 'wish_list_id']);
		
		$paginateUrl = url()->current().'?page='. $lesson->currentPage() ;
		$currentPage = $lesson->currentPage();
		$apps = [
			'apps' => $apps,
			'url' =>$paginateUrl,
			'currentPage' => $currentPage,
		];
//		$appData = App::where('title', 'like', '%'. $title . '%')->select('id', 'title','icon','package', 'rateAvarage', 'isFree', 'inAppPayment')->paginate(10)->makeHidden('image_path');
		$result = [
			'result' => $apps,
//			'appData' => $appData
		];
		return response()->json($result);
	}
	
	
	public function show($id)
	{
		//
	}
	
	public function edit($id)
	{
		//
	}
	
	
	public function update(Request $request, $id)
	{
		//
	}
	
	public function destroy($id)
	{
		//
	}
}
