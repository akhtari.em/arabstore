<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\Vitrine\Admin\Models\Vitrine;
use App\Modules\VitrineItem\Admin\Models\VitrineItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\CoreCommon;
class VitrineController extends Controller {
	
	public function index()
	{
		$vitrines = Vitrine::latest('id')->select('id')->get()->makeHidden(['vitrine_items_list']);
		$result = [
			'result' => $vitrines
		];
		return response()->json($result);
	}
	
//	public function __construct()
//	{
//		$this->middleware('auth.basic');
//	}
	
	public function create()
	{
		//
	}
	
	
	public function show(Request $request)
	{
		$type = $request->input('type_of_show');
		$validator = Validator::make($request->all(), [
			'type_of_show' => 'in:home,game'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		
		
		return $this->getInfo($type);
	}
	
	private function getInfo($type)
	{
		$user = Auth('api')->user();
		if ($user)
		{
			$info = [];
			$vitrines = Vitrine::where('status', 1)->where('type_of_show', $type)->select(['id', 'title', 'type'])->latest('id')->get();
			$i = 0;
			$t = [];
			foreach ($vitrines as $item)
			{
				$info[$i] = [
					'id' => $item->id,
					'title' => $item->title,
					'type' => $item->type,
				];
				
				$j = 0;
				foreach ($item->vitrine_items_list as $vitrine_item)
				{
					if ($user->type == 'on')
					{
						$apps =  App::where('age',$user->age)->where('id',$vitrine_item->app_id)->select(['id', 'title','image','video','rateAvarage', 'age'])->get();
						foreach ($apps as $value)
						{
							if ($value->age == $user->age )
							{
								
								$info[$i]['vitrine_items'][$j] = [
									'vitrine_id' => $vitrine_item->vitrine_id,
									'app_id' => $vitrine_item->app_id,
									'type' => $vitrine_item->type,
									'url' => $vitrine_item->url,
//								'app' => App::where('age',$user->age)->where('id',$vitrine_item->app_id)->select(['id', 'title','image','video','rateAvarage'])->get()
									'app' => array_only($vitrine_item->app_info->toArray(), ['id', 'title', 'icon_path', 'rateAvarage','app_apk','apk_url','share_url','age', 'type'])
								];
								
								$j++;
							}

						}
					}
					else
					{
						$info[$i]['vitrine_items'][$j] = [
							'vitrine_id' => $vitrine_item->vitrine_id,
							'app_id' => $vitrine_item->app_id,
							'type' => $vitrine_item->type,
							'url' => $vitrine_item->url,
//								'app' => App::where('age',$user->age)->where('id',$vitrine_item->app_id)->select(['id', 'title','image','video','rateAvarage'])->get()
							'app' => array_only($vitrine_item->app_info->toArray(), ['id', 'title', 'icon_path', 'rateAvarage','app_apk','apk_url','share_url','age', 'type'])
						];
						
						$j++;
					}
				}
				
				$i++;
			}
			
			$results = [
				'result' => $info,
//
			];
			
			return response()->json($results);
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
		
	}
	
	public function MoreVitrine($vitrineId)
	{
		$vitrineFind = Vitrine::find($vitrineId);
		$user = Auth('api')->user();
		
		if ($vitrineFind)
		{
			$info = [];
			$vitrineItems = VitrineItem::where('vitrine_id', $vitrineFind->id)->where('status', 1)->select(['id', 'app_id'])->get();
			foreach ($vitrineItems as $vitrineItem)
			{
				if ($user->type == 'on')
				{
					$apps =  App::where('age',$user->age)->where('id',$vitrineItem->app_id)->select(['id', 'title','image','video','rateAvarage', 'age'])->get();
					foreach ($apps as $value)
					{
						if ($value->age == $user->age)
						{
							$info[] =
								array_only($vitrineItem->app_info->toArray(), ['id','age', 'title', 'icon_path', 'rateAvarage','app_apk','apk_url','share_url']);
						}
					}
				}
				else
				{
					$info[] =
						array_only($vitrineItem->app_info->toArray(), ['id','age', 'title', 'icon_path', 'rateAvarage','app_apk','apk_url','share_url']);
				}
				
				// find app  for user according  age

			}
			$results = [
				'result' => $info,
			];
			return response()->json($results);
		}
		else
		{
			$error = [
				'error' => 'vitrine_dose_not_exist'
			];
			return response()->json($error);
		}
	}
	

}
