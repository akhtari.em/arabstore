<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ClientRequest;
use App\Modules\Client\Admin\Models\Client;
use App\Modules\Confirmation\Confirmation;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Null_;
use PhpParser\Node\Expr\New_;
use Response;
use Carbon\Carbon;
use Kavenegar;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Illuminate\Support\Facades\Hash;

class ApiClientManagerController extends Controller {
	private $userService;
	
	public function __construct(UserService $userService)
	{
		$this->userService = $userService;
	}
	
	public function Profile(ClientRequest $request)
	{
		$name = $request->input('name');
		$family = $request->input('family');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$userEmail = Client:: find($email);
		if ($userEmail)
		{
			$error = [
				'error' => 'user_is_exist'
			];
			return response()->json($error, 200);
		}
		else
		{
			$client = new Client();
			$client->name = $name;
			$client->family = $family;
			$client->phone = $phone;
			$client->email = $email;
			$client->password = Hash::make($email);
			$client->save();
			$result = [
				'result' => '1'
			];
			return response()->json($result, 200);
		}
	}
	
	public function Login(Request $request)
	{
		$loginType = $request->input('loginType');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$name = $request->input('name');
		$family = $request->input('family');
		$image = $request->input('avatar');
		$validator = Validator::make($request->all(), [
			'loginType' => 'in:phone,google,facebook,email'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		if ($loginType == 'phone')
		{
			return $this->Phone($request);
			
		}
		elseif ($loginType == 'email')
		{
			return $this->Email($request);
		}
		
		elseif ($loginType == "facebook")
		{
			return $this->Facebook($request);
			
		}
		else
		{
			return $this->Google($request, $name, $family, $phone, $email, $image);
		}
	}
	
	public function Confirmation(Request $request)
	{
		$phone = $request->input('phone');
		$verificationcode = $request->input('code');
		$phone = $request->input('phone');
		$persianNumber = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
		$englishNumber = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
		$phone = str_replace($persianNumber, $englishNumber, $phone);
		$verificationcode = str_replace($persianNumber, $englishNumber, $verificationcode);
		$validator = Validator::make(Input::all(), [
			'phone' => 'required',
			'code' => 'required'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		$userPhone = Client::where('phone', $phone)->first();
		//check user_phone
		if ($userPhone)
		{
			//			dd($verificationcode );
			//			if ($userPhone->confirmation())
			//			{
			if ($verificationcode === '1234')  //check user code equal with code
				//				if ($verificationcode == $userPhone->confirmation()->code)  //check user code equal with code
			{
				//if user is not enable then enable the user
				if (!$userPhone->status)
				{
					$userPhone->status = '1';
					$userPhone->save();
				}
				$result = [
					'result' => '1'
				];
				return response()->json($result, 200);
			}
			//			}
			$error = [
				'error' => 'wrong_verification_code'
			];
			return response()->json($error);
		}
		else
		{
			$error = [
				'message' => 'user_not_found'
			
			];
			return response()->json($error);
		}
	}
	
	public function IndexProfile()
	{
		$user = auth('api')->user();
		if ($user)
		{
			if ($user->image == "")
			{
				$userFind = Client::select('name', 'family', 'email', 'phone', 'id')->where('id', $user->id)->get()->makeHidden(['image_path']);
				$result = [
					'result' => $userFind,
				];
				return response()->json($result);
			}
			else
			{
				$userFind = Client::select('name', 'family','image', 'email', 'phone', 'id')->where('id', $user->id)->get();
				$result = [
					'result' => $userFind,
				];
				return response()->json($result);
			}
			
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_exist'
			];
			return response()->json($error);
		}
	}
	
	public function UpdateProfile(ClientRequest $request)
	{
		$name = $request->input('name');
		$family = $request->input('family');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$image = $request->input('avatar');
		$age = $request->input('age');
		$type = $request->input('type');
		$password = $request->input('parent_password');
		$user = auth('api')->user();
		if ($user)
		{
			$client = Client::where('id', $user->id)->first();
			if ($client)
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				if (empty($client->image))
				{
					$client->image = "";
				}
				if (!empty($filename))
				{
					$client->image = $filename;
				}
				if (!empty($age))
				{
					$client->age = $age;
				}
				if (!empty($type))
				{
					$client->type = $type;
				}
				
				if (!empty($email))
				{
					$client->email = $email;
				}
				if (!empty($name))
				{
					$client->name = $name;
				}
				if (!empty($family))
				{
					$client->family = $family;
				}
				if (!empty($phone))
				{
					$client->phone = $phone;
				}
//				$pass = $client->parent_password;
//				if ($pass == "")
//				{
//					$hash =  Hash::make($password);
//					$request->merge(['parent_password' => $hash]);
//					$client->type = $type;
//					$client->update($request->all());
//					$result = [
//						'result' => UserService::getAvatarDisplayPath("/uploads/avatars/". $client->image),
//					];
//					return response()->json($result);
//				}
//				if (!empty($type))
//				{
//					$client->type = $type;
//				}
				if ($type)
				{
					$pass = $client->parent_password;
					
					if ($pass == "")
					{
						$hash =  Hash::make($password);
						$request->merge(['parent_password' => $hash]);
						$client->type = $type;
						$client->update($request->all());
						$result = [
							'result' => UserService::getAvatarDisplayPath("/uploads/avatars/". $client->image),
						];
						return response()->json($result);
					}
					else
					{
						if ( Hash::check($password,$pass))
						{
							
							$hash =  Hash::make($password);
							$request->merge(['parent_password' => $hash]);
							$client->update($request->all());
							$result = [
								'result' => UserService::getAvatarDisplayPath("/uploads/avatars/". $client->image),
							];
							return response()->json($result);
						}
						else
						{
							$error = [
								'result' => '0'
							];
							return response()->json($error);
						}
					}
				}
				$client->update($request->all());
				$result = [
					'result' => UserService::getAvatarDisplayPath("/uploads/avatars/". $client->image),
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'user_dose_not_exist'
				];
				return response()->json($error);
			}
		}
	}
	
	public function Phone(Request $request)
	{
		$phone = $request->input('phone');
		$areaCode = $request->input('area_code');
		$persianNumber = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
		$englishNumber = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
		$phone = str_replace($persianNumber, $englishNumber, $phone);
		$areaCode = str_replace($persianNumber, $englishNumber, $areaCode);
		$validator = Validator::make($request->all(), [
			'phone' => [
				'required',
				'numeric'
			],
			'area_code' => 'required',
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		// find user phone if exist
		$findPhone = Client::where('phone', $phone)->first();
		if ($findPhone)
		{
			if ($findPhone->image == "")
			{
				$message = [
					'result' => 1,
					'clientId' => $findPhone->id,
					'name' => $findPhone->name,
					'family' => $findPhone->family,
					'email' => $findPhone->email,
					'phone' => $findPhone->phone,
					'image' => UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg'),
					'age' => $findPhone->age,
					'type' => $findPhone->type,
					'parent_password' => $findPhone->parent_password
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			else
			{
				$message = [
					'result' => 1,
					'clientId' => $findPhone->id,
					'name' => $findPhone->name,
					'family' => $findPhone->family,
					'email' => $findPhone->email,
					'phone' => $findPhone->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findPhone->image),
					'age' => $findPhone->age,
					'type' => $findPhone->type,
					'parent_password' => $findPhone->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			
		}
		else
		{
			
			$code = mt_rand(1000, 9999);  //generate code 6 character
			$client = new Client();
			$client->phone = $phone;
			$client->loginType = "phone";
			$client->area_code = $areaCode;
			$client->save();
			$confirm = new Confirmation();
			$confirm->expire_code = Carbon::now()->addMinutes(4);  //expire code 6 character after 4 min
			$confirm->code = $code;
			$confirm->client_id = $client->id;
			$confirm->save();
			$findClient = Client::where('id', $client->id)->first();
			$findClient->password = Hash::make($client->id);
			$findClient->save();
			//send sms to user
			//			try
			//			{
			//				$receptor = $phone;
			//				$template = "";
			//				$type = "sms";
			//				$token = $code;
			//				$token2 = "";
			//				$token3 = "";
			//				$result = Kavenegar::VerifyLookup($receptor, $token, $token2, $token3, $template, $type);
			//
			//			} catch (ApiException $e)
			//			{
			//				return response()->json([
			//					'code' => '404',
			//					'message' => 'ApiException',
			//					'errors' => $e->errorMessage()
			//				], 404);
			//			} catch (HttpException $e)
			//			{
			//				return response()->json([
			//					'code' => '404',
			//					'message' => 'HttpException',
			//					'errors' => $e->errorMessage()
			//				], 404);
			//			}
			$clientID = $confirm->client_id;
			$userInfo = Client::where('id', $clientID)->first();
			$message = [
				'clientId' => $userInfo->id,
				'name' => "",
				'family' => "",
				'email' => "",
				'phone' => "",
				'image' => UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg'),
				'age' => "16",
				'type' => "",
				'parent_password' => "",
				'result' => '1',
			];
			$resultMessage = [
				'result' => $message,
			
			];
			return response()->json($resultMessage);
		}
	}
	
	public function Email(Request $request)
	{
		$email = $request->input('email');
		$validator = Validator::make($request->all(), [
			'email' => 'required|email'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		$findEmail = Client::where('email', $email)->first();
		if ($findEmail)
		{
			// check user hsa image
			if ($findEmail->image == "")
			{
				$message = [
					'result' => 1,
					'clientId' => $findEmail->id,
					'name' => $findEmail->name,
					'family' => $findEmail->family,
					'email' => $findEmail->email,
					'phone' => $findEmail->phone,
					'image' => UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg'),
					'age' => $findEmail->age,
					'type' => $findEmail->type,
					'parent_password' => $findEmail->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			else
			{
				$message = [
					'result' => 1,
					'clientId' => $findEmail->id,
					'name' => $findEmail->name,
					'family' => $findEmail->family,
					'email' => $findEmail->email,
					'phone' => $findEmail->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findEmail->image),
					'age' => $findEmail->age,
					'type' => $findEmail->type,
					'parent_password' => $findEmail->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			
		}
		else
		{
			
			$client = new  Client();
			$client->email = $email;
			$client->loginType = "email";
			$client->save();
			$findClient = Client::where('id', $client->id)->first();
			$findClient->password = Hash::make($client->id);
			$findClient->save();
			$message = [
				'result' => '1',
				'clientId' => $client->id,
				'name' => "",
				'family' => "",
				'email' => "",
				'phone' => "",
				'image' => UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg'),
				'age' => "16",
				'type' => "",
				'parent_password' => ""
			];
			$resultMessage = [
				'result' => $message,
			
			];
			return response()->json($resultMessage, 200);
		}
	}
	
	public function Facebook(Request $request)
	{
		$phone = $request->input('phone');
		$email = $request->input('email');
		$name = $request->input('name');
		$family = $request->input('family');
		$image = $request->input('avatar');
		$validator = Validator::make($request->all(), [
			'phone' => '',
			'avatar' => 'image|mimes:jpeg,jpg,png',
			'email' => 'email'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		$emailFind = Client::where('email', $email)->first();
		$findPhone = Client::where('phone', $phone)->first();
		if ($emailFind)
		{
			if ($phone == null)
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$emailFind->name = $name;
				$emailFind->family = $family;
				if (!empty($filename))
				{
					$emailFind->image = $filename;
				}
				else
				{
					$emailFind->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$emailFind->email = $email;
				$emailFind->loginType = "facebook";
				$emailFind->update();
				
				// update password
				$findClient = Client::where('id', $emailFind->id)->first();
				$findClient->password = Hash::make($emailFind->id);
				$message = [
					'result' => '1',
					'clientId' => $findClient->id,
					'name' => $findClient->name,
					'family' => $findClient->family,
					'email' => $findClient->email,
					'phone' => $findClient->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
					'age' => $findClient->age,
					'type' => $findClient->type,
					'parent_password' => $findClient->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				];
				return response()->json($resultMessage);
			}
			else
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$emailFind->name = $name;
				$emailFind->family = $family;
				if (!empty($filename))
				{
					$emailFind->image = $filename;
				}
				else
				{
					$emailFind->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$emailFind->loginType = "facebook";
				$emailFind->phone = $phone;
				$emailFind->email = $email;
				$emailFind->update();
				
				// update password
				$findClient = Client::where('id', $emailFind->id)->first();
				$findClient->password = Hash::make($emailFind->id);
				$message = [
					'result' => '1',
					'clientId' => $emailFind->id,
					'name' => $emailFind->name,
					'family' => $emailFind->family,
					'email' => $emailFind->email,
					'phone' => $emailFind->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $emailFind->image),
					'age' => $emailFind->age,
					'type' => $emailFind->type,
					'parent_password' => $emailFind->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
		}
		else
		{
			
			if ($phone == null)
			{
				
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$client = new Client();
				$client->name = $name;
				$client->family = $family;
				if (!empty($filename))
				{
					$client->image = $filename;
				}
				else
				{
					
					$client->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$client->loginType = "facebook";
				$client->phone = $phone;
				$client->email = $email;
				$client->save();
				// update password if id change
				$findClient = Client::where('id', $client->id)->first();
				$findClient->password = Hash::make($client->id);
				$findClient->save();
				$message = [
					'result' => '1',
					'clientId' => $client->id,
					'name' => $findClient->name,
					'family' => $findClient->family,
					'email' => $findClient->email,
					'phone' => $findClient->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
					'age' => $findClient->age,
					'type' => $findClient->type,
					'parent_password' => $findClient->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			else
			{
				// check if phone exist and email not found then update client
				if ($findPhone)
				{
					if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
					{
						$filename = $this->userService->uploadAvatar($request->file('avatar'));
						$request->request->add(['image' => $filename]);
					}
					$findPhone->name = $name;
					$findPhone->family = $family;
					if (!empty($filename))
					{
						$findPhone->image = $filename;
					}
					else
					{
						$findPhone->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
					}
					$findPhone->loginType = "facebook";
					$findPhone->phone = $phone;
					$findPhone->email = $email;
					$findPhone->update();
					// update password
					$findClient = Client::where('id', $findPhone->id)->first();
					$findClient->password = Hash::make($findPhone->id);
					$message = [
						'result' => '1',
						'clientId' => $findPhone->id,
						'name' => $findPhone->name,
						'family' => $findPhone->family,
						'email' => $findPhone->email,
						'phone' => $findPhone->phone,
						'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findPhone->image),
						'age' => $findPhone->age,
						'type' => $findClient->type,
						'parent_password' => $findClient->parent_password
					
					];
					$resultMessage = [
						'result' => $message,
					
					];
					return response()->json($resultMessage);
				}
				else
				{
					if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
					{
						$filename = $this->userService->uploadAvatar($request->file('avatar'));
						$request->request->add(['image' => $filename]);
					}
					$client = new Client();
					$client->name = $name;
					$client->family = $family;
					if (!empty($filename))
					{
						$client->image = $filename;
					}
					else
					{
						$client->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
					}
					$client->loginType = "facebook";
					$client->phone = $phone;
					$client->email = $email;
					$client->save();
					// update password
					$findClient = Client::where('id', $client->id)->first();
					$findClient->password = Hash::make($client->id);
					$message = [
						'result' => '1',
						'clientId' => $client->id,
						'name' => $findClient->name,
						'family' => $findClient->family,
						'email' => $findClient->email,
						'phone' => $findClient->phone,
						'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
						'age' => $findClient->age,
						'type' => $findClient->type,
						'parent_password' => $findClient->parent_password
					
					];
					$resultMessage = [
						'result' => $message,
					
					];
					return response()->json($resultMessage);
				}
			}
		}
	}
	
	public function Google(Request $request, $name, $family, $phone, $email, $image)
	{
		$emailFind = Client::where('email', $email)->first();
		$findPhone = Client::where('phone', $phone)->first();
		if ($emailFind)
		{
			if ($phone == null)
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$emailFind->name = $name;
				$emailFind->family = $family;
				if (!empty($filename))
				{
					$emailFind->image = $filename;
				}
				else
				{
					$emailFind->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$emailFind->loginType = "google";
//				$emailFind->phone = $phone;
				$emailFind->email = $email;
				$emailFind->update();
				// update password if id change
				$findClient = Client::where('id', $emailFind->id)->first();
				$findClient->password = Hash::make($emailFind->id);
				$findClient->save();
				// update if multiple of same email
				$findClientId = Client::where('phone', $findPhone->phone)->get();
				if (($findClientId->count()) > 1)
				{
					$findClientId->first->delete();
				}
				$message = [
					'result' => '1',
					'clientId' => $emailFind->id,
					'name' => $emailFind->name,
					'family' => $emailFind->family,
					'email' => $emailFind->email,
					'phone' => $emailFind->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $emailFind->image),
					'age' => $emailFind->age,
					'type' => $emailFind->type,
					'parent_password' => $emailFind->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				];
				return response()->json($resultMessage);
			}
			
			else
			{
				
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$emailFind->name = $name;
				$emailFind->family = $family;
				if (!empty($filename))
				{
					$emailFind->image = $filename;
				}
				else
				{
					$emailFind->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$emailFind->loginType = "google";
				$emailFind->phone = $phone;
				$emailFind->email = $email;
				$emailFind->update();
				// update password if id change
				$findClient = Client::where('id', $emailFind->id)->first();
				$findClient->password = Hash::make($emailFind->id);
				$message = [
					'result' => '1',
					'clientId' => $emailFind->id,
					'name' => $emailFind->name,
					'family' => $emailFind->family,
					'email' => $emailFind->email,
					'phone' => $emailFind->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $emailFind->image),
					'age' => $emailFind->age,
					'type' => $emailFind->type,
					'parent_password' => $emailFind->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
		}
		else
		{
			$validator = Validator::make($request->all(), [
				'phone' => '',
				'avatar' => 'image|mimes:jpeg,jpg,png',
				'email' => 'email'
			]);
			if ($validator->fails())
			{
				$error = [
					'error' => $validator->errors()
				];
				return response()->json($error);
			}
			if ($phone == null)
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$client = new Client();
				$client->name = $name;
				$client->family = $family;
				if (!empty($filename))
				{
					$client->image = $filename;
				}
				else
				{
					$client->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$client->loginType = "google";
				$client->phone = $phone;
				$client->email = $email;
				$client->save();
				$findClient = Client::where('id', $client->id)->first();
				$findClient->password = Hash::make($client->id);
				$findClient->save();
				$message = [
					'result' => '1',
					'clientId' => $findClient->id,
					'name' => $findClient->name,
					'family' => $findClient->family,
					'email' => $findClient->email,
					'phone' => $findClient->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
					'age' => $findClient->age,
					'type' => $findClient->type,
					'parent_password' => $findClient->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			elseif ($findPhone)
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$findPhone->name = $name;
				$findPhone->family = $family;
				if (!empty($filename))
				{
					$findPhone->image = $filename;
				}
				else
				{
					$findPhone->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$findPhone->email = $email;
				$findPhone->phone = $phone;
				$findPhone->loginType = "google";
				$findPhone->update();
				// update password if id change
				$findClient = Client::where('id', $findPhone->id)->first();
				$findClient->password = Hash::make($findPhone->id);
				$message = [
					'result' => '1',
					'clientId' => $findClient->id,
					'name' => $findClient->name,
					'family' => $findClient->family,
					'email' => $findClient->email,
					'phone' => $findClient->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
					'age' => $findClient->age,
					'type' => $findClient->type,
					'parent_password' => $findClient->parent_password
				
				];
				$resultMessage = [
					'result' => $message,
				
				];
				return response()->json($resultMessage);
			}
			else
			{
				if ($request->hasFile('avatar') && $request->file('avatar')->isValid())
				{
					$filename = $this->userService->uploadAvatar($request->file('avatar'));
					$request->request->add(['image' => $filename]);
				}
				$client = new Client();
				$client->name = $name;
				$client->family = $family;
				if (!empty($filename))
				{
					$client->image = $filename;
				}
				else
				{
					$client->image = UserService::getAvatarDisplayPath('/uploads/avatars/user.jpg');
				}
				$client->loginType = "google";
				$client->phone = $phone;
				$client->email = $email;
				$client->save();
				$findClient = Client::where('id', $client->id)->first();
				$findClient->password = Hash::make($client->id);
				$findClient->save();
				$message = [
					'result' => '1',
					'clientId' => $findClient->id,
					'name' => $findClient->name,
					'family' => $findClient->family,
					'email' => $findClient->email,
					'phone' => $findClient->phone,
					'image' => UserService::getAvatarDisplayPath("/uploads/avatars/". $findClient->image),
					'age' => $findClient->age,
					'type' => $findClient->type,
					'parent_password' => $findClient->parent_password
				];
				$resultMessage = [
					'result' => $message,
				];
				return response()->json($resultMessage);
			}
		}
	}
	
	public function logout(Request $request)
	{
		$clientId = $request->input('id');
		$validator = Validator::make($request->all(), [
			'id' => 'required|exists:client,id'
		]);
		if ($validator->fails())
		{
			$error = [
				'error' => $validator->errors()
			];
			return response()->json($error);
		}
		$findClient = Client::find($clientId);
		if ($findClient)
		{
			$result = [
				'result' => 1
			];
			return response()->json($result);
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	}
}

