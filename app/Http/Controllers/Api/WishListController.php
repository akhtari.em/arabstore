<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\WishList\Admin\Models\WishList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WishListController extends Controller
{
	public function storeWishList(Request $request)
	{
		$appId = $request->input('app_id');
		$user = auth('api')->check();
		if ($user)
		{
			$app = App::find($appId);
			$userInfo = auth('api')->user();
			if ($app)
			{
				$request->merge(['client_id' => $userInfo->id]);
				$wishList = WishList::create($request->all());
				$wishListId = [
					'id'=>  $wishList->id
				];
				
//				$appData = App::where('id', $app->id)->select(['id', 'title', 'icon', 'rateAvarage', 'package', 'isFree', 'inAppPayment'])->get()->makeHidden(['image_path', 'video_oath']);
				$result = [
					'result' => 1,
					'wish_list_id' => $wishListId
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'app_id_dose_not_exist'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
		
	}
	
	public function userWishList(Request $request)
	{
		$page = $request->input('page');
		$user = auth('api')->check();
		if ($user)
		{
			$userInfo = auth('api')->user();
			$wishes = WishList::where('client_id', $userInfo->id)->select('id', 'app_id')->paginate(10);
			$appIdes = [];
			foreach ($wishes as $wish)
			{
				$appIdes[$wish->app_id] = $wish->toArray();
			}
			$url = url()->current() . '?page=' . $wishes->currentPage();
			$apps = App::whereIn('id', array_keys($appIdes))->select('id', 'title', 'icon', 'rateAvarage')->get()->makeHidden(['app_media', 'image_path', 'app_apk', 'video_path', 'apk_url']);
			$message = [
				'result' => $apps,
				'appData' => $url,
			];
			return response()->json($message);
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
		
	}
	
	public function removeWishList(Request $request)
	{
		$wishList = $request->input('id');
		$validate = Validator::make($request->all(),[
			'id' => 'required|exists:wish_list,id'
		]);
		if ($validate->fails())
		{
			$error = [
				'error' => $validate->errors()
			];
			return response()->json($error);
		}
		$findWishList = WishList::find($wishList);
		
		// check user login
		$user = Auth('api')->check();
		if ($user)
		{
			if ($findWishList)
			{
				$findWishList->delete();
				$result = [
					'result' => 1,
				];
				return response()->json($result);
			}
			else
			{
				$error = [
					'error' => 'id_dose_not_exists'
				];
				return response()->json($error);
			}
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_login'
			];
			return response()->json($error);
		}
	
	}
	
}
