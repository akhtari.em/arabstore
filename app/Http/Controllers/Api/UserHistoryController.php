<?php

namespace App\Http\Controllers\Api;

use App\Modules\App\Admin\Models\App;
use App\Modules\ClientDownload\Admin\Models\ClientDownload;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserHistoryController extends Controller
{
	public function userAppDownloaded(Request $request)
	{
		$page = $request->input('page');
		$user = auth('api')->check();
		if ($user)
		{
			$userInfo = auth('api')->user();
			$clientDownloads = ClientDownload::where('client_id', $userInfo->id)->select(['id', 'app_id'])->get();
			$appIdes = [];
			foreach ($clientDownloads as $clientDownload)
			{
				$appIdes[$clientDownload->app_id] = $clientDownload->toArray();
			}
			$apps = App::whereIn('id', array_keys($appIdes))->select(['id', 'title', 'icon'])->paginate(2)->makeHidden(['app_media', 'app_apk', 'image_path', 'wish_list_id', 'apk_url', 'video_path']);
			$appsPaginate = App::whereIn('id', array_keys($appIdes))->select(['id', 'title', 'icon'])->paginate(2);
			$url = url()->current().'?page='. $appsPaginate->currentPage();
			
			$message = [
				'result' => $apps,
				'paginateUrl' =>$url,
			
			];
			return response()->json($message);
		}
		else
		{
			$error = [
				'error' => 'user_dose_not_exist'
			];
			return response()->json($error);
		}
	}
}
