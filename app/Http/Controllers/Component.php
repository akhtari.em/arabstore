<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Modules\Employee\Models\Message;
class Component extends Controller
{

	public static function Widget($moduleName,$viewName,$parameters)
    {
    	if(view()->exists($moduleName.'::'.$viewName)){
    	echo view($moduleName.'::'.$viewName)->with('parameters',$parameters);
    	}

	}

}
