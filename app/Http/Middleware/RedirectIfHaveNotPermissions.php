<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfHaveNotPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('web')->check() == true) {
            if(Auth::guard('web')->user()->id == 1)
            {

            }
            else
                if(substr(strrchr($request->url(),"Admin/"), 6) !='')
                {   $havePermission=false;
                    //Request Came From Admin
                    $side='admin';
                    $subModuleName=$request->route()->getName();
                    $permissions=json_decode(Auth::guard('web')->user()->permissions,true)[$side];
                    $userAllSinglePermissions=[];
                    foreach($permissions as $permission)
                    {
                        foreach($permission['permissions'] as $perm_json)
                        {
                            $perm=json_decode($perm_json,true);
                            foreach($perm[key($perm)] as $access_name) {
                                array_push($userAllSinglePermissions, $access_name);
                            }
                        }
                    }
                    if(!in_array($subModuleName,$userAllSinglePermissions))
                    {
                        return redirect('/');
                    }
                }

        } elseif (Auth::guard('developer')->check() == true) {

        }






        return $next($request);
    }
}
