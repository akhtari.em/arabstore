<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;
use Illuminate\Http\Request;

class AppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'title' => 'required',
//            'package' => 'required',
            'category' => 'required|exists:app_category,id',
            'company' => '',
            'website' => 'nullable|url',
            'email' => 'nullable|email',
            'phone' => 'nullable|numeric',
            'mobile' => 'nullable|numeric',
            'price' => 'nullable|numeric',
            'age' => 'in:3,7,12,16',
            'sex' => 'in:male,female',
            'made' => 'in:iranian,foreign',
            'detail' => '',
            'order' => '',
            'status' => 'in:0,1',
            'vip' => '',
            'isFree' => 'in:0,1',
	        'developer' => 'exists:developer,id',
	        'appvideo' => 'mimes:mp4',
	        'imageicon' => 'required|mimes:jpeg,jpg,png|max:1024',
	        'avatar' => 'required|mimes:jpeg,jpg,png|max:1024',
	        'inAppPayment' => 'in:1,0',
        ];
	    $route = Route::currentRouteName();
	
	    if ($route == 'EditAppView' || $route == 'EditApp')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
		    if (empty($request->file('imageicon'))) $rules['imageicon'] = '';
//		    if (empty($request->file('appvideo'))) $rules['appvideo'] = '';
	    }
        return $rules;
    }
}
