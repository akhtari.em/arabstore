<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;
use Illuminate\Http\Request;

class AppMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'appvideo' => '|mimes:mkv,mp4',
            'app_id' => 'required|exists:app,id',
	        'type' => 'in:video,image',
	        'avatar' => 'nullable',
	        'status' => 'in:1,0',
	        'isImportant' => 'nullable|in:1,0',
	        'url' => 'nullable|url'
        ];
	    $route = Route::currentRouteName();
	
	    if ($route == 'EditAppMediaView' || $route == 'EditAppMedia')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
		    if (empty($request->file('appvideo'))) $rules['appvideo'] = '';
	    }
	    return $rules;
    }
}
