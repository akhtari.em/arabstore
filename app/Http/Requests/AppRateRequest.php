<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
	        'app_id' => 'required|exists:app,id',
	        'rate' => 'in:1,2,3,4,5'
        ];
        return $rules;
    }
}
