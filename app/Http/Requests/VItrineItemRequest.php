<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Route;
class VItrineItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'vitrine_id' => 'required|exists:vitrine,id',
	        'title' => 'required',
	        'avatar' => 'mimes:jpeg,jpg,png|max:1024|file',
	        'url' => 'nullable|required_if:type,url|url',
	        'type' => 'in:url,app'
         
        ];
	    $route = Route::currentRouteName();

	    if ($route == 'EditVitrineItemView' || $route == 'EditVitrineItem')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
	    }
        return $rules;
    }
}
