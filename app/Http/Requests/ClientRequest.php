<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Route;
use Illuminate\Support\Facades\Input;
class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'phone' => 'required|unique:client|numeric',
	        'name' => '',
	        'email' => 'required|email|unique:client',
	        'city' => '',
	        'sex' => 'in:male,female',
	        'birthDate' => '',
	        'bio' => '',
	        'area_code' => '',
	        'status' => 'in:1,0',
	        'level' => '',
	        'follower' => '',
	        'following' => '',
	        'score' => '',
	        'credit' => '',
	        'family' => '',
	        'avatar' => 'mimes:jpeg,jpg,png|max:1024',
	        'loginType' => 'in:phone,email,google,facebook',
	        'age' => 'in:3,7,12,16',
	        'type'=> 'in:off,on',
//	        'parent_password' => 'required_if:type,on'
        ];
	    $route = Route::currentRouteName();
	    if ($route == 'EditClient' || $route == 'EditClientView')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
		    $rules['phone'] = 'unique:client,phone,' . $this->segment(4);
		    $rules['email'] = 'unique:client,email,' . $this->segment(4);
	    }
	    if ($route == 'UpdateApiClient')
	    {
		    $rules['phone'] = 'unique:client,phone,'. $this->segment(3);
		    $rules['email'] = 'unique:client,email,'. $this->segment(3);
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
	    }
	    return $rules;
    }
}
