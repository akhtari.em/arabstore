<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;
use Illuminate\Http\Request;
class AppApkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
	        'app_id' => 'required|exists:app,id',
	        'apkfile' => '',
	        'versionName' => '',
	        'versionCode' => '',
	        'status' => 'in:1,0',
	        'avatar' => 'mimes:jpeg,jpg,png|file|max:1024',
	        'download' => 'nullable|numeric',
	        'volume' => 'nullable',
	        'publishDate' => 'required|date'
        ];
        
	    $route = Route::currentRouteName();
	    if ($route == 'EditAppApkView' || $route == 'EditAppApk')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
		    if (empty($request->file('apkfile'))) $rules['apkfile'] = '';
		    if (empty($request->file('volume'))) $rules['volume'] = '';
	    }
	    return $rules;
    }
}
