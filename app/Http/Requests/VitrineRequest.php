<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VitrineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
	        'type' => 'in:slider,scroll',
	        'type_of_show' => 'in:home,game'
        ];
        return $rules;
    }
}
