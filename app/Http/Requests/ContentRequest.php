<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Route;

class ContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
	    $rules = [
		    'title' => 'required',
		    'details' => 'required',
		    'avatar' => 'required|mimes:jpeg,jpg,png|max:1024',
	    ];
	    $route = Route::currentRouteName();
	
	    if ($route == 'EditContentView' || $route == 'EditContent')
	    {
		    if (empty($request->file('avatar'))) $rules['avatar'] = '';
	    }
	    return $rules;
    }
}
