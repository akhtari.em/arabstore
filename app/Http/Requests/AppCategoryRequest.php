<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;
use Illuminate\Http\Request;
class AppCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'title' => 'required|unique:app_category',
	        'imageicon' => 'required|mimes:jpeg,jpg,png|file',
	        'avatar' => 'required|mimes:jpeg,jpg,png|file',
	        'order' => 'required|numeric',
        ];
	    $route = Route::currentRouteName();

        if ($route == 'EditAppCategory' || $route == 'EditAppCategoryView')
        {
            if (empty($request->file('avatar'))) $rules['avatar'] = '';
            if (empty($request->file('imageicon'))) $rules['imageicon'] = '';
            $rules['title'] = 'unique:app_category,title,' .$this->segment(4);
        }
	    return $rules;
    }
}
