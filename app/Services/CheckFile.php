<?php
/**
 * Created by PhpStorm.
 * User: seven system
 * Date: 12/11/2018
 * Time: 10:29 AM
 */

namespace App\Services;


trait CheckFile
{
	
	public function checkUploadFile($appvideo)
	{
		$filename  = str_random(6) . '.' . $appvideo->getClientOriginalExtension();
		$file_extension = substr($filename, strrpos($filename, '.'));
		return (($file_extension) == '.apk') ? true : false;
	}
	
	public function checkMediaFile($appvideo)
	{
		$appVideos = castToArray($appvideo);
		foreach ($appVideos as $appvideo)
		{
			$filename  = str_random(6) . '.' . $appvideo->getClientOriginalExtension();
			$file_extension = substr($filename, strrpos($filename, '.'));
			return (($file_extension) == '.mp4' || ($file_extension) == '.png' || ($file_extension) == '.jpg' || ($file_extension) == 'jpeg') ? true : false;
		}
		
	}
}