<?php
/**
 * Developed by Alireza Hamedashki.
 * Email: a.hamedashki@gmail.com
 * Mobile: +98 938 900 4559
 * Date: 4/3/2018
 * Time: 2:29 PM
 */

namespace App\Services;


use Intervention\Image\Facades\Image;

class UserService
{
    /**
     * Upload User's Avatar
     *
     * @param $avatar
     * @param null $user
     * @return string
     */
    public function uploadAvatar($avatar, $user=NULL) {
        $filename  = time() . '.' . $avatar->getClientOriginalExtension();
	    $path = $this->getUploadPath('/uploads/avatars/' . $filename);
	    // remove old avatar
	    if ($user) {
		    if(file_exists($this->getUploadPath('/uploads/avatars/' . $user->avatar))){
			    @unlink($this->getUploadPath('/uploads/avatars/' . $user->avatar));
		    }
	    }
	    Image::make($avatar->getRealPath())->save($path);
	    return $filename;
    }

    /**
     * Remove Current User's Avatar
     *
     * @param $path
     * @param $photo
     */
    public function removePhoto($path, $photo)
    {
        if (is_array($path)) {
            if(file_exists($this->getUploadPath($path[0] . $photo))){
                @unlink($this->getUploadPath($path[0] . $photo));
                @unlink($this->getUploadPath($path[1] . $photo));
            }
        } else {
            if(file_exists($this->getUploadPath($path . $photo))){
                @unlink($this->getUploadPath($path . $photo));
            }
        }
    }
	
	/**
	 * Get Upload base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getUploadPath($file_path)
    {
    	return env('BASE_UPLOAD_PATH', public_path()) . $file_path;
    }
	
	/**
	 * Get Avatar Display base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getAvatarDisplayPath($file_path)
	{
		return env('APP_URL', 'http://localhost/ArabStore') . $file_path;
	}
}