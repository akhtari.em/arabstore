<?php
/**
 * Created by PhpStorm.
 * User: seven system
 * Date: 03/10/2018
 * Time: 03:45 PM
 */

namespace App\Services;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
class AppServices
{
	public function uploadAvatar($avatar)
	{
			$avatars = castToArray($avatar);
			foreach ($avatars as $avatar)
			{
				$filename  =  str_random(6) . '.' . $avatar->getClientOriginalExtension();
				
					$path = $this->getUploadPath('/uploads/App/' . $filename);
					Image::make($avatar->getRealPath())->save($path);
				return $filename;
			}
	}


    public function uploadApp($avatar, $user=NULL) {

        $filename  = time() . '.' . $avatar->getClientOriginalExtension();

        $path = $this->getUploadPath('/uploads/App/' . $filename);
        // remove old avatar
        if ($user) {
            if(file_exists($this->getUploadPath('/uploads/App/' . $user->avatar))){
                @unlink($this->getUploadPath('/uploads/App/' . $user->avatar));
            }
        }
        Image::make($avatar->getRealPath())->save($path);
        return $filename;
    }
	
	public function uploadAvatar2($avatar)
	{
			$filename  =  str_random(6) . '.' . $avatar->getClientOriginalExtension();
			
			$path = $this->getUploadPath('/uploads/App/' . $filename);
			Image::make($avatar->getRealPath())->save($path);
			return $filename;
		
	}
	/**
	 * Remove Current User's Avatar
	 *
	 * @param $path
	 * @param $photo
	 */
	public function removePhoto($path, $photo)
	{
		if (is_array($path)) {
			if(file_exists($this->getUploadPath($path[0] . $photo))){
				@unlink($this->getUploadPath($path[0] . $photo));
				@unlink($this->getUploadPath($path[1] . $photo));
			}
		} else {
			if(file_exists($this->getUploadPath($path . $photo))){
				@unlink($this->getUploadPath($path . $photo));
			}
		}
	}
	
	/**
	 * Get Upload base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getUploadPath($file_path)
	{
		return env('BASE_UPLOAD_PATH', public_path()). $file_path;
	}
	
	/**
	 * Get Avatar Display base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getAvatarDisplayPath($file_path)
	{
		return env('APP_URL', '//localhost/ArabStore') . $file_path;
	}





    public function uploadVideo($appvideo, $user=NULL)
	{
		$appvideos = castToArray($appvideo);
		foreach ($appvideos as  $appvideo)
		{
			$filename = str_random(6) . '.' . $appvideo->getClientOriginalExtension();
			$path = $this->getUploadPath('/uploads/App/');
			$appvideo->move($path, $filename);
			return $filename;
		}
	}

    public function uploadAppName($name, $appvideo)
    {
        $appvideos = castToArray($appvideo);
        foreach ($appvideos as  $appvideo)
        {
            $filename = time() . '_' . $name ;
            $path = $this->getUploadPath('/uploads/App/');
            $appvideo->move($path, $filename);
            return $filename;
        }

    }
	
	public function uploadFile($appvideo, $user=NULL)
	{
		$filename =    $appvideo->getClientOriginalExtension();
		$file_extension = substr($filename, strrpos($filename, '.'));
		if ($file_extension == '.apk')
		{
			$path =  $this->getUploadPath('/uploads/App/');
			$appvideo->move($path, $filename);
			return $filename;
		}
		else
		{
			return false;
		}
	}
	

	
	
}