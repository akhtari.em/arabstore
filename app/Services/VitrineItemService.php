<?php
/**
 * Created by PhpStorm.
 * User: seven system
 * Date: 02/10/2018
 * Time: 05:53 PM
 */

namespace App\Services;
use Intervention\Image\Facades\Image;

class VitrineItemService
{
	
	public function uploadAvatar($avatar, $user=NULL) {
		$filename  = time() . '.' . $avatar->getClientOriginalExtension();
		$path = $this->getUploadPath('/uploads/vitrineItem/' . $filename);
		if ($user) {
			if(file_exists($this->getUploadPath('/uploads/vitrineItem/' . $user->avatar))){
				@unlink($this->getUploadPath('/uploads/vitrineItem/' . $user->avatar));
			}
		}
		Image::make($avatar->getRealPath())->save($path);
		return $filename;
	}
	
	/**
	 * Remove Current User's Avatar
	 *
	 * @param $path
	 * @param $photo
	 */
	public function removePhoto($path, $photo)
	{
		if (is_array($path)) {
			if(file_exists($this->getUploadPath($path[0] . $photo))){
				@unlink($this->getUploadPath($path[0] . $photo));
				@unlink($this->getUploadPath($path[1] . $photo));
			}
		} else {
			if(file_exists($this->getUploadPath($path . $photo))){
				@unlink($this->getUploadPath($path . $photo));
			}
		}
	}
	
	/**
	 * Get Upload base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getUploadPath($file_path)
	{
		return env('BASE_UPLOAD_PATH', public_path()). $file_path;
	}
	
	/**
	 * Get Avatar Display base path based on env : BASE_UPLOAD_PATH
	 * @param $file_path
	 * @return string
	 */
	public static function getAvatarDisplayPath($file_path)
	{
		return env('APP_URL', 'http://localhost/ArabStore') . $file_path;
	}
	
	public function uploadVitrine($avatar, $user=NULL) {
		$filename  = time() . '.' . $avatar->getClientOriginalExtension();
		$path = $this->getUploadPath('/uploads/vitrineItem/' . $filename);
		if ($user) {
			if(file_exists($this->getUploadPath('/uploads/vitrineItem/' . $user->avatar))){
				@unlink($this->getUploadPath('/uploads/vitrineItem/' . $user->avatar));
			}
		}
		Image::make($avatar->getRealPath())->resize(532,300)->save($path);
		return $filename;
	}
}