<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function()
//Route::group(['middleware' => 'throttle:2'], function ()
{
	Route::post('Confirmation', 'ApiClientManagerController@Confirmation')->name('Confirmation');
	Route::post('Login', 'ApiClientManagerController@Login')->name('Login');
	Route::resource('AppCategory', 'AppCategoryController');
//	Route::post('App', 'AppController@index');
	Route::post('MoreAppCategorySimilar', 'AppController@MoreAppCategorySimilar' )->name('MoreAppCategorySimilar');
	Route::post('AppComment', 'AppController@appComment')->name('AppComment');
	Route::resource('Search', 'SearchController');
	Route::post('Logout', 'ApiClientManagerController@logout')->name('Logout');
	Route::post('MarketVersion', 'MarketVersionController@marketVersion')->name('MarketVersion');
	Route::post('ListAppCategory', 'AppCategoryController@listAppCategory')->name('ListAppCategory');
	
	//	Route::post('SearchApp', 'SearchController@store')->name('SearchApp');
//	Route::get('AppHome/{id}', 'AppController@appHome')->name('AppHome');
	Route::group(['middleware' => 'auth:api'], function (){
		Route::get('Index', 'ApiClientManagerController@IndexProfile')->name('IndexApiClient');
		Route::post('UpdateProfile', 'ApiClientManagerController@UpdateProfile')->name('UpdateApiClient');
		Route::resource('Comment', 'CommentController');
		Route::resource('Favorite', 'FavoriteController');
		Route::post('AppsInfo', 'AppController@appsInfo')->name('AppsInfo');
		
		Route::get('UserFavorite', 'FavoriteController@userListFavorites')->name('UserListFavorite');
//		Route::post('AppDownload', 'AppController@appDownload')->name('AppDownload');
		Route::post('CountAppDownload', 'AppController@countAppDownload')->name('CountAppDownload');
		Route::post('AppInstall', 'AppController@appInstall')->name('AppInstall');
		Route::post('AppView', 'AppController@appView')->name('AppView');
		Route::post('AppUpdate', 'AppController@appUpdate')->name('AppUpdate');
		Route::post('WishList', 'WishListController@storeWishList')->name('WishList');
		Route::post('Rate', 'AppRateController@store')->name('Rate');
		Route::post('UserWishList', 'WishListController@userWishList')->name('UserWishList');
		Route::post('UserAppDetails', 'CommentController@userAppDetails')->name('UserAppDetails');
		Route::post('UserComments', 'CommentController@userComment')->name('UserComments');
		Route::post('RemoveWishList', 'WishListController@removeWishList')->name('RemoveWishList');
		Route::post('AppUserHistory', 'UserHistoryController@userAppDownloaded')->name('UserAppDownloadHistory');
		Route::post('AppInfo', 'AppController@appInfo' )->name('AppInfo');   // show app information such as category,... and users comments
		Route::get('ShowVitrinesId', 'VitrineController@index')->name('vitrineShow');
		Route::get('MoreVitrine/{vitrineId}', 'VitrineController@MoreVitrine')->name('MoreVitrine');
		Route::post('vitrine', 'VitrineController@show')->name('vitrineShow');
		
	});
});
