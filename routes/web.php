<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Modules\UserCategory\Admin\Models\Client;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CoreCommon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

app()->setLocale('fa');

Route::get('/Admin', function () {
    return redirect(route('UsersLogin'));
});

Route::get('/Web', function () {
    return redirect('Master/Web');
});

//Uploader Routes

//For Upload Files
Route::post('upload/{name}&{module}&{side}', function ($name, $module, $side) {

    $hash_name = Hash::make(time() . Input::get('qqfilename'));
    $filePath = public_path() . '/Data/' . $module . '/' . $side . '/' . $hash_name;
    $fileUrl = url('/') . '/Data/' . $module . '/' . $side . '/' . $hash_name;
    //$fileUrl=strrchr($fileUrl,"/");
    move_uploaded_file($_FILES['qqfile']['tmp_name'], $filePath);
    return '{"success":true,"name":"' . $name . '","picName":"' . $fileUrl . '"}';
});


//For Load Existing Files
Route::get('add/{database}&{field}&{id}&{type}', function ($database, $field, $id, $type) {


    if ($type == 'field') {
        $id_temp = explode(',', $id)[0];
        $image = DB::table($database)->where('id', $id)->get([$field])[0];
        if ($image->$field != null) {
            $image_name = substr($image->$field, strrpos($image->$field, '/') + 1);
            $return_param = ['name' => $image_name, 'uuid' => $id_temp, 'thumbnailUrl' => $image->$field];
            return [$return_param];
        }

    } else if ($type == 'database') {
        $id_temp = explode(',', $id);
        $final_res = [];
        foreach ($id_temp as $row_id) {
            $image = DB::table($database)->where('id', $row_id)->get([$field])[0];
            $image_name = substr($image->$field, strrpos($image->$field, '/') + 1);
            array_push($final_res, ['name' => $image_name, 'uuid' => $row_id, 'thumbnailUrl' => $image->$field]);
        }
        return $final_res;
    }
});

//For Delete Files

Route::delete('delete/{uuid}', function ($uuid) {
    $database = Input::get('database');
    $field = Input::get('field');
    $type = Input::get('type');
    if ($type == 'field') {
        $img = DB::table($database)->where('id', $uuid)->update([$field => null]);
    } else if ($type == 'database') {
        if ($database == 'app_apk') {
            DB::table($database)->where('id', $uuid)->update(['remove' => time()]);
        } else {
            DB::table($database)->where('id', $uuid)->delete();
        }
    }
    return '{"success":true}';
});

Route::get('login', 'Auth\LoginController@showLogin')->name('login');