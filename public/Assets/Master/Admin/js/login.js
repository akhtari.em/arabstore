$('#submit').click(function(){
	var username=$('#username').val();
	var password=$('#password').val();
	var is_valid=true;
	if(username == '' || password == '')
	{
		is_valid=false;
		alert('تمامی فیلد ها باید وارد شود.');
	}

	if(is_valid)
	{
		$.post("UsersLogin",
            {
                _token:$('#nekot').val() ,
                username:username,
                password:password
            },
            function (data, status) {
            	console.log(data);
               if(data['status'] == 'true')
               {
               		window.location=data['url'];
               }
               else if(data['status'] == 'false')
               {
               		alert('username or password is Wrong.');
               }
               
                
            });
	}

});

$('body').keydown(function (e) {
	if(e.keyCode == 13)
	{
		$('#submit').click();
	}
});