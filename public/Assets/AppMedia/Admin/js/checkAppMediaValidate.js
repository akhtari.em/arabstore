function checkAppMedia() {
	
	if(document.getElementById("avatar").files.length == 0)
	{ alert("ایکون نمیتواند خالی باشد");
		return false;
	}
	
	if(document.getElementById("appvideo").files.length == 0)
	{ alert("ویدئو نمیتواند خالی باشد");
		return false;
	}

	if(document.form.app_id.selectedIndex=="")
	{
		alert('اپلیکیشن نمی تواند خالی باشد');
		return false;
	}
	
	return true;
}