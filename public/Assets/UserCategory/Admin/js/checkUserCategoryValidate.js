function AddUserCategoryValidate() {
	var title = document.getElementById('title').value;
	var permissions = document.getElementById('permissions').value;

	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if (permissions == "") {
		alert('سطح دسترسی نمی تواند خالی باشد');
		return false;
	}
	return true;
}