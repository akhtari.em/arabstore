function AddVitrineItemValidate() {
	var title = document.getElementById('title').value;
	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if(document.form.app_id.selectedIndex=="")
	{
		alert('دسته بندی اپ نمی تواند خالی باشد');
		return false;
	}
	if(document.getElementById("avatar").files.length == 0)
	{ alert("ایکون نمیتواند خالی باشد");
		return false;
	}
	
	if(document.form.vitrine_id.selectedIndex=="")
	{
		alert('ویترین نمی تواند خالی باشد');
		return false;
	}
	return true;
}

function EditVitrineItemValidate() {
	var title = document.getElementById('title').value;
	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	return true;
}