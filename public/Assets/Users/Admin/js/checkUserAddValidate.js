function AddUserValidate() {
	var password = document.getElementById('password').value;
	var userCategory = document.getElementById('userCategory').value;
	var publicName = document.getElementById("publicName").value;
	var userName = document.getElementById('username').value;
	if (password == "") {
		alert("پسورد نمی تواند خالی باشد");
		return false;
	}
	if (userCategory == "") {
		alert('گروه کاربری نمی تواند خالی باشد');
		return false;
	}
	if (publicName == "") {
		alert('نام نمایشی نمی تواند خالی باشد ');
		return false;
	}
	if (userName == "") {
		alert('نام کاربری نمی تواند خالی باشد');
		return false;
	}
	return true;
}

function EditUserValidate() {
	var userCategory = document.getElementById('userCategory').value;
	var publicName = document.getElementById("publicName").value;
	var userName = document.getElementById('username').value;
	
	if (userName == "") {
		alert('نام کاربری نمی تواند خالی باشد');
		return false;
	}
	
	if (userCategory == "") {
		alert('گروه کاربری نمی تواند خالی باشد');
		return false;
	}
	if (publicName == "") {
		alert('نام نمایشی نمی تواند خالی باشد ');
		return false;
	}
	
	
	return true;
}
