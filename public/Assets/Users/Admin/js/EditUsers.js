$('#userCategory').change(function(){
	if($(this).val() != 0)
	{	
		$('#modules_user').html('');
		$.get("../Ajax_GetUserCategoryPermission",
            {
                _token:$('#nekot').val() ,
                userCategoryId:$(this).val()
            },
            function (data, status) {
               	$('#modules_user').html('').html(data);
                
            });
	}else if($(this).val() == 0)
	{
		$('#modules_user').html('');
	}
});