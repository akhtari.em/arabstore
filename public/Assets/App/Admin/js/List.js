$(document).ready(function()
{  
    $('#loadingPage').fadeIn();
	$.get("Ajax_GetList",
            {
                _token:$('#nekot').val() ,
            },
            function (data, status) {
                $('#loadingPage').fadeOut();
               	$('.streaming-table').html('').html(data);
                
            });
});

$('#search').keyup(function(){
	    $('#loadingPage').fadeIn();
		$.get("Ajax_GetList",
        {
            _token:$('#nekot').val() ,
            search : $('#search').val()
        },
        function (data, status) {
            $('#loadingPage').fadeOut();
           	$('.streaming-table').html('').html(data);
            
    	});

});

function paginating(pageNumber){
         $('#loadingPage').fadeIn();

         
    $.get("Ajax_GetList",
            {
                _token:$('#nekot').val() ,
                pageNumber : pageNumber,
                search : $('#search').val()
            },
            function (data, status) {
                $('#loadingPage').fadeOut();
                $('.streaming-table').html('').html(data);
                
            });

}

function deleteRequest(id){

    var data={'id':id,'_token':$('#nekot').val()};

    $.ajax({
    type: "DELETE",
    url: "Delete",
    data: data,
    success: function(msg){
        console.log(msg);
        if(msg=='true')
        {   
            $.get("Ajax_GetList",
            {
                _token:$('#nekot').val() ,
            },
            function (data, status) {

                $('.streaming-table').html('').html(data);
                
            });
            alert('با موفقیت انجام شد');
        }
        else
        {
            alert('Faild');
        }
    },
    error: function() {
        alert("Something Wrong...");
    }
    });

    

}