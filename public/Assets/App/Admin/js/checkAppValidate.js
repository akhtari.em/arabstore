function checkApp() {
	var title = document.getElementById('title').value;
	var package = document.getElementById('package').value;

	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if (package == "") {
		alert("نام پکیج نمی تواند خالی باشد");
		return false;
	}
	
	if(document.getElementById("avatar").files.length == 0)
	{ alert("ایکون نمیتواند خالی باشد");
		return false;
	}
	// if(document.getElementById("imageicon").files.length == 0)
	// { alert("عکس نمیتواند خالی باشد");
	// 	return false;
	// }
	

	if(document.form.category.selectedIndex=="")
	{
		alert('دسته بندی نمی تواند خالی باشد');
		return false;
	}
	
	if(document.form.developer.selectedIndex=="")
	{
		alert('دسته بندی نمی تواند خالی باشد');
		return false;
	}
	return true;
}

function checkEditApp() {
	var title = document.getElementById('title').value;
	var package = document.getElementById('package').value;
	var detail = document.getElementById('detail').value;
	
	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if (package == "") {
		alert("نام پکیج نمی تواند خالی باشد");
		return false;
	}
	if (detail == "") {
		alert("جزییات نمی تواند خالی باشد");
		return false;
	}
	if (document.getElementById('category').options.length == 0)
	{
		alert('دسته بندی نمی تواند خالی باشد');
		return false;
	}
	if (document.getElementById('developer').options.length == 0)
	{
		alert('دولوپر نمی تواند خالی باشد');
		return false;
	}
	
	return true;
	
	
}