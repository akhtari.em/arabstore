function AddAppCategoryValidate() {
	var title = document.getElementById('title').value;
	var order = document.getElementById('order').value;

	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if (order == "") {
		alert('شماره سفارش نمی تواند خالی باشد');
		return false;
	}
	if(document.getElementById("avatar").files.length == 0)
	{ alert("ایکون نمیتواند خالی باشد");
		return false;
	}
	
	if(document.getElementById("imageicon").files.length == 0)
	{ alert("عکس نمیتواند خالی باشد");
		return false;
	}
	return true;
}

function EditAppCategoryValidate() {
	var title = document.getElementById('title').value;
	var order = document.getElementById('order').value;
	
	if (title == "") {
		alert("عنوان نمی تواند خالی باشد");
		return false;
	}
	if (order == "") {
		alert('شماره سفارش نمی تواند خالی باشد');
		return false;
	}

	return true;
}