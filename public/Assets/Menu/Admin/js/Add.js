$('#category').change(function(){
	$('#subCategory').html('');
	if($(this).val() != 0){
		$.get("Ajax_GetSubCategories",
            {
                _token:$('#nekot').val() ,
                categoryId: $(this).val()
            },
            function (data, status) {
               	for(var i=0;i<data.length;i++)
               	{	

               		$('#subCategory').append('<option name="subCategory" value="'+data[i]['id']+'">'+data[i]['title']+'</option>');
               	}
                
            });
	}
	else
	{
		$('#subCategory').html('');
	}
	
});