/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'fa';
	
	config.filebrowserBrowseUrl = 'engine/classes/editor/elfinder/elfinder.php?mode=file';
	config.filebrowserImageBrowseUrl = 'engine/classes/editor/elfinder/elfinder.php?mode=image';
	config.filebrowserFlashBrowseUrl = 'engine/classes/editor/elfinder/elfinder.php?mode=flash';
};
