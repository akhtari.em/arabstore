<?php
   return [
      'username' => 'نام کاربری' ,
      'password' => 'رمز عبور',
      'login' => 'ورود',
      'managerLogin' => 'ورود مدیر',
      'heavenGatesApp' => 'برنامه دروازه بهشت',
      'dashboard' => 'داشبورد',
      'logout' => 'خروج',
       'setting' => 'تنظیمات',
	  'postList' => 'لیست یادواره ها ',
	  'category' => 'دسته بندی یادواره ها',
	  'addCategory' => 'افزودن دسته جدید',
	  'seachInName' => 'جستجو از طریق نام',
      'name' => ' نام',
	  'picture' => 'عکس',
	  'option' => 'اقدام',
	  'edit' => 'ویرایش',
	  'delete' => 'حذف',
	  'editCategory' => 'ویرایش دسته',
	  'categoryName' => 'نام دسته',
	  'confirm' => 'تایید اقدام',
	  'confirmText' => 'آیا مطمئنید که میخواهید این دسته را حذف کنید؟',
	  'addingCategory' => 'افزودن دسته  بندی',
	  'addPost' => 'افزودن یادواره',
	  'postHeader' => 'سربرگ یادواره',
	  'postPicture' => 'تصویر یادواره',
	  'postDate' => 'تاریخ یادواره',
	  'postCategory' => 'دسته بندی',
	  'show' => 'نمایش',
	  'postDescription' => 'توضیحات یادواره',
	  'adding' => 'افزودن',
	  'deletePostConfirmation' => 'آیا مطمئنید که میخواهید این یادواره را حذف کنید؟',
 	  'editPost' => 'ویرایش یادواره ها',
 	  'postDetails' => 'جزئیات فهرست',
 	  'postId' => 'شناسه',
 	  'manager' => 'مدیر',
 	  'email' => 'ایمیل',
 	  'oldPassword' => 'گذزواژه قدیمی' ,
	  'newPassword' => 'گذزواژه جدید' ,
	  'repeatNewPassword' => 'وارد کردن مجدد گذرواژه جدید',
	  'addUser' => 'اضافه کردن کاربر',
 	  'deleteUser' => 'حذف کردن کاربر',
	  'users' => 'Users',
      'addUsers' => 'Add User',
      'usersList' => 'Users List',
      'mainLinks'=> 'Main List',
      'userCategory' => 'User Categories',
	  'addUserCategory' =>'Add UserCategory',
	  'userCategoryList' => 'UserCategory List',
	  'selectUserCategory' =>'User Category',
	  
	  

	  
	  

      
      


   ];
?>