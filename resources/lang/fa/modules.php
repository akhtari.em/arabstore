<?php
   return [

   'Users' => 'کاربران' ,
   'UserCategory' => 'گروه بندی کاربران',

   'Add' => 'افزودن',
   'Delete' => 'حذف',
   'Edit' => 'ویرایش',
   'List' => 'فهرست',
   'add' => 'افزودن',
   'delete' => 'حذف',
   'edit' => 'ویرایش',
   'list' => 'فهرست',
   'AddUserCategoryView' => 'افزودن',
   'UserCategoryListView' => 'فهرست',
   'AddUsersView' => 'افزودن',
   'UsersListView' => 'فهرست',
   'AppCategory' => 'دسته بندی اپلیکیشن ها',
   'App' => 'اپلیکیشن ها',
   'Vitrine' => 'ویترین',
   'VitrineItem' => 'آیتم ویترین',
   'Client' => 'مشتری ها',
   'AddClientView' => 'افزودن',
   'ClientView' => 'فهرست ها',
   'AppMedia' => 'مدیا ',
   'AppApk' => 'فایل نصب ',
	'ContactForm' => 'فرم تماس با ما',
	'ContactUs' => ' تماس با ما',
	'Content' => 'محتوا',
	'Menu' => 'نوار ابزار',
   ];
?>