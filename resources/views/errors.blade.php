

@if (Session::has('message'))
	<p class="alert alert-success" data-dismiss="alert">{{Session::get('message')}}</p>
@endif

@if (Session::has('success'))
	<p class="alert alert-success" data-dismiss="alert">{{Session::get('success')}}</p>
@endif

@if (Session::has('DeveloperSuccessLogin'))
	<p class="alert alert-success WebErrors" data-dismiss="alert">{{Session::get('DeveloperSuccessLogin')}}</p>
@endif

@if (Session::has('error'))
	<p class="alert alert-danger alert-success WebErrors" data-dismiss="alert">{{Session::get('error')}}</p>
@endif

@if (count($errors))
	@foreach($errors->all() as $error)
		<p class="alert alert-danger alert-success WebErrors" data-dismiss="alert">{{$error}}</p>
	@endforeach
@endif