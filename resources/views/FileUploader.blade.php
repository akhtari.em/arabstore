
<link rel="stylesheet" href="{{asset('Uploader/fine-uploader-new.css')}}">

<script src="{{asset('Uploader/jquery.fine-uploader.js')}}"></script>

<script type="text/template"   id="qq-template-manual-trigger-{{$data['name']}}">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="فایل خود را اینجا بیندازید">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="buttons">
            <div class="qq-upload-button-selector qq-upload-button">
                <div>انتخاب فایل</div>
            </div>
            <button style="margin-left: 10px;
    height: 100%;
    border: 0px;
    border-radius: 2px;
    height: 36px;" type="button" id="trigger-upload-{{$data['name']}}" class="btn btn-primary trigger-upload-class">
                <i class="icon-upload icon-white"></i> آپلود
            </button>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">لغو</button>
                <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">سعی دوباره</button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">حذف</button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">بستن</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">خیر</button>
                <button type="button" class="qq-ok-button-selector">بله</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>


    <div id="fine-uploader-manual-trigger-{{$data['name']}}" style="margin-bottom: 15px;direction: ltr" ></div>



<script>


    $('#fine-uploader-manual-trigger-{{$data['name']}}').fineUploader({
        template: 'qq-template-manual-trigger-{{$data['name']}}',
        request: {
            endpoint: '{{url('/')}}/upload/{{$data['name']}}&{{$data['module']}}&{{$data['side']}}'
        },
        validation: {
            allowedExtensions: [@foreach($data['permission'] as $perm)
                                '{{$perm}}',
                                @endforeach],
            itemLimit: '{{$data['count']}}',
            sizeLimit: '{{$data['size']*1000000}}' // to megaByte
        },
        @if(!empty($data['database']) && !empty($data['id']) && !empty($data['type']) && !empty($data['field'])  )
        session: {
           
           endpoint: '{{url('/')}}/add/{{$data['database']}}&{{$data['field']}}&{{$data['id']}}&{{$data['type']}}',
           params: {},
           customHeaders: {},
           refreshOnReset: true
        },
       
        deleteFile: {
           enabled: true,
           forceConfirm: true,
           params: {'database':'{{$data['database']}}', 'field':'{{$data['field']}}','type':'{{$data['type']}}'},
           endpoint: '{{url('/')}}/delete'
       },
        @endif
        callbacks: {
             onSessionRequestComplete: function(response, success, xhrOrXdr) {
               
             }
         },
        autoUpload: false,
        debug: true
    });

    $('#trigger-upload-{{$data['name']}}').click(function() {
        $('#fine-uploader-manual-trigger-{{$data['name']}}').fineUploader('uploadStoredFiles');
    });

   



</script>
