<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVitrineItemTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vitrine_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vitrine_id');
            $table->foreign('vitrine_id')->references('id')->on('vitrine')->onDelete('cascade');
            $table->string('title');
            $table->string('media')->nullable();
            $table->string('action')->nullable();
            $table->integer('app_id')->unsigned();
            $table->unsignedInteger('fromdate')->nullable();
            $table->unsignedInteger('todate')->nullable();
            $table->unsignedInteger('order')->nullable();
            $table->boolean('status')->default(1);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vitrine_item', function ($table) {
            $table->foreign('app_id')->references('id')->on('app');
        });
    }
}
