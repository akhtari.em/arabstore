<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppApkTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('app_apk', function (Blueprint $table) {
            $table->increments('id')->unsigned(false);
            $table->integer('app_id')->unsigned();
            $table->string('file');
            $table->string('versionName');
            $table->float('versionCode')->unique();
            $table->mediumText('permission')->nullable();
            $table->text('changeLog')->nullable();
            $table->string('minSDK')->nullable();
            $table->string('publishDate')->nullable();
            $table->string('volume');
            $table->unsignedInteger('download')->nullable()->default(0);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('app_apk', function ($table) {
            $table->foreign('app_id')->references('id')->on('app');
        });
    }
}
