<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 45)->unique();
	        $table->string('password');
	        $table->string('phoneNumber')->nullable();
	        $table->string('email')->nullable();
            $table->string('address')->nullable();
			$table->unsignedInteger('user_category_id');
	        $table->foreign('user_category_id')->references('id')->on('user_category')->onDelete('cascade');
	        $table->text('permissions')->nullable();
	        $table->rememberToken();
	        $table->timestamps();
	        $table->string('name', 45)->nullable();
	        $table->string('family', 45)->nullable();
			$table->string('publicName', 45);
	        $table->boolean('status')->default(1);
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
