<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppMediaTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('app_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->enum('type', ['image', 'video']);
            $table->string('file')->nullable();
            $table->string('videoImage')->nullable();
            $table->string('url')->nullable();
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('app_media', function ($table) {
            $table->foreign('app_id')->references('id')->on('app');
        });
    }
}
