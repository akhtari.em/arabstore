<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVitrineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitrine', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('action')->nullable();
            $table->unsignedInteger('itemid')->default(0)->nullable();
            $table->enum('type', ['slider', 'scroll'])->nullable();
            $table->unsignedInteger('order');
            $table->boolean('status')->default(0);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitrine');
    }
}
