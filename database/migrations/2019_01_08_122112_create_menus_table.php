<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang')->default('fa');
            $table->string('title')->nullable();
            $table->string('module')->nullable();
            $table->string('item')->nullable();
            $table->string('url')->nullable();
            $table->integer('parent')->default(0);
            $table->integer('_ord')->default(0);
            $table->enum('position', ['bottom', 'top'])->nullable()->default('bottom');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
