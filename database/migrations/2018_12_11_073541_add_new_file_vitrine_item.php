<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFileVitrineItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vitrine_item', function (Blueprint $table) {
            $table->string('url')->nullable();
            $table->enum('type', ['url', 'app'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vitrine_item', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('type');
        });
    }
}
