<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->string('family')->nullable();
            $table->string('city')->nullable();
	        $table->enum('sex', ['male', 'female'])->nullable()->default('male');
	        $table->string('birthDate')->nullable();
	        $table->string('image')->nullable();
	        $table->string('bio')->nullable();
	        $table->unsignedInteger('code')->nullable();
	        $table->unsignedInteger('level')->default(0);
	        $table->unsignedInteger('follower')->default(0);
	        $table->unsignedInteger('following')->default(0);
	        $table->unsignedInteger('score')->default(0);
	        $table->unsignedInteger('credit')->default(0);
	        $table->enum('status', ['1','0'])->default(0);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
