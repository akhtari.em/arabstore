<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('app', function (Blueprint $table) {
            $table->increments('id')->unsigned(false);
            $table->string('lang')->default('fa');
            $table->string('title');
            $table->enum('age', ['under5', '5-8', 'above9'])->nullable();
            $table->enum('sex', ['male', 'female'])->nullable();
            $table->enum('made', ['iranian', 'foreign'])->nullable();
            $table->string('package')->unique();
            $table->longText('detail')->nullable();
            $table->longText('shortDetail')->nullable();
            $table->longText('manifest')->nullable();
            $table->unsignedInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('app_category')->onDelete('cascade');
            $table->unsignedInteger('developer')->nullable();
            $table->foreign('developer')->references('id')->on('developer')->onDelete('cascade');
            $table->integer('ageCategory')->nullable();
            $table->string('icon')->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('company')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->boolean('isFree')->defaule(1);
            $table->string('price')->nullable();
            $table->boolean('inAppPayment')->default(0);
            $table->unsignedInteger('owner')->nullable();
            $table->unsignedInteger('downloadCount')->nullable();
            $table->unsignedInteger('installCount')->nullable();
            $table->unsignedInteger('viewCount')->nullable();
            $table->string('rateAvarage')->nullable();
            $table->unsignedInteger('commentCount')->nullable();
            $table->unsignedInteger('favoriteCount')->nullable();
            $table->boolean('status')->default(1);
            $table->unsignedInteger('publishRequest')->default(0);
            $table->unsignedInteger('publishTime')->nullable();
            $table->unsignedInteger('hideRequest')->default(0);
            $table->unsignedInteger('userid')->nullable();
            $table->foreign('userid')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('vip')->default(0);
            $table->unsignedInteger('order')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app');
    }
}
