<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMarketVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_version', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('version_code')->unique();
            $table->string('version_name');
            $table->boolean('status')->default(1);
            $table->string('note');
            $table->string('file');
            $table->boolean('force_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_version');
    }
}
