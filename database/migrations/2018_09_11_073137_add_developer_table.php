<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeveloperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->string('company');
            $table->string('email');
            $table->string('mobile');
            $table->string('password');
            $table->string('phone');
            $table->string('fax');
            $table->string('address');
            $table->string('postalCode');
            $table->string('nickName');
            $table->string('logo');
            $table->string('melliCart');
            $table->string('melliImg');
            $table->string('code');
            $table->string('loginType');
            $table->boolean('emailstatus')->default(0);
            $table->boolean('mobilestatus')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developer');
    }
}
