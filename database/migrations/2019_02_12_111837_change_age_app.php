<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;



class ChangeAgeApp extends Migration
{

    public function up()
    {
        DB::statement("ALTER TABLE app CHANGE COLUMN age age ENUM('3', '7', '12','16') NOT NULL DEFAULT '16'");

    }

    public function down()
    {
        DB::statement("ALTER TABLE app CHANGE COLUMN age ENUM('under5', '5-8', 'above9')");
    }
}