<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldAppCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_category', function (Blueprint $table) {
            $table->dropColumn('row1');
            $table->dropColumn('row2');
            $table->dropColumn('row3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_category', function (Blueprint $table) {
	        $table->integer('row1')->default(0);
	        $table->integer('row2')->default(0);
	        $table->integer('row3')->default(0);
        });
    }
}
