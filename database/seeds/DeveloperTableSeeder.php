<?php

use Illuminate\Database\Seeder;
use App\Modules\Developer\Admin\Models\Developer;
use Illuminate\Support\Facades\Hash;
class DeveloperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Developer::insert([
        	'id'=>'1',
	        'name' => 'admin',
	        'family' => 'admin',
	        'company' => 'parax',
	        'email' => 'superuser@gmail.com',
	        'mobile' => '09372837491',
	        'password' => Hash::make('123456'),
	        'phone' => '88350351',
	        'fax' => '11111111',
	        'address' => 'خیابان وزرا خیابان شفیعی پلاک35',
	        'postalCode' => '1111111111',
	        'nickName' => 'admin',
	        'logo' => '1524374415_menu-icon12.png',
	        'melliCart' => '3790003204',
	        'melliImg' => '1524374415_menu-icon12.png',
	        'code' => '123',
	        'loginType' => 'gmail',
	        'emailstatus' => '1',
	        'mobilestatus' => '1',
	        'status' => '1',
        ]);
    }
}
