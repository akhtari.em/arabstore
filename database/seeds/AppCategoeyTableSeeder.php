<?php

use Illuminate\Database\Seeder;
use App\Modules\AppCategory\Admin\Models\AppCategory;
class AppCategoeyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$apeCategories = [
    	    [
			    'id' => '1',
			    'title' => 'استراتژي',
			    'icon' => '1.png',
			    'icon2' => '2.png',
			    'order' => '2',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '2',
			    'title' => 'فکري',
			    'icon' => '3.png',
			    'icon2' => '4.png',
			    'order' => '9',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '3',
			    'title' => 'سرگرمي',
			    'icon' => '5.png',
			    'icon2' => '6.png',
			    'order' => '8',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '4',
			    'title' => 'اکشن',
			    'icon' => '2.png',
			    'icon2' => '3.png',
			    'order' => '1',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '5',
			    'title' => 'تفنني',
			    'icon' => '7.png',
			    'icon2' => '8.png',
			    'order' => '7',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '6',
			    'title' => 'رانندگي',
			    'icon' => '9.png',
			    'icon2' => '10.png',
			    'order' => '5',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '7',
			    'title' => 'امتيازي',
			    'icon' => '11.png',
			    'icon2' => '12.png',
			    'order' => '6',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '8',
			    'title' => 'شبيه سازي',
			    'icon' => '13.png',
			    'icon2' => '8.png',
			    'order' => '10',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '9',
			    'title' => 'ورزشي',
			    'icon' => '9.png',
			    'icon2' => '16.png',
			    'order' => '4',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '10',
			    'title' => 'موسيقي',
			    'icon' => '17.png',
			    'icon2' => '18.png',
			    'order' => '11',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '11',
			    'title' => 'آموزشي',
			    'icon' => '19.png',
			    'icon2' => '13.png',
			    'order' => '3',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '12',
			    'title' => 'بيشتر',
			    'icon' => '20.png',
			    'icon2' => '1.png',
			    'order' => '1',
			    'status' => '1',
			    'parent' => '15',
			    'remove' => '0'
		    ],
		    [
			    'id' => '13',
			    'title' => 'بازي',
			    'icon' => '5.png',
			    'icon2' => '6.png',
			    'order' => '1',
			    'status' => '1',
			    'parent' => '0',
			    'remove' => '0'
		    ]
	    ];
    	foreach ($apeCategories as $apeCategory)
	    {
	    	AppCategory::create($apeCategory);
	    }
    }
}
