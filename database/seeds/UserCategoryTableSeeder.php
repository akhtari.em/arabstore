<?php

use Illuminate\Database\Seeder;
use App\Modules\UserCategory\Admin\Models\UserCategory;
class UserCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userCategories = [
            [
		        'id' => '1',
		        'title' => 'ادمین',
	            'permissions' => '{"admin":{"App":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppView\",\"AddApp\"]}","{\"List\":[\"AppListView\"]}"]},"AppApk":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppApkView\",\"AddAppApk\"]}","{\"List\":[\"AppApkListView\"]}"]},"AppCategory":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppCategoryView\",\"AddAppCategory\"]}","{\"List\":[\"AppCategoryListView\"]}"]},"AppMedia":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppMediaView\",\"AddAppMedia\"]}","{\"List\":[\"AppMediaListView\"]}"]},"Client":{"permAll":"yes","permissions":["{\"Add\":[\"AddClientView\",\"AddClient\"]}","{\"List\":[\"ClientListView\"]}"]},"Comment":{"permAll":"yes","permissions":["{\"Add\":[]}","{\"List\":[\"AppListView\"]}"]},"ContactForm":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppView\",\"AddApp\"]}","{\"List\":[\"AppListView\"]}"]},"ContactUs":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppView\",\"AddApp\"]}","{\"List\":[\"AppListView\"]}"]},"Content":{"permAll":"yes","permissions":["{\"Add\":[\"AddAppView\",\"AddApp\"]}","{\"List\":[\"AppListView\"]}"]},"Menu":{"permAll":"yes","permissions":["{\"Add\":[\"Menu.Add.View\",\"Menu.Add\"]}","{\"Edit\":[\"Menu.Edit.View\",\"Menu.Edit\"]}","{\"Delete\":[\"Menu.Delete\"]}","{\"List\":[\"Menu.List.View\"]}"]},"UserCategory":{"permAll":"yes","permissions":["{\"Add\":[\"AddUserCategoryView\",\"AddUserCategory\"]}","{\"List\":[\"UserCategoryListView\"]}"]},"Users":{"permAll":"yes","permissions":["{\"Add\":[\"AddUsersView\",\"AddUsers\"]}","{\"List\":[\"UsersListView\"]}"]},"Vitrine":{"permAll":"yes","permissions":["{\"Add\":[\"AddVitrineView\",\"AddVitrine\"]}","{\"List\":[\"VitrineListView\"]}"]},"VitrineItem":{"permAll":"yes","permissions":["{\"Add\":[\"AddVitrineItemView\",\"AddVitrineItem\"]}","{\"List\":[\"VitrineItemListView\"]}"]}}}',
		        'status' => '1'
	        ],
	        [
	        	'id' => '2',
		        'title' => 'حسابدار',
		        'permissions' => '{"admin":{"UserCategory":{"permAll":"no","permissions":["{\"Add\":[\"AddUserCategoryView\",\"AddUserCategory\"]}","{\"List\":[\"UserCategoryListView\"]}"]},"Users":{"permAll":"yes","permissions":["{\"Add\":[\"AddUsersView\",\"AddUsers\"]}"]}}}',
		        'status' => '1'
	        	
	        ]
        ];
        foreach ($userCategories as $category)
        {
        	UserCategory::create($category);
        }
    }
}

