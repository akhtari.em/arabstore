<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	 
        User::insert([
            'id' => '1',
	        'username' => 'admin',
	        'password' => Hash::make('123456'),
	        'phoneNumber' => '',
	        'email' => '',
	        'address' => '',
	        'user_category_id' => 1,
	        'permissions' => '{"admin":{"UserCategory":{"permAll":"yes","permissions":["{\\"Add\\":[\\"AddUserCategoryView\\",\\"AddUserCategory\\"]}","{\\"List\\":[\\"UserCategoryListView\\"]}"]},"Users":{"permAll":"yes","permissions":["{\\"Add\\":[\\"AddUsersView\\",\\"AddUsers\\"]}","{\\"List\\":[\\"UsersListView\\"]}"]}}}',
	        'name' => '',
	        'family' => '',
	        'publicName' => 'admin',
	        'status' => 1
        ]);
    }
}
